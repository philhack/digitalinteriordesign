﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<AccessDesign.Models.RoomPhoto>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <% Html.EnableClientValidation(); %>
    <h1>Photos of Your Room</h1>
    <div id="room-photo-nav">
        <a href="/RoomPhoto/Upload/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-UploadRoomPhotos.png" alt="" border="0" class="room-photo-nav-image"/></a><a href="/RoomPhoto/Details/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-ViewEditPhotosTreomaOn.png" alt="" border="0"/></a><a href="/RoomPhoto/Comments/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-AddComments.png" alt="" border="0" class="room-photo-nav-image"/></a>
     </div>
     <br />
    <p>Click the picture to view a larger version. To delete a picture of your room, just click the delete button.</p>
    <br />
    <div id="gallery">
    <% foreach (var item in Model) { %>
    
<div class="gallery">
                <div class="gallery-item"><a href="<%: item.WebUrlPath %>" title="<%: item.PhotoName %>. " class="lightbox"><img src="<%: item.ThumbnailUrlPath %>" width="100" height="100" alt="<%: item.PhotoName %>" border="0" /></a></div>
                <div class="gallery-delete-button"><%: Html.ActionLink("DELETE", "Delete", new { id=item.RoomPhotoId })%></div>
</div>
    <% } %>


    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">

    <script type="text/javascript" src="/Scripts/jquery.lightbox-0.5.js"></script>
    <link rel="stylesheet" type="text/css" href="/Content/Gallery.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/Content/jquery.lightbox-0.5.css" media="screen" />
    
    <script type="text/javascript">
        $(function () {
            $('a.lightbox').lightBox // Select all links with lightbox class
            ({
                imageBlank: '/Content/Images/LightBox/lightbox-blank.gif',
                imageLoading: '/Content/Images/LightBox/lightbox-ico-loading.gif',
                imageBtnClose: '/Content/Images/LightBox/lightbox-btn-close.gif',
                imageBtnPrev: '/Content/Images/LightBox/lightbox-btn-prev.gif',
                imageBtnNext: '/Content/Images/LightBox/lightbox-btn-next.gif'
            });

            $(".room-photo-nav-image")
            .mouseover(function () {
                var src = $(this).attr("src").match(/[^\.]+/) + "TreomaOn.png";
                $(this).attr("src", src);
            })
            .mouseout(function () {
                var src = $(this).attr("src").replace("TreomaOn", "");
                $(this).attr("src", src);
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarContent" runat="server">
</asp:Content>

