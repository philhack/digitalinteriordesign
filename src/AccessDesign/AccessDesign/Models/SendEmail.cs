﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net.Mail;

namespace AccessDesign.Models
{
    public class SendEmail
    {
        //private StringBuilder emailBody { get; set; }
        //private S

        public bool SendSingleEmail(StringBuilder emailBody, string fromAddress, string fromDisplayName, string toAddress, string emailSubject)
        {
            // Build the email message
            MailMessage emailToSend = new MailMessage();
            emailToSend.From = new MailAddress(fromAddress,fromDisplayName);
            emailToSend.To.Add(new MailAddress(toAddress));
            emailToSend.Subject = emailSubject;
            emailToSend.Body = emailBody.ToString();
            emailToSend.IsBodyHtml = true;

            // Attempt to send the email message
            try
            {
                SmtpClient client = new SmtpClient();
                client.Send(emailToSend);
                return (true);
            }
            catch (Exception)
            {
                // TODO: Add Exception logging methods here
                return (false);
            }
        }
    }
}