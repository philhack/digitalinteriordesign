﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.ChangePasswordModel>" %>

<asp:Content ID="changePasswordTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Change Password
</asp:Content>

<asp:Content ID="changePasswordContent" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Change Password</h1>
    <p>
        Use the form below to change your password. 
    </p>
    <p>
        New passwords are required to be a minimum of <%: ViewData["PasswordLength"] %> characters in length. All fields are required.
    </p>
    <br /><br />

    <% Html.EnableClientValidation(); %>

    <% using (Html.BeginForm()) { %>
        <%: Html.ValidationSummary(true) %>
        <div class="form-container">
                <div class="text-box-label-same-line">
                    <%: Html.LabelFor(m => m.OldPassword) %>
                </div>
                <div class="text-box">
                    <%: Html.PasswordFor(m => m.OldPassword, new { @class = "txtbox", @size = 25 })%>
                    <%: Html.ValidationMessageFor(m => m.OldPassword) %>
                </div>
                <br />
                <div class="text-box-label-same-line">
                    <%: Html.LabelFor(m => m.NewPassword) %>
                </div>
                <div class="text-box">
                    <%: Html.PasswordFor(m => m.NewPassword, new { @class = "txtbox", @size = 25 })%>
                    <%: Html.ValidationMessageFor(m => m.NewPassword) %>
                </div>
                <br />
                <div class="text-box-label-same-line">
                    <%: Html.LabelFor(m => m.ConfirmPassword) %>
                </div>
                <div class="text-box">
                    <%: Html.PasswordFor(m => m.ConfirmPassword, new { @class = "txtbox", @size = 25 })%>
                    <%: Html.ValidationMessageFor(m => m.ConfirmPassword) %>
                </div>
                <br />
                <div id="standard-button">
                    <input type="submit" class="button" value="Submit" />
                 </div>
        </div>
    <% } %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .validation-summary-errors ul {display:inline; font-weight:normal}    
    </style>
</asp:Content>