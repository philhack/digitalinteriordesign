﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.GeneralQuestionnaire>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Update General Questionnaire
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Update Your Questionnaire</h1>
    <br />
    <%if (Convert.ToBoolean(ViewData["CreatedRooms"]) == true)
      { %>
        <div id="app-steps-generic">
            <a href="/Profile"><img src="/Content/Images/app-steps-profile-3-edit-01.png" alt="" border="0" /></a><a href="/GeneralQuestionnaire"><img src="/Content/Images/app-steps-profile-3-edit-02.png" alt="" border="0"/></a><a href="/Room/Create"><img src="/Content/Images/app-steps-profile-3-edit-03.png" alt="" border="0"/></a>
            <h6 class="step-description">STEP 3/3 COMPLETED. THANK YOU</h6>
        </div>
    <% }
      else
      { %>
          <div id="app-steps-generic">
              <a href="/Profile"><img src="/Content/Images/app-steps-profile-3-01.png" alt="" border="0" /></a><a href="/GeneralQuestionnaire"><img src="/Content/Images/app-steps-profile-3-02.png" alt="" border="0"/></a><a href="/Room/Create"><img src="/Content/Images/app-steps-profile-3-03.png" alt="" border="0"/></a>
            <h6 class="step-description">STEP 2/3 COMPLETED. PLEASE COMPLETE STEP 3 - START A NEW ROOM</h6>
          </div>
    
    <% } %>

    <br /> <br />
    <p>Update your personal preference questionnaire below.  Ensure that all of the required fields have information in them and click "Save". You can view and update this questionnaire by clicking "MY QUESTIONNAIRE" on the left side menu.</p>
    <br />
    <div class="text-box"><span class="required-field">*</span> Required fields.</div>
    <br /><br />

    <% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm()) {%>

            <% Html.RenderPartial("EditFields", Model); %>

            <%: Html.ValidationSummary(false, "*Please fill in all the required questions above and then click save.")%>   
            <br />
            <div id="standard-button">
                    <input type="submit"  class="button" value="Save" />
            </div> 

    <% } %>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">

    <script src="/Scripts/MvcFoolproofValidation.js" type="text/javascript"></script>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>