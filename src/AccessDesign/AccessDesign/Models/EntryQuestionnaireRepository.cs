﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AccessDesign.Models
{
    public class EntryQuestionnaireRepository
    {
        private AccessDesignEntities entities = new AccessDesignEntities();

        // Return the entry questions based on the entry question id
        public EntryQuestionnaire GetEntryQuestionnaireByRoomId(int id)
        {
            return entities.EntryQuestionnaires.FirstOrDefault(e => e.RoomId == id);
        }

        public EntryQuestionnaire GetEntryQuestionnaireByEntryQuestionnaireId(int id)
        {
            return entities.EntryQuestionnaires.FirstOrDefault(b => b.EntryQuestionnaireId == id);
        }

        // Checks if the questionnaire exists based on room id, if it does, it returns true, otherwise it returns false
        public bool DoesEntryQuestionnaireExist(int id)
        {
            if (entities.EntryQuestionnaires.Count(e => e.RoomId == id) == 1)   // check if the entry questionnaire exists
                return true;    // the questionnaire exists, so return true
            else
                return false;   // the questionnaire does not exist
        }

        /// <summary>
        /// Returns a list of gender types.  This can then be used in a drop down list for the question: How do you want your room to feel?
        /// </summary>
        /// <param name="genderType">Gender Type</param>
        /// <returns></returns>
        public static SelectList GetGenderTypeList(string genderType)
        {
            List<SelectListItem> numberList = new List<SelectListItem>() 
            {
				new SelectListItem() { Value = "", Text = "  " },
                new SelectListItem() { Value = "Masculine", Text = " Masculine " },
                new SelectListItem() { Value = "Feminine", Text = " Feminine " },
                new SelectListItem() { Value = "Neutral", Text = " Neutral " }
            };
            return new SelectList(numberList, "Value", "Text", genderType ?? "");
        }

        #region Room Create Insert and Delete Statements
        //
        // Insert
        public void Add(EntryQuestionnaire entryQuestionnaire)
        {
            entities.EntryQuestionnaires.AddObject(entryQuestionnaire);
        }


        //
        // Delete
        public void Delete(EntryQuestionnaire entryQuestionnaire)
        {
            entities.EntryQuestionnaires.DeleteObject(entryQuestionnaire);  // delete the entry Questionnaire
        }

        //
        // Persistence
        public void Save()
        {
            entities.SaveChanges();
        }

        #endregion

    }
}