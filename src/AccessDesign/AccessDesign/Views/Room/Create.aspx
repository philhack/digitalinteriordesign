﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Room>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Start a New Room - Name Room and Select Type
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
     <% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm()) {%>
    <h1>Start a New Room</h1>
    <br />
    <%if (Convert.ToBoolean(ViewData["CreatedRooms"]) == true) { %>          
        <div id="app-steps-generic">
                  <a href="/Profile"><img src="/Content/Images/app-steps-profile-3-edit-01.png" alt="" border="0" /></a><a href="/GeneralQuestionnaire"><img src="/Content/Images/app-steps-profile-3-edit-02.png" alt="" border="0"/></a><a href="/Room/Create"><img src="/Content/Images/app-steps-profile-3-edit-03.png" alt="" border="0"/></a>
                  <h6 class="step-description">STEP 3/3 COMPLETE. THANK YOU</h6>
        </div>
        <br /><br />
        <h3>Create Additional Rooms Using The Form Below</h3>
        <p>To create an additional room, please give it a name, and select the type of room. For example, you could name your room <i>"My Office"</i>. Your room's name must not exceed 14 characters.</p>

    <% } else { %>
            <div id="app-steps-generic">
                  <a href="/Profile"><img src="/Content/Images/app-steps-profile-3-01.png" alt="" border="0" /></a><a href="/GeneralQuestionnaire"><img src="/Content/Images/app-steps-profile-3-02.png" alt="" border="0"/></a><a href="/Room/Create"><img src="/Content/Images/app-steps-profile-3-03.png" alt="" border="0"/></a>
                <h6 class="step-description">STEP 2/3 COMPLETE. PLEASE COMPLETE STEP 3 - START A NEW ROOM</h6>
        </div>
        <br /><br />
        <p>To create your room, please give it a name, and select the type of room. For example, you could name your room <i>"My Office"</i>. Your room's name must not exceed 14 characters.</p>
    <% } %>

    <br /><br />
            
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.Name) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.Name, new { @class = "txtbox", @size = 18 })%>
                <%: Html.ValidationMessageFor(model => model.Name) %>
            </div>
            
            <br />
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.RoomTypeName) %><span class="required-field">*</span>
            </div>
            <div class="dropdown">
                <%: Html.DropDownListFor(model => model.RoomTypeName, ViewData["RoomType"] as SelectList, new { @class = "option" })%>
                <%: Html.ValidationMessageFor(model => model.RoomTypeName)%>
            </div>
            <br /><br />
            <div id="standard-button">
                    <input type="submit"  class="button" value="Submit" />
            </div> 

    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SidebarContent" runat="server">
</asp:Content>

