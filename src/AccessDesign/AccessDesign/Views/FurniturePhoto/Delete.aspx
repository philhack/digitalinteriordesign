﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.FurniturePhoto>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Delete Furniture Photo
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Delete Confirmation</h1>
    <div id="room-photo-nav">
        <a href="/Furniture/Create/<%: ViewData["RoomId"]%>"><img src="/Content/Images/Furniture-Navigation-New.png" alt="Create new piece of furniture" border="0" class="room-photo-nav-image" /></a><img src="/Content/Images/app-steps-blank-02.png" alt="Blank Step 2" border="0" /><img src="/Content/Images/app-steps-blank-03.png" alt="Blank Step 3" border="0" />
     </div>
     <br />
     <p>Are you sure that you want to delete this photo? To permanently remove this photo, click the Delete button below.</p>
     <br />
        <div class="delete-photo">
            <img src="<%: Model.ThumbnailUrlPath %>" alt="<%: Model.PhotoName %>" />
        </div>
        <br />

    <div>
    <% using (Html.BeginForm()) { %>
    <div id="standard-button">
    		    <input name="confirmButton" type="submit" value="Delete" class="button" />
    </div>
    <% } %>
    </div>
        

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".room-photo-nav-image")
            .mouseover(function () {
                var src = $(this).attr("src").match(/[^\.]+/) + "TreomaOn.png";
                $(this).attr("src", src);
            })
            .mouseout(function () {
                var src = $(this).attr("src").replace("TreomaOn", "");
                $(this).attr("src", src);
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarContent" runat="server">
</asp:Content>

