﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Room>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Change Room Name
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Change Room Name</h1>
    <br /> 
    <p>You can change the name of your room below:</p>
    <br />
     <% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm()) {%>
                
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.Name) %>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.Name, new { @class = "txtbox", @size = 18 }) %>
                <%: Html.ValidationMessageFor(model => model.Name) %>
            </div>
            <br />
            <div id="standard-button">
                    <input type="submit"  class="button" value="Save" />
            </div> 
    <% } %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SidebarContent" runat="server">
</asp:Content>

