﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AccessDesign.Models
{
    public class ConsultationRepository
    {
        private AccessDesignEntities entities = new AccessDesignEntities();


        public int GetNumberOfInitialConsultations(int id)
        {
            return entities.RoomPhotos.Count(p => p.RoomId == id);
        }

        /// <summary>
        /// Returns the number of consultations for this room and if specified only for a specific consultation type
        /// </summary>
        /// <param name="roomId">room id</param>
        /// <param name="consultationType">Optinal consultation type - This can be either "initial" or "followUp"</param>
        /// <returns>the number of consultations for this room</returns>
        public int GetNumberOfConsultationsForRoom(int roomId, string consultationType = null)
        {
            if(consultationType == null)
                return entities.Consultations.Count(c => c.RoomId == roomId);
            else
                return entities.Consultations.Count(c => c.RoomId == roomId && c.Type == consultationType);
        }

        /// <summary>
        /// Checks if the consultation exists based on room id and the consultation type, if it does, it returns true, otherwise it returns false
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="consultationType">This can be either "initial" or "followUp"</param>
        /// <returns></returns>
        public bool DoesConsultationExist(int roomId, string consultationType)
        {
            if (entities.Consultations.Count(c => c.RoomId == roomId && c.Type == consultationType) == 1)   // check if a profile based on the username exists
                return true;    // the profile exists, so return true
            else
                return false;   // the profile does not exist
        }

        /// <summary>
        /// Gets the consultation based on the room id and type
        /// </summary>
        /// <param name="roomId">Room ID</param>
        /// <param name="consultationType">Type of Consulation</param>
        /// <returns></returns>
        public Consultation GetConsultationByRoomId(int roomId, string consultationType)
        {
            return entities.Consultations.FirstOrDefault(c => c.RoomId == roomId && c.Type == consultationType);
        }

        /// <summary>
        /// Gets the consultation based on the consultation id
        /// </summary>
        /// <param name="id">consultation id</param>
        /// <param name="consultationType">type of consultation</param>
        /// <returns></returns>
        public Consultation GetConsultationByConsultationId(int consultationId, string consultationType = null)
        {
            if(consultationType == null)
                return entities.Consultations.FirstOrDefault(c => c.ConsultationId == consultationId);
            else
                return entities.Consultations.FirstOrDefault(c => c.ConsultationId == consultationId && c.Type == consultationType);
        }



		

        /// <summary>
        /// Returns a list of consultation times
        /// </summary>
        /// <param name="time">the default time that you want the list to display</param>
        /// <returns></returns>
        public static SelectList GetConsultationTimes(string time)
        {
            List<SelectListItem> numberList = new List<SelectListItem>() 
            {
                new SelectListItem() { Value = "9:00 AM", Text = " 9:00 AM " },
                new SelectListItem() { Value = "9:30 AM", Text = " 9:30 AM " },
                new SelectListItem() { Value = "10:00 AM", Text = " 10:00 AM " },
                new SelectListItem() { Value = "10:30 AM", Text = " 10:30 AM " },
                new SelectListItem() { Value = "11:00 AM", Text = " 11:00 AM " },
                new SelectListItem() { Value = "11:30 AM", Text = " 11:30 AM " },
                new SelectListItem() { Value = "12:00 PM", Text = " 12:00 PM " },
                new SelectListItem() { Value = "12:30 PM", Text = " 12:30 PM " },
                new SelectListItem() { Value = "1:00 PM", Text = " 1:00 PM " },
                new SelectListItem() { Value = "1:30 PM", Text = " 1:30 PM " },
                new SelectListItem() { Value = "2:00 PM", Text = " 2:00 PM " },
                new SelectListItem() { Value = "2:30 PM", Text = " 2:30 PM " },
                new SelectListItem() { Value = "3:00 PM", Text = " 3:00 PM " },
                new SelectListItem() { Value = "3:30 PM", Text = " 3:30 PM " },
                new SelectListItem() { Value = "4:00 PM", Text = " 4:00 PM " },
                new SelectListItem() { Value = "4:30 PM", Text = " 4:30 PM " }
            };
            return new SelectList(numberList, "Value", "Text", time ?? "9:00 AM");
        }

        /// <summary>
        /// Returns a list of possible ways for the designer to contact the customer
        /// </summary>
        /// <param name="contactType">Currently this could be either phone or skype</param>
        /// <returns></returns>
        public static SelectList GetPreferedContactMethods(string contactMethod)
        {
            List<SelectListItem> contactMethodList = new List<SelectListItem>() 
            {
                new SelectListItem() { Value = "phone", Text = " Phone " },
                new SelectListItem() { Value = "skype", Text = " Skype " }
            };
            return new SelectList(contactMethodList, "Value", "Text", contactMethod ?? "phone");
        }

        #region Consultation Create Insert and Delete Statements
        //
        // Insert
        public void Add(Consultation consultation)
        {
            entities.Consultations.AddObject(consultation);
        }


        //
        // Delete
        public void Delete(Consultation consultation)
        {
            entities.Consultations.DeleteObject(consultation);  // delete the bathroom Questionnaire
        }

        //
        // Persistence
        public void Save()
        {
            entities.SaveChanges();
        }

        #endregion


    }
}