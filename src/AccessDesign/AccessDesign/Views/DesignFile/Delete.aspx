﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.DesignFile>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Delete Confirmation: <%: Model.DesignFileType.Name %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h2>Delete Confirmation</h2>

    <div>
        <p>Please confirm that you want to delete the file for :
        <i> <%: Model.DesignFileType.Name %> </i>. The actual name of this file is: <i><%: Model.FileName %></i>?</p>
    </div>

    <div>
    <% using (Html.BeginForm()) { %>
    		    <input name="confirmButton" type="submit" value="Delete" />

    <% } %>
    </div>
    <p>
        <%: Html.ActionLink("Back to List", "Index", "DesignFiles", new { id = ViewData["RoomId"] }, null)%>
    </p>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

