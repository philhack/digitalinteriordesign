﻿<%@ Import Namespace="System.Web.Mvc" %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.SendUserNameModel>" %>
<%@ Import Namespace="AccessDesign.Helpers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Forgot User Name?
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Forgot User Name?</h2>

    <% Html.EnableClientValidation(); %>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
            <legend>Fields</legend>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.Email) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Email) %>
                <%: Html.ValidationMessageFor(model => model.Email) %>
            </div>
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.ConfirmEmail)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.ConfirmEmail)%>
                <%: Html.ValidationMessageFor(model => model.ConfirmEmail)%>
            </div>

            <div class="editor-label">
                Type the charaters you see in the image below.
            </div>
            <div class="display-field">
           <%= Html.GenerateCaptcha() %>
            </div>

            <p>
                <input type="submit" value="Submit" />
            </p>
        </fieldset>

    <% } %>

    <div>
        <%: Html.ActionLink("Back to List", "Index") %>
    </div>

</asp:Content>

