﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Room Payment Cancelled
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h2>Room Payment Cancelled</h2>
    <p>Your room payment has been cancelled.  If you have any questions about designing your room or going through the purchase process, please do not hesitate to contact us.</p>
    <p><b>Email:  <a href="mailto:info@digitalinteriordesign.com">info@digitalinteriordesign.com</a></b></p>
    <p></p>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>
