﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<AccessDesign.Models.Room>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Find Current Designers Rooms
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Find Current Designers Rooms</h1>

    <table>
        <tr>
            <th></th>
            <th>
                RoomId
            </th>
            <th>
                UserName
            </th>
            <th>
                Dimensions
            </th>
            <th>
                Name
            </th>
            <th>
                CurrentStepId
            </th>
            <th>
                DateUpdated
            </th>
            <th>
                RoomTypeName
            </th>
            <th>
                RoomPaidFor
            </th>
            <th>
                RoomCost
            </th>
            <th>
                TotalFreeRevisionsUsed
            </th>
            <th>
                TotalRevisionsPaidFor
            </th>
            <th>
                CostPerRevision
            </th>
            <th>
                PhotoDetails
            </th>
            <th>
                MeasurementDetails
            </th>
            <th>
                AssignedToDesigner
            </th>
            <th>
                CurrentDesignStepId
            </th>
        </tr>

    <% foreach (var item in Model) { %>
    
        <tr>
            <td>
                <%: Html.ActionLink("Edit", "Edit", new { id=item.RoomId }) %> |
                <%: Html.ActionLink("Details", "Details", new { id=item.RoomId })%> |
                <%: Html.ActionLink("Delete", "Delete", new { id=item.RoomId })%>
            </td>
            <td>
                <%: item.RoomId %>
            </td>
            <td>
                <%: item.UserName %>
            </td>
            <td>
                <%: item.Dimensions %>
            </td>
            <td>
                <%: item.Name %>
            </td>
            <td>
                <%: item.CurrentStepId %>
            </td>
            <td>
                <%: String.Format("{0:g}", item.DateUpdated) %>
            </td>
            <td>
                <%: item.RoomTypeName %>
            </td>
            <td>
                <%: item.RoomPaidFor %>
            </td>
            <td>
                <%: String.Format("{0:F}", item.RoomCost) %>
            </td>
            <td>
                <%: item.TotalFreeRevisionsUsed %>
            </td>
            <td>
                <%: item.TotalRevisionsPaidFor %>
            </td>
            <td>
                <%: String.Format("{0:F}", item.CostPerRevision) %>
            </td>
            <td>
                <%: item.PhotoDetails %>
            </td>
            <td>
                <%: item.MeasurementDetails %>
            </td>
            <td>
                <%: item.AssignedToDesigner %>
            </td>
            <td>
                <%: item.CurrentDesignStepId %>
            </td>
        </tr>
    
    <% } %>

    </table>

    <p>
        <%: Html.ActionLink("Create New", "Create") %>
    </p>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

