﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessDesign.Models;
using System;

namespace AccessDesign.Controllers
{
    public class ComponentsController : Controller
    {
        RoomRepository roomRepository = new RoomRepository();
        ProfileRepository profileRepository = new ProfileRepository();  // make a connection to the profile repository so that we can extract the clients first and last name
        GeneralQuestionnaireRepository generalQuestionnaireRepository = new GeneralQuestionnaireRepository();
        //
        // GET: /Components/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Components/RoomMenu/
        // Returns a listing of the currently logged on users rooms.  If no room exists for the user, then a default room is created.  This is used in a partial page
        //[Authorize, ChildActionOnlyAttribute]
        [Authorize]
        public ActionResult RoomMenu()
        {
            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            IEnumerable<Room> rooms = roomRepository.FindAllMyRooms(userName);
            if (rooms.Count() == 0 )          // if no rooms are returned, then we want to create a default room that will be displayed in the menu
            {
                int menuItemValue = 0;      // used to create a value for the menu item when no rooms exist for the currenly logged on user - set this to 0 (0 = this is a brand new room)
                bool profileExists = profileRepository.DoesProfileExist(userName);      // check if the profile exists for the currently logged on user
                bool generalQuestionnaireExists = generalQuestionnaireRepository.DoesGeneralQuestionnaireExist(userName);   // check if the general questionnaire exists for the currenly logged on user

                if (generalQuestionnaireExists)     // general questionnaire is complete
                    menuItemValue = 2;  // set the menu item value to 2
                else if (!generalQuestionnaireExists && profileExists)   // general questionnaire is not complete, but the profile exists
                    menuItemValue = 1;  //  set value to 1 since the profile is complete, but the general questionnaire is not complete

                Room room = new Room()      // create a new room and set all the required values to bunk data.  This room is purely used for display and will not be stored in the DB.
                {
                    RoomId = 0,
                    UserName = userName,
                    Dimensions = ".",
                    Name = "Your Room",                          // set the room name - this will be displayed in the view
                    CurrentStepId = 0,
                    DateUpdated = DateTime.Now,
                    RoomTypeName = ".",
                    RoomPaidFor = 0,
                    RoomCost = 0,
                    TotalFreeRevisionsUsed = 0,
                    TotalRevisionsPaidFor = 0,
                    CostPerRevision = 0,
                    PhotoDetails = ".",
                    MeasurementDetails = ".",
                    AssignedToDesigner = ".",
                    CurrentDesignStepId = 0,
                    ProfileId = 0,
                    MenuStepId = menuItemValue          // assign this value to the current menu item value
                };

                List<Room> bogusRooms = new List<Room>();       // generate a new empty list of rooms
                bogusRooms.Add(room);           // add the room with the bogus data that indicates the step to the list of rooms
                IEnumerable<Room> enumerableBogusRooms = bogusRooms;        // cast this as IEnumberable so that the view can understand what is being returned to it
                return PartialView("RoomMenu", enumerableBogusRooms);               // return the view with the list of a single room
            }
            else
                return PartialView("RoomMenu", rooms);
        }
    }
}
