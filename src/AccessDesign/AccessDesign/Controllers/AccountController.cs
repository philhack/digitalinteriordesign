﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using AccessDesign.Models;
using AccessDesign.Helpers;
using System.Text;



namespace AccessDesign.Controllers
{

    [HandleError]
    public class AccountController : Controller
    {
        ProfileRepository profileRepository = new ProfileRepository();      // make a connection to the profile repository
        GeneralQuestionnaireRepository generalQuestionnaireRepository = new GeneralQuestionnaireRepository();   // make a connection to the general questionnaire repository
        RoomRepository roomRepository = new RoomRepository();       // make a connection to the room repository

        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }

        protected override void Initialize(RequestContext requestContext)
        {
            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            if (MembershipService == null) { MembershipService = new AccountMembershipService(); }

            base.Initialize(requestContext);
        }

        // **************************************
        // URL: /Account/LogOn
        // **************************************

        public ActionResult LogOn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (MembershipService.ValidateUser(model.UserName, model.Password))
                {
                    FormsService.SignIn(model.UserName, model.RememberMe);
                    if (!String.IsNullOrEmpty(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        if (!profileRepository.DoesProfileExist(model.UserName))
                        {
                            return RedirectToAction("Create", "Profile");   // the profile does not exist so send user to the profile screen so that they can create a profile
                        }
                        else if (!generalQuestionnaireRepository.DoesGeneralQuestionnaireExist(model.UserName))
                        {
                            return RedirectToAction("Create", "GeneralQuestionnaire");      // the general questionnaire has not been created, so send user to the create questionnaire screen
                        }
                        else if (!roomRepository.DoesARoomExistForTheUser(model.UserName))
                        {
                            return RedirectToAction("Create", "Room");  // transfer the user to a screen so that they can create their first room
                        }
                        else
                        {
                            return RedirectToAction("LoggedOn", "Home");        // the user has completed the profile and questionnaire.  Send them to the logged on view.
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // **************************************
        // URL: /Account/LogOff
        // **************************************

        public ActionResult LogOff()
        {
            FormsService.SignOut();

            return RedirectToAction("Index", "Home");
        }

        // **************************************
        // URL: /Account/Register
        // **************************************

        public ActionResult Register()
        {
            ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            model.UserName = model.Email;   // set the username equal to the email address upon registration since we're using an email address as the username
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                MembershipCreateStatus createStatus = MembershipService.CreateUser(model.UserName, model.Password, model.Email);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    FormsService.SignIn(model.UserName, false /* createPersistentCookie */);
                    // send user an email that their account has been created
                    

                    // Create an email message that contains the new password
                    StringBuilder emailBody = new StringBuilder();

                    string loginUrl = "https://" + System.Configuration.ConfigurationManager.AppSettings["applicationFQDN"] + "/account/LogOn";      // Generate the login URL
                    emailBody.Append("Thank you for registering with Digital Interior Design. We look forward to working with you on your project." + "<br /><br />"); 
                    emailBody.Append("Here are the details for logging into <a href=\"" + loginUrl + "\" target=\"_blank\">" + loginUrl + "</a>." + "<br /><br />");
                    emailBody.Append("User Name: " + model.UserName + "<br />");                    
                    emailBody.Append("If you have any questions about working with Digital Interior Design or how to use the site, please email us at <a href=\"mailto:info@digitalinteriordesign.com\">info@digitalinteriordesign.com</a> or phone us at 1.888.736.9660." + "<br /><br />");
                    emailBody.Append("Thank you," + "<br /><br />");
                    emailBody.Append("Digital Interior Design Customer Service" + "<br />");
                    emailBody.Append(System.Configuration.ConfigurationManager.AppSettings["tollFreePhoneNumber"] + "<br />");
                    emailBody.Append("<a href=\"mailto:info@digitalinteriordesign.com\">info@digitalinteriordesign.com</a>");

                    SendEmail emailMessage = new SendEmail();   // Create a new Send Email instance
                    emailMessage.SendSingleEmail(emailBody, System.Configuration.ConfigurationManager.AppSettings["fromEmail"], System.Configuration.ConfigurationManager.AppSettings["fromDisplayName"], model.UserName, "Welcome to Digital Interior Design!");

                    return RedirectToAction("Create", "Profile");
                }
                else
                {
                    ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));
                }
            }

            // If we got this far, something failed, redisplay form
            ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            return View(model);
        }

        // **************************************
        // URL: /Account/ChangePassword
        // **************************************

        [Authorize]
        public ActionResult ChangePassword()
        {
            ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                if (MembershipService.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword))
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }

            // If we got this far, something failed, redisplay form
            ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            return View(model);
        }

        // **************************************
        // URL: /Account/ChangePasswordSuccess
        // **************************************

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        // ******************************************************************************


        // **************************************
        // URL: /Account/ResetPassword
        // **************************************


        public ActionResult ResetPassword()
        {
            return View();
        }


        /*
         * To Change this to allow Recaptcha validation
         *  1. Uncomment the captchaValidator.
         *  2. Accept the bool captchaValid as a parameter   (public ActionResult ResetPassword(ResetPasswordModel model, bool captchaValid))
         *  3. Uncomment the if (!captchaValid)
         * 
         */
        [CaptchaValidator]
        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordModel model, bool captchaValid)
        {
            if (!captchaValid)
            {
                ModelState.AddModelError("", "The reCaptcha words that you have entered in below do not match.  Please type in the words you see in the image below..");
            }
            
            if (ModelState.IsValid)
            {
                AccountMembershipService member = new AccountMembershipService();

                if (member.ResetPassword(model.Email))
                {
                    return RedirectToAction("ResetPasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "Sorry, there was a problem resetting your password.  Please ensure that you have entered the correct email address and try again.");
                }
                               
                
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // **************************************
        // URL: /Account/ResetPasswordSuccess
        // **************************************

        public ActionResult ResetPasswordSuccess()
        {
            return View();
        }

    }
}
