﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Foolproof;


namespace AccessDesign.Models
{
    [MetadataType(typeof(Consultation_Validation))]
    public partial class Consultation
    {

    }

    public class Consultation_Validation
    {
        //[HiddenInput]
        public string RoomId { get; set; }

        [DisplayName("Date: ")]     // Radio
        [Required(ErrorMessage = "Required")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        public string Date { get; set; }

        [DisplayName("Time: ")]     // Radio
        [Required(ErrorMessage = "Required")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        public string Time { get; set; }

        [DisplayName("Time Zone: ")]     // Radio
        [Required(ErrorMessage = "Required")]
        [StringLength(3, ErrorMessage = "Please enter a maximum of 3 characters.")]
        public string TimeZone { get; set; }

        [DisplayName("Contact Method: ")]     // Radio
        [Required(ErrorMessage = "Required")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        public string ContactMethod { get; set; }

        [DisplayName("Skype ID: ")]     // Radio
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        [RequiredIf("ContactMethod", "skype", ErrorMessage = "Required")]
        public string SkypeId { get; set; }

        [DisplayName("Phone Number: ")]
        [Required(ErrorMessage = "Phone Number is required")]
        [RegularExpression(@"(\(\d{3}\)|\b\d{3})(\.| |\-)\d{3}(\.| |\-)\d{4}\b", ErrorMessage = "Please enter a phone number (ie: 555-555-5555)")]    // may need to fine tune this.
        [StringLength(20, ErrorMessage = "Phone Number is too long (Maximum 20 characters")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Required")]
        [StringLength(8, ErrorMessage = "Please enter a maximum of 8 characters..")]
        public string Type { get; set; }

        [Required(ErrorMessage = "Required")]
        public bool Complete { get; set; }
    }
}