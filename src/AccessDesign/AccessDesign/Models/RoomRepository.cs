﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Mvc;
using System.Runtime.InteropServices;

using System.Data;
using System.Text;
using System.Data.Objects;
using System.Globalization;

namespace AccessDesign.Models
{
    public class RoomRepository
    {
        private AccessDesignEntities entities = new AccessDesignEntities();

        //
        // Query Methods

        /// <summary>
        /// Get's all of the room types
        /// </summary>
        /// <returns>List of all room types and their respective values</returns>
        public IQueryable<RoomType> GetAllRoomTypes()
        {
            return from rt in entities.RoomTypes
                   orderby rt.Name
                   select rt;
        }

        public RoomType GetSpecificRoomType(string roomTypeName)
        {
            return entities.RoomTypes.FirstOrDefault(rt => rt.Name == roomTypeName);
        }

        /// <summary>
        /// Checks if at least 1 room exists
        /// </summary>
        /// <param name="userName">username of the user that we want to check</param>
        /// <returns>true if 1 or more room exists, otherwise returns false</returns>
        public bool DoesARoomExistForTheUser(string userName)
        {
            if (entities.Rooms.Count(r => r.UserName == userName) >= 1)   // check if at least 1 room exists
                return true;    // at least 1 room exists, so return true
            else
                return false;   // a room does not exist
        }

        // Get's all rooms
        public IQueryable<Room> FindAllRooms()
        {
            return entities.Rooms;
        }

        /// <summary>
        /// Returns the number of rooms that the user owns based on the user's username
        /// </summary>
        /// <param name="userName">The current user's username</param>
        /// <returns>the number of rooms the user owns</returns>
        public int GetNumberOfRoomsForUser(string userName)
        {
            return entities.Rooms.Count(p => p.UserName == userName);
        }


        // Get's all rooms belonging to the username passed to the function
        public IQueryable<Room> FindAllMyRooms(string userName)
        {
            // TODO:  May want to change this to order by Room Name asc.
            return from room in entities.Rooms
                   where room.UserName == userName
                   orderby room.DateUpdated descending
                   select room;
        }

        // Return the room based on the room id
        public Room GetRoomById(int id)
        {
            return entities.Rooms.FirstOrDefault(r => r.RoomId == id);
        }

        /// <summary>
        /// Get all of the room details, including photos, profile, current step, and current design step, design files, and design file types
        /// This is used by designers to get the details of a room that their working on.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Room GetAllRoomDetailsById(int id)
        {
            Room room = (from r in entities.Rooms.Include("Profile").Include("CurrentStep").Include("CurrentDesignStep")
                         where r.RoomId == id
                         select r).FirstOrDefault();

            return room;
        }

        // Return the room based on the room id and the username
        public Room GetRoomByIdVerifyUserName(int id, string userName)
        {
            // checks if the id of the room and the username match
            Room room = (from r in entities.Rooms
                         where r.RoomId == id && r.UserName == userName
                         select r).FirstOrDefault();
            return room;
        }

        // Get's the room and all of the photos for that room based on the username
        public Room GetSingleRoomPhotosByUserName(string userName)
        {
            Room room = (from r in entities.Rooms.Include("RoomPhoto")
                         where r.UserName == userName
                         select r).FirstOrDefault();
            return room;
        }

        // Get's the Room and All the Questions and Answers for that Room by UserName
        public Room GetSingleRoomQuestionsAndAnswersByRoomIdAndUserName(int id, string userName)
        {
            Room room = (from r in entities.Rooms.Include("Answers").Include("Questions").Include("QuestionGroups")
                         where r.UserName == userName
                         select r).FirstOrDefault();
            return room;
        }


        /// <summary>
        /// Returns a list of all of the rooms and profiles for users that have created rooms
        /// </summary>
        /// <returns>An IEnumerable list of profiles</returns>
        public IEnumerable<Room> GetAllRoomsAndProfilesForAllUsersWithRooms()
        {
            return from room in entities.Rooms.Include("Profile")
                   orderby room.UserName
                   select room;
        }

        /// <summary>
        /// Get's the number of room photos that are associated with a room for a particular room id
        /// </summary>
        /// <param name="id">room id</param>
        /// <returns></returns>
        public int GetNumberOfRoomPhotosForARoom(int id)
        {
            return entities.RoomPhotos.Count(p => p.RoomId == id);
        }

        /// <summary>
        /// Get's the number of furniture pieces that are associated with a room for a particular room id
        /// </summary>
        /// <param name="id">room id</param>
        /// <returns></returns>
        public int GetNumberOfFurniturePiecesForARoom(int id)
        {
            return entities.Furnitures.Count(f => f.RoomId == id);
        }

        /// <summary>
        /// Get the number of design files that are associated with a room for a particular room id
        /// </summary>
        /// <param name="id">room id</param>
        /// <returns></returns>
        public int GetNumberOfDesignFilesForARoom(int id)
        {
            return entities.DesignFiles.Count(d => d.RoomId == id);
        }

        /// <summary>
        /// Get's the number of room measurements that are associated with a room for a particular room id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int GetNumberOfRoomMeasurementsForARoom(int id)
        {
            return entities.RoomMeasurements.Count(r => r.RoomId == id);
        }

        /// <summary>
        /// Returns a list of all of the possible current steps that a customer could be at
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CurrentStep> GetAllCurrentCustomerSteps()
        {
            return from currentSteps in entities.CurrentSteps
                   orderby currentSteps.CurrentStepId
                   select currentSteps;
        }

        /// <summary>
        /// Returns a list of all of the possible current steps that a designer could be at
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CurrentDesignStep> GetAllCurrentDesignerSteps()
        {
            return from currentDesignerSteps in entities.CurrentDesignSteps
                   orderby currentDesignerSteps.CurrentDesignStepId
                   select currentDesignerSteps;
        }

        /// <summary>
        /// Returns a list of all of the possible menu steps that a designer could be at
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MenuStep> GetAllMenuSteps()
        {
            return from menuSteps in entities.MenuSteps
                   orderby menuSteps.MenuStepId
                   select menuSteps;
        }

        /// <summary>
        /// Get's the current menu step id based on the room id that is passed to it.
        /// </summary>
        /// <param name="id">room id</param>
        /// <returns>current menu step for this room</returns>
        public int GetCurrentMenuStep(int id)
        {
            return (from r in entities.Rooms
                         where r.RoomId == id
                         select r.MenuStepId).FirstOrDefault();
        }

        /// <summary>
        /// Returns the room photo comments for a particular room
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetRoomPhotoComments(int id)
        {
            string photoComments = entities.Rooms.FirstOrDefault(r => r.RoomId == id).PhotoDetails;
            if(String.IsNullOrEmpty(photoComments))
                return " ";
            else
                return photoComments;
        }

        // Checks if the room exists based on id and userName, if it does, it returns true, otherwise it returns false
        public bool DoesRoomExist(int id, string userName)
        {
            if (entities.Rooms.Count(u => u.RoomId == id && u.UserName == userName) == 1)   // check if a profile based on the username exists
                return true;    // the profile exists, so return true
            else
                return false;   // the profile does not exist
        }

        /// <summary>
        /// Get the room type based on the room id that is passed to it.
        /// </summary>
        /// <param name="id">Room Id</param>
        /// <returns></returns>
        public string GetRoomTypeByRoomId(int id)
        {
            string roomType = (from r in entities.Rooms
                         where r.RoomId == id
                         select r.RoomTypeName).FirstOrDefault();
            return roomType;
        }





        /// <summary>
        /// Returns all of the rooms assigned to a particular designer based on the designers username
        /// </summary>
        /// <param name="userName">The username of the designer</param>
        /// <returns></returns>
        public IQueryable<Room> GetRoomsByDesignerUserName(string userName)
        {
            return from room in entities.Rooms.Include("CurrentStep").Include("CurrentDesignStep")
                   where room.AssignedToDesigner == userName
                   orderby room.DateUpdated ascending
                   select room;
        }

        /// <summary>
        /// Returns all rooms that contain any values of the roomName that you specified in them.
        /// Select room where roomName LIKE '%roomName%'
        /// </summary>
        /// <param name="roomName"></param>
        /// <returns></returns>
        public IQueryable<Room> GetRoomsByRoomName(string roomName)
        {
            return from room in entities.Rooms
                   where room.Name.Contains(roomName)
                   orderby room.Name ascending
                   select room;
        }

        /// <summary>
        /// Updates the room's date/time using a stored proceedure.  This is utilized for the left navigation menu.
        /// This is faster then updating the entire room model every time
        /// </summary>
        /// <param name="roomId">Room ID that needs to be updated</param>
        /// <param name="lastUpdated">Current DateTime</param>
        /// <returns></returns>
        public bool UpdateRoomDateUpdated(int roomId, DateTime lastUpdated)
        {
            int count = -1; // initialize the counter
            try
            {
                count = entities.UpdateRoomDateUpdated(roomId, lastUpdated);   // update the date/time for a single room and returns the number of rows that have been updated
            }
            catch (Exception)
            {
                // if an exception occures, return false
                return (false);
            }

            // only one row should be updated since the roomId if unique. If the update is successful, return true, otherwise return false.
            if (count != 1)
                return (false);
            else
                return (true);
        }

#region Room Create Insert and Delete Statements
        //
        // Insert
        public void Add(Room room)
        {
            entities.Rooms.AddObject(room);
        }


        //
        // Delete
        public void Delete(Room room)
        {
            entities.Rooms.DeleteObject(room);  // delete the room
        }

        //
        // Persistence
        public void Save()
        {
            entities.SaveChanges();
        }

#endregion

    }

}