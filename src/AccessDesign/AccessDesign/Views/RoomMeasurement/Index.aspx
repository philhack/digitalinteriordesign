﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Room>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	How To Measure Your Room
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>How To Measure Your Room</h1>
    <div id="room-photo-nav">
            <a href="/RoomMeasurement/Upload/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-UploadMeasurements.png" alt="" border="0" class="room-photo-nav-image"/></a><a href="/RoomMeasurement/Details/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-ViewEditMeasurements.png" alt="" border="0" class="room-photo-nav-image"/></a><a href="/RoomMeasurement/Comments/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-AddComments.png" alt="" border="0" class="room-photo-nav-image"/></a>
    </div>   
    <br />
    
    <p>Now it's time to take the measurements of your room. Please follow the steps <a href="#">here</a> to measure the room.  Once you have measured the room you can either use a digital camera or a scanner to upload them to the designer.  Please use the navigation above to <%: Html.ActionLink("upload", "Upload", "RoomMeasurement", new { id = ViewData["RoomId"] }, new { })%> your photos/scans of your room measurements, <%: Html.ActionLink("view/update", "Details", "RoomMeasurement", new { id = ViewData["RoomId"] }, new { })%> your photos, and <%: Html.ActionLink("add comments", "Comments", "RoomMeasurement", new { id = ViewData["RoomId"] }, new { })%> about your the measurements. </p><br /><p>Once this is complete, the designer will have all of the information that they require to create your design. You will be notified by email when your design is ready. You can then schedule a follow up consultation with the designer and a arrange for a revision if necessary.</p>
    <br /><br />
    <p><a href="/Downloads/how-to-measure-your-room.pdf" target="_blank"><img src="/Content/Images/pdf-logo-mini.jpg" alt="How to measure your room"/></a>&nbsp;<a href="/Downloads/how-to-measure-your-room.pdf" target="_blank">How to measure your room</a></p>
    <br />
    <br />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".room-photo-nav-image")
            .mouseover(function () {
                var src = $(this).attr("src").match(/[^\.]+/) + "TreomaOn.png";
                $(this).attr("src", src);
            })
            .mouseout(function () {
                var src = $(this).attr("src").replace("TreomaOn", "");
                $(this).attr("src", src);
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

