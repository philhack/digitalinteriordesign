﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.IO;

namespace AccessDesign.Models
{
    // This class sanitizes the user's photos that are being uploaded.
    public class Sanitizer
    {
        /// <summary>
        /// Sanitizes the name of the user's photos that were uploaded from their computers
        /// </summary>
        /// <param name="name">
        /// name is the string to sanitize
        /// </param>
        /// <returns></returns>
        public static string SanitizePhotoName(string name)
        {
            // first trim the raw string
            string safe = name.Trim().ToLower();

            //strip off the file extension (ie: .jpg"
            string fileExtension = safe.Substring((safe.Length - 4), 4);    // save the file extension
            if (fileExtension == "jpeg")        // if the file extension is a jpeg file, then add a dot in-front.
                fileExtension = "." + fileExtension;


            safe = safe.Remove((safe.Length - 4), 4);       // remove the file extension
            
            // replace spaces with hyphens
            safe = safe.Replace(" ", "-").ToLower();

            // replace any 'double spaces' with singles
            if (safe.IndexOf("--") > -1)
                while (safe.IndexOf("--") > -1)
                    safe = safe.Replace("--", "-");

            // trim out illegal characters
            safe = Regex.Replace(safe, "[^a-z0-9\\-]", "");

            // trim the length
            if (safe.Length > 50)
                safe = safe.Substring(0, 49);

            // clean the beginning and end of the filename
            char[] replace = { '-', '.' };
            safe = safe.TrimStart(replace);
            safe = safe.TrimEnd(replace);

            safe = safe + fileExtension;

            return safe;
        }


        /// <summary>
        /// Checks if the image file extension is a valid jpg or jpeg file
        /// </summary>
        /// <param name="name">file name of the the file to be checked</param>
        /// <returns>true if the image if adheres to the rules, otherwise it returns false</returns>
        public bool IsValidImage(string name)
        {
            string fileExtension = name.Substring((name.Length - 4), 4).ToLower();
            string[] imageExensions = { ".jpg", "jpeg" };   // 4 character image extensions

            foreach (string imageExension in imageExensions)    // loop through the file's extension, if it matches return true, otherwise after checking all extensions, return false.
            {
                if (fileExtension == imageExension)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if the file extension of the file being uploaded matches the criteria of jpg, jpeg, pdf, docx, doc
        /// </summary>
        /// <param name="name">file name of the file to be checked</param>
        /// <returns>true if the file is valid</returns>
        public bool IsValidDesignerFile(string name)
        {
            string fileExtension = name.Substring((name.Length - 4), 4).ToLower();
            string[] fileExensions = { ".jpg", "jpeg", ".pdf", "docx", ".doc" };   // 4 character image extensions

            foreach (string fileExension in fileExensions)    // loop through the file's extension, if it matches return true, otherwise after checking all extensions, return false.
            {
                if (fileExtension == fileExension)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Gernates a new file name consisting of ROOMID-GUID.extension
        /// </summary>
        /// <param name="name">Original file name</param>
        /// <param name="roomId">Room ID</param>
        /// <returns></returns>
        public static string CreateGuidRoomPhoto(string name, int roomId)
        {
            Sanitizer sani = new Sanitizer();
            string newFileName = roomId + "-" + Guid.NewGuid().ToString() + sani.CleanFileExtension(name); ;    // Set the new file name to "roomID-GUID.extension"
            return newFileName;
        }

        /// <summary>
        /// Generates a new file name consisting of ROOMID-FURNITUREID-GUID.extension
        /// </summary>
        /// <param name="name">Original file name</param>
        /// <param name="roomId">Room ID</param>
        /// <param name="furnitureId">Furnature ID</param>
        /// <returns></returns>
        public static string CreateGuidRoomFurniturePhoto(string name, int roomId, int furnitureId)
        {
            Sanitizer sani = new Sanitizer();
            string newFileName = roomId + "-" + furnitureId + "-"+ Guid.NewGuid().ToString() + sani.CleanFileExtension(name); ;    // Set the new file name to "roomID-furnitureID-GUID.extension"
            return newFileName;
        }

        /// <summary>
        /// Cleans the jpg file extension (checks if it's .jpg or .jpeg, fixes it and returns the file extension)
        /// </summary>
        /// <param name="originalFileName">Full original file name that the user uploaded</param>
        /// <returns></returns>
        private string CleanFileExtension(string originalFileName)
        {
            // first trim the raw string
            string safe = originalFileName.Trim().ToLower();

            //strip off the file extension (ie: .jpg"
            string fileExtension = safe.Substring((safe.Length - 4), 4);    // save the file extension
            if (fileExtension == "jpeg")        // if the file extension is a jpeg file, then add a dot in-front.
                fileExtension = "." + fileExtension;

            return fileExtension;
        }


    }
}