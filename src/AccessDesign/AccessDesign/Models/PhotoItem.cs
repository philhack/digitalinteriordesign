﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccessDesign.Models
{
    public class PhotoItem
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string Path { get; set; }
        public string wbPath { get; set; }
        public string tnPath { get; set; }
    }
}