﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessDesign.Models;
using System.Web.Profile;
using System.IO;
using System.Text;
using System.Net;
using System.Net.Mail;
using com.apress.paypal;        // used to encrypt the paypal buttons
using System.Configuration;

namespace AccessDesign.Controllers
{
    
    public class CommerceController : Controller
    {
        /*
         * Important:  For this to work.....
            Both:
             - digital-prd-pkcs12.p12
             - paypal_cert_pem_digital_prd.txt  (generated by paypal)  must exist in the ~/AppData/ folder on the server.

            digital-prd-pkcs12.p12 must be installed into the computer personal certificate store on the server!!!! 
         * 
         * If these are not setup like this, you will get a:  System.Security.Cryptography.CryptographicException: The system cannot find the file specified.
         */

        RoomRepository _roomRepository = new RoomRepository();   // create a new instance of the room repository
        OrderRepository _orderRepository = new OrderRepository();    // create a new instance of the order repository
        ProfileRepository _profileRepository = new ProfileRepository();      // create a new instance of the profile repository

        private static readonly string payPalAccount = System.Configuration.ConfigurationManager.AppSettings["payPalAccount"];
        private static readonly string payPalIdentityToken = System.Configuration.ConfigurationManager.AppSettings["payPalIdentityToken"];
        private static readonly string payPalServer = System.Configuration.ConfigurationManager.AppSettings["payPalServer"];
        private static readonly string signerPfxPassword = System.Configuration.ConfigurationManager.AppSettings["signerPfxPassword"];     // password for the p12 public/private key pair
        private static readonly string paypalCertId = System.Configuration.ConfigurationManager.AppSettings["paypalCertId"];
        private static readonly string paypalCancelUrl = System.Configuration.ConfigurationManager.AppSettings["paypalCancelUrl"];
        private static readonly string paypalReturnUrl = System.Configuration.ConfigurationManager.AppSettings["paypalReturnUrl"];
        private static readonly string fromEmail = System.Configuration.ConfigurationManager.AppSettings["fromEmail"];
        private static readonly string fromDisplayName = System.Configuration.ConfigurationManager.AppSettings["fromDisplayName"];


        /// <summary>
        /// Transactions the data request.
        /// </summary>
        /// <param name="requestPaypalData">The request paypal data.</param>
        /// <returns></returns>
        [NonAction]
        private string TransactionDataRequest(string tx)
        {
            // read the original IPN post
            string formValues = Encoding.ASCII.GetString(HttpContext.Request.BinaryRead(HttpContext.Request.ContentLength));
            string requestFormValues = formValues + String.Format("&cmd={0}&at={1}&tx={2}",
                "_notify-synch",
                payPalIdentityToken,
                tx
            );

            // create the pay pal request
            HttpWebRequest payPalRequest = (HttpWebRequest)WebRequest.Create(payPalServer);
            payPalRequest.Method = "POST";
            payPalRequest.ContentType = "application/x-www-form-urlencoded";
            payPalRequest.ContentLength = requestFormValues.Length;

            // write the request back IPN strings
            using (StreamWriter writer = new StreamWriter(payPalRequest.GetRequestStream(), Encoding.ASCII))
            {
                writer.Write(requestFormValues);
                writer.Close();
            }

            // send the request to pay pal
            using (HttpWebResponse payPayResponse = (HttpWebResponse)payPalRequest.GetResponse())
            using (Stream payPalResponseStream = payPayResponse.GetResponseStream())
            using (StreamReader reader = new StreamReader(payPalResponseStream, Encoding.UTF8))
            {
                string ipnStatus = reader.ReadToEnd();
                return ipnStatus;
            }
        }

        /// <summary>
        /// Extracts the value.
        /// </summary>
        /// <param name="pdt">The PDT.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        /// <seealso href="http://mvcsamples.codeplex.com/SourceControl/changeset/view/22882#386911">Thanks to Rob Conery of wekeroad.com for this code</seealso>
        [NonAction]
        private string ExtractValue(string pdt, string key)
        {
            string[] keys = pdt.Split('\n');
            string output = String.Empty;
            string thisKey = String.Empty;

            foreach (string item in keys)
            {
                string[] bits = item.Split('=');
                if (bits.Length > 1)
                {
                    output = bits[1];
                    thisKey = bits[0];
                    if (thisKey.Equals(key, StringComparison.InvariantCultureIgnoreCase))
                        break;
                }
            }

            return HttpContext.Server.UrlDecode(output);
        }

        /// <summary>
        /// Sends the tracking email.
        /// </summary>
        /// <param name="trackingID">The tracking ID.</param>
        /// <param name="customerEmail">The customer email.</param>
        [NonAction]
        private void SendTrackingEmail(string trackingID, string customerEmail)
        {
            MailMessage mailMessage = new MailMessage();

            //mailMessage.From = new MailAddress(Configuration.TheBeerHouseSection.Current.Commerce.PayPalAccount);
            mailMessage.To.Add(new MailAddress(customerEmail));

            mailMessage.Subject = "Order has been shipped";
            mailMessage.Body = "Your package has been shipped, your tracking # is " + trackingID;

            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Send(mailMessage);
        }

        //
        // GET: /Commerce/5
        /// <summary>
        /// Returns a view that allows the user to view their invoice or pay for their room
        /// </summary>
        /// <param name="id">Room ID</param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Index(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();        // Get's the current logged on user name
            Room room = _roomRepository.GetRoomByIdVerifyUserName(id, userName);
            if (room == null)       // check to ensure that a room was returned before proceeding
            {
                TempData["ErrorMessage"] = "Sorry, an error was encountered while trying to retrieve your room.";       // throw an error
                return View("Error");
            }
            room.DateUpdated = DateTime.Now;        // update the room for the menu system
            _roomRepository.Save();
            if (room.MenuStepId < 7)        // the user has not completed the previous step (book consulation (step 7))
            {
                TempData["ErrorMessage"] = "Please use the form below to arrange a consulation before you pay for your room design.";
                return RedirectToAction("Index", "Consultation", new { id = id });
            }
            else if(room.MenuStepId == 7)        // the user has completed the previous step (book consulation (step 7)), so allow them to pay for their room
            {
                return RedirectToAction("PayForRoom", new {id=id});
            }
            else        // the user has purchased their room so allow them to view their invoice (ie:  their step must be:  >= 8)
            {
                return View(room);
            }
        }

        [HttpPost, Authorize]
        public ActionResult CompleteOrder(string id) {
            TempData["SuccessMessage"] = "Your order has been processed successfully.";
            var roomId = Convert.ToInt32(id);
            var userName = HttpContext.User.Identity.Name;
            var profile = _profileRepository.GetProfileByUserName(userName);
            var room = _roomRepository.GetRoomById(roomId);

            // set the room's status as paid and save it to the database
            if (room.MenuStepId < 8)        // make sure that the user has not already completed this step
            {
                room.MenuStepId = 8;        // set the current menu step to 8, which is completion of the pay for your room section
                room.CurrentStepId = 6;     // set the current user step to 6, which is completion of the pay for your room section
            }
            room.RoomPaidFor = 1;   // set the RoomPaidFor status to 1 which means that the room has been paid for.
            _roomRepository.Save();  // persist the changes for this room to the database

            var order = new Order()
            {
                AddedBy = userName,
                AddedDate = DateTime.Now,
                CustomerEmail = userName,
                Shipping = 0,
                ShippingCity = profile.City,
                ShippingCountry = profile.Country,
                ShippingFirstName = profile.FirstName,
                ShippingLastName = profile.LastName,
                ShippingMethod = "Included",                                    //shoppingCart.ShippingMethod.Title,
                ShippingStreet = profile.Address,
                ShippingPostalCode = profile.Zip,
                ShippingState = profile.State,
                RoomId = room.RoomId,
                OrderRoomName = room.Name,
                Status = "Completed",
                OrderType = "Room",                                         // OrderType can either be a "Room" or a "Revision". This controller action only deals with rooms, so in this case it is "Room"
                TransactionId = "6JD29970EU312578G",
                ReceiptId = "2293-7705-5108-0361",           // we only get a receipt id when it's a Credit Card transaction.  If it's from a paypal account, this returns as 0.00
                SubTotal = Convert.ToDecimal(1350.00),                //  shoppingCart.Total        TODO: get the total amount
                Total = Convert.ToDecimal(1512.00),
                TaxAmount = Convert.ToDecimal(162.00),
                TaxType = room.RoomTaxType,
                Currency = "CAD"
            };

            _orderRepository.Add(order);
            _orderRepository.Save();

            var invoice = new Invoice()     // generate a new invoice so that it can be displayed in the view
            {
                CustomerId = 10000 + profile.ProfileId,
                RoomId = room.RoomId,
                OrderNumber = "S" + (10000 + order.OrderId).ToString(),
                RoomDescription = "Professional interior design services for a " + room.RoomTypeName.ToLower(),
                RoomName = room.Name,
                RoomCost = Convert.ToDecimal(room.RoomCost),
                TaxType = room.RoomTaxType + " " + room.RoomTaxPercent,
                TaxAmount = Convert.ToDecimal(room.RoomTaxCost),
                Discount = 0.0M,
                GrandTotal = Convert.ToDecimal(room.RoomTotalCost),
                CurrencyCode = order.Currency,
                Username = profile.UserName,
                Address = profile.Address,
                City = profile.City,
                State = profile.State,
                Zip = profile.Zip,
                Country = profile.Country,
                Name = profile.FirstName + " " + profile.LastName
            };

            return View(invoice);
        }


        //
        //  GET: /Commerce/Cancel
        /// <summary>
        ///  Used when the user wants to cancel their order after they go to the paypal screen.
        ///  Currently all this does is send our design team and email that a user has cancelled their purchase so they they can follow up if they want.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult Cancel()
        {
            string userName = User.Identity.Name;
            Profile profile = _profileRepository.GetProfileByUserName(userName);
            if (profile != null)
            {
                // email our design team
                // Send email to designers warning them that the this customer cancelled their paypal order screen.  This allows them to pro-actively follow up with the customer if the customer does not actually book a time.
                try
                {
                    // Create an email message that contains the new password
                    StringBuilder emailBody = new StringBuilder();
                    emailBody.Append("This customer was purchasing their room, got to the paypal screen and then cancelled their order. If they do not put an order through in a few days, you might want to follow up with them to see if there is anything you can help the with.<br /><br />");
                    emailBody.Append("Customer username:    " + profile.UserName + "<br /><br />");
                    emailBody.Append("Customer Name:    " + profile.FirstName + " " + profile.LastName + "<br /><br />");
                    emailBody.Append("Customer Email:    " + profile.UserName + "<br /><br />");
                    emailBody.Append("Customer Phone Number:    " + profile.PhoneNumber + "<br /><br />");
                    emailBody.Append("Customer State/Province:    " + profile.State + "<br /><br />");
                    emailBody.Append("Customer Country:    " + profile.Country + "<br /><br />");

                    SendEmail emailMessage = new SendEmail();   // Create a new Send Email instance
                    emailMessage.SendSingleEmail(emailBody, fromEmail, fromDisplayName, fromEmail, "PayPal - Order Cancel Screen");
                }
                catch (Exception)
                {
                    //TODO: Perform some logging here.  We don't want to display an error to the customer, but would like to silently log this.
                }
            }

            // TODO:  search the rooms for the most up-to-date date modified value, get the room id, then redirect-to-action the payment screen with that id
            return View();
        }

        /// <summary>
        /// Allows the customer to pay for their room
        /// </summary>
        /// <param name="id">The room id that the customer needs to pay for</param>
        /// <returns></returns>
        [HttpGet, Authorize]
        public ActionResult PayForRoom(int id)
        {
            double taxRate = 0.000;    // the percentage in tax that the user will be charged.  this amount differs based on the province that the user is in.
            double taxAmount = 0.000;      // the amount in dollars that the user will be charged in tax  ( we will calculate this amount)
            double taxMultiplyer = 0.000;   // the percentage multiplyer for calculating the total cost of a room including the tax amount
            double totalRoomCost = 0.000;       // the total cost for the room
            string currencyCode = "USD";    // initialize the default currency type that will used for our transactions
            string paypalCertPath = Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["paypalCertPath"]);       // path of the paypal public certificate
            string signerPfxPath = Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["signerPfxPath"]);           // path of our generated p12 cert (which includes both the public and private keys and is password encrypted)
            string userName = HttpContext.User.Identity.Name.ToString();                                                                            // Get's the current logged on user name
            ViewData["PayPalServer"] = payPalServer;
            ViewData["ShowTax"] = false;        // initialize the show tax setting that is used in the view to false.  We'll excplicitly set this to true if the client is from Canada
            ViewData["Currency"] = "All prices shown are in US Dollars";        // initialize the currency code to USD that will be displayed in the view.

            Profile profile = _profileRepository.GetProfileByUserName(userName);                     // get the profile, then do a test to ensure that the profile was retrieved
            if (profile == null)
            {
                TempData["ErrorMessage"] = "Sorry, an error was encountered while trying to retrieve your profile.";        // throw an error
                return View("Error");
            }
            else 
            {
                Room room = _roomRepository.GetRoomByIdVerifyUserName(id, userName);         // get the room then test to ensure that a room was retrieved
                if(room == null)
                {
                    TempData["ErrorMessage"] = "Sorry, an error was encountered while trying to retrieve your room.";       // throw an error
                    return View("Error");
                }
                else          // the profile and the room exist so let's get this transaction started.
                {
                    if (room.RoomPaidFor == 1)
                    {
                        TempData["ErrorMessage"] = "You have already paid for this room.  You do not need to pay for this room again.";
                        return View("Error");
                    }
                    else        // the room has not been paid for so let's setup things for the transaction
                    {
                        if (profile.Country == "CA")        // if the user is located in Canada, we're going to charge tax and charge the user in Canadian currency.
                        {
                            State state = _profileRepository.GetProviceDetails(profile.State);           // get the details from the current user's province
                            taxRate = Convert.ToDouble(state.TaxPercent);         // set the tax rate to the customer's location
                            room.RoomTaxType = state.TaxType;           // set the tax type that will be stored in the room equal to the state's tax type
                            room.RoomTaxPercent = state.TaxPercent;
                            currencyCode = "CAD";               // set this to Canadian Dollars

                            ViewData["ShowTax"] = true;                         // set the show tax setting to true
                            ViewData["TaxRate"] = state.TaxPercent;     // set these values for the Canadian's they will be displayed in the view 
                            ViewData["TaxType"] = state.TaxType;
                            ViewData["Currency"] = "All prices shown are in Canadian Dollars";      // set the message for the type of currency that will be used
                        }                      
                                               
                        // update the room with the total room cost
                        totalRoomCost = Convert.ToDouble(room.RoomCost);        // get the room cost from the database
                        taxAmount = totalRoomCost * (taxRate * .01);        // calculate the total amount for taxes
                        taxMultiplyer = 1 + (taxRate * .01);        // this is a the calculated tax rate multiplier
                        totalRoomCost = totalRoomCost * taxMultiplyer;      // this is the total cost of the room including tax.
                      
                        // update the room cost in the database
                        room.RoomTotalCost = Convert.ToDecimal(Math.Round(totalRoomCost, 2));        // rount the room cost to the nearest 2 decimal places, convert the type to decimal and save it to the room
                        room.RoomTaxCost = Convert.ToDecimal(Math.Round(taxAmount, 2));     // update the room with the total cost for tax
                        room.DateUpdated = DateTime.Now;        // update the last updated date of the room
                        _roomRepository.Save();      // save these new values to the DB

                        return View(room);
                    }
                }
            }
        }

        //
        // GET:/Commerce/PrintableInvoice
        /// <summary>
        /// Renders the information to display a printable invoice
        /// </summary>
        /// <param name="id">Room ID</param>
        /// <returns></returns>
        [Authorize]
        public ActionResult PrintableInvoice(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();                                                                            // Get's the current logged on user name
            Profile profile = _profileRepository.GetProfileByUserName(userName);                     // get the profile, then do a test to ensure that the profile was retrieved
            if (profile == null)
            {
                TempData["ErrorMessage"] = "Sorry, an error was encountered while trying to retrieve your profile.";        // throw an error
                return View("Error");
            }
            Room room = _roomRepository.GetRoomByIdVerifyUserName(id, userName);         // get the room then test to ensure that a room was retrieved
            if (room == null)
            {
                TempData["ErrorMessage"] = "Sorry, an error was encountered while trying to retrieve your room.";       // throw an error
                return View("Error");
            }
            Order order = _orderRepository.GetOrderForRoom(id);
            if (order == null)
            {
                TempData["ErrorMessage"] = "Sorry, an error was encountered while trying to retrieve the order information for this room.";       // throw an error
                return View("Error");
            }          

            Invoice invoice = new Invoice()     // generate a new invoice
            {
                CustomerId = 10000 + profile.ProfileId,
                RoomId = room.RoomId,
                OrderNumber = "S" +(10000 + order.OrderId).ToString(),
                RoomDescription =  "Professional interior design services for a " + room.RoomTypeName.ToLower(),
                RoomName = room.Name,
                RoomCost = Convert.ToDecimal(room.RoomCost),
                TaxType = room.RoomTaxType + " " + room.RoomTaxPercent,
                TaxAmount = Convert.ToDecimal(room.RoomTaxCost),
                Discount = 0.0M,
                GrandTotal = Convert.ToDecimal(room.RoomTotalCost),
                CurrencyCode = order.Currency,
                Username = profile.UserName,
                Address = profile.Address,
                City = profile.City,
                State = profile.State,
                Zip = profile.Zip,
                Country = profile.Country,
                Name = profile.FirstName + " " + profile.LastName
            };
            
            return View(invoice);
        }
    }
}
