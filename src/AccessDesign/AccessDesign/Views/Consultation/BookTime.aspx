﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Room>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <% if (Convert.ToBoolean(TempData["FollowUpConsultation"]) == true)
       { %>
        Book Follow-up Consultation | Step 2 of 2 - Select a Date and Time?
    <% } else  { %>
	    Book Free 30 Minute Consultation | Step 2 of 2 - Select a Date and Time?
    <% } %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>

    <% if (Convert.ToBoolean(TempData["FollowUpConsultation"]) == true)
       { %>
    <h1>Book Follow-up Consultation</h1>
    <p>If you need to book or reschedule your 30 minute follow-up appointment, please follow the steps below.</p>
    <% } else { %>
    <h1>Book Free 30 Minute Consultation</h1>
    <p>If you have already booked your appoinment, please proceed to the next step: '<%: Html.ActionLink("Measuring Your Room", "Index", "RoomMeasurement", new { id = Model.RoomId }, new { })%>'.</p>
    <p>If you need to book or reschedule your appointment, please follow the steps below.</p>

    <% } %>
    <br />
    <h3>Step 2 of 2: Schedule a Date and Time</h3>
    <iframe src ="https://app.simplifyaccounts.com/Booking/Booking.aspx?coguid=46fe40c4-d22d-4d40-b24f-1862ef875b86" width="700" height="600"  allowtransparency="true" frameborder="0" scrolling="no">
  <p>Your browser does not support iframes. Please email info@digitalinteriordesign.com to book an appointment.</p>
</iframe>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarContent" runat="server">
</asp:Content>
