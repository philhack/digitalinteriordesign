﻿<%@  Language="C#"  Inherits="System.Web.Mvc.ViewUserControl<AccessDesign.Models.Consultation>" %>

    <%: Html.HiddenFor(model => model.Complete) %>
    <%: Html.HiddenFor(model => model.RoomId) %>
    <%: Html.HiddenFor(model => model.TimeZone) %>
    <%: Html.HiddenFor(model => model.Type) %>
    
    <div class="text-box-label-same-line">
        <%: Html.LabelFor(m => m.Date)%><span class="required-field">*</span>
    </div>
    <div class="text-box">
        <%: Html.TextBoxFor(m => m.Date, new { @class = "txtbox w8em format-m-d-y divider-dash highlight-days-67 range-low-today", @size = 30 })%>
        <%: Html.ValidationMessageFor(m => m.Date)%>
    </div>
    <br />
    <div class="text-box-label-same-line">
        <%: Html.LabelFor(m => m.Time)%><span class="required-field">*</span>
    </div>
    <div class="dropdown">
        <%: Html.DropDownListFor(model => model.Time, ViewData["ConsultationTime"] as SelectList, new { @class = "option" })%>
        <%: Html.ValidationMessageFor(model => model.Time)%>
    </div>
    <br />
        
    <div class="text-box-label-same-line">
        <%: Html.LabelFor(m => m.ContactMethod)%><span class="required-field">*</span>
    </div>
    <div class="dropdown">
        <%: Html.DropDownListFor(model => model.ContactMethod, ViewData["PreferedContactMethod"] as SelectList, new { @class = "option" })%>
        <%: Html.ValidationMessageFor(model => model.ContactMethod)%>
    </div>
    <br />
    <div class="text-box-label-same-line">
        <%: Html.LabelFor(m => m.SkypeId)%>
    </div>
    <div class="text-box">
        <%: Html.TextBoxFor(m => m.SkypeId, new { @class = "txtbox", @size = 30 })%>
        <%: Html.ValidationMessageFor(m => m.SkypeId)%>
    </div>
    <br />
    <div class="text-box-label-same-line">
        <%: Html.LabelFor(m => m.PhoneNumber)%><span class="required-field">*</span>
    </div>
    <div class="text-box">
        <%: Html.TextBoxFor(m => m.PhoneNumber, new { @class = "txtbox", @size = 30 })%>
        <%: Html.ValidationMessageFor(m => m.PhoneNumber)%>
    </div>