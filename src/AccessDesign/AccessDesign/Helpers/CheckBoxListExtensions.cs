﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace AccessDesign.Helpers
{
    public static class CheckBoxListExtensions
    {
        public static MvcHtmlString CheckBoxList(this HtmlHelper helper, string name, IEnumerable<KeyValuePair<string, string>> items, IEnumerable<string> selectedValues)
        {
            var checkBoxListItems = from i in items
                                    select new CheckBoxListItem
                                    {
                                        Text = i.Key,
                                        Value = i.Value,
                                        Selected = selectedValues != null && selectedValues.Contains(i.Value)
                                    };

            return CheckBoxList(helper, name, checkBoxListItems);
        }

        public static MvcHtmlString CheckBoxList(this HtmlHelper helper, string name, IEnumerable<CheckBoxListItem> checkBoxListItems)
        {
            return CheckBoxList(helper, name, checkBoxListItems, null);
        }


        public static MvcHtmlString CheckBoxList(this HtmlHelper helper, string name, IEnumerable<CheckBoxListItem> checkBoxListItems, IDictionary<string, object> htmlAttributes)
        {
            var ul = new TagBuilder("ul");

            foreach (var listItem in checkBoxListItems)
            {
                var li = new TagBuilder("li");
                var span = new TagBuilder("label");
                var id = (name + "_" + listItem.Text).Replace(".", li.IdAttributeDotReplacement);

                // Set the attributes
                var checkbox = new TagBuilder("input");
                checkbox.MergeAttribute("type", "checkbox");
                checkbox.MergeAttribute("name", name);
                checkbox.MergeAttribute("value", listItem.Value);
                checkbox.MergeAttribute("id", id);
                if (listItem.Selected)
                    checkbox.MergeAttribute("checked", "checked");

                if (htmlAttributes != null)
                    checkbox.MergeAttributes(htmlAttributes, true);

                // Render the tags
                li.InnerHtml = checkbox.ToString(TagRenderMode.SelfClosing);
                span.SetInnerText(listItem.Text);
                span.MergeAttribute("for", id);
                li.InnerHtml += span.ToString();

                // Add to the ul tag
                ul.InnerHtml += li.ToString();
            }

            // Add validation higlighting if necessary
            ModelState modelState;
            if (helper.ViewData.ModelState.TryGetValue(name, out modelState) && modelState.Errors.Count > 0)
            {
                ul.AddCssClass(HtmlHelper.ValidationInputCssClassName);
            }

            return MvcHtmlString.Create(ul.ToString());
        }
    }
}