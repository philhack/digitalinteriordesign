﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<AccessDesign.Models.Room>>" %>
    <% foreach (var item in Model) { %>
                            <% if(item.RoomId == 0)
                               { %>
                                    <li class="expand-nav"><a href="#" class="expand-link"><%: item.Name.ToUpper() %></a>
                                    <div>
                                        <ul id="subnavlist">
                                                <% if (item.MenuStepId == 1) { Html.RenderPartial("RoomMenuDisplay01", item); }
                                               else if (item.MenuStepId == 2) { Html.RenderPartial("RoomMenuDisplay02", item); }
                                               else { Html.RenderPartial("RoomMenuDisplay00", item); }%> 
                                        </ul>
                                    </div>
                            <% } else { %>
                                    <li class="expand-nav"><a href="#" class="expand-link"><%: item.Name.ToUpper() %></a>
                                    <div>
                                        <ul id="subnavlist">
                                            <% switch (item.MenuStepId)
                                           {
                                               case 1:  %>
                                                    <% Html.RenderPartial("RoomMenuDisplay01", item); %>
                                               <% break; 
                                               case 2: %>
                                                    <% Html.RenderPartial("RoomMenuDisplay02", item); %>    
                                               <% break;
                                               case 3:  %>
                                                    <% Html.RenderPartial("RoomMenuDisplay03", item); %>
                                               <%break;
                                               case 4:  %>
                                                    <% Html.RenderPartial("RoomMenuDisplay04", item); %>    
                                               <% break;
                                               case 5: %>
                                                    <% Html.RenderPartial("RoomMenuDisplay05", item); %>    
                                               <% break;
                                               case 6: %>
                                                    <% Html.RenderPartial("RoomMenuDisplay06", item); %>    
                                               <% break;
                                               case 7: %>
                                                    <% Html.RenderPartial("RoomMenuDisplay07", item); %>    
                                               <% break;
                                               case 8: %>
                                                    <% Html.RenderPartial("RoomMenuDisplay08", item); %>    
                                               <% break;
                                               case 9: %>
                                                    <% Html.RenderPartial("RoomMenuDisplay09", item); %>    
                                               <% break;
                                               case 10: %>
                                                    <% Html.RenderPartial("RoomMenuDisplay10", item); %>    
                                               <% break;
                                               case 11: %>
                                                    <% Html.RenderPartial("RoomMenuDisplay11", item); %>    
                                               <% break; %>

                                    <% } %>
                                        </ul>
                                    </div>
                        <% } %>
    <% } %>