﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<div>
        <%: Html.ActionLink("-> Upload Photos ", "Upload", "RoomPhoto", new { id=ViewData["RoomId"] }, new {}) %>
        <%: Html.ActionLink("-> View/Edit Photos ", "Details", "RoomPhoto", new { id = ViewData["RoomId"] }, new { })%>
        <%: Html.ActionLink("-> Add Comments ", "Comments", "RoomPhoto", new { id = ViewData["RoomId"] }, new { })%>
        <%: Html.ActionLink("->  Continue to furniture photos ", "Index", "Furniture", new { id = ViewData["RoomId"] }, new { })%>
</div>