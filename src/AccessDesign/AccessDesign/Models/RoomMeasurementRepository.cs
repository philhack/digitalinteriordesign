﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Mvc;
using System.Runtime.InteropServices;

using System.Data;
using System.Text;
using System.Data.Objects;
using System.Globalization;

namespace AccessDesign.Models
{
    public class RoomMeasurementRepository
    {
        private AccessDesignEntities entities = new AccessDesignEntities();

        #region GetMethods

        // 
        /// <summary>
        /// Returns all of the room measurements based on the room id
        /// </summary>
        /// <param name="id">The room id of the measurements that you wish to retreive</param>
        /// <returns></returns>
        public IQueryable<RoomMeasurement> FindAllRoomMeasurementsForSpecificRoom(int id)
        {
            return from roomMeasurements in entities.RoomMeasurements
                   where roomMeasurements.RoomId == id
                   orderby roomMeasurements.RoomMeasurementId
                   select roomMeasurements;
        }

        // 
        /// <summary>
        /// Returns a specific room measurements based on room measurement id
        /// </summary>
        /// <param name="id">The room photo id that you wish to retrieve</param>
        /// <returns></returns>
        public RoomMeasurement GetSingleRoomMeasurementByRoomMeasurementId(int id)
        {
            return entities.RoomMeasurements.FirstOrDefault(r => r.RoomMeasurementId == id);
        }

        /// <summary>
        /// Returns a signle room measurement based on the room measurement name that is passed to it
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public RoomMeasurement GetSingleRoomMeasurementByName(string name)
        {
            return entities.RoomMeasurements.FirstOrDefault(r => r.MeasurementName == name);
        }

        /// <summary>
        /// Returns the measurement comments for a particular room
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetRoomMeasurementDetails(int id)
        {
            string roomMeasurementDetails = entities.Rooms.FirstOrDefault(r => r.RoomId == id).MeasurementDetails;
            if (String.IsNullOrEmpty(roomMeasurementDetails))
                return " ";
            else
                return roomMeasurementDetails;
        }


        #endregion

        #region RoomMeasurement Create Insert and Delete Statements
        //
        // Insert
        public void Add(RoomMeasurement roomMeasurement)
        {
            entities.RoomMeasurements.AddObject(roomMeasurement);
        }


        //
        // Delete
        public void Delete(RoomMeasurement roomMeasurement)
        {
            entities.RoomMeasurements.DeleteObject(roomMeasurement);    // delete the room measurement
        }

        //
        // Persistence
        public void Save()
        {
            entities.SaveChanges();
        }


        #endregion
    }
}