﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AccessDesign.Models
{
    public class GuestRoomQuestionnaireRepository
    {
        private AccessDesignEntities entities = new AccessDesignEntities();

        // Return the guestRoom questions based on the guestRoom question id
        public GuestRoomQuestionnaire GetGuestRoomQuestionnaireByRoomId(int id)
        {
            return entities.GuestRoomQuestionnaires.FirstOrDefault(g => g.RoomId == id);
        }

        public GuestRoomQuestionnaire GetGuestRoomQuestionnaireByGuestRoomQuestionnaireId(int id)
        {
            return entities.GuestRoomQuestionnaires.FirstOrDefault(g => g.GuestRoomQuestionnaireId == id);
        }

        // Checks if the questionnaire exists based on room id, if it does, it returns true, otherwise it returns false
        public bool DoesGuestRoomQuestionnaireExist(int id)
        {
            if (entities.GuestRoomQuestionnaires.Count(g => g.RoomId == id) == 1)   // check if the guestRoom questionnaire exists
                return true;    // the questionnaire exists, so return true
            else
                return false;   // the questionnaire does not exist
        }

        /// <summary>
        /// Returns a list of gender types.  This can then be used in a drop down list for the question: How do you want your room to feel?
        /// </summary>
        /// <param name="genderType">Gender Type</param>
        /// <returns></returns>
        public static SelectList GetGenderTypeList(string genderType)
        {
            List<SelectListItem> numberList = new List<SelectListItem>() 
            {
				new SelectListItem() { Value = "", Text = "  " },
                new SelectListItem() { Value = "Masculine", Text = " Masculine " },
                new SelectListItem() { Value = "Feminine", Text = " Feminine " },
                new SelectListItem() { Value = "Neutral", Text = " Neutral " }
            };
            return new SelectList(numberList, "Value", "Text", genderType ?? "");
        }


        #region Room Create Insert and Delete Statements
        //
        // Insert
        public void Add(GuestRoomQuestionnaire guestRoomQuestionnaire)
        {
            entities.GuestRoomQuestionnaires.AddObject(guestRoomQuestionnaire);
        }


        //
        // Delete
        public void Delete(GuestRoomQuestionnaire guestRoomQuestionnaire)
        {
            entities.GuestRoomQuestionnaires.DeleteObject(guestRoomQuestionnaire);  // delete the guestRoom Questionnaire
        }

        //
        // Persistence
        public void Save()
        {
            entities.SaveChanges();
        }

        #endregion

    }
}