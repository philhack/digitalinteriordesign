﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.GeneralQuestionnaire>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	My Questionnaire
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>My Questionnaire</h1>
    <br />
    <div id="app-steps-generic">
              <a href="/Profile"><img src="/Content/Images/app-steps-profile-2-01.png" alt="" border="0" /></a><a href="/GeneralQuestionnaire"><img src="/Content/Images/app-steps-profile-2-02.png" alt="" border="0"/></a><a href="/Room/Create"><img src="/Content/Images/app-steps-profile-2-03.png" alt="" border="0"/></a>
            <h6 class="step-description">STEP 1/3 COMPLETE. PLEASE COMPLETE STEP 2 - MY QUESTIONNAIRE</h6>
    </div>
    <br /> <br />
    <p>Please fill in the questionnaire below to the best of your ability.  This questionnaire is about your personal preference and not specific to any particular room.  If you don’t have time to complete the questionnaire all at once, fill in as much information as you can, ensure that all of the required fields have information in them and click "Submit".</p>
    <br />
    <p>You can view and update this questionnaire by clicking <i>"MY QUESTIONNAIRE"</i> on the left side menu.</p>
    <br />
    <div class="text-box"><span class="required-field">*</span> Required fields.</div>
    <br /><br />

    <% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm()) {%>

            <% Html.RenderPartial("EditFields", Model); %>

            <%: Html.ValidationSummary(false, "*Please fill in all the required questions above and then click submit.")%>   
            <%: Html.ValidationSummary(false, "*Please fill in all the required questions above and then click submit.")%>   
            <br />
            <div id="standard-button">
                    <input type="submit"  class="button" value="Submit" />
            </div> 

    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">

    <script src="/Scripts/MvcFoolproofValidation.js" type="text/javascript"></script>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>