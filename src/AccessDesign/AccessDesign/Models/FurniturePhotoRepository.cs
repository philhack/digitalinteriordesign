﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Mvc;
using System.Runtime.InteropServices;

using System.Data;
using System.Text;
using System.Data.Objects;
using System.Globalization;

namespace AccessDesign.Models
{
    public class FurniturePhotoRepository
    {
        private AccessDesignEntities entities = new AccessDesignEntities();

#region GetMethods

        // 
        /// <summary>
        /// Returns all of the furniture photos based on the furniture id
        /// </summary>
        /// <param name="furnitureId">The furniture id of the photos that you wish to retreive</param>
        /// <returns></returns>
        public IQueryable<FurniturePhoto> FindAllFurniturePhotosForSpecificFurniturePiece(int furnitureId)
        {
            return from furniturePhotos in entities.FurniturePhotos
                   where furniturePhotos.FurnitureId == furnitureId
                   orderby furniturePhotos.FurniturePhotoId
                   select furniturePhotos;
        }

        // 
        /// <summary>
        /// Returns a specific furniture photo based on furniture photo id
        /// </summary>
        /// <param name="furniturePhotoId">The room photo id that you wish to retrieve</param>
        /// <returns></returns>
        public FurniturePhoto GetSingleFurniturePhotoByFurniturePhotoId(int furniturePhotoId)
        {
            return entities.FurniturePhotos.FirstOrDefault(f => f.FurniturePhotoId == furniturePhotoId);
        }



        public FurniturePhoto GetSingleFurniturePhotoByNme(string photoName)
        {
            return entities.FurniturePhotos.FirstOrDefault(f => f.PhotoName == photoName);
        }

#endregion


#region FurniturePhoto Create Insert and Delete Statements
        //
        // Insert
        public void Add(FurniturePhoto furniturePhoto)
        {
            entities.FurniturePhotos.AddObject(furniturePhoto);
        }


        //
        // Delete
        public void Delete(FurniturePhoto furniturePhoto)
        {
            entities.FurniturePhotos.DeleteObject(furniturePhoto);    // delete the roomPhoto
        }

        //
        // Persistence
        public void Save()
        {
            entities.SaveChanges();
        }

#endregion
    }
}