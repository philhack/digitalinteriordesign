﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessDesign.Models;

namespace AccessDesign.Controllers
{
    public class DiningRoomQuestionnaireController : Controller
    {
        RoomRepository roomRepository = new RoomRepository();
        DiningRoomQuestionnaireRepository diningRoomQuestionnaireRepository = new DiningRoomQuestionnaireRepository();

        //
        // GET: /DiningRoomQuestionnaire/id
        /// <summary>
        /// Returns a view containing the DiningRoomQuestionnaire that's associated with the current room id. If a questionnaire exists, then it allows the user to edit it.
        /// If a questionnaire does not exist, then it redirects the user to create a new questionnaire.  If the room doesn't belong to the currently logged on user, it
        /// will throw an error.
        /// </summary>
        /// <param name="id">RoomId</param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Index(int id)
        {

            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // ensure that the currently logged on user owns this room before displaying it              
            if (room == null)
            {
                TempData["ErrorMessage"] = "This room does not exist.  If you feel this is an error, please contact us.";
                return View("Error");
            }
            else if (room.RoomTypeName.Equals("Dining Room"))      // if the room type is a diningRoom
            {
                if (diningRoomQuestionnaireRepository.DoesDiningRoomQuestionnaireExist(room.RoomId))
                {
                    return RedirectToAction("Edit", "DiningRoomQuestionnaire", new { id = room.RoomId });              // if a diningRoom questionnaire already exists for this room, then redirect them to the edit page
                }
                else
                {
                    return RedirectToAction("Create", "DiningRoomQuestionnaire", new { id = room.RoomId });         // otherwise check the type of room and if's a "diningRoom" type, then generate a new questionnaire
                }
            }
            else
            {
                TempData["ErrorMessage"] = "The room you have requested is not a dining room";
                return View("Error");
            }
        }

        //
        // GET: /DiningRoomQuestionnaire/Details/5
        /// <summary>
        /// Display the details of the questionnaire to the designer
        /// </summary>
        /// <param name="id">room id</param>
        /// <returns></returns>
        [Authorize(Roles = "Designers")]
        public ActionResult Details(int id)
        {
            Room room = roomRepository.GetRoomById(id);     // get the current room so that we can verify the room type
            if (room == null)
            {
                TempData["ErrorMessage"] = "This room does not exist. ";
                return View("Error");
            }
            else if (room.RoomTypeName.Equals("Dining Room"))      // if the room type is a diningRoom
            {
                DiningRoomQuestionnaire diningRoomQuestionnaire = diningRoomQuestionnaireRepository.GetDiningRoomQuestionnaireByRoomId(room.RoomId);
                if (diningRoomQuestionnaire == null)
                {
                    // the questionnaire does not exist.  Throw an error
                    TempData["ErrorMessage"] = "A dining room questionnaire does not exist for this room. ";
                    return View("Error");
                }
                else
                {
                    return View(diningRoomQuestionnaire);
                }
            }
            else
            {
                TempData["ErrorMessage"] = "This room does not exist or it is not a dining room. ";
                return View("Error");
            }
        }

        //
        // GET: /DiningRoomQuestionnaire/Create/5
        /// <summary>
        /// Client creates a diningRoom questionnaire
        /// </summary>
        /// <param name="id">room id</param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Create(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // ensure that the currently logged on user owns this room before displaying it              
            if (room == null)
            {
                TempData["ErrorMessage"] = "This room does not exist.  If you feel this is an error, please contact us.";
                return View("Error");
            }
            else if (room.RoomTypeName.Equals("Dining Room"))      // if the room type is a diningRoom
            {
                if (diningRoomQuestionnaireRepository.DoesDiningRoomQuestionnaireExist(room.RoomId))        // if a diningRoom questionnaire already exists, throw an error message.
                {
                    TempData["ErrorMessage"] = "Sorry, a questionnaire already exists for this room";
                    return View("Error");
                }
                else
                {
                    // otherwise check the type of room and if's a "diningRoom" type, then generate a new questionnaire
                    DiningRoomQuestionnaire diningRoomQuestionnaire = new DiningRoomQuestionnaire()
                    {
                        RoomId = room.RoomId
                    };

                    ViewData["GenderTypeList"] = DiningRoomQuestionnaireRepository.GetGenderTypeList("");		// generate the drop down list for the gender types
                    return View(diningRoomQuestionnaire);
                }
            }
            else
            {
                TempData["ErrorMessage"] = "The room you have requested is not a dining room";
                return View("Error");
            }
        }

        //
        // POST: /DiningRoomQuestionnaire/Create

        [HttpPost, Authorize]
        public ActionResult Create(DiningRoomQuestionnaire diningRoomQuestionnaire)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            //  check to ensure the currenly logged on user owns this room.  we're verifying this since the form could be tampered with then posted back
            // if the modelstate is valid, create a diningRoom questionnaire

            int roomId = diningRoomQuestionnaire.RoomId;      // get the room id for the current questionnaire that is being submitted

            if (ModelState.IsValid)
            {
                if (roomRepository.DoesRoomExist(roomId, userName))
                {
                    Room room = roomRepository.GetRoomById(roomId);     // Get the room by the roomId.
                    room.MenuStepId = 4;        // set the menu step number id to 4 since the room specific questionnaire is now completed
                    room.CurrentStepId = 2;     // set the user's current step id to 2 since the room specific questionnaire is now completed
                    room.DateUpdated = DateTime.Now;      // set the date updated so that this can be used on the left side room navigation menu to order the rooms
                    if (TryUpdateModel(room))
                    {
                        diningRoomQuestionnaireRepository.Add(diningRoomQuestionnaire);        // add the new questionnaire
                        diningRoomQuestionnaireRepository.Save();                                                // save the new questionnaire to the db
                        roomRepository.Save();      // save the updated room values to the DB
                        TempData["SuccessMessage"] = "Your questionnaire is complete. Please proceed with uploading photos of your room.";
                        return RedirectToAction("Upload", "RoomPhoto", new { id = roomId });        // redirect the user to the room photo upload section of the website.
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "Sorry, an unknown error occured while creating your room's questionnaire.";
                    return View(diningRoomQuestionnaire);
                }
            }
            TempData["ErrorMessage"] = "Please fill in the required fields below.";
            return View(diningRoomQuestionnaire);
        }

        //
        // GET: /DiningRoomQuestionnaire/Edit/5
        /// <summary>
        ///  Client edit's their diningRoom questionnaire
        /// </summary>
        /// <param name="id">room id</param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // ensure that the currently logged on user owns this room before displaying it              
            if (room == null)
            {
                TempData["ErrorMessage"] = "This room does not exist.  If you feel this is an error, please contact us.";
                return View("Error");
            }
            else if (room.RoomTypeName.Equals("Dining Room"))      // if the room type is a diningRoom
            {
                // if a diningRoom questionnaire already exists for this room, then allow the user to edit the questionnaire
                if (diningRoomQuestionnaireRepository.DoesDiningRoomQuestionnaireExist(room.RoomId))
                {
                    // the id has passed through all of the sanity checks.  It's legit.  Allow the user to edit the questionnaire
                    DiningRoomQuestionnaire diningRoomQuestionnaire = diningRoomQuestionnaireRepository.GetDiningRoomQuestionnaireByRoomId(room.RoomId);    // get the diningRoom questionnaire from the DB
                    ViewData["CurrentMenuStepId"] = room.MenuStepId;       // used for the navigation inside the view;
                    ViewData["GenderTypeList"] = DiningRoomQuestionnaireRepository.GetGenderTypeList(diningRoomQuestionnaire.HowDoYouWantYourRoomToFeelGender);		// // generate the drop down list for the gender types
                    return View(diningRoomQuestionnaire);
                }
                else
                {
                    TempData["ErrorMessage"] = "Sorry, a questionnaire does not exist for this room";
                    return View("Error");
                }
            }
            else
            {
                TempData["ErrorMessage"] = "The room you have requested is not a DiningRoom";
                return View("Error");
            }
        }

        //
        // POST: /DiningRoomQuestionnaire/Edit/5
        [HttpPost, Authorize]
        public ActionResult Edit(int id, FormCollection collection)
        //public ActionResult Edit(DiningRoomQuestionnaire diningRoomQuestionnaire)
        {
            DiningRoomQuestionnaire diningRoomQuestionnaire = diningRoomQuestionnaireRepository.GetDiningRoomQuestionnaireByRoomId(id);
            if (ModelState.IsValid)
            {
                if (TryUpdateModel(diningRoomQuestionnaire))
                {
                    diningRoomQuestionnaireRepository.Save();
                    TempData["SuccessMessage"] = "Your questionnaire has been successfully updated.";       // set the status message
                    return RedirectToAction("Edit", new { id = diningRoomQuestionnaire.RoomId });
                }
            }
            TempData["ErrorMessage"] = "Sorry, there was an error updating your questionnaire.";
            return View(diningRoomQuestionnaire);
        }
    }
}