﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AccessDesign.Models.KitchenQuestionnaire>" %>

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.DesignStyleOfRoom) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.ValidationMessageFor(model => model.DesignStyleOfRoom) %>
                <%: Html.RadioButtonFor(model=>model.DesignStyleOfRoom,"Modern") %>Modern <br />
                <%: Html.RadioButtonFor(model=>model.DesignStyleOfRoom,"Traditional") %>Traditional <br />
                <%: Html.RadioButtonFor(model=>model.DesignStyleOfRoom,"Country Cottage") %>Country Cottage <br />
                <%: Html.RadioButtonFor(model=>model.DesignStyleOfRoom,"Eclectic") %>Eclectic <br />
                <%: Html.RadioButtonFor(model=>model.DesignStyleOfRoom,"Mid-century Modern") %>Mid-century Modern <br />
                <%: Html.RadioButtonFor(model=>model.DesignStyleOfRoom,"Glam") %>Glam <br />
                <%: Html.RadioButtonFor(model=>model.DesignStyleOfRoom,"Shabby Chic") %>Shabby Chic <br />
                <%: Html.RadioButtonFor(model=>model.DesignStyleOfRoom,"Other") %>Other - Please specifiy:  <%: Html.TextBoxFor(model => model.DesignStyleOfRoomOther, new { @class = "txtbox", @size = 50 })%>
                <%: Html.ValidationMessageFor(model => model.DesignStyleOfRoomOther)%>
            </div>

            <br /> 
                     
            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.SpecificColorsYouWantToUse) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.SpecificColorsYouWantToUse, "Yes")%>Yes  <br />
                <%: Html.RadioButtonFor(model => model.SpecificColorsYouWantToUse, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.SpecificColorsYouWantToUse) %>
            </div>

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.SpecificColorsYouWantToUseYes)%>
            </div>
            <div class="text-area">
                <%: Html.TextAreaFor(model => model.SpecificColorsYouWantToUseYes, new { @class = "txtarea", @rows = 6, @cols = 80 })%>
                <%: Html.ValidationMessageFor(model => model.SpecificColorsYouWantToUseYes)%>
            </div>

            <br />
            
            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.HowDoYouWantRoomToFeel)%>
            </div>
            <div class="text-area">
                <%: Html.TextAreaFor(model => model.HowDoYouWantRoomToFeel, new { @class = "txtarea", @rows = 6, @cols = 80 })%>
                <%: Html.ValidationMessageFor(model => model.HowDoYouWantRoomToFeel)%>
            </div>

            <br />

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.FurnitureAccessoriesStayingInRoom) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.FurnitureAccessoriesStayingInRoom, "Yes")%>Yes <br />
                <%: Html.RadioButtonFor(model => model.FurnitureAccessoriesStayingInRoom, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.FurnitureAccessoriesStayingInRoom) %>
            </div>
            
            <br />

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.YourBudget) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.YourBudget, new { @class= "txtbox", @size = 70 })%>
                <%: Html.ValidationMessageFor(model => model.YourBudget) %>
            </div>
            
            <br />

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.MustHavesItemsInRoomNotAlreadyOwned) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.MustHavesItemsInRoomNotAlreadyOwned, "Yes")%>Yes <br />
                <%: Html.RadioButtonFor(model => model.MustHavesItemsInRoomNotAlreadyOwned, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.MustHavesItemsInRoomNotAlreadyOwned) %>
            </div>
            
            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.MustHavesItemsInRoomNotAlreadyOwnedYes) %>
            </div>
             <div class="text-area">
                <%: Html.TextAreaFor(model => model.MustHavesItemsInRoomNotAlreadyOwnedYes, new { @class = "txtarea", @rows = 6, @cols = 80 })%>
                <%: Html.ValidationMessageFor(model => model.MustHavesItemsInRoomNotAlreadyOwnedYes) %>
            </div>
            
            <br />

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.NumberOfCooks) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.NumberOfCooks, new { @class= "txtbox", @size = 70 })%>
                <%: Html.ValidationMessageFor(model => model.NumberOfCooks) %>
            </div>

            <br />

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.DoYouCookAlot) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.DoYouCookAlot, "Yes")%>Yes <br />
                <%: Html.RadioButtonFor(model => model.DoYouCookAlot, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.DoYouCookAlot) %>
            </div>

            <br />
            
            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.TypeOfOven)%><span class="required-field">*</span>
            </div>
            <div class="dropdown-questionnaire">
                <%: Html.DropDownListFor(model => model.TypeOfOven, ViewData["OvenTypeList"] as SelectList, new { @class = "option" })%>
                <%: Html.ValidationMessageFor(model => model.TypeOfOven)%>
            </div>

            <br /> 
                     
            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.OtherActivitiesInKitchen) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.OtherActivitiesInKitchen, "Yes")%>Yes  <br />
                <%: Html.RadioButtonFor(model => model.OtherActivitiesInKitchen, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.OtherActivitiesInKitchen) %>
            </div>

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.OtherActivitiesInKitchenYes)%>
            </div>
            <div class="text-area">
                <%: Html.TextAreaFor(model => model.OtherActivitiesInKitchenYes, new { @class = "txtarea", @rows = 6, @cols = 80 })%>
                <%: Html.ValidationMessageFor(model => model.OtherActivitiesInKitchenYes)%>
            </div>

            <br />

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.MainEatingRoom)%><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.MainEatingRoom, "Yes")%>Yes <br />
                <%: Html.RadioButtonFor(model => model.MainEatingRoom, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.MainEatingRoom)%>
            </div>

            <br />

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.EntertainFrequently)%><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.EntertainFrequently, "Yes")%>Yes <br />
                <%: Html.RadioButtonFor(model => model.EntertainFrequently, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.EntertainFrequently)%>
            </div>
            
            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.HowDoYouWantYourRoomToFeel)%>
            </div>
            <div class="text-area">
                <%: Html.TextAreaFor(model => model.HowDoYouWantYourRoomToFeel, new { @class = "txtarea", @rows = 6, @cols = 80 })%>
                <%: Html.ValidationMessageFor(model => model.HowDoYouWantYourRoomToFeel)%>
            </div>
			
			<div class="text-box-label-above">
                <%: Html.LabelFor(model => model.DoYouLikeContrast) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.DoYouLikeContrast, "Yes")%>Yes <br />
                <%: Html.RadioButtonFor(model => model.DoYouLikeContrast, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.DoYouLikeContrast) %>
            </div>		
			
			<div class="text-box-label-above">
                <%: Html.LabelFor(model => model.DoYouLikeTextures) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.DoYouLikeTextures, "Yes")%>Yes <br />
                <%: Html.RadioButtonFor(model => model.DoYouLikeTextures, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.DoYouLikeTextures) %>
            </div>

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.HowDoYouWantYourRoomToFeelGender)%><span class="required-field">*</span>
            </div>
            <div class="dropdown-questionnaire">
                <%: Html.DropDownListFor(model => model.HowDoYouWantYourRoomToFeelGender, ViewData["GenderTypeList"] as SelectList, new { @class = "option" })%>
                <%: Html.ValidationMessageFor(model => model.HowDoYouWantYourRoomToFeelGender)%>
            </div>

			<div class="text-box-label-above">
                <%: Html.LabelFor(model => model.AdditionalImportantInformation)%>
            </div>
            <div class="text-area">
                <%: Html.TextAreaFor(model => model.AdditionalImportantInformation, new { @class = "txtarea", @rows = 6, @cols = 80 })%>
                <%: Html.ValidationMessageFor(model => model.AdditionalImportantInformation)%>
            </div>