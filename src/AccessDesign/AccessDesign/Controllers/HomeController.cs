﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessDesign.Models;

namespace AccessDesign.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            // samples of the error message
            //TempData["ErrorMessage"] = "test error message.";
            //TempData["SuccessMessage"] = "test success message";
            //TempData["WarningMessage"] = "Test warning message";
            //TempData["InformationMessage"] = "Test information message";
            
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        /// <summary>
        /// View that is presented to the user after they have logged on, filled out their profile, and filled out their general questionnaire
        /// </summary>
        /// <returns></returns>
        [Authorize, HttpGet]
        public ActionResult LoggedOn()
        {
            return View();
        }
    }
}
