﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<AccessDesign.Models.FurniturePhoto>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Furniture Details: <%: ViewData["FurnitureName"]%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <% Html.EnableClientValidation(); %>
    <div class="furniture-delete-button-left-text"><h1>Furniture: <%: ViewData["FurnitureName"]%></h1></div><div class="furniture-delete-button"><a href="/Furniture/Delete/<%: ViewData["Furniture"] %>"><img src="/Content/Images/delete-button.png" alt="Delete this piece of furniture" /></a></div>
    <div id="room-photo-nav">
        <a href="/Furniture/Create/<%: ViewData["RoomId"]  %>"><img src="/Content/Images/Furniture-Navigation-New.png" alt="" border="0" class="room-photo-nav-image" /></a><a href="/Furniture/Edit/<%: ViewData["Furniture"] %>"><img src="/Content/Images/Furniture-Navigation-Edit.png" alt="" border="0" class="room-photo-nav-image"/></a><a href="/FurniturePhoto/Upload/<%: ViewData["Furniture"] %>"><img src="/Content/Images/Furniture-Navigation-Upload.png" alt="" border="0" class="room-photo-nav-image"/></a>
     </div>
     <br />
     <p></p>
     <br />
    <div><p>Below are the details of your piece of furniture.</p></div>
    <br />
    <div class="room-photo-gallery">
        <div class="furniture-left-label-same-line"><label>Furniture Name:</label></div><div class="furniture-right-label-same-line"><%: ViewData["FurnitureName"] %></div>
        <div class="furniture-left-label-same-line"><label>Width:</label></div><div class="furniture-right-label-same-line"><%: ViewData["FurnitureWidth"]%> &quot;</div>
        <div class="furniture-left-label-same-line"><label>Depth:</label></div><div class="furniture-right-label-same-line"><%: ViewData["FurnitureDepth"]%> &quot;</div>
        <div class="furniture-left-label-same-line"><label>Height:</label></div><div class="furniture-right-label-same-line"><%: ViewData["FurnitureHeight"]%> &quot;</div>
        <div class="furniture-left-label-same-line"><label>Comments:</label></div><div class="furniture-right-label-same-line"><%: ViewData["FurnitureComments"]%></div>
    </div>
    <br /><br />
      <div class="room-photo-gallery">
        <h2>Photos of <%: ViewData["FurnitureName"]%></h2>
        <p>Click the picture to view a larger version. To delete a picture of your room, just click the delete button.</p>
        <br />
        <div id="gallery">
        <% foreach (var item in Model) { %>
                <div class="gallery">
                                <div class="gallery-item"><a href="<%: item.WebUrlPath %>" title="<%: item.PhotoName %>. " class="lightbox"><img src="<%: item.ThumbnailUrlPath %>" width="100" height="100" alt="<%: item.PhotoName %>" border="0" /></a></div>
                                <div class="gallery-delete-button"><%: Html.ActionLink("Delete", "Delete", new { id=item.FurniturePhotoId })%></div>
                </div>
        <% } %>
        </div>
    </div>

    

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">

    <script type="text/javascript" src="/Scripts/jquery.lightbox-0.5.js"></script>
    <link rel="stylesheet" type="text/css" href="/Content/Gallery.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/Content/jquery.lightbox-0.5.css" media="screen" />
    
    <script type="text/javascript">
        $(function () {
            $('a.lightbox').lightBox // Select all links with lightbox class
            ({
                imageBlank: '/Content/Images/LightBox/lightbox-blank.gif',
                imageLoading: '/Content/Images/LightBox/lightbox-ico-loading.gif',
                imageBtnClose: '/Content/Images/LightBox/lightbox-btn-close.gif',
                imageBtnPrev: '/Content/Images/LightBox/lightbox-btn-prev.gif',
                imageBtnNext: '/Content/Images/LightBox/lightbox-btn-next.gif'
            });

            $(".room-photo-nav-image")
            .mouseover(function () {
                var src = $(this).attr("src").match(/[^\.]+/) + "TreomaOn.png";
                $(this).attr("src", src);
            })
            .mouseout(function () {
                var src = $(this).attr("src").replace("TreomaOn", "");
                $(this).attr("src", src);
            });

        });
    </script>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarContent" runat="server">
</asp:Content>


