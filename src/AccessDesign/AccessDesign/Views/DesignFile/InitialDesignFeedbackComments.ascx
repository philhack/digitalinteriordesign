﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AccessDesign.Models.DesignFile>" %>
<%@ Import Namespace="AccessDesign.Helpers" %>
<%@ Import Namespace="AccessDesign.Models" %>
<%@ Import Namespace="System.Collections.Generic" %>

   <% using(Html.BeginCollectionItem("designFiles")) { %>  

            <%: Html.HiddenFor(d => d.DesignFileId) %>
            <%: Html.TextAreaFor(d => d.Comments, new { @rows = 10, @cols = 72 })%>
              <%: Html.ValidationMessageFor(d => d.Comments) %>

   <% } %>



