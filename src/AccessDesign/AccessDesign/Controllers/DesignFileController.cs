﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessDesign.Models;
using System.Web.Hosting;
using System.IO;
using System.Web.Security;
//using com.mosso.cloudfiles;
//using com.mosso.cloudfiles.domain;
using System.Text;

namespace AccessDesign.Controllers
{
    public class DesignFileController : Controller
    {
        DesignFileRepository designFileRepository = new DesignFileRepository();
        RoomRepository roomRepository = new RoomRepository();
        ProfileRepository profileRepository = new ProfileRepository();

        private static readonly string UploadPath = System.Configuration.ConfigurationManager.AppSettings["uploadPath"];
        private static readonly string UploadPathForUrl = System.Configuration.ConfigurationManager.AppSettings["uploadPathForUrl"];
        private static readonly string RoomDesignFileDirectory = System.Configuration.ConfigurationManager.AppSettings["roomDesignFileDirectory"];
        private static readonly string RoomDesignFileRSUrl = System.Configuration.ConfigurationManager.AppSettings["roomDesignFileRSUrl"];


        #region Designers Section
        //
        // GET: /DesignFile/5
        [Authorize(Roles = "Designers")]
        public ActionResult Index(int id)
        {
            ViewData["RoomId"] = id;
            IEnumerable<DesignFile> designFiles = designFileRepository.FindAllDesignFiles(id);
            return View(designFiles);
        }

        [HttpGet, Authorize]
        public ActionResult Download(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();        // get the username of the currently logged on user
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // get the room
            if (room == null)                // If the room does not exist, return an error
            {
                TempData["ErrorMessage"] = "Sorry, an error occured while attempting to retrieve your room.";
                return View("Error");
            }
            else
            {
                if (room.MenuStepId < 9)    // the user has not completed step 8 (measure you room)
                {
                    TempData["ErrorMessage"] = "Please measure your room by following the steps below.";
                    return RedirectToAction("Index", "RoomMeasurement", new { id = id });      // pass the user back to the room measurement section so they can measure their room
                }
                else if (room.MenuStepId >= 10)     // the design files have been uploaded by the designer.  The user has been notified. Let the user download the design files
                {
                    IEnumerable<DesignFile> designFiles = designFileRepository.FindAllDesignFilesWithTypes(id);      // get all of the design files for this room.
                    return View(designFiles);
                }
                else          // the design files have not been uploaded yet by the designer.  Display a differemt view
                {
                    return RedirectToAction("DesignNotReady", "DesignFIle", new { id = id });      // pass the user back to the room measurement section so they can measure their room
                }
            }
        }

        [HttpGet, Authorize]
        public ActionResult DesignNotReady(int id)
        {
            return View();
        }


        //
        // GET: /DesignFile/Create/5
        /// <summary>
        /// Create a new design file for a specific room
        /// </summary>
        /// <param name="id">room id</param>
        /// <returns></returns>
        [Authorize(Roles = "Designers")]
        public ActionResult Create(int id)
        {
            DesignFile designFile = new DesignFile()        // create a new design file
            {
                RoomId = id
            };

            ViewData["DesignFileTypes"] = designFileRepository.GetDesignFileTypeList(1);     // generate a drop down list box with all of the design file types

            return View(designFile);
        }


        //
        // POST: /DesignFile/Create
        [HttpPost, Authorize(Roles = "Designers")]
        public ActionResult Create(DesignFile designFile)
        {
            if (ModelState.IsValid)
            {
                if (designFileRepository.DoesFileTypeAlreadyExistForRoom(designFile.RoomId, designFile.DesignFileTypeId))
                {
                    TempData["ErrorMessage"] = "Sorry, a file of this type already exists for this room. You must delete this file before you can upload a new file of the same type.";
                    return RedirectToAction("Create", new { id = designFile.RoomId });
                }
                HttpPostedFileBase fileData = Request.Files["UrlPath"];     // UrlPath is just a temporary area in the model so that we can receive the file

                Sanitizer sanitize = new Sanitizer();   // create a new instance of the sanitizer
                if (!sanitize.IsValidDesignerFile(fileData.FileName))   // check if the file name contains a valid file extension.  We are checking this again incase someone tampered with the javascript.
                {
                    TempData["ErrorMessage"] = "Error uploading file: The file must end in .jpg, .jpeg, .pdf, .docx, .doc.";
                    return RedirectToAction("Create", new { id = designFile.RoomId });
                }

                string sanitizedFileName = Sanitizer.CreateGuidRoomPhoto(fileData.FileName.ToString(), designFile.RoomId);       // generate the file name "RoomId-GUID.ext"
                string serverDesignFilePath = @Server.MapPath(UploadPath + "\\");      // used as a basis for where to temporarily save the files to

                designFile.ServerPath = UploadPath + "\\" + RoomDesignFileDirectory + "\\" + sanitizedFileName;      // mapped to the temporary server path
                designFile.UrlPath = UploadPathForUrl + RoomDesignFileDirectory + "/" + sanitizedFileName;           // mapped to the teporary url path (incase we decide to ditch rackspace in the future)
                designFile.RsContainer = RoomDesignFileDirectory;                                                                         // rack space's container that houses the design files
                designFile.RsUrl = RoomDesignFileRSUrl + sanitizedFileName;                                                                // rack space url to the container the houses the files
                designFile.FileName = sanitizedFileName;

                // Save the file to disk so that we can upload it to rackspace
                try
                {
                    fileData.SaveAs(@Server.MapPath(designFile.ServerPath));        // save the file to the local disk
                    /* DISABLED RACKSPACE FILES FOR NOW  - if we want to use this, simply just uncomment this line
                    UserCredentials userCreds = new UserCredentials(serverSettingsConfig.RackSpaceUserName, serverSettingsConfig.RackSpaceAPIKey);      // upload the file to the rack space cloud
                    Connection connection = new com.mosso.cloudfiles.Connection(userCreds);
                    connection.PutStorageItem(serverSettingsConfig.RoomDesignFileDirectory, @Server.MapPath(designFile.ServerPath));                 // Container name = "rdf"
                    System.IO.File.Delete(@Server.MapPath(designFile.ServerPath));          // delete the design file that were temporarily stored on the server
                     */ 

                }
                catch (Exception)
                {
                    TempData["ErrorMessage"] = "Sorry, an errror occured while we attempting to upload your file.";
                    return RedirectToAction("Create", new { id = designFile.RoomId });
                }

                try
                {
                    designFileRepository.Add(designFile);
                    designFileRepository.Save();
                }
                catch (Exception)
                {
                    TempData["ErrorMessage"] = "Sorry, an errror occured while we attempting to upload your file.";
                    return RedirectToAction("Create", new { id = designFile.RoomId });
                }

            }
            ViewData["DesignFileTypes"] = designFileRepository.GetDesignFileTypeList(designFile.DesignFileTypeId);     // generate a drop down list box with all of the design file types
            TempData["SuccessMessage"] = "Your file has been successfully uploaded.";
            return View("Create", designFile);
        }


        //
        // GET: /DesignFile/Delete/5
        [Authorize(Roles = "Designers")]
        public ActionResult Delete(int id)
        {
            DesignFile designFile = designFileRepository.GetDesignFileById(id);     // get the design file
            ViewData["RoomId"] = designFile.RoomId;                 // used in the view.
            if (designFile == null)
            {
                TempData["ErrorMessage"] = "Sorry, the design file was not found in the database";
                return View("Error");
            }
            else
                return View(designFile);
        }


        //
        // POST: /DesignFile/Delete/5
        [HttpPost, Authorize(Roles = "Designers")]
        public ActionResult Delete(int id, FormCollection collection)
        {
            DesignFile designFile = designFileRepository.GetDesignFileById(id);     // get the design file
            int roomId = 0;         // used to temporarily hold the room id.  Initialize to 0;

            if (designFile == null)
            {
                TempData["ErrorMessage"] = "Sorry, the design file was not found in the database";
                return View("Error");
            }
            else
            {
                // Get the room id so that it can be used in the view
                roomId = designFile.RoomId;     // set the room id of the current room

                // delete the file from Rack Space Files
                try
                {
                    System.IO.File.Delete(@Server.MapPath(designFile.ServerPath));
                    /* DECIDED TO NOT USE RACKSPACE FILES RIGHT NOW  -  if we want to use this, just uncomment these lines, comment out the System.IO.File.Delete line above, and uncomment the 2 rackspace include directives at the top of this file
                    UserCredentials userCreds = new UserCredentials(serverSettingsConfig.RackSpaceUserName, serverSettingsConfig.RackSpaceAPIKey);      // upload the file to the rack space cloud
                    Connection connection = new com.mosso.cloudfiles.Connection(userCreds);
                    connection.DeleteStorageItem(designFile.RsContainer, designFile.FileName);            // File to delete on rackspace files side
                     */ 
                }
                catch (Exception)
                {
                    // TODO: Don't pass this error back, enable logging instead
                    TempData["ErrorMessage"] = "Sorry, we were unable to delete the design file.";
                    return View("Error");
                }


                // delete the room from the database
                try
                {
                    designFileRepository.Delete(designFile);        // delete the design file from the db
                    designFileRepository.Save();                    // save the db
                    IEnumerable<DesignFile> designFiles = designFileRepository.FindAllDesignFiles(roomId);      // get all of the design files for this room. This will be passed back to the index view.
                    TempData["SuccessMessage"] = "Your design file has been deleted.";
                    ViewData["RoomId"] = roomId;                    // set the viewdata for the room id since this will be used in the index view
                    return View("Index", designFiles);              // display the index view so that the user can see all of their design files with the success message that the file has been deleted.
                }
                catch (Exception)
                {
                    //TODO:  Log error
                    TempData["ErrorMessage"] = "An error occured while attempting to delete your design file from the database.";
                    return View("Error");
                }
            }
            // ******************************
        }

        #endregion

        #region Notify Users

        

        /// <summary>
        /// Used by the designer once they have completed the first final design.
        /// This action will mark the design as complete and notify the customer via email that their first design is complete and to login and take a look.
        /// </summary>
        /// <param name="id">Room ID</param>
        /// <param name="phase">The phase the the design is in.  This can be either "final" or "revision" or "shipped"</param>
        /// <returns></returns>
        [HttpGet, Authorize(Roles = "Designers")]
        public ActionResult DesignComplete(int id, string phase)
        {
            string tollFreePhoneNumber = System.Configuration.ConfigurationManager.AppSettings["tollFreePhoneNumber"];
            String emailSubject = null;     // stores the subject of the email
            Room room = roomRepository.GetRoomById(id);     // get the room
            Profile profile = profileRepository.GetProfileByUserName(room.UserName);    // get the profile for the user
            if (String.IsNullOrEmpty(phase))
            {
                TempData["ErrorMessage"] = "Phase is a required value, which can be either 'final' or 'revision'.";
                return View("Error");
            }
            if (room.MenuStepId < 9)
            {
                TempData["ErrorMessage"] = "This customer has not yet finished the measure your room step.  You can not send them the design files until that step is complete.";
                return View("Error");
            }
            else if (room.MenuStepId == 9 && phase == "final")   // if the menu step is "Measure your room", then set the menu step to be "Final Documents"
            {
                room.MenuStepId = 10;    // set the step to be final documents
                // TODO: Email the customer advising them that the final documents are ready
            }
            else if (phase == "revision")   // if the menu step is "Measure your room", then set the menu step to be "Final Documents"
            {
                // TODO: Email the customer advising them that the revised documents are ready
            }
            if (phase == "shipped")
            {
                room.CurrentDesignStepId = 6;   // the design files have been shipped, so set this step to 6 (design kit shipped)
                // TODO: Email the customer advising them that the physical design package has shipped
            }

            try
            {
                room.DateUpdated = DateTime.Now;
                roomRepository.Save();      // persist this info to the DB.
            }
            catch(Exception)
            {
                TempData["ErrorMessage"] = "Unable to update the menu step Id. The customer has NOT been notified";
                return View("Error");
            }
            
            // notify the customer via email that their design is complete.
            try
            {
                // Create an email message that contains the new password
                StringBuilder emailBody = new StringBuilder();

                if (phase == "final")
                {
                    emailSubject = "Your Design Files are Ready";
                    emailBody.Append("Dear " + profile.FirstName + ",<br/><br/>");
                    emailBody.Append("Your room design has been completed. Please click <a href=\"https://app.digitalinteriordesign.com\">here</a> to login your account and view your design files.");
                    emailBody.Append("Your design files can be viewed at any time by loging into your account, clicking on your room name on the left side of the screen, and then clicking 'Final Design Files'.");
                    emailBody.Append("<br /><br />Sincerly,<br />");
                    emailBody.Append("Digital Interior Design<br />");
                    emailBody.Append(tollFreePhoneNumber + "<br />");
                    emailBody.Append("<a href=\"mailto:info@digitalinteriordesign.com\">info@digitalinteriordesign.com</a>");
                    emailBody.Append("<a href=\"http://www.digitalinteriordesign.com\">http://www.digitalinteriordesign.com</a>");
                }
                else if (phase == "shipped")
                {
                    emailSubject = "Your Design Package has been shipped";
                    emailBody.Append("Dear " + profile.FirstName + ",<br/><br/>");
                    emailBody.Append("Your room design package has been shipped to you and should arrive shortly.");
                    emailBody.Append("Please remember that your design files and receipt can be viewed at any time by loging into your account.");
                    emailBody.Append("<br /><br />Sincerly,<br />");
                    emailBody.Append("Digital Interior Design<br />");
                    emailBody.Append(tollFreePhoneNumber + "<br />");
                    emailBody.Append("<a href=\"mailto:info@digitalinteriordesign.com\">info@digitalinteriordesign.com</a>");
                    emailBody.Append("<a href=\"http://www.digitalinteriordesign.com\">http://www.digitalinteriordesign.com</a>");
                }
                else
                {
                    emailSubject = "Your Revised Design Files are Ready";
                    emailBody.Append("Dear " + profile.FirstName + ",<br/><br/>");
                    emailBody.Append("Your room design revision has been completed. Please click <a href=\"https://app.digitalinteriordesign.com\">here</a> to login your account and view your revised design files.");
                    emailBody.Append("Your design files can be viewed at any time by loging into your account, clicking on your room name on the left side of the screen, and then clicking 'Final Design Files'.");
                    emailBody.Append("<br /><br />Sincerly,<br />");
                    emailBody.Append("Digital Interior Design<br />");
                    emailBody.Append(tollFreePhoneNumber + "<br />");
                    emailBody.Append("<a href=\"mailto:info@digitalinteriordesign.com\">info@digitalinteriordesign.com</a>");
                    emailBody.Append("<a href=\"http://www.digitalinteriordesign.com\">http://www.digitalinteriordesign.com</a>");
                }

                SendEmail emailMessage = new SendEmail();   // Create a new Send Email instance
                emailMessage.SendSingleEmail(emailBody, System.Configuration.ConfigurationManager.AppSettings["fromEmail"], System.Configuration.ConfigurationManager.AppSettings["fromEmail"], profile.UserName, emailSubject);
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Unable to send email. Error" + ex.Message.ToString();
                return View("Error");
                //TODO: Perform some logging here.  We don't want to display an error to the customer, but would like to silently log this.
            }

            if (phase == "final")
            {
                TempData["SuccessMessage"] = "The client, " + profile.FirstName + " " + profile.LastName + " has been notified that their final design files are ready.";
            }
            else if (phase == "shipped")
            {
                TempData["SuccessMessage"] = "The client, " + profile.FirstName + " " + profile.LastName + " has been notified that their design package has been shipped.";
            }
            else
            {
                TempData["SuccessMessage"] = "The client, " + profile.FirstName + " " + profile.LastName + " has been notified that their revised design files are ready.";
            }
            return RedirectToAction("Details", "Search", new { id = room.RoomId });     // the operation was successful, send the user back to the search details screen.
        }


        #endregion

        #region End Users
        //
        // GET: /DesignFile/InitialDesignFeedback/5
        // This action renders a view that lists the 3 initial design files and allows the user to comment on these design files.
        [Authorize]
        public ActionResult InitialDesignFeedback(int id)
        {
            IEnumerable<DesignFile> designFiles = designFileRepository.GetInitialDesignFiles(id);   // load all of the inital design files for the room.  This includes 1st and 2nd inspiration boards, and initial floor plan
            ViewData["RoomId"] = id;    // set the room id 
            ViewData["InitialSignOff"] = ""; // sign off radio button (Asks the user:  Do you want to submit your feedback to the designer and sign off on these inpiration boards and floor plan.  (n order to proceed to the next step and to allow our designer to finish working on your room, you must check this box)
            return View(designFiles);
        }


        [HttpPost, Authorize]
        public ActionResult InitialDesignFeedback(IEnumerable<DesignFile> designFiles, FormCollection collection)
        {
            //bool initialDesignSignedOff = Convert.ToBoolean(ViewData["InitialSignOff"]);
            string value = collection["InitialSignOff"];

            bool updateSuccessful = false;  // keeps track of if the update completed correctly. Initialize to false.
            if (ModelState.IsValid)
            {
                foreach (DesignFile designFile in designFiles)
                {
                    if (designFile.Comments != null)
                    {
                        updateSuccessful = designFileRepository.UpdateDesignFileComments(designFile.DesignFileId, designFile.Comments);
                    }
                }
            }
/*            if (updateSuccessful && initialDesignSignedOff)
            {
                // The user has signed off on the initial design.  Update the menu value.  Email the designer
                TempData["SuccessMessage"] = "Your comments have been successfully updated and submitted to our designer for review.";
                return View(designFiles);
            } */
            if (updateSuccessful)
            {
                TempData["SuccessMessage"] = "The comments have been successfully updated.";
                return View(designFiles);
            }
            else
            {
                TempData["ErrorMessage"] = "Sorry, there was an error updating the comments";
                return View(designFiles);
            }
        }
        #endregion
    }
}
