﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessDesign.Models;
using System.Web.Hosting;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;


namespace AccessDesign.Controllers
{
    public class RoomController : Controller
    {
        RoomRepository roomRepository = new RoomRepository();
        ProfileRepository profileRepository = new ProfileRepository();  // make a connection to the profile repository so that we can extract the clients first and last name
        OrderRepository orderRepository = new OrderRepository();        // we need to connect to the order repository
        RoomPhotoRepository roomPhotoRepository = new RoomPhotoRepository();    // make a connection to the Room Photo Repository
        FurnitureRepository furnitureRepository = new FurnitureRepository();    // make a connection to the furniture repository
        FurniturePhotoRepository furniturePhotoRepository = new FurniturePhotoRepository();     // make a connection to the furniture photo repository
        RoomMeasurementRepository roomMeasurementRepository = new RoomMeasurementRepository();     // make a connection to the Room Measurement Repository
        GeneralQuestionnaireRepository generalQuestionnaireRepository = new GeneralQuestionnaireRepository();   // make a connection to the general questionnaire repository

        //
        // GET: /Room/
        [Authorize]
        public ActionResult Index()
        {
            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            var rooms = roomRepository.FindAllMyRooms(userName).ToList();   // Gets all of the rooms that the currently logged on user has created
            return View("Index", rooms);
        }


        //
        // GET: /Room/Details/5
        // Displays the room's name and type to the end user

        [Authorize]
        public ActionResult Details(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);

            if (room == null)
            {
                TempData["ErrorMessage"] = "There was a problem finding this room in the database.";
                return View("Error");
            }
            else
            {
                return View("Details", room);
            }
        }

        //
        // GET: /Room/Create

        [Authorize]
        public ActionResult Create()
        {
            var roomTypes = roomRepository.GetAllRoomTypes();   // get a list of all of the room Types
            ViewData["RoomType"] = new SelectList(roomTypes, "Name", "Name");

            // check if profile and general questionnaire have been completed. If they have not, redirect the use to the appropriate step
            string userName = HttpContext.User.Identity.Name.ToString();
            bool profileExists = profileRepository.DoesProfileExist(userName);
            bool generalQuestionnaireExists = generalQuestionnaireRepository.DoesGeneralQuestionnaireExist(userName);

            if (!profileExists && !generalQuestionnaireExists)      // both the profile and general questionnaire do not exist
            {
                TempData["ErrorMessage"] = "Please fill out the profile and the general questionnaire before creating your room.";
                return RedirectToAction("Create", "Profile");      // send the user to the profile screen with an error message
            }
            else if (!profileExists && generalQuestionnaireExists)  // the profile does not exist, but the general questionnaire does
            {
                TempData["ErrorMessage"] = "Please fill out the profile before creating your room.";
                return RedirectToAction("Create", "Profile");      // send the user to the profile screen with an error message
            }
            else if (profileExists && !generalQuestionnaireExists)      // the profile exists, but the general questionnaire still needs to be filled out
            {
                TempData["ErrorMessage"] = "Please fill out the general questionnaire before creating your room.";
                return RedirectToAction("Create", "GeneralQuestionnaire");      // send the user to the profile screen with an error message
            }
            else
            {
                // check if the user has created a room.  This information is used by the create view to display the current progress bar and paragraph text.
                if (roomRepository.GetNumberOfRoomsForUser(userName) > 0)
                {
                    ViewData["CreatedRooms"] = true;    // the user has 1 or more rooms that they created
                }
                else
                {
                    ViewData["CreatedRooms"] = false;   // the user has not created any rooms.
                }

                // otherwise all of the 2 preceeding steps have been completed, so allow the user to create a new room
                Room room = new Room()
                {

                };

                return View(room);
            }
        } 

        //
        // POST: /Room/Create

        [HttpPost, Authorize]
        public ActionResult Create(Room room)
        {
            string userName = HttpContext.User.Identity.Name.ToString();

            // if the modelstate is valid, create a new room
            if (ModelState.IsValid)
            {
                RoomType selectedRoomType = roomRepository.GetSpecificRoomType(room.RoomTypeName);     // get the details of the currently selected room type
                int profileId = profileRepository.GetProfileIdByUserName(userName);

                room.ProfileId = profileId;     // set the profile Id equal to the currently logged on user's profile id
                room.UserName = userName;
                room.DateCreated = DateTime.Now;        // this will be used in the future to compare the created vs. updated date to see if there are any users that need assistance with their room creation steps.
                room.DateUpdated = DateTime.Now;
                room.CurrentStepId = 1;   // set the current step as 1 since we have just completed the first step (creating the room)
                room.MenuStepId = 3;    // set the room menu step to 3 since "Name room and select room type is complete"
                room.RoomCost = selectedRoomType.Cost;
                room.CostPerRevision = selectedRoomType.CostForRevision;
                room.RoomPaidFor = 0;   // initialize the valuie to 0. This is also done in SQL server, but I prefer this to be explicitly done.
                room.TotalFreeRevisionsUsed = 0;    // initialize the value to 0
                room.TotalRevisionsPaidFor = 0;     // initialize the value to 0
                roomRepository.Add(room);
                roomRepository.Save();

                TempData["SuccessMessage"] = "Your room has been successfully created. Please fill out the questionnaire below.";
                return RedirectToAction("Index", "Questionnaire", new { id = room.RoomId });
            }
            TempData["ErrorMessage"] = "Sorry, an unknown error occured while creating your room.";
            return View(room);
        }


        //
        // GET: /Room/AssignToDesigner/5
        [Authorize(Roles = "Designers")]
        public ActionResult AssignToDesigner(int id)
        {
            Room room = roomRepository.GetRoomById(id);     // get the room that we want to assign to the designer
            ViewData["Designers"] = ProfileRepository.GetDesignerList(room.AssignedToDesigner);     // get a list of the available designers
            
            return View(room);
        }

        //
        // POST: /Room/AssignToDesigner/5
        [HttpPost, Authorize(Roles = "Designers")]
        public ActionResult AssignToDesigner(int id, FormCollection formValues)
        {
            Room room = roomRepository.GetRoomById(id);

            // Create a country drop down list
            ViewData["Designers"] = ProfileRepository.GetDesignerList(room.AssignedToDesigner);

            if (ModelState.IsValid && TryUpdateModel(room))
            {
                UpdateModel(room);
                roomRepository.Save();
                TempData["SuccessMessage"] = "This room has been successfully assigned to a new designer.";
                return RedirectToAction("Details", "Search", new { id = room.RoomId  });
            }

            return View(room);
        }
        
        //
        // GET: /Room/Edit/5
 
        [Authorize]
        public ActionResult Edit(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);
            if (room == null)
            {
                TempData["ErrorMessage"] = "Sorry, that room was not found.";
                return View("Error");
            }
            else
                return View(room);
        }

        //
        // POST: /Room/Edit/5

        [Authorize]
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);

            if (ModelState.IsValid && TryUpdateModel(room))
            {
                UpdateModel(room);

                roomRepository.Save();

                TempData["SuccessMessage"] = "Your room name has been updated";
                return RedirectToAction("Edit", new { id = room.RoomId });
            }
            return View(room);
        }

        //
        // GET: /Room/Delete/5
 
        [Authorize]
        public ActionResult Delete(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);

            if (room == null)
            {
                TempData["ErrorMessage"] = "Sorry, that room was not found";
                return View("Error");
            }
            else
                return View(room);
        }

        //
        // POST: /Room/Delete/5

        [HttpPost, Authorize]
        public ActionResult Delete(int id, string confirmButton)
        {
            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // get the current room           

            if (room == null)
            {
                TempData["ErrorMessage"] = "Sorry, the room was not found in the database";
                return View("Error");
            }
            else
            {
                // check if an order exists for this room.  If an order exists, we do not want to delete this room since it contains
                // buisness information
                if (orderRepository.OrderExistsForRoom(room.RoomId))
                {
                    TempData["ErrorMessage"] = "Sorry, you can not delete a room that you have already paid for.";
                    return View("Error");
                }
                else
                {
                    // Begin - Room Photo Deletion
                    // The directory that stores the files exists, so for each of the files, lets see if they exists.  If they exists, lets delete it
                    IQueryable<RoomPhoto> roomPhotos = roomPhotoRepository.FindAllRoomPhotosForSpecificRoom(id);        // get list of all of the photos for this room         

                    // loop through all of this rooms photos and delete them 1 by 1
                    foreach(RoomPhoto roomPhoto in roomPhotos)
                    {
                        // Begin - Delete the original room photo
                        string fullPhotoPath = @Server.MapPath(roomPhoto.OriginalServerPath);   // get the original file path and map it to this environment
                            if (System.IO.File.Exists(@fullPhotoPath))
                            {
                                try
                                {
                                    System.IO.File.Delete(@fullPhotoPath);
                                }
                                catch (System.IO.IOException ex)
                                {
                                    // TODO: Don't pass this error back, enable logging instead
                                    TempData["ErrorMessage"] = "Unable to delete photo:  Error: " + ex.Message.ToString();
                                    return View("Error");
                                }
                            }
                        // End - Delete the original room photo

                        // Begin - Delete the thumbnail room photo
                            string fullThumbnailPhotoPath = @Server.MapPath(roomPhoto.ThumbnailServerPath);   // get the original file path and map it to this environment
                            if (System.IO.File.Exists(@fullThumbnailPhotoPath))
                            {
                                try
                                {
                                    System.IO.File.Delete(@fullThumbnailPhotoPath);
                                }
                                catch (System.IO.IOException ex)
                                {
                                    // TODO: Don't pass this error back, enable logging instead
                                    TempData["ErrorMessage"] = "Unable to delete photo:  Error: " + ex.Message.ToString();
                                    return View("Error");
                                }
                            }
                        // End - Delete the thumbnail room photo

                        // Begin - Delete the web room photo
                            string fullWebPhotoPath = @Server.MapPath(roomPhoto.WebServerPath);   // get the original file path and map it to this environment
                            if (System.IO.File.Exists(@fullWebPhotoPath))
                            {
                                try
                                {
                                    System.IO.File.Delete(@fullWebPhotoPath);
                                }
                                catch (System.IO.IOException ex)
                                {
                                    // TODO: Don't pass this error back, enable logging instead
                                    TempData["ErrorMessage"] = "Unable to delete photo:  Error: " + ex.Message.ToString();
                                    return View("Error");
                                }
                            }
                        // End - Delete the web room photo
                    }
                    // End Room Photo Deletion

                    // Begin - Room Measurement Deletion
                    // The directory that stores the files exists, so for each of the files, lets see if they exists.  If they exists, lets delete it
                    IQueryable<RoomMeasurement> roomMeasurements = roomMeasurementRepository.FindAllRoomMeasurementsForSpecificRoom(id);    // get list of all of the measurements for this room  

                    // loop through all of this rooms measurements and delete them 1 by 1
                    foreach (RoomMeasurement roomMeasurement in roomMeasurements)
                    {
                        // Begin - Delete the original room measurement
                        string fullPhotoPath = @Server.MapPath(roomMeasurement.OriginalServerPath);   // get the original file path and map it to this environment
                        if (System.IO.File.Exists(@fullPhotoPath))
                        {
                            try
                            {
                                System.IO.File.Delete(@fullPhotoPath);
                            }
                            catch (System.IO.IOException ex)
                            {
                                // TODO: Don't pass this error back, enable logging instead
                                TempData["ErrorMessage"] = "Unable to delete room measurement:  Error: " + ex.Message.ToString();
                                return View("Error");
                            }
                        }
                        // End - Delete the original room measurement

                        // Begin - Delete the thumbnail room measurement
                        string fullThumbnailPhotoPath = @Server.MapPath(roomMeasurement.ThumbnailServerPath);   // get the original file path and map it to this environment
                        if (System.IO.File.Exists(@fullThumbnailPhotoPath))
                        {
                            try
                            {
                                System.IO.File.Delete(@fullThumbnailPhotoPath);
                            }
                            catch (System.IO.IOException ex)
                            {
                                // TODO: Don't pass this error back, enable logging instead
                                TempData["ErrorMessage"] = "Unable to delete room measurement:  Error: " + ex.Message.ToString();
                                return View("Error");
                            }
                        }
                        // End - Delete the thumbnail room measurement

                        // Begin - Delete the web room measurement
                        string fullWebPhotoPath = @Server.MapPath(roomMeasurement.WebServerPath);   // get the original file path and map it to this environment
                        if (System.IO.File.Exists(@fullWebPhotoPath))
                        {
                            try
                            {
                                System.IO.File.Delete(@fullWebPhotoPath);
                            }
                            catch (System.IO.IOException ex)
                            {
                                // TODO: Don't pass this error back, enable logging instead
                                TempData["ErrorMessage"] = "Unable to delete room measurement:  Error: " + ex.Message.ToString();
                                return View("Error");
                            }
                        }
                        // End - Delete the web room measurement
                    }
                    // End Room Measurement Deletion

                    // Begin - Room Furniture Deletion
                    // The directory that stores the files exists, so for each of the files, lets see if they exists.  If they exists, lets delete it
                    IQueryable<Furniture> furnitures = furnitureRepository.FindAllFurnitureForRoom(id);     // get a list of all of the furniture pieces in this room

                    // Loop through all of the pieces of furniture that are associated with this room
                    foreach (Furniture furniture in furnitures)
                    {
                        IQueryable<FurniturePhoto> furniturePhotos = furniturePhotoRepository.FindAllFurniturePhotosForSpecificFurniturePiece(furniture.FurnitureId);       // get all of the furniture photos associated with this furniture piece

                        // loop through all of this furniture piece's photos and delete them 1 by 1
                        foreach (FurniturePhoto furniturePhoto in furniturePhotos)
                        {
                            // Begin - Delete the original furniture photo
                            string fullPhotoPath = @Server.MapPath(furniturePhoto.OriginalServerPath);   // get the original file path and map it to this environment
                            if (System.IO.File.Exists(@fullPhotoPath))
                            {
                                try
                                {
                                    System.IO.File.Delete(@fullPhotoPath);
                                }
                                catch (System.IO.IOException ex)
                                {
                                    // TODO: Don't pass this error back, enable logging instead
                                    TempData["ErrorMessage"] = "Unable to delete photo:  Error: " + ex.Message.ToString();
                                    return View("Error");
                                }
                            }
                            // End - Delete the original furniture photo

                            // Begin - Delete the thumbnail furniture photo
                            string fullThumbnailPhotoPath = @Server.MapPath(furniturePhoto.ThumbnailServerPath);   // get the original file path and map it to this environment
                            if (System.IO.File.Exists(@fullThumbnailPhotoPath))
                            {
                                try
                                {
                                    System.IO.File.Delete(@fullThumbnailPhotoPath);
                                }
                                catch (System.IO.IOException ex)
                                {
                                    // TODO: Don't pass this error back, enable logging instead
                                    TempData["ErrorMessage"] = "Unable to delete photo:  Error: " + ex.Message.ToString();
                                    return View("Error");
                                }
                            }
                            // End - Delete the thumbnail furniture photo

                            // Begin - Delete the web furniture photo
                            string fullWebPhotoPath = @Server.MapPath(furniturePhoto.WebServerPath);   // get the original file path and map it to this environment
                            if (System.IO.File.Exists(@fullWebPhotoPath))
                            {
                                try
                                {
                                    System.IO.File.Delete(@fullWebPhotoPath);
                                }
                                catch (System.IO.IOException ex)
                                {
                                    // TODO: Don't pass this error back, enable logging instead
                                    TempData["ErrorMessage"] = "Unable to delete photo:  Error: " + ex.Message.ToString();
                                    return View("Error");
                                }
                            }
                            // End - Delete the furniture room photo
                        }

                    }

                    // End - Room Furniture Deletion   

                    // delete the room from the database
                        try
                        {
                            roomRepository.Delete(room);
                            roomRepository.Save();
                            TempData["SuccessMessage"] = "Your room has been deleted.";
                            return View("Deleted");
                        }
                        catch (Exception)
                        {
                            //TODO:  Log error
                            TempData["ErrorMessage"] = "An error occured while attempting to delete your room from the database.";
                            return View("Error");
                        }
                }
            }
        }




 
            


            /*
                    // since we display hidden fields that could be tampered with, ensure that the room belongs to the currently logged on user.
                    // Try this with fiddler.  If it's an issue than, for each answer, loop through and check the database based on the room id to ensure that the room id matches the user id.
                    if (currentRoomId != answer.RoomId)
                    {
                        currentRoomId = answer.RoomId;  // set the current room = to the room id obtained from the form
                        if (!roomRepository.DoesRoomExist(currentRoomId, userName))     // check if the room id belongs to the logged in user.
                        {
                            try
                            {
                                ViewData["Status"] = "Sorry, this room does not belong to you.";    // This line is currently not utilized
                                return View("NotFound");
                            }
                            catch (Exception) { } // TODO: implement some handling
                        }

                    }
             */
                       



                //room.UserName = HttpContext.User.Identity.Name.ToString();
                //room.DateUpdated = DateTime.Now;
                //room.CurrentStepId = 1;   // set the current step as 1 since we have just completed the first step (creating the room)
                //roomRepository.Add(answer);
                //roomRepository.Save();

        



#region BookFreeConsultation
        // GET: /Room/BookFirstAppointment/30
        // accepts the room id
        [Authorize]
        public ActionResult BookFreeConsultation(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name

            // ensure that the logged on user owns this room
            if (!roomRepository.DoesRoomExist(id, userName))
            {
                TempData["ErrorMessage"] = "Sorry, this room was not found.";
                return View("Error");    // if the current user does not own this room, then return the Not Found view
            }

            return View();
        }


#endregion
    }
}