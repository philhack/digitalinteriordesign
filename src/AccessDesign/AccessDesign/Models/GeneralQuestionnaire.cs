﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Foolproof;

namespace AccessDesign.Models
{
    [MetadataType(typeof(GeneralQuestionnaire_Validation))]
    public partial class GeneralQuestionnaire
    {

    }

    public class GeneralQuestionnaire_Validation
    {
        /*
        *  Uses:   http://foolproof.codeplex.com/      :  	Beta 0.9.3774       :   Sun May 2 2010
        *  [Is]
        *  [EqualTo]
        *  [NotEqualTo]
        *  [GreaterThan]
        *  [LessThan]
        *  [GreaterThanOrEqualTo]
        *  [LessThanOrEqualTo]
        *  [RequiredIf]
        *  [RequiredIfNot]
        *  [RequiredIfTrue]
        *  [RequiredIfFalse]
        *  [RequiredIfEmpty]
        *  [RequiredIfNotEmpty]
        *  [RequiredIfRegExMatch]
        *  [RequiredIfNotRegExMatch]
        */

        [HiddenInput]
        public string UserName  { get; set; }

        [DisplayName("What is your design style?")]
        [Required(ErrorMessage = "Required")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        public string DesignStyle { get; set; }

        [DisplayName("")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        [RequiredIf("DesignStyle", "Other", ErrorMessage = "Required")]
        public string DesignStyleOther { get; set; }

        [DisplayName("What colors do you love?")]
        //[Required(ErrorMessage = "Required")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        public string ColorsYouLove  { get; set; }

        [DisplayName("")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        //[RequiredIf("ColorsYouLove", "Other", ErrorMessage = "Required")]
        public string ColorsYouLoveOther  { get; set; }

        [DisplayName("What colors do you dislike?")]
        //[Required(ErrorMessage = "Required")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        public string ColorsYouDislike  { get; set; }

        [DisplayName("")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        //[RequiredIf("ColorsYouDislike", "Other", ErrorMessage = "Required")]
        public string ColorsYouDislikeOther  { get; set; }

        [DisplayName("Do you like strong, bold colours, a neutral pallet, or something in between?")]
        [Required(ErrorMessage = "Required")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        public string TypeOfColorsYouLike  { get; set; }

        [DisplayName("Do you have a spouse?")]
        [Required(ErrorMessage = "Required")]
        [StringLength(3, ErrorMessage = "Please enter a maximum of 3 characters.")]
        public string HaveASpouse  { get; set; }

        [DisplayName("Do you have kids?")]
        [Required(ErrorMessage = "Required")]
        [StringLength(3, ErrorMessage = "Please enter a maximum of 3 characters.")]
        public string HaveKids  { get; set; }

        [DisplayName("If yes, please enter the number of children that you have and their ages.")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        [RequiredIf("HaveKids", "Yes", ErrorMessage = "Required")]
        public string HaveKidsAgesOfKids  { get; set; }

        [DisplayName("Do you have pets?")]
        [Required(ErrorMessage = "Required")]
        [StringLength(3, ErrorMessage = "Please enter a maximum of 3 characters.")]
        public string HavePets  { get; set; }

        [DisplayName("If yes, please enter the number and types of your pets.")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        [RequiredIf("HavePets", "Yes", ErrorMessage = "Required")]
        public string HavePetsTypeAndNumber  { get; set; }

        [DisplayName("What hobbies and interests do you have?")]
        [Required(ErrorMessage = "Required")]
        [StringLength(2000, ErrorMessage = "Please enter a maximum of 2000 characters.")]
        public string HobbiesAndInterests  { get; set; }

        [DisplayName("What types of patterns do you like?")]
        //[Required(ErrorMessage = "Required")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        public string TypesOfPatternsYouLike  { get; set; }

        [DisplayName("")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        //[RequiredIf("TypesOfPatternsYouLike", "Other", ErrorMessage = "Required")]
        public string TypesOfPatternsYouLikeOther  { get; set; }

        [DisplayName("Is being 'green' important to you?")]
        [Required(ErrorMessage = "Required")]
        [StringLength(5, ErrorMessage = "Please enter a maximum of 5 characters.")]
        public string BeingGreenImportant  { get; set; }

        [DisplayName("")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        [RequiredIf("BeingGreenImportant", "Other", ErrorMessage = "Required")]
        public string BeingGreenImportantOther  { get; set; }

        [DisplayName("Do you rent or own your home?")]
        [Required(ErrorMessage = "Required")]
        [StringLength(4, ErrorMessage = "Please enter a maximum of 4 characters.")]
        public string RentOrOwnYourHome  { get; set; }

        [DisplayName("How long do you plan on living in this home?")]
        [Required(ErrorMessage = "Required")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        public string HowLongDoYouPlanToLiveInHome  { get; set; }

        [DisplayName("Should this space be designed for wheelchair access?")]
        [Required(ErrorMessage = "Required")]
        [StringLength(3, ErrorMessage = "Please enter a maximum of 3 characters.")]
        public string NeedWheelchairAccess { get; set; }
    }
}