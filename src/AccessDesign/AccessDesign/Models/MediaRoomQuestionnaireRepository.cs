﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AccessDesign.Models
{
    public class MediaRoomQuestionnaireRepository
    {
        private AccessDesignEntities entities = new AccessDesignEntities();

        // Return the mediaRoom questions based on the mediaRoom question id
        public MediaRoomQuestionnaire GetMediaRoomQuestionnaireByRoomId(int id)
        {
            return entities.MediaRoomQuestionnaires.FirstOrDefault(m => m.RoomId == id);
        }

        public MediaRoomQuestionnaire GetMediaRoomQuestionnaireByMediaRoomQuestionnaireId(int id)
        {
            return entities.MediaRoomQuestionnaires.FirstOrDefault(m => m.MediaRoomQuestionnaireId == id);
        }

        // Checks if the questionnaire exists based on room id, if it does, it returns true, otherwise it returns false
        public bool DoesMediaRoomQuestionnaireExist(int id)
        {
            if (entities.MediaRoomQuestionnaires.Count(m => m.RoomId == id) == 1)   // check if the mediaRoom questionnaire exists
                return true;    // the questionnaire exists, so return true
            else
                return false;   // the questionnaire does not exist
        }

        /// <summary>
        /// Returns a list of gender types.  This can then be used in a drop down list for the question: How do you want your room to feel?
        /// </summary>
        /// <param name="genderType">Gender Type</param>
        /// <returns></returns>
        public static SelectList GetGenderTypeList(string genderType)
        {
            List<SelectListItem> numberList = new List<SelectListItem>() 
            {
				new SelectListItem() { Value = "", Text = "  " },
                new SelectListItem() { Value = "Masculine", Text = " Masculine " },
                new SelectListItem() { Value = "Feminine", Text = " Feminine " },
                new SelectListItem() { Value = "Neutral", Text = " Neutral " }
            };
            return new SelectList(numberList, "Value", "Text", genderType ?? "");
        }

        #region Room Create Insert and Delete Statements
        //
        // Insert
        public void Add(MediaRoomQuestionnaire mediaRoomQuestionnaire)
        {
            entities.MediaRoomQuestionnaires.AddObject(mediaRoomQuestionnaire);
        }


        //
        // Delete
        public void Delete(MediaRoomQuestionnaire mediaRoomQuestionnaire)
        {
            entities.MediaRoomQuestionnaires.DeleteObject(mediaRoomQuestionnaire);  // delete the mediaRoom Questionnaire
        }

        //
        // Persistence
        public void Save()
        {
            entities.SaveChanges();
        }

        #endregion

    }
}