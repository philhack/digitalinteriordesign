﻿$(document).ready(function () {

    // Un-comment to display the 1st section as open
    //$(".accordion h2").eq(0).addClass("active");
    //$(".accordion p").eq(0).show();

    $(".accordion h2").click(function () {
        $(this).next("p").slideToggle("slow")
		.siblings("p:visible").slideUp("slow");
        $(this).toggleClass("active");
        $(this).siblings("h3").removeClass("active");
    });

});