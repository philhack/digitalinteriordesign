﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AccessDesign.Models
{
    public class BathroomQuestionnaireRepository
    {
        private AccessDesignEntities entities = new AccessDesignEntities();

        // Return the bathroom questions based on the bathroom question id
        public BathroomQuestionnaire GetBathRoomQuestionnaireByRoomId(int id)
        {
            return entities.BathroomQuestionnaires.FirstOrDefault(b => b.RoomId == id);
        }

        public BathroomQuestionnaire GetBathroomQuestionnaireByBathroomQuestionnaireId(int id)
        {
            return entities.BathroomQuestionnaires.FirstOrDefault(b => b.BathroomQuestionnaireId == id);
        }

        // Checks if the questionnaire exists based on room id, if it does, it returns true, otherwise it returns false
        public bool DoesBathroomQuestionnaireExist(int id)
        {
            if (entities.BathroomQuestionnaires.Count(u => u.RoomId == id) == 1)   // check if the bathroom questionnaire exists
                return true;    // the questionnaire exists, so return true
            else
                return false;   // the questionnaire does not exist
        }


        /// <summary>
        /// Returns a list of numbers from 1 to 7.  This can then be used in a drop down list for the number of people that share the same bathroom
        /// </summary>
        /// <param name="number">Number of people that share the same bathroom</param>
        /// <returns></returns>
        public static SelectList GetNumberList(string number)
        {
            List<SelectListItem> numberList = new List<SelectListItem>() 
            {
                new SelectListItem() { Value = "1", Text = " 1 " },
                new SelectListItem() { Value = "2", Text = " 2 " },
                new SelectListItem() { Value = "3", Text = " 3 " },
                new SelectListItem() { Value = "4", Text = " 4 " },
                new SelectListItem() { Value = "5", Text = " 5 " },
                new SelectListItem() { Value = "6", Text = " 6 " }
            };
            return new SelectList(numberList, "Value", "Text", number ?? "1");
        }


        /// <summary>
        /// Returns a list of gender types.  This can then be used in a drop down list for the question: How Do you want your room to feel masculine, feminine, or neutral?
        /// </summary>
        /// <param name="genderType">Gender Type</param>
        /// <returns></returns>
        public static SelectList GetGenderTypeList(string genderType)
        {
            List<SelectListItem> numberList = new List<SelectListItem>() 
            {
				new SelectListItem() { Value = "", Text = "  " },
                new SelectListItem() { Value = "Masculine", Text = " Masculine " },
                new SelectListItem() { Value = "Feminine", Text = " Feminine " },
                new SelectListItem() { Value = "Neutral", Text = " Neutral " }
            };
            return new SelectList(numberList, "Value", "Text", genderType ?? "");
        }



        #region Room Create Insert and Delete Statements
        //
        // Insert
        public void Add(BathroomQuestionnaire bathroomQuestionnaire)
        {
            entities.BathroomQuestionnaires.AddObject(bathroomQuestionnaire);
        }


        //
        // Delete
        public void Delete(BathroomQuestionnaire bathroomQuestionnaire)
        {
            entities.BathroomQuestionnaires.DeleteObject(bathroomQuestionnaire);  // delete the bathroom Questionnaire
        }

        //
        // Persistence
        public void Save()
        {
            entities.SaveChanges();
        }

        #endregion

    }
}