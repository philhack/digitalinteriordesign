﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Room Deleted
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Room Deleted</h1>

    <div>
        <p>Your room was successfully deleted.</p>
    </div>
    <div>
        <p>Click <%: Html.ActionLink("here","Index","Room") %> to view your list of rooms.</p>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SidebarContent" runat="server">
</asp:Content>
