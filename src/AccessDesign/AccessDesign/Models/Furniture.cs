﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AccessDesign.Models
{
    //
    // Furniture
    [MetadataType(typeof(Furniture_Validation))]
    public partial class Furniture
    {
    }

    public class Furniture_Validation
    {
        [Required(ErrorMessage = "A name for your piece of furniture is requried")]
        [StringLength(10, ErrorMessage = "Furniture name is too long (Maximum 10 characters")]
        [DisplayName("Name")]
        public string Name { get; set; }

        [StringLength(10000, ErrorMessage = "Comments are too long (Maximum 10,000 characters")]
        [DisplayName("Comments")]
        public string Comments { get; set; }

        [Required(ErrorMessage = "The width of your piece of furniture is requried")]
        [DisplayName("Width (inches)")]
        [RegularExpression(@"^[-]?([1-9]{1}[0-9]{0,}(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|\.[0-9]{1,2})$", ErrorMessage = "The width must be a number. IE: 10.25")]
        public string Width { get; set; }

        [Required(ErrorMessage = "The width of your piece of furniture is requried")]
        [DisplayName("Depth (inches)")]
        [RegularExpression(@"^[-]?([1-9]{1}[0-9]{0,}(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|\.[0-9]{1,2})$", ErrorMessage = "The depth must be a number. IE: 10.25")]
        public string Depth { get; set; }

        [Required(ErrorMessage = "The width of your piece of furniture is requried")]
        [DisplayName("Height (inches)")]
        [RegularExpression(@"^[-]?([1-9]{1}[0-9]{0,}(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|\.[0-9]{1,2})$", ErrorMessage = "The hight must be a number. IE: 10.25")]
        public string Height { get; set; }
    }
}