﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessDesign.Models;

namespace AccessDesign.Controllers
{
    public class QuestionnaireController : Controller
    {
        RoomRepository roomRepository = new RoomRepository();       // connect to the room repository
        //
        // GET: /Questionnaire/RoomId

        public ActionResult Index(int id)
        {
            // check if the room exists and if it's not empty.  If it's not empty then redirect the user to their 
            // questionnaire based on the room type
            string roomType = roomRepository.GetRoomTypeByRoomId(id);
            if(roomType != null)
            {
                roomRepository.UpdateRoomDateUpdated(id, DateTime.Now);     // update the room so that the navigation updates- This return a bool value, but we're not doing anything with this value right now
                switch (roomType)
                {
                    case "Bathroom" :
                        return RedirectToAction("Index", "BathroomQuestionnaire", id);
                    case "Dining Room" :
                        return RedirectToAction("Index", "DiningRoomQuestionnaire", id);
                    case "Entryway" :
                        return RedirectToAction("Index", "EntryQuestionnaire", id);
                    case "Guest Room" :
                        return RedirectToAction("Index", "GuestRoomQuestionnaire", id);
                    case "Home Office" :
                        return RedirectToAction("Index", "OfficeQuestionnaire", id);
                    case "Kids Room" :
                        return RedirectToAction("Index", "KidsRoomQuestionnaire", id);
                    case "Kitchen":
                        return RedirectToAction("Index", "KitchenQuestionnaire", id);
                    case "Living Room" :
                        return RedirectToAction("Index", "LivingRoomQuestionnaire", id);
                    case "Master Bedroom" :
                        return RedirectToAction("Index", "MasterBedroomQuestionnaire", id);
                    case "Media Room" :
                        return RedirectToAction("Index", "MediaRoomQuestionnaire", id);
                }
            }
                TempData["ErrorMessage"] = "Please name a room and select the room type before filling out the room specific questionnaire.";
                return RedirectToAction("Create", "Room");     // redirect the user to a create room message since the room will not load. 
                                                                                            // This is either because someone is trying to hack the site or because the room has not been created yet.
        }

        [Authorize(Roles = "Designers")]
        public ActionResult Details(int id)
        {
            // check if the room exists and if it's not empty.  If it's not empty then redirect the user to their 
            // questionnaire based on the room type
            string roomType = roomRepository.GetRoomTypeByRoomId(id);
            if (roomType != null)
            {
                switch (roomType)
                {
                    case "Bathroom":
                        return RedirectToAction("Details", "BathroomQuestionnaire", new { id = id });
                    case "Dining Room":
                        return RedirectToAction("Details", "DiningRoomQuestionnaire", new { id = id });
                    case "Entryway":
                        return RedirectToAction("Details", "EntryQuestionnaire", new { id = id });
                    case "Guest Room":
                        return RedirectToAction("Details", "GuestRoomQuestionnaire", new { id = id });
                    case "Home Office":
                        return RedirectToAction("Details", "OfficeQuestionnaire", new { id = id });
                    case "Kids Room":
                        return RedirectToAction("Details", "KidsRoomQuestionnaire", new { id = id });
                    case "Kitchen":
                        return RedirectToAction("Details", "KitchenQuestionnaire", new { id = id });
                    case "Living Room":
                        return RedirectToAction("Details", "LivingRoomQuestionnaire", new { id = id });
                    case "Master Bedroom":
                        return RedirectToAction("Details", "MasterBedroomQuestionnaire", new { id = id });
                    case "Media Room":
                        return RedirectToAction("Details", "MediaRoomQuestionnaire", new { id = id });
                }
            }
            TempData["ErrorMessage"] = "Sorry, there was a problem locating that room.";
            return RedirectToAction("Error");     // redirect the user to a create room message since the room will not load. 
            // This is either because someone is trying to hack the site or because the room has not been created yet.
        }

    }
}
