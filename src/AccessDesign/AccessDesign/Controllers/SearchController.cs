﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessDesign.Models;
using MvcContrib.Pagination;
using MvcContrib.UI.Grid;
using MvcContrib.Sorting;

namespace AccessDesign.Controllers
{
    public class SearchController : Controller
    {
        RoomRepository roomRepository = new RoomRepository();
        ProfileRepository profileRepository = new ProfileRepository();  // make a connection to the profile repository so that we can extract the clients first and last name

        //
        // GET: /Search/
        [Authorize(Roles = "Designers")]
        public ActionResult Index()
        {
            return View();
        }


        //
        // GET: /Search/FindAllRooms/
        // Returns a view listing all of the rooms
        [Authorize(Roles = "Designers")]
        public ActionResult FindAllRooms()
        {
            IQueryable<Room> rooms = roomRepository.FindAllRooms();
            return View(rooms);
        }



        //
        // GET: /Search/FindSpecificRoomById/5
        [Authorize(Roles = "Designers")]
        public ActionResult FindSpecificRoomById(int id)
        {
            Room room = roomRepository.GetRoomById(id);
            return View(room);
        }


        //
        // GET: /Search/FindRoomsByUserName?roomname=My%20Room
        [Authorize(Roles = "Designers")]
        public ActionResult FindRoomsByRoomName(string roomName)
        {
            IQueryable<Room> rooms = roomRepository.GetRoomsByRoomName(roomName);
            return View(rooms);
        }


        //
        // GET: /Search/FindRoomsByUserName/id
        [Authorize(Roles = "Designers")]
        public ActionResult FindRoomsByUserName(string id)
        {
            IQueryable<Room> rooms = roomRepository.FindAllMyRooms(id);
            return View(rooms);
        }


        //
        // GET: /Search/FindCurrentDesignersRooms/
        [Authorize(Roles = "Designers")]
        public ActionResult FindCurrentDesignersRooms()
        {
            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            IQueryable<Room> rooms = roomRepository.GetRoomsByDesignerUserName(userName);
            return View(rooms);
        }


        //
        // GET: /Search/FindRoomsByDesigner/phil@digitalinteriordesign.com
        [Authorize(Roles = "Designers")]
        public ActionResult FindRoomsByDesigner(string id)
        {
            IQueryable<Room> rooms = roomRepository.GetRoomsByDesignerUserName(id);
            return View(rooms);
        }

        //
        // GET: /Search/DisplayAllUsers
        // uses MVC Contrib
        [Authorize(Roles = "Designers")]
        public ActionResult DisplayAllUsers(int? page, GridSortOptions sort)
        {
            ViewData["sort"] = sort;
            IEnumerable<Room> rooms = roomRepository.GetAllRoomsAndProfilesForAllUsersWithRooms();

            if (!string.IsNullOrEmpty(sort.Column))
            {
                rooms = rooms.OrderBy(sort.Column, sort.Direction);
            }

            rooms = rooms.AsPagination(page.GetValueOrDefault(1), 20);

            ViewData["BuildVersion"] = this.GetType().Assembly.GetName().Version.ToString();        // get the current build version so that it can be displayed in the admin section.
            return View(rooms);
        }

        //
        // GET: /Search/Details/5
        // Displays details about the user's profile, room, and design
        [Authorize(Roles = "Designers")]
        public ActionResult Details(int id)
        {
            Room room = roomRepository.GetAllRoomDetailsById(id);

            ViewData["NumberOfRoomPhotos"] = roomRepository.GetNumberOfRoomPhotosForARoom(id);              // gets the number of room photos that are associated with a room
            ViewData["NumberOfFurniturePieces"] = roomRepository.GetNumberOfFurniturePiecesForARoom(id);    // gets the number of furniture pieces that are associated with a room
            ViewData["NumberOfDesginFiles"] = roomRepository.GetNumberOfDesignFilesForARoom(id);            // gets the number of design files that are associated with a room
            ViewData["NumberOfMeasurements"] = roomRepository.GetNumberOfRoomMeasurementsForARoom(id);      // gets the number of room measurements that are associated with a room
            ViewData["CustomerSteps"] = roomRepository.GetAllCurrentCustomerSteps();
            ViewData["DesignerSteps"] = roomRepository.GetAllCurrentDesignerSteps();
            ViewData["MenuSteps"] = roomRepository.GetAllMenuSteps();

            return View(room);
        }

    }
}
