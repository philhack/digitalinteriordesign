﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Profile>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Create My Profile
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Create My Account</h1>
    <br />
    <div id="app-steps-generic">
              <a href="/Profile"><img src="/Content/Images/app-steps-profile-1-01.png" alt="" border="0" /></a><a href="/GeneralQuestionnaire"><img src="/Content/Images/app-steps-profile-1-02.png" alt="" border="0"/></a><a href="/Room/Create"><img src="/Content/Images/app-steps-profile-1-03.png" alt="" border="0"/></a>
              <h6 class="step-description">STEP 1/3. PLEASE COMPLETE STEP 1 - MY ACCOUNT</h6>
    </div>
    <br /> <br />
    <p>Your account creation is almost complete. Please fill in your information below and click submit.</p>
    <br />
    <div class="text-box"><span class="required-field">*</span> Required fields.</div>
    <br /><br />
    <% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm()) {%>

    <div>
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.FirstName) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.FirstName, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.FirstName) %>
            </div>
            <br />
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.LastName) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.LastName, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.LastName) %>
            </div>
            <br />
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.PhoneNumber) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.PhoneNumber, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.PhoneNumber) %>
            </div>
            <br />
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.FaxNumber) %>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.FaxNumber, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.FaxNumber) %>
            </div>
            <br />
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.SkypeId) %>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.SkypeId, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.SkypeId)%>
            </div>
            <br />
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.Address) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.Address, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.Address) %>
            </div>
            <br />
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.City) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.City, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.City) %>
            </div>
            <br />
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.State) %><span class="required-field">*</span>
            </div>
            <div class="dropdown">
                <%: Html.DropDownListFor(model => model.State, ViewData["States"] as SelectList, new { @class = "option" })%>
                <%: Html.ValidationMessageFor(model => model.State) %>
            </div>
            <br />
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.Zip) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.Zip, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.Zip) %>
            </div>
            <br />
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.Country) %><span class="required-field">*</span>
            </div>
            <div class="dropdown">
                <%: Html.DropDownListFor(model => model.Country, ViewData["Countries"] as SelectList, new { @class="option" })%>
                <%: Html.ValidationMessageFor(model => model.Country) %>
            </div>
            <br /><br /><br />
            <div id="standard-button">
                    <input type="submit"  class="button" value="Submit" />
            </div> 
    </div>
    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SidebarContent" runat="server">
</asp:Content>

