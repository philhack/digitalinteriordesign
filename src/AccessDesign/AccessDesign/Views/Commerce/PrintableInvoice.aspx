﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Invoice>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Printable Invoice</title>
</head>
<body>
        <table width="700" border="0">
          <tr> 
            <td width="387"><img src="/Content/Images/digital-interior-design-logo.png" alt="Digital Interior Design invoice logo" /></td>
            <td width="297"><p>Digital Interior Design (Treoma Design Inc.)<br />305 - 2015 Trafalgar St.<br />Vancouver, BC,<br />V6K3S5 Canada</p>
            <p>&nbsp;</p></td>
          </tr>
        </table>
        <p>&nbsp;</p>
        <h2>Order Summary</h2>
        <table width="673" border="0">
          <tr>
            <td width="562"><%: Model.RoomDescription %><br />Room Name: <%: Model.RoomName %><br />Room ID: <%: Model.RoomId %></td>
            <td width="128"><%: String.Format("{0:F}", Model.RoomCost) %></td>
          </tr>
          <tr>
            <td>Tax:  <% if(Model.TaxType != " ") { %> <%: Model.TaxType  %> % <% } %></td>
            <td><%: String.Format("{0:F}", Model.TaxAmount) %></td>
          </tr>
          <tr>
            <td>Discount</td>
            <td><%: String.Format("{0:F}", Model.Discount) %></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><h2>Grand Total (<%: Model.CurrencyCode %>)</h2></td>
            <td><h2><%: String.Format("{0:F}", Model.GrandTotal) %> </h2></td>
          </tr>
        </table>
        <p>&nbsp;</p>
        <h2>Customer Information</h2>
        <table width="465" border="0">
          <tr>
            <td width="115">Customer ID</td>
            <td width="340"><%: Model.CustomerId %></td>
          </tr>
          <tr>
            <td>Order Number</td>
            <td><%: Model.OrderNumber %></td>
          </tr>
          <tr>
            <td>Username</td>
            <td><%: Model.Username %></td>
          </tr>
        </table>
        <p><b><%: Model.Name %></b><br /><%: Model.Address %><br /><%: Model.City %>, <%: Model.State %><br /><%: Model.Zip %>  <%: Model.Country %></p>
</body>
</html>

