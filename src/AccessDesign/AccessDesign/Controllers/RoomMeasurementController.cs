﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessDesign.Models;
using System.Web.Hosting;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web.Security;

namespace AccessDesign.Controllers
{
    public class RoomMeasurementController : Controller
    {
        RoomRepository roomRepository = new RoomRepository();                                                       // make a connection to the roomRepository model
        RoomMeasurementRepository roomMeasurementRepository = new RoomMeasurementRepository();                      // make a connection to the roomMeasurementRepository
        private static readonly string UploadPath = System.Configuration.ConfigurationManager.AppSettings["uploadPath"];
        private static readonly string UploadPathForUrl = System.Configuration.ConfigurationManager.AppSettings["uploadPathForUrl"];
        private static readonly string RoomMeasurementsOriginalDirectory = System.Configuration.ConfigurationManager.AppSettings["roomMeasurementsOriginalDirectory"];
        private static readonly string RoomMeasurementsThumbnailDirectory = System.Configuration.ConfigurationManager.AppSettings["roomMeasurementsThumbnailDirectory"];
        private static readonly string RoomMeasurementsWebDirectory = System.Configuration.ConfigurationManager.AppSettings["roomMeasurementsWebDirectory"];
        private static readonly string RoomMeasurementsOriginalRSUrl = System.Configuration.ConfigurationManager.AppSettings["roomMeasurementsOriginalRSUrl"];
        private static readonly string RoomMeasurementsThumbnailRSUrl = System.Configuration.ConfigurationManager.AppSettings["roomMeasurementsThumbnailRSUrl"];
        private static readonly string RoomMeasurementsWebRSUrl = System.Configuration.ConfigurationManager.AppSettings["roomMeasurementsWebRSUrl"];
        

#region Upload Measurement

        //
        // GET: /RoomMeasurement/Upload

        /// <summary>
        ///  Loads the main upload room measurements web page
        /// </summary>
        /// <param name="id">
        /// id is the current room id
        /// </param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Upload(int id)
        {
            ViewData["RoomId"] = id;
            return View();
        }

        //
        // POST: /RoomMeasurement/Upload


        /// <summary>
        /// Save the posted file to the Uploads folder and return a message
        /// back to the uploadify jQuery plugin.
        /// </summary>
        /// <param name="id"></param>
        /// Accepts the id of the current room
        /// <param name="fileData"></param>
        /// <returns></returns>
        //[HttpPost, Authorize]
        [HttpPost]
        public ActionResult Upload(string token, int id, HttpPostedFileBase fileData)
        {
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(token);
            if (ticket != null)
            {
                var identity = new FormsIdentity(ticket);
                if (identity.IsAuthenticated)
                {

                    string userName = identity.Name.ToString(); // Get's the current logged on user name based on the forms authentication token
                    ViewData["RoomId"] = id;    // required to keep the view data room id
                    if(roomRepository.DoesRoomExist(id, userName))      // check if the room exists for the currenly logged on user and the id that is passed in.  Ya, this causes more DB reads, but we don't want virus's uploaded.
                    {

                        Sanitizer sanitize = new Sanitizer();   // create a new instance of the sanitizer
                        if (!sanitize.IsValidImage(fileData.FileName))   // check if the file name contains a valid file extension.  We are checking this again incase someone tampered with the javascript.
                            return Content("Error uploading file: The file must end in .jpg, .jpeg");

                        string sanitizedFileName = Sanitizer.CreateGuidRoomPhoto(fileData.FileName.ToString(), id);       // generate the file name "RoomId-GUID.ext"
                        string serverPhotoPath = @Server.MapPath(UploadPath + "\\");      // used as a basis for where to save the files to

                        // setup the initial paths and store them in the database
                        // we're not saving this to the database yet until all of the images have been generated and saved
                        // to use these server paths, you must map them.  IE:   @Server.MapPath(@roomPhoto.OriginalServerPath);
                        RoomMeasurement roomMeasurement = new RoomMeasurement
                        {
                            RoomId = id,
                            MeasurementName = sanitizedFileName,
                            OriginalServerPath = UploadPath + "\\" + RoomMeasurementsOriginalDirectory + "\\" + sanitizedFileName,                    // mapped path to the original image file
                            OriginalUrlPath = UploadPathForUrl + RoomMeasurementsOriginalDirectory + "/" + sanitizedFileName,
                            ThumbnailServerPath = UploadPath + "\\" + RoomMeasurementsThumbnailDirectory + "\\" + sanitizedFileName,                 // mapped path to the thumbnail directory
                            ThumbnailUrlPath = UploadPathForUrl + RoomMeasurementsThumbnailDirectory + "/" + sanitizedFileName,
                            WebServerPath = UploadPath + "\\" + RoomMeasurementsWebDirectory + "\\" + sanitizedFileName,                              // mapped path to the room photos web directory
                            WebUrlPath = UploadPathForUrl + RoomMeasurementsWebDirectory + "/" + sanitizedFileName,
                            OriginalRSContainer = RoomMeasurementsOriginalDirectory,
                            OriginalRSUrl = RoomMeasurementsOriginalRSUrl + sanitizedFileName,
                            ThumbnailRSContainer = RoomMeasurementsThumbnailDirectory,
                            ThumbnailRSUrl = RoomMeasurementsThumbnailRSUrl + sanitizedFileName,
                            WebRSContainer = RoomMeasurementsWebDirectory,
                            WebRSUrl = RoomMeasurementsWebRSUrl + sanitizedFileName
                        
                        };


                        // Save the file to disk so that we can generate the thumbnail and web images from it
                        try
                        {
                            fileData.SaveAs(@Server.MapPath(roomMeasurement.OriginalServerPath));
                        }
                        catch (Exception ex)
                        {
                            return Content("Error uploading file: " + ex.Message);
                        }


                        // if we're here, the we know that the file has been saved to disk, so let's create a thumbnail and web images.
                        try
                        {
                            // Generate a 100px x 100px thumnnail image. Set the focus as the center of the image. Crop the outsides
                            Image targetThumbnailPhoto = Image.FromFile(@Server.MapPath(roomMeasurement.OriginalServerPath));
                            Image imgThumbnailPhoto = ImageResize.Crop(targetThumbnailPhoto, 100, 100, ImageResize.AnchorPosition.Center);
                            imgThumbnailPhoto.Save(@Server.MapPath(roomMeasurement.ThumbnailServerPath), ImageFormat.Jpeg);
                            targetThumbnailPhoto.Dispose();
                            imgThumbnailPhoto.Dispose();

                            // Generate a 600px image for the web. Do not crop anything. Instead add additional padding.
                            Image targetWebPhoto = Image.FromFile(@Server.MapPath(roomMeasurement.OriginalServerPath));
                            Image imgWebPhoto = ImageResize.FixedSize(targetWebPhoto, 600, 600);
                            imgWebPhoto.Save(@Server.MapPath(roomMeasurement.WebServerPath), ImageFormat.Jpeg);
                            targetWebPhoto.Dispose();
                            imgWebPhoto.Dispose();

                        }
                        catch (Exception ex)
                        {
                            return Content("Error uploading file: " + ex.Message);
                        }
                        try
                        {
                            Room room = roomRepository.GetRoomById(id);     // fetch the current room from the database so that we can update it's last updated time
                            room.DateUpdated = DateTime.Now;                        // update the date/time
                            if (room.MenuStepId < 9)        // if the step is less than 9, then the pay for room step is complete. If it's greater than or equal to 99, the user is adding more measurement photos
                            {
                                room.MenuStepId = 9;                          // mark the room measurement upload step complete
                                room.CurrentStepId = 7;                     // mark the room measurement upload step complete
                            }
                            roomMeasurementRepository.Add(roomMeasurement);
                            roomMeasurementRepository.Save();
                            roomRepository.Save();      // save the updated room values to the DB
                        }
                        catch (Exception)
                        {
                            return Content("Error adding file to the database");
                        }
                        return Content("File uploaded successfully!");
                    }
                }
            }
            throw new InvalidOperationException("The user is not authenticated.");
        }

        /// <summary>
        /// Return a list of files in the Uploads folder back 
        /// to the $.get jQuery Ajax call.
        /// </summary>
        /// <param name="id">
        /// The id of the current room
        /// </param>
        /// <returns></returns>
        [HttpGet, Authorize]
        public ActionResult GetPhotoList(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            if (!roomRepository.DoesRoomExist(id, userName))                // Check that the currently logged on user owns the room
            {
                TempData["ErrorMessage"] = "Sorry, we were unable to find this room.";
                return View("Error");
            }
            else
            {
                IEnumerable<RoomMeasurement> roomMeasurements = roomMeasurementRepository.FindAllRoomMeasurementsForSpecificRoom(id);   // load all the room measurements for the specific room
                return View("ListView", roomMeasurements);
            }
        }

#endregion


        //
        // GET: /RoomMeasurement/5
        /// <summary>
        /// Displays the view that allows the user to navigate through the upload room measurement section.
        ///             1. Upload Photos
        ///             2. View/Edit Photos
        ///             3. Add Comments
        /// </summary>
        /// <param name="id">Room ID</param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Index(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // get the current room
            if (room == null)                // If the room does not exist, return an error
            {
                TempData["ErrorMessage"] = "Sorry, an error occured while attempting to retrieve your room.";
                return View("Error");
            }
            else
            {
                if (room.MenuStepId < 8)    // the user has not completed step 8 (pay for room)
                {
                    TempData["ErrorMessage"] = "Please pay for your room design by clicking the button below before measuring your room.";
                    return RedirectToAction("Index", "Commerce", new { id = id });      // pass the user back to the commerce section so they can pay for their room
                }
                else
                {
                    roomRepository.UpdateRoomDateUpdated(id, DateTime.Now);     //u pdate the room - This return a bool value, but we're not doing anything with this value right now
                    ViewData["RoomId"] = id;    // this is required by the room photo navigation
                    return View(room);      // otherwise return the view with the room
                }
            }
        }
                    

        //
        // GET: /RoomMeasurement/Details/5
        // parameter int id is the room ID
        [Authorize]
        public ActionResult Details(int id)
        {
            ViewData["RoomId"] = id;    // required to keep the view data room id
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            if (!roomRepository.DoesRoomExist(id, userName))                // Check that the currently logged on user owns the room
            {
                TempData["ErrorMessage"] = "Sorry, we were unable to find this room.";
                return View("Error");
            }
            else
            {
                IEnumerable<RoomMeasurement> roomMeasurements = roomMeasurementRepository.FindAllRoomMeasurementsForSpecificRoom(id);    // load all photos for the room
                return View(roomMeasurements);
            }
        }

        //
        // GET: /RoomMeasurement/Comments/5
        // Updates the room measurement details section
        [Authorize]
        public ActionResult Comments(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // get the current room
            if (room == null)                // If the room does not exist, return an error
            {
                TempData["ErrorMessage"] = "Sorry, we were unable to find this room.";
                return View("Error");
            }
            else
            {
                ViewData["RoomId"] = id;    // used by the room photo navigation
                return View(room);      // otherwise return the view with the room
            }
        }

        //
        // POST: /RoomMeasurement/Comments/5
        // Updates the room measurement details section
        [Authorize, HttpPost]
        public ActionResult Comments(int id, FormCollection collection)
        {
            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);
            ViewData["RoomId"] = id;    // used by the room photo navigation
            if (room == null)                // If the room does not exist, return an error
            {
                TempData["ErrorMessage"] = "Sorry, we were unable to find this room.";
                return View("Error");
            }
            else
            {
                room.DateUpdated = DateTime.Now;        // set the date updated to the current time
                if (ModelState.IsValid && TryUpdateModel(room))
                {
                    UpdateModel(room);
                    roomRepository.Save();
                    TempData["SuccessMessage"] = "Your room measurement comments have been successfully updated.";
                    return RedirectToAction("Comments", new { id = room.RoomId });
                }
                else
                {
                    TempData["ErrorMessage"] = "Sorry, there was a problem updating your room measurement comments.  Please try again.";
                    return View(room);
                }
            }
        }


        //
        // GET: /RoomMeasurement/Delete/5
        // where id is the id of the photo to delete
        [Authorize]
        public ActionResult Delete(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            RoomMeasurement roomMeasurement = roomMeasurementRepository.GetSingleRoomMeasurementByRoomMeasurementId(id);   // get the specific measurement from the DB that we want to delete.

            ViewData["RoomId"] = roomMeasurement.RoomId;         // used in the view to allow the user go back to a previous page
            if (roomRepository.DoesRoomExist(roomMeasurement.RoomId, userName))     // make sure the person who wants to delete this actually owns it
            {
                return View(roomMeasurement);        // return the furniture photo
            }
            else
            {
                TempData["ErrorMessage"] = "Sorry there, was a problem locating this room's measurment.";     // someone's trying to delete an image that dosn't belong to them.  give them an error
                return View("Error");
            }
        }

        //
        // POST: /RoomMeasurement/Delete/5

        [HttpPost, Authorize]
        public ActionResult Delete(int id, string confirmButton)
        {
            RoomMeasurement roomMeasurement = roomMeasurementRepository.GetSingleRoomMeasurementByRoomMeasurementId(id);     // get the specific measurement from the DB.
            string roomId = roomMeasurement.RoomId.ToString();  // extract the roomId from the room before it's deleted
            ViewData["RoomId"] = roomId;     // Set the view data since we'll use it in the next request

            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            if (!roomRepository.DoesRoomExist(roomMeasurement.RoomId, userName))                // Check that the currently logged on user owns the room
            {
                TempData["ErrorMessage"] = "Sorry, we were unable to find this room.";
                return View("Error");
            }

            Room room = roomRepository.GetRoomByIdVerifyUserName(roomMeasurement.RoomId, userName);       // get the room and verify the currenly logged on user owns it.
            if (room == null)
            {
                TempData["ErrorMessage"] = "Sorry, we were unable to find this room measurement.";      // if the room doesn't exist or the currenly logged on user does not own the room, then throw an error
                return View("Error");
            }

            try
            {
                // delete the room images
                System.IO.File.Delete(@Server.MapPath(roomMeasurement.OriginalServerPath));
                System.IO.File.Delete(@Server.MapPath(roomMeasurement.ThumbnailServerPath));
                System.IO.File.Delete(@Server.MapPath(roomMeasurement.WebServerPath));
            }
            catch (Exception)
            {
                TempData["ErrorMessage"] = "Sorry, we're unable to delete these room measurements.";
                return RedirectToAction("Details", "RoomMeasurement", new { id = roomId });
                // TODO: add logic to log this error message
            }

            try
            {
                // delete the measurement from the database
                roomMeasurementRepository.Delete(roomMeasurement);
                roomMeasurementRepository.Save();
                room.DateUpdated = DateTime.Now;        // update the last modifed date for the room
                roomRepository.Save();
            }
            catch (Exception)
            {
                TempData["ErrorMessage"] = "Sorry, we're unable to delete that room measurement";
                return RedirectToAction("Details", "RoomMeasurement", new { id = roomId });
            }

            TempData["SuccessMessage"] = "Your room measurement has been successfully deleted.";
            return RedirectToAction("Details", "RoomMeasurement", new { id = roomId });
        }

        // GET: /RoomMeasurement/DisplayToDesigner
        [Authorize(Roles = "Designers")]
        public ActionResult DisplayToDesigner(int id)
        {
            IEnumerable<RoomMeasurement> roomMeasurement = roomMeasurementRepository.FindAllRoomMeasurementsForSpecificRoom(id);
            ViewData["RoomMeasurementDetails"] = roomMeasurementRepository.GetRoomMeasurementDetails(id);
            TempData["RoomId"] = id;
            return View(roomMeasurement);
        }
    }
}
