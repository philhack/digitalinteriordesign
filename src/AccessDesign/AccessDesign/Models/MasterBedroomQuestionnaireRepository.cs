﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AccessDesign.Models
{
    public class MasterBedroomQuestionnaireRepository
    {
        private AccessDesignEntities entities = new AccessDesignEntities();

        // Return the masterBedroom questions based on the masterBedroom question id
        public MasterBedroomQuestionnaire GetMasterBedroomQuestionnaireByRoomId(int id)
        {
            return entities.MasterBedroomQuestionnaires.FirstOrDefault(m => m.RoomId == id);
        }

        public MasterBedroomQuestionnaire GetMasterBedroomQuestionnaireByMasterBedroomQuestionnaireId(int id)
        {
            return entities.MasterBedroomQuestionnaires.FirstOrDefault(m => m.MasterBedroomQuestionnaireId == id);
        }

        // Checks if the questionnaire exists based on room id, if it does, it returns true, otherwise it returns false
        public bool DoesMasterBedroomQuestionnaireExist(int id)
        {
            if (entities.MasterBedroomQuestionnaires.Count(m => m.RoomId == id) == 1)   // check if the masterBedroom questionnaire exists
                return true;    // the questionnaire exists, so return true
            else
                return false;   // the questionnaire does not exist
        }


        /// <summary>
        /// Returns a list of numbers from 1 to 7.  This can then be used in a drop down list for the number of people that share the same masterBedroom
        /// </summary>
        /// <param name="number">Number of people that share the same masterBedroom</param>
        /// <returns></returns>
        public static SelectList GetBedList(string number)
        {
            List<SelectListItem> bedList = new List<SelectListItem>() 
            {
                new SelectListItem() { Value = "", Text = "  " },
                new SelectListItem() { Value = "Two Twins", Text = " Two Twins " },
                new SelectListItem() { Value = "Full", Text = " Full " },
                new SelectListItem() { Value = "Queen", Text = " Queen " },
                new SelectListItem() { Value = "King", Text = " King " }
            };
            return new SelectList(bedList, "Value", "Text", number ?? "");
        }


        /// <summary>
        /// Returns a list of gender types.  This can then be used in a drop down list for the question: How do you want your room to feel?
        /// </summary>
        /// <param name="genderType">Gender Type</param>
        /// <returns></returns>
        public static SelectList GetGenderTypeList(string genderType)
        {
            List<SelectListItem> numberList = new List<SelectListItem>() 
            {
				new SelectListItem() { Value = "", Text = "  " },
                new SelectListItem() { Value = "Masculine", Text = " Masculine " },
                new SelectListItem() { Value = "Feminine", Text = " Feminine " },
                new SelectListItem() { Value = "Neutral", Text = " Neutral " }
            };
            return new SelectList(numberList, "Value", "Text", genderType ?? "");
        }


        #region Room Create Insert and Delete Statements
        //
        // Insert
        public void Add(MasterBedroomQuestionnaire masterBedroomQuestionnaire)
        {
            entities.MasterBedroomQuestionnaires.AddObject(masterBedroomQuestionnaire);
        }


        //
        // Delete
        public void Delete(MasterBedroomQuestionnaire masterBedroomQuestionnaire)
        {
            entities.MasterBedroomQuestionnaires.DeleteObject(masterBedroomQuestionnaire);  // delete the masterBedroom Questionnaire
        }

        //
        // Persistence
        public void Save()
        {
            entities.SaveChanges();
        }

        #endregion

    }
}