﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessDesign.Models;

namespace AccessDesign.Controllers
{
    public class ProfileController : Controller
    {
        ProfileRepository profileRepository = new ProfileRepository();
        //
        // GET: /Profile/

        [Authorize]
        public ActionResult Index()
        {
            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            bool profileExists = profileRepository.DoesProfileExist(userName);  // Check if a profile exists for the current user

            if (profileExists)  // check if the current profile exists, if it does, send the user to the edit screen, otherwise send them to the create screen
                return RedirectToAction("Edit");
            else
                return RedirectToAction("Create");
            //Response.Write("<h1>User ID = " + userName + ". Profile exists? = " + profileExists.ToString() + "</h1>");
            
        }

        //
        // GET: /Profile/Details
        [Authorize]
        public ActionResult Details()
        {
            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            Profile profile = profileRepository.GetMyProfile(userName);

            if (profile == null)
            {
                TempData["ErrorMessage"] = "Sorry, the username you have entered was not found.";
                return View("Error");
            }
            else
                return View("Details", profile);
            
        }

        //
        // GET: /Profile/Edit
        [Authorize]
        public ActionResult Edit()
        {
            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            if (profileRepository.DoesProfileExist(userName))
            {
                Profile profile = profileRepository.GetMyProfile(userName); // get the current user's profile and edit it

                // Create a country drop down list
                ViewData["Countries"] = ProfileRepository.GetCountryList(profile.Country);

                // create a state drop down list
                ViewData["States"] = profileRepository.GetStateList(profile.State);

                return View("Edit", profile);
            }
            else
                return RedirectToAction("Create");

        }

        //
        // POST: /Profile/Edit
        [Authorize, HttpPost]
        public ActionResult Edit(FormCollection formValues)
        {
            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            Profile profile = profileRepository.GetMyProfile(userName);
            profile.Zip = profile.Zip.ToUpper();        // Ensure that the zip code is saved in upper case

            ViewData["Countries"] = ProfileRepository.GetCountryList(profile.Country);          // Create a country drop down list
            ViewData["States"] = profileRepository.GetStateList(profile.State);                         // create a state drop down list

                if (ModelState.IsValid && TryUpdateModel(profile))
                {
                    profileRepository.Save();
                    TempData["SuccessMessage"] = "Your profile has been successfully updated.";
                    return RedirectToAction("Edit", "Profile");
                }
            return View(profile);
            
        }

        //
        // GET: /Profile/Create

        [Authorize]
        public ActionResult Create()
        {
            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            if(profileRepository.DoesProfileExist(userName))
                return RedirectToAction("Edit");   // This profile already exists, so edit the current profile
            else
            {
                // a profile dosn't already exist for this user, so create a new one.
                Profile profile = new Profile()
                {

                };

                ViewData["Countries"] = ProfileRepository.GetCountryList(profile.Country);          // Create a country drop down list
                ViewData["States"] = profileRepository.GetStateList(profile.State);                         // create a state drop down list

                return View(profile);
            }
        }


        //
        // POST: /Profile/Create
        [Authorize, HttpPost]
        public ActionResult Create(Profile profile)
        {
            ViewData["Countries"] = ProfileRepository.GetCountryList(profile.Country);          // Create a country drop down list
            ViewData["States"] = profileRepository.GetStateList(profile.State);                         // create a state drop down list

            if (ModelState.IsValid)
            {
                profile.UserName = HttpContext.User.Identity.Name.ToString();
                profile.DateUpdated = DateTime.Now;
                profile.Zip = profile.Zip.ToUpper();        // Ensure that the zip code is saved in upper case
                profileRepository.Add(profile);
                profileRepository.Save();
                TempData["SuccessMessage"] = "Your profile has been successfully created. Please proceed with filling in the questionnaire below";
                return RedirectToAction("Create", "GeneralQuestionnaire");
            }
            return View(profile);
        }

    }
}
