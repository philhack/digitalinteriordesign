﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<AccessDesign.Models.DesignFile>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Design Files
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Design Files</h1>
    <p><%: Html.ActionLink("upload design file", "Create", new { id=ViewData["RoomId"] })%>
    <br />
        <%: Html.ActionLink("notify client that FINAL design files are ready", "DesignComplete", "DesignFile", new { id = ViewData["RoomId"], phase = "final" }, null)%>
    <br />
        <%: Html.ActionLink("notify client that REVISED design files are ready", "DesignComplete", "DesignFile", new { id = ViewData["RoomId"], phase = "revision" }, null)%>
    <br />
        <%: Html.ActionLink("notify client that DESIGN PACKAGE has been SHIPPED", "DesignComplete", "DesignFile", new { id = ViewData["RoomId"], phase = "shipped" }, null)%>
    </p>

    <br /><br />
    <h2>Design File List</h2>
    <br />
    <table width="100%">
        <tr>
            <th width="30%" align="left">
                Design File
            </th>
            <th width="20%" align="left">
                Delete
            </th>
            <th width="50%" align="left">
                Comments
            </th>
        </tr>

    <% foreach (var item in Model) { %>
    
        <tr>
            <td>
                <a href="<%: item.UrlPath %>" target="_blank"><%: item.DesignFileType.Name %></a>
            </td>
            <td>
                <%: Html.ActionLink("Delete", "Delete", new { id=item.DesignFileId })%>
            </td>
             <td>
                <%: item.Comments %>
            </td>
        </tr>
    
    <% } %>

    </table>
    <br /><br /><br />
    <div>
        <%: Html.ActionLink("Back to Room", "Details", "Search", new { id=ViewData["RoomId"] }, null)%>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

