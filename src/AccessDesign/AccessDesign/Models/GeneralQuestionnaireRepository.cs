﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AccessDesign.Models
{
    public class GeneralQuestionnaireRepository
    {
        private AccessDesignEntities entities = new AccessDesignEntities();

        //
        // Query Methods

        // Return the general questionnaire questions based on the username
        public GeneralQuestionnaire GetGeneralQuestionnaireByUserName(string userName)
        {
            return entities.GeneralQuestionnaires.FirstOrDefault(g => g.UserName == userName);
        }

        // Checks if the questionnaire exists based on userName if it does, it returns true, otherwise it returns false
        public bool DoesGeneralQuestionnaireExist(string userName)
        {
            if (entities.GeneralQuestionnaires.Count(g => g.UserName == userName) == 1)   // check if the mediaRoom questionnaire exists
                return true;    // the questionnaire exists, so return true
            else
                return false;   // the questionnaire does not exist
        }

        // Returns the general questionnaire id based on the username that is submitted
        public int GetGeneralQuestionnaireIdByUserName(string userName)
        {
            return entities.GeneralQuestionnaires.FirstOrDefault(g => g.UserName == userName).GeneralQuestionnaireId;
        }

        #region Room Create Insert and Delete Statements
        //
        // Insert
        public void Add(GeneralQuestionnaire generalQuestionnaire)
        {
            entities.GeneralQuestionnaires.AddObject(generalQuestionnaire);
        }


        //
        // Delete
        public void Delete(GeneralQuestionnaire generalQuestionnaire)
        {
            entities.GeneralQuestionnaires.DeleteObject(generalQuestionnaire);  // delete the general questionnaire
        }

        //
        // Persistence
        public void Save()
        {
            entities.SaveChanges();
        }

        #endregion
        /// <summary>
        /// Returns a list of colors in a key value pair format that are used for checkboxes on the general questionnaire form
        /// </summary>
        /// <returns>KeyValuePair of colors</returns>
        public IEnumerable<KeyValuePair<string, string>> GetColorList()
        {
            IEnumerable<KeyValuePair<string, string>> KeyValuePairItems = new List<KeyValuePair<string, string>>()
			{
			    new KeyValuePair<string, string>("Blue", "Blue"),
                new KeyValuePair<string, string>("Green", "Green"),
                new KeyValuePair<string, string>("Orange", "Orange"),
                new KeyValuePair<string, string>("Red", "Red"),
                new KeyValuePair<string, string>("Yellow", "Yellow"),
                new KeyValuePair<string, string>("Purple", "Purple"),
                new KeyValuePair<string, string>("Pink", "Pink"),
                new KeyValuePair<string, string>("Grey", "Grey"),
                new KeyValuePair<string, string>("Brown", "Brown"),
                new KeyValuePair<string, string>("Black", "Black"),
                new KeyValuePair<string, string>("White", "White"),
                new KeyValuePair<string, string>("Other", "Other"),
			};
            return KeyValuePairItems;
        }


        
        /// <summary>
        /// Returns a list of pattern types in a key value pair format that are used for checkboxes on the general questionnaire form
        /// </summary>
        /// <returns>KeyValuePair of colors</returns>
        public IEnumerable<KeyValuePair<string, string>> GetPatternTypesList()
        {
            IEnumerable<KeyValuePair<string, string>> KeyValuePairItems = new List<KeyValuePair<string, string>>()
			{
			    new KeyValuePair<string, string>("Geometric", "Geometric"),
                new KeyValuePair<string, string>("Floral", "Floral"),
                new KeyValuePair<string, string>("Contemporary", "Contemporary"),
                new KeyValuePair<string, string>("Textural", "Textural"),
                new KeyValuePair<string, string>("Traditional", "Traditional"),
                new KeyValuePair<string, string>("Botanical", "Botanical"),
                new KeyValuePair<string, string>("None", "None"),
                new KeyValuePair<string, string>("Other", "Other"),
			};
            return KeyValuePairItems;
        }

        
    }
}