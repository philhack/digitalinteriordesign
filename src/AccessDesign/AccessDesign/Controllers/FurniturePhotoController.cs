﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessDesign.Models;
using System.Web.Hosting;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web.Security;


namespace AccessDesign.Controllers
{
    public class FurniturePhotoController : Controller
    {
        RoomRepository roomRepository = new RoomRepository();       // make a connection to the roomRepository model
        FurnitureRepository furnitureRepository = new FurnitureRepository();       // make a connection to the furnitureRepository model
        FurniturePhotoRepository furniturePhotoRepository = new FurniturePhotoRepository();    // make a connection to the Furniture Photo Repository

        private static readonly string UploadPath = System.Configuration.ConfigurationManager.AppSettings["uploadPath"];
        private static readonly string UploadPathForUrl = System.Configuration.ConfigurationManager.AppSettings["uploadPathForUrl"];

        private static readonly string RoomFurniturePhotosOriginalDirectory = System.Configuration.ConfigurationManager.AppSettings["roomFurniturePhotosOriginalDirectory"];
        private static readonly string RoomFurniturePhotosThumbnailDirectory = System.Configuration.ConfigurationManager.AppSettings["roomFurniturePhotosThumbnailDirectory"];
        private static readonly string RoomFurniturePhotosWebDirectory = System.Configuration.ConfigurationManager.AppSettings["roomFurniturePhotosWebDirectory"];
        private static readonly string RoomFurniturePhotosOriginalRSUrl = System.Configuration.ConfigurationManager.AppSettings["roomFurniturePhotosOriginalRSUrl"];
        private static readonly string RoomFurniturePhotosThumbnailRSUrl = System.Configuration.ConfigurationManager.AppSettings["roomFurniturePhotosThumbnailRSUrl"];
        private static readonly string RoomFurniturePhotosWebRSUrl = System.Configuration.ConfigurationManager.AppSettings["roomFurniturePhotosWebRSUrl"];

#region Upload Furniture Photo

        //
        // GET: /FurniturePhoto/Upload

        /// <summary>
        ///  Loads the main furniture upload photos web page
        /// </summary>
        /// <param name="id">
        /// id is the current furniture piece
        /// </param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Upload(int id)
        {
            ViewData["RoomId"] = furnitureRepository.GetRoomIdFromFurnitureId(id);      // get the room id.  This is used for the view so that we can allow the user to return to their previous view
            ViewData["FurnitureId"] = id;
            return View();
        }

        //
        // POST: /FurniturePhoto/Upload


        /// <summary>
        /// Save the posted file to the Uploads folder and return a message
        /// back to the uploadify jQuery plugin.
        /// </summary>
        /// <param name="id"></param>
        /// Accepts the id of the current furniture piece
        /// <param name="fileData"></param>
        /// <returns></returns>
        //[HttpPost, Authorize]
        [HttpPost]
        public ActionResult Upload(string token, int id, HttpPostedFileBase fileData)
        {
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(token);
            if (ticket != null)
            {
                var identity = new FormsIdentity(ticket);
                if (identity.IsAuthenticated)
                {

                    ViewData["FurnitureId"] = id;    // required to keep the view data furniture id
                    int roomId = furnitureRepository.GetRoomIdFromFurnitureId(id);      // get the room id that's associated with this room
                    ViewData["RoomId"] = roomId;        // used for the view so that the user can return to the previous screen

                    string userName = identity.Name.ToString(); // Get's the current logged on user name based on the forms authentication token
                    if (!roomRepository.DoesRoomExist(roomId, userName))    // perform a check to ensure that the logged on user is allowed to upload the photos
                    {
                        TempData["ErrorMessage"] = "Sorry, the room associated with this piece of furniture was not found in the database.";
                        return View("Error");
                    }


                    Sanitizer sanitize = new Sanitizer();   // create a new instance of the sanitizer
                    if (!sanitize.IsValidImage(fileData.FileName))   // check if the file name contains a valid file extension.  We are checking this again incase someone tampered with the javascript.
                        return Content("Error uploading file: The file must end in .jpg, .jpeg");

                    string sanitizedFileName = Sanitizer.CreateGuidRoomFurniturePhoto(fileData.FileName.ToString(), roomId, id);       // generate the file name "RoomId-FurnitureId-GUID.ext"
                    string serverPhotoPath = @Server.MapPath(UploadPath + "\\");      // used as a basis for where to save the files to

                    // setup the initial paths and store them in the database
                    // we're not saving this to the database yet until all of the images have been generated and saved
                    // to use these server paths, you must map them.  IE:   @Server.MapPath(@roomPhoto.OriginalServerPath);
                    FurniturePhoto furniturePhoto = new FurniturePhoto
                    {
                        FurnitureId = id,
                        PhotoName = sanitizedFileName,
                        OriginalServerPath = UploadPath + "\\" + RoomFurniturePhotosOriginalDirectory + "\\" + sanitizedFileName,                    // mapped path to the furniture photos original image file
                        OriginalUrlPath = UploadPathForUrl + RoomFurniturePhotosOriginalDirectory + "/" + sanitizedFileName,
                        ThumbnailServerPath = UploadPath + "\\" + RoomFurniturePhotosThumbnailDirectory + "\\" + sanitizedFileName,                 // mapped path to the furniture photos thumbnail directory
                        ThumbnailUrlPath = UploadPathForUrl + RoomFurniturePhotosThumbnailDirectory + "/" + sanitizedFileName,
                        WebServerPath = UploadPath + "\\" + RoomFurniturePhotosWebDirectory + "\\" + sanitizedFileName,                              // mapped path to the furniture photos web directory
                        WebUrlPath = UploadPathForUrl + RoomFurniturePhotosWebDirectory + "/" + sanitizedFileName,
                        OriginalRSContainer = RoomFurniturePhotosOriginalDirectory,
                        OriginalRSUrl = RoomFurniturePhotosOriginalRSUrl + sanitizedFileName,
                        ThumbnailRSContainer = RoomFurniturePhotosThumbnailDirectory,
                        ThumbnailRSUrl = RoomFurniturePhotosThumbnailRSUrl + sanitizedFileName,
                        WebRSContainer = RoomFurniturePhotosWebDirectory,
                        WebRSUrl = RoomFurniturePhotosWebRSUrl + sanitizedFileName
                    };

                    
                    // Save the file to disk so that we can generate the thumbnail and web images from it
                    try
                    {
                        fileData.SaveAs(@Server.MapPath(furniturePhoto.OriginalServerPath));
                    }
                    catch (Exception ex)
                    {
                        return Content("Error uploading file: " + ex.Message);
                    }

                        
                    // if we're here, the we know that the file has been saved to disk, so let's create a thumbnail and web images.
                    try
                    {
                        // Generate a 100px x 100px thumnnail image. Set the focus as the center of the image. Crop the outsides
                        Image targetThumbnailPhoto = Image.FromFile(@Server.MapPath(furniturePhoto.OriginalServerPath));
                        Image imgThumbnailPhoto = ImageResize.Crop(targetThumbnailPhoto, 100, 100, ImageResize.AnchorPosition.Center);
                        imgThumbnailPhoto.Save(@Server.MapPath(furniturePhoto.ThumbnailServerPath), ImageFormat.Jpeg);
                        targetThumbnailPhoto.Dispose();
                        imgThumbnailPhoto.Dispose();

                        // Generate a 600px image for the web. Do not crop anything. Instead add additional padding.
                        Image targetWebPhoto = Image.FromFile(@Server.MapPath(furniturePhoto.OriginalServerPath));
                        Image imgWebPhoto = ImageResize.FixedSize(targetWebPhoto, 600, 600);
                        imgWebPhoto.Save(@Server.MapPath(furniturePhoto.WebServerPath), ImageFormat.Jpeg);
                        targetWebPhoto.Dispose();
                        imgWebPhoto.Dispose();

                    }
                    catch (Exception ex)
                    {
                        return Content("Error uploading file: " + ex.Message);
                    }

                    try
                    {
                        Room room = roomRepository.GetRoomById(roomId);     // fetch the current room from the database so that we can update it's last updated time
                        room.DateUpdated = DateTime.Now;                                // update the date/time
                        UpdateModel(room);                                                           // update the room model
                        roomRepository.Save();                                                      // save the updated room values to the DB

                        Furniture furniture = furnitureRepository.GetFurnitureById(id);     // get the piece of furniture from the DB
                        furniture.Image = furniturePhoto.ThumbnailUrlPath;       // set the thumbnail image to be the most recent image of the group of furniture photos
                        UpdateModel(furniture);                                                     // update the furniture model
                        furnitureRepository.Save();                                                 // save the updated thumbnail to the furniture DB
                        
                        furniturePhotoRepository.Add(furniturePhoto);               // add the furniture photo to the entity framework
                        furniturePhotoRepository.Save();                                        // save it to the DB
                    }
                    catch (Exception)
                    {
                        return Content("Error adding file to the database");
                    }

                    return Content("File uploaded successfully!");
                }
            }
            throw new InvalidOperationException("The user is not authenticated.");
        }

        /// <summary>
        /// Return a list of files in the Uploads folder back 
        /// to the $.get jQuery Ajax call.
        /// </summary>
        /// <param name="id">
        /// The id of the current furinute piece
        /// </param>
        /// <returns></returns>
        [HttpGet, Authorize]
        public ActionResult GetPhotoList(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            int roomId = furnitureRepository.GetRoomIdFromFurnitureId(id);      // get the room id that's associated with this room
            ViewData["RoomId"] = roomId;        // used in the view so that the user can return to the previous screen
            if (!roomRepository.DoesRoomExist(roomId, userName))    // perform a check to ensure that the logged on user is allowed to upload the photos
            {
                TempData["ErrorMessage"] = "Sorry, the room associated with this piece of furniture was not found in the database.";
                return View("Error");
            }
            else
            {
                IEnumerable<FurniturePhoto> furniturePhotos = furniturePhotoRepository.FindAllFurniturePhotosForSpecificFurniturePiece(id);     // load all photos for the furniture piece room  
                return View("ListView", furniturePhotos);
            }
        }


        //
        // GET: /FurniturePhoto/DisplayToDesigner
        /// <summary>
        /// Displays all of the furniture photos for a piece of furniture to a designer
        /// </summary>
        /// <param name="id">furniture id</param>
        /// <returns></returns>
        [Authorize(Roles = "Designers")]
        public ActionResult DisplayToDesigner(int id)
        {
            IEnumerable<FurniturePhoto> furniturePhotos = furniturePhotoRepository.FindAllFurniturePhotosForSpecificFurniturePiece(id);
            ViewData["RoomId"] = furnitureRepository.GetRoomIdFromFurnitureId(id);      // get the room id for use in the view
            return View(furniturePhotos);
        }

#endregion


        //
        // GET: /FurniturePhoto/

        [Authorize]
        public ActionResult Index()
        {

            return View();
        }

        //
        // GET: /FurniturePhoto/Details/5
        // parameter int id is the furiture id
        [Authorize]
        public ActionResult Details(int id)
        {
            ViewData["Furniture"] = id;    // required to keep the view data room id
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name

            // Check that the currently logged on user owns the room that's associated with this piece of furniture
            int roomId = furnitureRepository.GetRoomIdFromFurnitureId(id);      // get the room id that's associated with this room
            ViewData["RoomId"] = roomId;        // used in the view to allow the user to go back to the previous screen
            if (!roomRepository.DoesRoomExist(roomId, userName))    // perform a check to ensure that the logged on user is allowed to upload the photos
            {
                TempData["ErrorMessage"] = "The room associated with this piece of furniture was not found.";
                return View("Error");
            }
            else
            {
                Furniture furniture = furnitureRepository.GetFurnitureById(id);     // get the piece of furniture so that we can display it's details
                ViewData["FurnitureName"] = furniture.Name;
                ViewData["FurnitureComments"] = furniture.Comments;
                ViewData["FurnitureWidth"] = furniture.Width;
                ViewData["FurnitureDepth"] = furniture.Depth;
                ViewData["FurnitureHeight"] = furniture.Height;

                IEnumerable<FurniturePhoto> furniturePhotos = furniturePhotoRepository.FindAllFurniturePhotosForSpecificFurniturePiece(id);     // loads all photos associated with the furniture piece
                return View(furniturePhotos);
            }
        }



        //
        // GET: /FurniturePhoto/Delete/5
        // where id is the id of the photo to delete
        [Authorize]
        public ActionResult Delete(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();
            // TODO:  Ensure that room that contains the photo actually belongs to this user.  THis also needs to be done in the RoomPhotoController
            FurniturePhoto furniturePhoto = furniturePhotoRepository.GetSingleFurniturePhotoByFurniturePhotoId(id);     // get the specific photo from the DB that we want to delete.
            int roomId = furnitureRepository.GetRoomIdFromFurnitureId(furniturePhoto.FurnitureId);      // get the room id
            ViewData["RoomId"] = roomId;        // used in the view to allow the user go back to a previous page
            if (roomRepository.DoesRoomExist(roomId, userName))     // make sure the person who wants to delete this actually owns it
            {
                return View(furniturePhoto);        // return the furniture photo
            }
            else
            {
                TempData["ErrorMessage"] = "Sorry there, was a problem locating this furniture photo.";     // someone's trying to delete an image that dosn't belong to them.  give them an error
                return View("Error");
            }
        }

        //
        // POST: /FurniturePhoto/Delete/5

        [HttpPost, Authorize]
        public ActionResult Delete(int id, string confirmButton)
        {
            FurniturePhoto furniturePhoto = furniturePhotoRepository.GetSingleFurniturePhotoByFurniturePhotoId(id); // get the specific photo from the DB.
            int furnitureId = furniturePhoto.FurnitureId;  // extract the furnitureId from the photo database entry before it's deleted
            ViewData["Furniture"] = furnitureId;     // Set the view data since we'll use it in the next request

            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            int roomId = furnitureRepository.GetRoomIdFromFurnitureId(furnitureId);      // get the room id that's associated with this room
            ViewData["RoomId"] = roomId;        // used in the view to allow the user to go back to the preivous screen
            if (!roomRepository.DoesRoomExist(roomId, userName))    // perform a check to ensure that the logged on user is allowed to upload the photos
            {
                TempData["ErrorMessage"] = "Sorry, the room associated with this piece of furniture was not found in the database.";
                return View("Error");
            }
            try
            {
                // delete the room images
                System.IO.File.Delete(@Server.MapPath(furniturePhoto.OriginalServerPath));
                System.IO.File.Delete(@Server.MapPath(furniturePhoto.ThumbnailServerPath));
                System.IO.File.Delete(@Server.MapPath(furniturePhoto.WebServerPath));
            }
            catch (Exception)
            {
                TempData["ErrorMessage"] = "Sorry, we're unable to delete the photo associated with this piece of furniture.";
                return RedirectToAction("Delete", new { photoId = id });
                // TODO: add logic to log this error message
            }

            try
            {
                // delete the furniture photo from the database
                furniturePhotoRepository.Delete(furniturePhoto);
                furniturePhotoRepository.Save();
            }
            catch (Exception)
            {
                TempData["ErrorMessage"] = "Sorry, we're unable to delete that photo";
                return RedirectToAction("Details", new { photoId = furnitureId });
            }

            TempData["SuccessMessage"] = "Your furniture photo has been successfully deleted.";
            return RedirectToAction("Details", new { id = furnitureId });
        }
    }
}
