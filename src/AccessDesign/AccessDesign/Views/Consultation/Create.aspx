﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Consultation>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Book Appointment
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>

<% if (TempData["ConsultationType"] == "initial")
       { %>
        <h1>Book Free 30 Minute Consultation</h1>
        <p>Please use the form below to book your free 30 minute consultation with one of our interior designers.</p>
    <% } else { %>
    <h1>Book Follow-up Consultation</h1>
        <p>Please use the form below to book your follow up consultation with one of our interior designers.</p>
    <% } %>
    
    <br />
    <% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm()) {%>

        <% Html.RenderPartial("EditFields", Model); %>

        <br />
        <br />
        <div id="standard-button">
                        <input type="submit"  class="button" value="Create" />
                </div> 

    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="/Scripts/datepicker.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="/Content/datepicker.css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
    <ul></ul>
</asp:Content>