﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<AccessDesign.Models.Room>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	My Rooms
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>My Rooms</h1>
    <br /><br />
    <p>Below is a listing of all of the rooms that your have created.  You can change the name of your room or delete your room below:</p>
    <br />
    <table width="60%">
        <tr>
            <th width="60%" align="left">Name</th>
            <th width="40%" align="left">Actions</th>
        </tr>

        <% foreach (var item in Model) { %>
    
            <tr>
                <td>
                    <%: item.Name %>
                </td>
                <td>
                    <%: Html.ActionLink("Change Name", "Edit", new { id=item.RoomId }) %> |
                    <%: Html.ActionLink("Delete", "Delete", new { id=item.RoomId })%>
                </td>
            </tr>
    
        <% } %>

    </table>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SidebarContent" runat="server">
</asp:Content>

