﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessDesign.Models;
using AccessDesign.Helpers;

namespace AccessDesign.Controllers
{
    public class GeneralQuestionnaireController : Controller
    {
        GeneralQuestionnaireRepository generalQuestionnaireRepository = new GeneralQuestionnaireRepository();
        ProfileRepository profileRepository = new ProfileRepository();
        RoomRepository roomRepository = new RoomRepository();

        //
        // GET: /GeneralQuestionnaire/
        [Authorize]
        public ActionResult Index()
        {
            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            bool questionnaireExists = generalQuestionnaireRepository.DoesGeneralQuestionnaireExist(userName);      // check if a general questionnaire exists for the currenly logged on user
            bool profileExists = profileRepository.DoesProfileExist(userName);

            if (questionnaireExists && profileExists)  // check if the current general questionnaire exists, if it does, send the user to the edit screen, otherwise send them to the create screen
            {
                return RedirectToAction("Edit");
            }
            else if (!profileExists)        // if the user has not completed their profile, redirect them to the profile creation page with an error message.
            {
                TempData["ErrorMessage"] = "Please fill in your profile information before you fill in your questionnaire.";
                return RedirectToAction("Create", "Profile");
            }
            else
            {
                return RedirectToAction("Create");
            }
        }

        //
        // GET: /GeneralQuestionnaire/user@domain.com
        // Displays the general questionnaire details of the user account in question to a designer
        [Authorize(Roles = "Designers")]
        public ActionResult Details(string id)
        {
            // check if the general questionnare exists for the username that was specified
            if (generalQuestionnaireRepository.DoesGeneralQuestionnaireExist(id))
            {
                GeneralQuestionnaire generalQuestionnaire = generalQuestionnaireRepository.GetGeneralQuestionnaireByUserName(id);   // retrieve the general questionnaire from the database
                return View("Details", generalQuestionnaire);       // return the general questionnaire to the view
            }
            else
            {
                // The questionnaire does not exist so thow an error
                TempData["ErrorMessage"] = "A general questionnaire does not exist for the username you specified. Please try another username or ask the client to fill out the general questionnaire";
                return View("Error");
            }
        }

        //
        // GET: /GeneralQuestionnaire/Create
        [Authorize]
        public ActionResult Create()
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            if(profileRepository.DoesProfileExist(userName))    // check if a profile exists for the currently logged on user. If it does, let them complete the questionnaire
            {

                // setup the colors you love lists
                ViewData["ColorsLoveKeyValuePairItems"] = generalQuestionnaireRepository.GetColorList();      // get a list of colors to be displayed as checkboxes in the questionnaire
                IEnumerable<string> ColorsLoveSelectedValues = new List<string>() { };        // create a blank list of selected values
                ViewData["ColorsLoveSelectedValues"] = ColorsLoveSelectedValues;                                  // assign the list as viewdata so that it can be utlized by the view

                // setup the colors you dislike list
                ViewData["ColorsDislikeKeyValuePairItems"] = generalQuestionnaireRepository.GetColorList();      // get a list of colors to be displayed as checkb
                IEnumerable<string> ColorsDislikeSelectedValues = new List<string>() { };        // create a blank list of selected values
                ViewData["ColorsDislikeSelectedValues"] = ColorsDislikeSelectedValues;                                  // assign the list as viewdata so that it can be utlized by the view


                // setup the patteren types lists
                ViewData["PatternTypesKeyValuePairItems"] = generalQuestionnaireRepository.GetPatternTypesList();      // get a list of pattern types to be displayed as checkboxes in the questionnaire
                IEnumerable<string> PatternTypesSelectedValues = new List<string>() { };        // create a blank list of selected values
                ViewData["PatternTypesSelectedValues"] = PatternTypesSelectedValues;                                  // assign the list as viewdata so that it can be utlized by the view



                if (generalQuestionnaireRepository.DoesGeneralQuestionnaireExist(userName))
                {
                    return RedirectToAction("Edit");   // This questionnaire already exists, so edit the current profile
                }
                else
                {
                    GeneralQuestionnaire generalQuestionnaire = new GeneralQuestionnaire()
                    {
                    };
                    return View(generalQuestionnaire);      // return a view containing the newly created questionnaire
                }
            }
            else         // a profile doesn't exist for this user, profile them with an error message and advise them to create a profile before filling out the questionnaire
            {
                TempData["ErrorMessage"] = "Please fill in your profile information below before you fill in your questionnaire.";      // create an error message
                return RedirectToAction("Create","Profile");       // send the user to the create profile page.
            }
        }

        //
        // POST: /GeneralQuestionnaire/Create
        // Save the newly created questionnaire to the database
        [HttpPost, Authorize]
        public ActionResult Create(GeneralQuestionnaire generalQuestionnaire, string[] colorsYouLoveList, string[] colorsYouDislikeList, string[] patternTypesList)
        {
            // setup the colors you love checkboxes
            ViewData["ColorsLoveKeyValuePairItems"] = generalQuestionnaireRepository.GetColorList();      // get a list of colors to be displayed as checkboxes in the questionnaire
            ViewData["ColorsLoveSelectedValues"] = null;      // initialize the selected values

            // setup the colors you dislike checkboxes
            ViewData["ColorsDislikeKeyValuePairItems"] = generalQuestionnaireRepository.GetColorList();      // get a list of colors to be displayed as checkboxes in the questionnaire
            ViewData["ColorsDislikeSelectedValues"] = null;      // initialize the selected values

            // setup the pattern type checkboxes
            ViewData["PatternTypesKeyValuePairItems"] = generalQuestionnaireRepository.GetPatternTypesList();      // get a list of colors to be displayed as checkboxes in the questionnaire
            ViewData["PatternTypesSelectedValues"] = null;      // initialize the selected values

            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            generalQuestionnaire.UserName = userName;                               // assign this questionnaire to the currenly logged on user

            if (colorsYouLoveList == null)
            {
                // setup the colors you love checkboxes
                string colorsYouLoveListForDB = "";         // set the string to blank
                generalQuestionnaire.ColorsYouLove = colorsYouLoveListForDB;        // asign the string
                ViewData["ColorsLoveSelectedValues"] = colorsYouLoveList;                 // assign a blank string to viewdata

            }
            else
            {
                // setup colors you love checkboxes
                string colorsYouLoveListForDB = String.Join(",", colorsYouLoveList);
                generalQuestionnaire.ColorsYouLove = colorsYouLoveListForDB;    // save the users selection of the colors as a joined string to the entity
                ViewData["ColorsLoveSelectedValues"] = colorsYouLoveList;     // set the view data so that it is ready if/when we return the user to the view
            }

            if (colorsYouDislikeList == null)
            {
                // setup the colors you dislike checkboxes
                string colorsYouDislikeListForDB = "";         // set the string to blank
                generalQuestionnaire.ColorsYouDislike = colorsYouDislikeListForDB;        // asign the string
                ViewData["ColorsDislikeSelectedValues"] = colorsYouDislikeList;                 // assign a blank string to viewdata
            }
            else
            {
                // setup colors you dislike checkboxes
                string colorsYouDislikeListForDB = String.Join(",", colorsYouDislikeList);
                generalQuestionnaire.ColorsYouDislike = colorsYouDislikeListForDB;    // save the users selection of the colors as a joined string to the entity
                ViewData["ColorsDislikeSelectedValues"] = colorsYouDislikeList;     // set the view data so that it is ready if/when we return the user to the view
            }

            if (patternTypesList == null)
            {
                // setup the pattern types checkboxes
                string patternTypesListForDB = "";         // set the string to blank
                generalQuestionnaire.TypesOfPatternsYouLike = patternTypesListForDB;        // asign the string
                ViewData["PatternTypesSelectedValues"] = patternTypesList;                 // assign a blank string to viewdata
            }
            else
            {
                // setup colors you dislike checkboxes
                string patternTypesListForDB = String.Join(",", patternTypesList);
                generalQuestionnaire.TypesOfPatternsYouLike = patternTypesListForDB;    // save the users selection of the colors as a joined string to the entity
                ViewData["PatternTypesSelectedValues"] = patternTypesList;     // set the view data so that it is ready if/when we return the user to the view
            }

            if (ModelState.IsValid)
            {
                if (!(generalQuestionnaireRepository.DoesGeneralQuestionnaireExist(userName)))       // perform a test again to ensure that the questionnaire does not already exist for this user
                {
                    generalQuestionnaireRepository.Add(generalQuestionnaire);           // save the questionnaire
                    generalQuestionnaireRepository.Save();
                    TempData["SuccessMessage"] = "Your questionnaire has been successfully completed. Please start creating your room.";
                    return RedirectToAction("Create", "Room");
                }
                else
                {
                    TempData["ErrorMessage"] = "Sorry, an unknown error occured while creating your questionnaire.";
                    return View(generalQuestionnaire);
                }
            }
             TempData["ErrorMessage"] = "Please fill in the required fields below.";        // default action that should not ever occur, but incase an odd scenario happens, this will display a nice error to our user
            return View(generalQuestionnaire);
        }

        //
        // /GeneralQuestionnaire/Edit
        public ActionResult Edit()
        {
            // setup the colors you love checkboxes
            ViewData["ColorsLoveKeyValuePairItems"] = generalQuestionnaireRepository.GetColorList();      // get a list of colors to be displayed as checkboxes in the questionnaire
            ViewData["ColorsLoveSelectedValues"] = null;      // initialize the selected values

            // setup the colors you dislike checboxes
            ViewData["ColorsDislikeKeyValuePairItems"] = generalQuestionnaireRepository.GetColorList();      // get a list of colors to be displayed as checkboxes in the questionnaire
            ViewData["ColorsDislikeSelectedValues"] = null;      // initialize the selected values

            // setup thepattern types checboxes
            ViewData["PatternTypesKeyValuePairItems"] = generalQuestionnaireRepository.GetPatternTypesList();      // get a list of the pattern types to be displayed as checkboxes in the questionnaire
            ViewData["PatternTypesSelectedValues"] = null;      // initialize the selected values

            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            if (generalQuestionnaireRepository.DoesGeneralQuestionnaireExist(userName))
            {
                GeneralQuestionnaire generalQuestionnaire = generalQuestionnaireRepository.GetGeneralQuestionnaireByUserName(userName); // gets the current users general questionnaire to edit it
                string selectedColorsYouLove = generalQuestionnaire.ColorsYouLove;      // load the colors you love from the db
                string selectedColorsYouDislike = generalQuestionnaire.ColorsYouDislike;    // load the color you dislike from the db
                string selectedPatternTypes = generalQuestionnaire.TypesOfPatternsYouLike;  // load the pattern types you like from the db

                if (selectedColorsYouLove != null)
                {
                    // setup the viewdata for the colors you love:
                    string[] selectedColorsYouLoveSplit = selectedColorsYouLove.Split(',');
                    List<string> ColorsLikeStringtoList = new List<string>(selectedColorsYouLoveSplit.Length);
                    ColorsLikeStringtoList.AddRange(selectedColorsYouLoveSplit);
                    IEnumerable<string> ColorsLoveSelectedValues = ColorsLikeStringtoList;
                    ViewData["ColorsLoveSelectedValues"] = ColorsLoveSelectedValues;
                }

                if (selectedColorsYouDislike != null)
                {
                    // setup the viewdata for the colors you dislike:
                    string[] selectedColorsYouDislikeSplit = selectedColorsYouDislike.Split(',');
                    List<string> ColorsDislikeStringtoList = new List<string>(selectedColorsYouDislikeSplit.Length);
                    ColorsDislikeStringtoList.AddRange(selectedColorsYouDislikeSplit);
                    IEnumerable<string> ColorsDislikeSelectedValues = ColorsDislikeStringtoList;
                    ViewData["ColorsDislikeSelectedValues"] = ColorsDislikeSelectedValues;
                }

                if (selectedPatternTypes != null)
                {
                    // setup the viewdata for the pattern types:
                    string[] selectedPatternTypesSplit = selectedPatternTypes.Split(',');
                    List<string> PatternTypesStringtoList = new List<string>(selectedPatternTypesSplit.Length);
                    PatternTypesStringtoList.AddRange(selectedPatternTypesSplit);
                    IEnumerable<string> PatternTypesSelectedValues = PatternTypesStringtoList;
                    ViewData["patternTypesSelectedValues"] = PatternTypesSelectedValues;
                }

                // check if the user has created a room.  This information is used by the edit view to display the current progress bar.
                if (roomRepository.GetNumberOfRoomsForUser(userName) > 0)
                {
                    ViewData["CreatedRooms"] = true;    // the user has 1 or more rooms that they created
                }
                else
                {
                    ViewData["CreatedRooms"] = false;   // the user has not created any rooms.
                }

                return View("Edit", generalQuestionnaire);
            }
            else
                return RedirectToAction("Create");
        }

        //
        // POST: /GeneralQuestionnaire/Edit
        [HttpPost, Authorize]
        public ActionResult Edit(FormCollection collection, string[] colorsYouLoveList, string[] colorsYouDislikeList, string[] patternTypesList)
        {
            // setup colors you love checkbox viewdata
            ViewData["ColorsLoveKeyValuePairItems"] = generalQuestionnaireRepository.GetColorList();      // get a list of colors to be displayed as checkboxes in the questionnaire
            ViewData["ColorsLoveSelectedValues"] = null;      // initialize the selected values

            // setup colors you dislike checkbox viewdata
            ViewData["ColorsDislikeKeyValuePairItems"] = generalQuestionnaireRepository.GetColorList();      // get a list of colors to be displayed as checkboxes in the questionnaire
            ViewData["ColorsDislikeSelectedValues"] = null;      // initialize the selected values

            // setup pattern types checkbox viewdata
            ViewData["PatternTypesKeyValuePairItems"] = generalQuestionnaireRepository.GetPatternTypesList();      // get a list of pattern types to be displayed as checkboxes in the questionnaire
            ViewData["PatternTypesSelectedValues"] = null;      // initialize the selected values

            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            GeneralQuestionnaire generalQuestionnaire = generalQuestionnaireRepository.GetGeneralQuestionnaireByUserName(userName);     // load the questionnaire

            if (colorsYouLoveList == null)
            {
                string colorsYouLoveListForDB = "";         // set the string to blank
                generalQuestionnaire.ColorsYouLove = colorsYouLoveListForDB;        // asign the string
                ViewData["ColorsLoveSelectedValues"] = colorsYouLoveList;                 // assign to viewdata
            }
            else
            {
                string colorsYouLoveListForDB = String.Join(",", colorsYouLoveList);
                generalQuestionnaire.ColorsYouLove = colorsYouLoveListForDB;    // save the users selection of the colors as a joined string to the entity
                ViewData["ColorsLoveSelectedValues"] = colorsYouLoveList;     // set the view data so that it is ready if/when we return the user to the view
            }

            if (colorsYouDislikeList == null)
            {
                string colorsYouDislikeListForDB = "";         // set the string to blank
                generalQuestionnaire.ColorsYouDislike = colorsYouDislikeListForDB;        // asign the string
                ViewData["ColorsDislikeSelectedValues"] = colorsYouDislikeList;                 // assign to viewdata
            }
            else
            {
                string colorsYouDislikeListForDB = String.Join(",", colorsYouDislikeList);
                generalQuestionnaire.ColorsYouDislike = colorsYouDislikeListForDB;    // save the users selection of the colors as a joined string to the entity
                ViewData["ColorsDislikeSelectedValues"] = colorsYouDislikeList;     // set the view data so that it is ready if/when we return the user to the view
            }

            if (patternTypesList == null)
            {
                string patternTypesListForDB = "";         // set the string to blank
                generalQuestionnaire.TypesOfPatternsYouLike = patternTypesListForDB;        // asign the string
                ViewData["PatternTypesSelectedValues"] = patternTypesList;                 // assign to viewdata
            }
            else
            {
                string patternTypesListForDB = String.Join(",", patternTypesList);
                generalQuestionnaire.TypesOfPatternsYouLike = patternTypesListForDB;    // save the users selection of the colors as a joined string to the entity
                ViewData["PatternTypesSelectedValues"] = patternTypesList;     // set the view data so that it is ready if/when we return the user to the view
            }
            
            if (ModelState.IsValid)
            {
                if (TryUpdateModel(generalQuestionnaire))
                {
                    generalQuestionnaireRepository.Save();
                    TempData["SuccessMessage"] = "Your questionnaire has been successfully updated.";       // set the status message
                    return RedirectToAction("Edit");
                }
            }
            TempData["ErrorMessage"] = "Sorry, there was an error updating your questionnaire.";
            return View(generalQuestionnaire);
        }
    }
}
