﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Invoice>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Order Complete
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Thank You, Your Order is Complete</h1>
    <div id="room-photo-nav">
        <a href="/Commerce/PrintableInvoice/<%: Model.RoomId  %>" target="_new"><img src="/Content/Images/apps-steps-view-invoice-01.png" alt="View printable invoice" border="0"  class="room-photo-nav-image" /></a><img src="/Content/Images/app-steps-blank-02.png" alt="Blank Step 2" border="0" /><img src="/Content/Images/app-steps-blank-03.png" alt="Blank Step 3" border="0" />
     </div>
     <br />
    <p>Your order has been sucessfully recieved. Our designers will continue to work on designing your room. Thank you for your business.</p>

    <h5>View Your Invoice</h5>
    <p>Click <b><%: Html.ActionLink("here","PrintableInvoice", new {id = Model.RoomId}, new { @target="_new" } )%></b> to view your printable invoice. 
    You can view your invoice for this room at any time by click on "Pay For Room" using the navigation menu on the left side of the screen.</p>

    <h5>Next Step: Measure Your Room</h5>
    <p><%: Html.ActionLink("Continue to the next step: Measure your room", "Index", "RoomMeasurement", new { id = Model.RoomId }, new { })%></p>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(function () {

            $(".room-photo-nav-image")
            .mouseover(function () {
                var src = $(this).attr("src").match(/[^\.]+/) + "TreomaOn.png";
                $(this).attr("src", src);
            })
            .mouseout(function () {
                var src = $(this).attr("src").replace("TreomaOn", "");
                $(this).attr("src", src);
            });

        });
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

