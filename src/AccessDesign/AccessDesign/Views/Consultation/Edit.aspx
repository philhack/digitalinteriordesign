﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Consultation>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Update Appointment
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">    
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>

    <% if (TempData["ConsultationType"] == "initial")
       { %>
        <h1>Update Free 30 Minute Consultation</h1>
    <% } else { %>
    <h1>Update Follow-up Consultation</h1>
    <% } %>
    <p>Please use the form below to change your appointment time.</p>
    
    <br />
    <% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm()) {%>        
        <% Html.RenderPartial("EditFields", Model); %>
            
        <br />
        <br />
        <div id="standard-button">
            <input type="submit"  class="button" value="Update" />
        </div>     
        

    <% } %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="/Scripts/datepicker.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="/Content/datepicker.css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

