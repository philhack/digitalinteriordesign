﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.LogOnModel>" %>

<asp:Content ID="loginTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Log In - Digital Interior Design
</asp:Content>

<asp:Content ID="loginContent" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Log In To Your Account</h1>
    <p>
        Please enter your username and password to log into your Digital Interior Design account. If you don't already have an account, <%: Html.ActionLink("register", "Register") %> for your free account.
    </p>
    <br />
    <%: Html.ValidationSummary() %>
    <br />
     <% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm()) { %>
        <div>                
                <div class="text-box-label-same-line">
                    <%: Html.LabelFor(m => m.UserName) %>
                </div>
                <div class="text-box">
                    <%: Html.TextBoxFor(m => m.UserName, new { @class = "txtbox", @size = 25 })%>
                    <%: Html.ValidationMessageFor(m => m.UserName) %>
                </div>
                <br />
                
                <div class="text-box-label-same-line">
                    <%: Html.LabelFor(m => m.Password) %>
                </div>
                <div class="text-box">
                    <%: Html.PasswordFor(m => m.Password, new { @class = "txtbox", @size = 25 })%>
                    <%: Html.ValidationMessageFor(m => m.Password) %>
                </div>
                <br />
                <div class="check-box-label-same-line">
                    <%: Html.LabelFor(m => m.RememberMe) %>
                </div>
                <div class="checkbox-remember-me">
                    <%: Html.CheckBoxFor(m => m.RememberMe) %>
                </div>

                <br /><br /><br />
                <div id="standard-button">
                    <input type="submit"  class="button" value="Log In" />
                 </div>        
        </div>
        
    <% } %>
    <br /><br /><br />
    <p>Forgot your password? Click <%: Html.ActionLink("here", "ResetPassword") %> to reset your password.</p>
</asp:Content>

<asp:Content ID="logOnSideBar" ContentPlaceHolderID="SideBarContent" runat="server">
    <ul>
        <li><%: Html.ActionLink("CREATE ACCOUNT", "Register", "Account")%></li>
        <li><%: Html.ActionLink("LOGIN", "LogOn", "Account", new { @class="selected" })%></li>
    </ul>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <meta name="description" content="Login to your Digital Interior Design  account." />
    <meta name="keywords" content="digital interior design, online interior design, digial interior design log in, interior design, interior decorating, edesign, e-design" />
    <style type="text/css">
        .validation-summary-errors ul {display:inline; font-weight:normal}    
    </style>
</asp:Content>