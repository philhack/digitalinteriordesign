﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<div class="wrapper">
  <div id="header">
    <div id="logo"><a href="http://www.digitalinteriordesign.com"><img src="/Content/Images/digital-interior-design-logo.png" alt="Digital Interior Design Logo" /></a></div>
    <div id="login-box">
      <ul>
        <% if (Context.User != null && Context.User.Identity != null && Context.User.Identity.IsAuthenticated) { %>
            <li><%: Html.ActionLink("logout", "LogOff", "Account")%></li>
        <% } else { %>
        <li><%: Html.ActionLink("login", "LogOn", "Account") %></li>
        <li><%: Html.ActionLink("create account", "Register", "Account")%></li>
        <% } %>
      </ul>
    </div>
  </div>
  <!-- end header div --></div>