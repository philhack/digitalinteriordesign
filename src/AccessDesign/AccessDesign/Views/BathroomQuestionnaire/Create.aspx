﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.BathroomQuestionnaire>" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Bathroom Questionnaire
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Bathroom Questionnaire</h1>
    <br />
    <div id="app-steps-generic">
              <a href="/Questionnaire/Index/<%: Model.RoomId %>"><img src="/Content/Images/app-steps-questionnaire-1-blue-unchecked.png" alt="" border="0" /></a><a href="/RoomPhoto/Index/<%: Model.RoomId %>"><img src="/Content/Images/app-steps-questionnaire-2-grey.png" alt="" border="0"/></a><a href="/Furniture/Index/<%: Model.RoomId %>"><img src="/Content/Images/app-steps-questionnaire-3-grey.png" alt="" border="0"/></a>
            <h6 class="step-description">STEP 1/3 COMPLETE. PLEASE COMPLETE STEP 1 - BATHROOM QUESTIONNAIRE</h6>
    </div>
    <br /><br />
    <p>Please fill in the bathroom questionnaire below to the best of your ability.  This questionnaire is specific to the bathroom that you want designed.  If you don’t have time to complete the questionnaire all at once, fill in as much information as you can, ensure that all of the required fields have information in them and click "Submit".</p>
    <br />
    <p>You can view and update this questionnaire by clicking <i>"Room Questionnaire"</i> on the left side menu.</p>
    <br />
    <div class="text-box"><span class="required-field">*</span> Required fields.</div>
    <br /><br />

    <% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm()) {%>

            <%: Html.HiddenFor(model => model.RoomId) %>            

            <% Html.RenderPartial("EditFields", Model); %>

            <%: Html.ValidationSummary(false, "*Please fill in all the required questions above and then click submit.")%>   
            <br />
            
            <div id="standard-button">
                    <input type="submit"  class="button" value="Submit" />
            </div> 

    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">

    <script src="/Scripts/MvcFoolproofValidation.js" type="text/javascript"></script>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

