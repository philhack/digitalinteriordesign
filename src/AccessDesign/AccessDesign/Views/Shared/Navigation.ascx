﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<div id="top-nav">
  <div id="top-nav-content">
    <ul>
      <% if (Context.User != null && Context.User.Identity != null && Context.User.Identity.IsAuthenticated) { %>
      <li><%: Html.ActionLink("my account", "Index", "Profile")%></li> <% } %>
      <li><a href="http://www.digitalinteriordesign.com/">home</a></li>
      <li><a href="http://www.digitalinteriordesign.com/philosophy">about us</a></li>
      <li><a href="http://www.digitalinteriordesign.com/faq">faq</a></li>
      <li><a href="http://www.digitalinteriordesign.com/howitworks">how it works</a></li>
      <li><a href="http://www.digitalinteriordesign.com/pricing">pricing</a></li>
      <li><a href="http://www.digitalinteriordesign.com/examples">examples</a></li>
      <li><a href="http://www.digitalinteriordesign.com/contact">contact</a></li>
      <li><a href="http://www.digitalinteriordesign.com/blog">blog</a></li>
    </ul>
    <div id="top-nav-social">
      <div><a href="http://www.facebook.com/pages/Treoma-Design/176496522363192" id="facebook-top" name="facebook-top" target="_blank"></a></div>
      <div><a href="http://twitter.com/Treoma_Design" id="twitter-top" name="twitter-top" target="_blank"></a></div>
    </div>
  </div>
</div>
