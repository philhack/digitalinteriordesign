﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Room>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Pay For Room Design
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Pay For Room Design</h1>
    <div id="room-photo-nav">
        <a href="/Commerce/PrintableInvoice/<%: Model.RoomId  %>" target="_new"><img src="/Content/Images/apps-steps-view-invoice-01.png" alt="View printable invoice" border="0" class="room-photo-nav-image" /></a><img src="/Content/Images/app-steps-blank-02.png" alt="Blank Step 2" border="0" /><img src="/Content/Images/app-steps-blank-03.png" alt="Blank Step 3" border="0" />
     </div>
     <br />
    <p>Your room has already been paid for.  Click <%: Html.ActionLink("here","PrintableInvoice", new {id = Model.RoomId}, new { @target="_new" } )%> to view your invoice.</p>
    <% if (Model.MenuStepId == 8)
       { %>
       <br />
       <p><%: Html.ActionLink("Continue to the next step: Measure your room", "Index", "RoomMeasurement", new { id = Model.RoomId }, new { })%></p>
    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(function () {
            
            $(".room-photo-nav-image")
            .mouseover(function () {
                var src = $(this).attr("src").match(/[^\.]+/) + "TreomaOn.png";
                $(this).attr("src", src);
            })
            .mouseout(function () {
                var src = $(this).attr("src").replace("TreomaOn", "");
                $(this).attr("src", src);
            });

        });
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

