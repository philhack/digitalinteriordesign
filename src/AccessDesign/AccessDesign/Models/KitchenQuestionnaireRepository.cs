﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AccessDesign.Models
{
    public class KitchenQuestionnaireRepository
    {
        private AccessDesignEntities entities = new AccessDesignEntities();

        // Return the kitchen questions based on the kitchen question id
        public KitchenQuestionnaire GetKitchenQuestionnaireByRoomId(int id)
        {
            return entities.KitchenQuestionnaires.FirstOrDefault(b => b.RoomId == id);
        }

        public KitchenQuestionnaire GetKitchenQuestionnaireByKitchenQuestionnaireId(int id)
        {
            return entities.KitchenQuestionnaires.FirstOrDefault(b => b.KitchenQuestionnaireId == id);
        }

        // Checks if the questionnaire exists based on room id, if it does, it returns true, otherwise it returns false
        public bool DoesKitchenQuestionnaireExist(int id)
        {
            if (entities.KitchenQuestionnaires.Count(u => u.RoomId == id) == 1)   // check if the kitchen questionnaire exists
                return true;    // the questionnaire exists, so return true
            else
                return false;   // the questionnaire does not exist
        }

        // ************* INSERT YOUR DROP DOWN LISTS HERE ***************************
        /// <summary>
        /// Returns a list of gender types.  This can then be used in a drop down list for the question: How do you want your room to feel?
        /// </summary>
        /// <param name="genderType">Gender Type</param>
        /// <returns></returns>
        public static SelectList GetGenderTypeList(string genderType)
        {
            List<SelectListItem> genderList = new List<SelectListItem>() 
            {
				new SelectListItem() { Value = "", Text = "  " },
                new SelectListItem() { Value = "Masculine", Text = " Masculine " },
                new SelectListItem() { Value = "Feminine", Text = " Feminine " },
                new SelectListItem() { Value = "Neutral", Text = " Neutral " }
            };
            return new SelectList(genderList, "Value", "Text", genderType ?? "");
        }


        /// <summary>
        /// Returns a list of oven types.  This can then be used in a drop down list for the question: Do you have or want a gas oven, electric oven or a convection oven?
        /// </summary>
        /// <param name="ovenType">The type of oven</param>
        /// <returns></returns>
        public static SelectList GetOvenTypeList(string ovenType)
        {
            List<SelectListItem> ovenList = new List<SelectListItem>() 
            {
				new SelectListItem() { Value = "", Text = "  " },
                new SelectListItem() { Value = "Gas", Text = " Gas " },
                new SelectListItem() { Value = "Electric", Text = " Electric " },
                new SelectListItem() { Value = "Convection", Text = " Convection " }
            };
            return new SelectList(ovenList, "Value", "Text", ovenType ?? "");
        }

        





        #region Room Create Insert and Delete Statements
        //
        // Insert
        public void Add(KitchenQuestionnaire kitchenQuestionnaire)
        {
            entities.KitchenQuestionnaires.AddObject(kitchenQuestionnaire);
        }


        //
        // Delete
        public void Delete(KitchenQuestionnaire kitchenQuestionnaire)
        {
            entities.KitchenQuestionnaires.DeleteObject(kitchenQuestionnaire);  // delete the kitchen Questionnaire
        }

        //
        // Persistence
        public void Save()
        {
            entities.SaveChanges();
        }

        #endregion

    }
}