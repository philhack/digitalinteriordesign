﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AccessDesign.Models
{
    public class KidsRoomQuestionnaireRepository
    {
        private AccessDesignEntities entities = new AccessDesignEntities();

        // Return the kidsRoom questions based on the kidsRoom question id
        public KidsRoomQuestionnaire GetKidsRoomQuestionnaireByRoomId(int id)
        {
            return entities.KidsRoomQuestionnaires.FirstOrDefault(k => k.RoomId == id);
        }

        public KidsRoomQuestionnaire GetKidsRoomQuestionnaireByKidsRoomQuestionnaireId(int id)
        {
            return entities.KidsRoomQuestionnaires.FirstOrDefault(k => k.KidsRoomQuestionnaireId == id);
        }

        // Checks if the questionnaire exists based on room id, if it does, it returns true, otherwise it returns false
        public bool DoesKidsRoomQuestionnaireExist(int id)
        {
            if (entities.KidsRoomQuestionnaires.Count(k => k.RoomId == id) == 1)   // check if the kidsRoom questionnaire exists
                return true;    // the questionnaire exists, so return true
            else
                return false;   // the questionnaire does not exist
        }

        /// <summary>
        /// Returns a list of gender types.  This can then be used in a drop down list for the question: How do you want your room to feel?
        /// </summary>
        /// <param name="genderType">Gender Type</param>
        /// <returns></returns>
        public static SelectList GetGenderTypeList(string genderType)
        {
            List<SelectListItem> numberList = new List<SelectListItem>() 
            {
				new SelectListItem() { Value = "", Text = "  " },
                new SelectListItem() { Value = "Masculine", Text = " Masculine " },
                new SelectListItem() { Value = "Feminine", Text = " Feminine " },
                new SelectListItem() { Value = "Neutral", Text = " Neutral " }
            };
            return new SelectList(numberList, "Value", "Text", genderType ?? "");
        }

        #region Room Create Insert and Delete Statements
        //
        // Insert
        public void Add(KidsRoomQuestionnaire kidsRoomQuestionnaire)
        {
            entities.KidsRoomQuestionnaires.AddObject(kidsRoomQuestionnaire);
        }


        //
        // Delete
        public void Delete(KidsRoomQuestionnaire kidsRoomQuestionnaire)
        {
            entities.KidsRoomQuestionnaires.DeleteObject(kidsRoomQuestionnaire);  // delete the kidsRoom Questionnaire
        }

        //
        // Persistence
        public void Save()
        {
            entities.SaveChanges();
        }

        #endregion

    }
}