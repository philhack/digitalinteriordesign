﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<div id="footer">
   <div id="footer-boxes">
    <div class="footer-box">
      <h3 class="footer-box-hd">ABOUT TREOMA</h3>
      <h6 class="footer-box-txt">Treoma Design is our in-person design service offered in the Vancouver area. Click <a href="http://www.treomadesign.com" target ="_blank">here</a> to go to TreomaDesign.com and find out more about what services we offer.</h6>
    </div>
    <div class="footer-box middle">
      <h3 class="footer-box-hd">DESIGN A ROOM</h3>
      <a href="/Account/Register"><img class="footer-box-img" alt="Sample floor plan" src="/Content/Images/design-a-room1.jpg" /></a>
      <a href="/Account/Register"><img class="footer-box-img" alt="Sample inspiration board" src="/Content/Images/design-a-room2.jpg" /></a>
      <a href="/Account/Register"><img class="footer-box-img" alt="Sample 3D rendering" src="/Content/Images/design-a-room3.jpg" /></a>
      <a href="/Account/Register"><img src="/Content/Images/start-now-LG.png" alt="Start Now" style="margin-left:20px; margin-top:20px;" /></a>
    </div>
    <div class="footer-box">
      <h3 class="footer-box-hd">CONNECT WITH US</h3>
      <h6 class="footer-box-txt">Follow us on your favorite social networking sites.</h6>
      <div class="spacer"></div>
      <div id="social-media">
        <ul>
          <li><a href="http://twitter.com/Treoma_Design" id="connect-twitter" target="_new"></a></li>
          <li><a href="http://www.facebook.com/pages/Treoma-Design/176496522363192" id="connect-facebook" target="_new"></a></li>
          <li><a href="mailto:info@digitalinteriordesign.com" id="connect-email"></a></li>
          <li><a href="http://www.treomadesign.com/feed" id="connect-rss" target="_new"></a></li>
        </ul>
      </div>
    </div>
  </div>
  <!-- end footer-boxes div -->
  <div id="footer-nav">
    <ul>
      <li id="copyright">© Copyright 2010 Treoma Design Inc. All rights reserved.</li>
      <li><%: Html.ActionLink("Privacy Policy", "PrivacyPolicy", "Documents")%></li>
      <li>|</li>
      <li><%: Html.ActionLink("Terms & Conditions", "TermsOfUse", "Documents")%></li>
      <li>|</li>
      <li><%: Html.ActionLink("FAQs", "FAQ", "Documents")%></li>
      <li>|</li>
      <li><%: Html.ActionLink("Contact Us", "Contact", "Home")%></li>
    </ul>
  </div>
<!-- end footer-nav div -->
</div>
<!-- end footer div -->