﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AccessDesign.Models.Room>" %>
<li class="sub-item checked"><%: Html.ActionLink("My Account", "Index", "Profile")%></li>
<li class="sub-item checked"><%: Html.ActionLink("My Questionnaire", "Index", "GeneralQuestionnaire")%></li>
<li class="sub-item checked"><%: Html.ActionLink("Name Room and Select Type", "Create", "Room")%></li>
<li class="sub-item"><%: Html.ActionLink("Room Questionnaire", "Index", "Questionnaire", new { id = Model.RoomId }, new { })%></li>
<li class="sub-item"><%: Html.ActionLink("Room/Inspiration Photos", "Index", "RoomPhoto", new { id = Model.RoomId }, new { })%></li>
<li class="sub-item"><%: Html.ActionLink("Furniture Photos", "Index", "Furniture", new { id = Model.RoomId }, new { })%></li>
<li class="sub-item"><%: Html.ActionLink("Book Consultation", "Index", "Consultation", new { id = Model.RoomId }, new { })%></li>
<li class="sub-item"><%: Html.ActionLink("Pay For Room", "Index", "Commerce", new { id = Model.RoomId }, new { })%></li>
<li class="sub-item"><%: Html.ActionLink("Measure Room", "Index", "RoomMeasurement", new { id = Model.RoomId }, new { })%></li>
<li class="sub-item"><%: Html.ActionLink("Final Documents", "Download", "DesignFile", new { id = Model.RoomId }, new { })%></li>
<li class="sub-item"><%: Html.ActionLink("Book Follow-up Consultation", "Index", "Consultation", new { id = Model.RoomId }, new { })%></li>