﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Mvc;
using System.Runtime.InteropServices;

using System.Data;
using System.Text;
using System.Data.Objects;
using System.Globalization;

namespace AccessDesign.Models
{
    public class RoomPhotoRepository
    {
        private AccessDesignEntities entities = new AccessDesignEntities();

        #region GetMethods

        // 
        /// <summary>
        /// Returns all of the room photos based on the room id
        /// </summary>
        /// <param name="roomId">The room id of the photos that you wish to retreive</param>
        /// <returns></returns>
        public IQueryable<RoomPhoto> FindAllRoomPhotosForSpecificRoom(int roomId)
        {
            return from roomPhotos in entities.RoomPhotos
                   where roomPhotos.RoomId == roomId
                   orderby roomPhotos.RoomPhotoId
                   select roomPhotos;
        }

        // 
        /// <summary>
        /// Returns a specific room photo based on room photo id
        /// </summary>
        /// <param name="roomPhotoId">The room photo id that you wish to retrieve</param>
        /// <returns></returns>
        public RoomPhoto GetSingleRoomPhotoByRoomPhotoId(int roomPhotoId)
        {
            return entities.RoomPhotos.FirstOrDefault(r => r.RoomPhotoId == roomPhotoId);
        }

        public RoomPhoto GetSingleRoomPhotoByName(string photoName)
        {
            return entities.RoomPhotos.FirstOrDefault(r => r.PhotoName == photoName);
        }

        /// <summary>
        /// Checks to see if at least one room photo exists for the current room. This is used to make sure a user has actually uploaded room photos
        /// before they try and complete the room photo upload step.
        /// </summary>
        /// <param name="id">Room Id</param>
        /// <returns>Returns true if at least 1 photo exists, otherwise, returns false.</returns>
        public bool DoesAtLeastOneRoomPhotosExist(int id)
        {
            if(entities.RoomPhotos.Count(r => r.RoomId == id) >= 1)     // check if there is at least 1 room photo uploaded
                return true;    // their is at least 1 photo, so return true
            else
                return false;   // there are 0 photos, so return false
        }

        
        #endregion

        #region RoomPhoto Create Insert and Delete Statements
        //
        // Insert
        public void Add(RoomPhoto roomPhoto)
        {
            entities.RoomPhotos.AddObject(roomPhoto);
        }


        //
        // Delete
        public void Delete(RoomPhoto roomPhoto)
        {
            entities.RoomPhotos.DeleteObject(roomPhoto);    // delete the roomPhoto
        }

        //
        // Persistence
        public void Save()
        {
            entities.SaveChanges();
        }


#endregion
    }
}