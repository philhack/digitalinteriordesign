﻿<%@  Language="C#"  Inherits="System.Web.Mvc.ViewUserControl<AccessDesign.Models.KidsRoomQuestionnaire>" %>

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.DesignStyleOfRoom) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.ValidationMessageFor(model => model.DesignStyleOfRoom) %>
                <%: Html.RadioButtonFor(model=>model.DesignStyleOfRoom,"Modern") %>Modern <br />
                <%: Html.RadioButtonFor(model=>model.DesignStyleOfRoom,"Traditional") %>Traditional <br />
                <%: Html.RadioButtonFor(model=>model.DesignStyleOfRoom,"Country Cottage") %>Country Cottage <br />
                <%: Html.RadioButtonFor(model=>model.DesignStyleOfRoom,"Eclectic") %>Eclectic <br />
                <%: Html.RadioButtonFor(model=>model.DesignStyleOfRoom,"Mid-century Modern") %>Mid-century Modern <br />
                <%: Html.RadioButtonFor(model=>model.DesignStyleOfRoom,"Glam") %>Glam <br />
                <%: Html.RadioButtonFor(model=>model.DesignStyleOfRoom,"Shabby Chic") %>Shabby Chic <br />
                <%: Html.RadioButtonFor(model=>model.DesignStyleOfRoom,"Other") %>Other - Please specifiy:  <%: Html.TextBoxFor(model => model.DesignStyleOfRoomOther, new { @class = "txtbox", @size = 50 })%>
                <%: Html.ValidationMessageFor(model => model.DesignStyleOfRoomOther)%>
            </div>

            <br /> 

             <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.SpecificColorsYouWantToUse) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.SpecificColorsYouWantToUse, "Yes")%>Yes  <br />
                <%: Html.RadioButtonFor(model => model.SpecificColorsYouWantToUse, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.SpecificColorsYouWantToUse) %>
            </div>

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.SpecificColorsYouWantToUseYes)%>
            </div>
            <div class="text-area">
                <%: Html.TextAreaFor(model => model.SpecificColorsYouWantToUseYes, new { @class = "txtarea", @rows = 6, @cols = 80 })%>
                <%: Html.ValidationMessageFor(model => model.SpecificColorsYouWantToUseYes)%>
            </div>

             <br />

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.FurnitureAccessoriesStayingInRoom) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.FurnitureAccessoriesStayingInRoom, "Yes")%>Yes <br />
                <%: Html.RadioButtonFor(model => model.FurnitureAccessoriesStayingInRoom, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.FurnitureAccessoriesStayingInRoom) %>
            </div>

            
            <br />

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.YourBudget) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.YourBudget, new { @class= "txtbox", @size = 70 })%>
                <%: Html.ValidationMessageFor(model => model.YourBudget) %>
            </div>

            <br />
            
            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.MustHavesItemsInRoomNotAlreadyOwned) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.MustHavesItemsInRoomNotAlreadyOwned, "Yes")%>Yes <br />
                <%: Html.RadioButtonFor(model => model.MustHavesItemsInRoomNotAlreadyOwned, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.MustHavesItemsInRoomNotAlreadyOwned) %>
            </div>
            
            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.MustHavesItemsInRoomNotAlreadyOwnedYes) %>
            </div>
            <div class="text-area">
                <%: Html.TextAreaFor(model => model.MustHavesItemsInRoomNotAlreadyOwnedYes, new { @class = "txtarea", @rows = 6, @cols = 80 })%>
                <%: Html.ValidationMessageFor(model => model.MustHavesItemsInRoomNotAlreadyOwnedYes) %>
            </div>         
            
            <br />

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.HowDoYouUseThisSpace) %><span class="required-field">*</span>
            </div>
            <div class="text-area">
                <%: Html.TextAreaFor(model => model.HowDoYouUseThisSpace, new { @class = "txtarea", @rows = 6, @cols = 80 })%>
                <%: Html.ValidationMessageFor(model => model.HowDoYouUseThisSpace) %>
            </div>
            
            <br />

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.StoreAnythingElseInRoom) %><span class="required-field">*</span>
            </div>
            <div class="text-area">
                <%: Html.TextAreaFor(model => model.StoreAnythingElseInRoom, new { @class = "txtarea", @rows = 6, @cols = 80 })%>
                <%: Html.ValidationMessageFor(model => model.StoreAnythingElseInRoom) %>
            </div>

            <br />

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.MoreThanOneChild) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.MoreThanOneChild, "Yes")%>Yes <br />
                <%: Html.RadioButtonFor(model => model.MoreThanOneChild, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.MoreThanOneChild) %>
            </div>
            
            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.ChildsAge) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.ChildsAge, new { @class= "txtbox", @size = 50 })%>
                <%: Html.ValidationMessageFor(model => model.ChildsAge) %>
            </div>

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.HowDoYouWantYourRoomToFeel)%>
            </div>
            <div class="text-area">
                <%: Html.TextAreaFor(model => model.HowDoYouWantYourRoomToFeel, new { @class = "txtarea", @rows = 6, @cols = 80 })%>
                <%: Html.ValidationMessageFor(model => model.HowDoYouWantYourRoomToFeel)%>
            </div>
			
			<div class="text-box-label-above">
                <%: Html.LabelFor(model => model.DoYouLikeContrast) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.DoYouLikeContrast, "Yes")%>Yes <br />
                <%: Html.RadioButtonFor(model => model.DoYouLikeContrast, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.DoYouLikeContrast) %>
            </div>		
			
			<div class="text-box-label-above">
                <%: Html.LabelFor(model => model.DoYouLikeTextures) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.DoYouLikeTextures, "Yes")%>Yes <br />
                <%: Html.RadioButtonFor(model => model.DoYouLikeTextures, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.DoYouLikeTextures) %>
            </div>
			
			<div class="text-box-label-above">
                <%: Html.LabelFor(model => model.HowDoYouWantYourRoomToFeelGender)%><span class="required-field">*</span>
            </div>
            <div class="dropdown-questionnaire">
                <%: Html.DropDownListFor(model => model.HowDoYouWantYourRoomToFeelGender, ViewData["GenderTypeList"] as SelectList, new { @class = "option" })%>
                <%: Html.ValidationMessageFor(model => model.HowDoYouWantYourRoomToFeelGender)%>
            </div>

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.GirlsRoomOrBoysRoom)%><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.GirlsRoomOrBoysRoom, "Girls Room")%>Girls Room  <br />
                <%: Html.RadioButtonFor(model => model.GirlsRoomOrBoysRoom, "Boys Room")%>Boys Room <br />
                <%: Html.ValidationMessageFor(model => model.GirlsRoomOrBoysRoom)%>
            </div>

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.GenderSpecificRoom)%><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.GenderSpecificRoom, "Yes")%>Yes  <br />
                <%: Html.RadioButtonFor(model => model.GenderSpecificRoom, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.GenderSpecificRoom)%>
            </div>

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.ThemeChildWants)%><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.ThemeChildWants, "Yes")%>Yes  <br />
                <%: Html.RadioButtonFor(model => model.ThemeChildWants, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.ThemeChildWants)%>
            </div>

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.ThemeChildWantsYes)%>
            </div>
            <div class="text-area">
                <%: Html.TextAreaFor(model => model.ThemeChildWantsYes, new { @class = "txtarea", @rows = 6, @cols = 80 })%>
                <%: Html.ValidationMessageFor(model => model.ThemeChildWantsYes)%>
            </div>


            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.ShouldRoomTransitionAsChildAges)%><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.ShouldRoomTransitionAsChildAges, "Yes")%>Yes  <br />
                <%: Html.RadioButtonFor(model => model.ShouldRoomTransitionAsChildAges, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.ShouldRoomTransitionAsChildAges)%>
            </div>

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.AdditionalImportantInformation)%>
            </div>
            <div class="text-area">
                <%: Html.TextAreaFor(model => model.AdditionalImportantInformation, new { @class = "txtarea", @rows = 6, @cols = 80 })%>
                <%: Html.ValidationMessageFor(model => model.AdditionalImportantInformation)%>
            </div>