﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Profile>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h2>Details</h2>

    <fieldset>
        <legend>Fields</legend>
        
        <div class="display-label">ProfileId</div>
        <div class="display-field"><%: Model.ProfileId %></div>
        
        <div class="display-label">FirstName</div>
        <div class="display-field"><%: Model.FirstName %></div>
        
        <div class="display-label">LastName</div>
        <div class="display-field"><%: Model.LastName %></div>
        
        <div class="display-label">PhoneNumber</div>
        <div class="display-field"><%: Model.PhoneNumber %></div>
        
        <div class="display-label">FaxNumber</div>
        <div class="display-field"><%: Model.FaxNumber %></div>

        <div class="display-label">Skype Id</div>
        <div class="display-field"><%: Model.SkypeId %></div>
        
        <div class="display-label">Address</div>
        <div class="display-field"><%: Model.Address %></div>
        
        <div class="display-label">City</div>
        <div class="display-field"><%: Model.City %></div>
        
        <div class="display-label">State</div>
        <div class="display-field"><%: Model.State %></div>
        
        <div class="display-label">Zip</div>
        <div class="display-field"><%: Model.Zip %></div>
        
        <div class="display-label">Country</div>
        <div class="display-field"><%: Model.Country %></div>
                
        
        <div class="display-label">DateUpdated</div>
        <div class="display-field"><%: String.Format("{0:g}", Model.DateUpdated) %></div>
        
        <div class="display-label">UserName</div>
        <div class="display-field"><%: Model.UserName %></div>
        
    </fieldset>
    <p>

        <%: Html.ActionLink("Edit", "Edit", new { id=Model.ProfileId }) %> |
        <%: Html.ActionLink("Back to List", "Index") %>
    </p>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SidebarContent" runat="server">
</asp:Content>

