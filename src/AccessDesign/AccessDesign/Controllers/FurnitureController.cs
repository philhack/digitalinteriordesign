﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessDesign.Models;
using System.Web.Hosting;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;

namespace AccessDesign.Controllers
{
    public class FurnitureController : Controller
    {
        FurnitureRepository furnitureRepository = new FurnitureRepository();    // make a connection to the furniture repository
        RoomRepository roomRepository = new RoomRepository();                   // make a connection to the room repository
        OrderRepository orderRepository = new OrderRepository();        // we need to connect to the order repository
        FurniturePhotoRepository furniturePhotoRepository = new FurniturePhotoRepository(); // make a connection to the furniture photo repository


        // GET: /Furniture/5
        /// <summary>
        /// GET: /Furniture/5
        /// Returns to the view all of the furnitures pieces for a particular room
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Index(int id)
        {
            // verify that the room belongs to the currently logged on user
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            if (!roomRepository.DoesRoomExist(id, userName))
            {
                TempData["ErrorMessage"] = "Sorry, this room was not found in the database.";
                return View("Error");
            }
            else
            {
                int menuStepId = roomRepository.GetCurrentMenuStep(id);
                if (menuStepId < 5)    // Step 5 is room photo upload.  User must complete uploading photos before creating furniture pieces
                {
                    TempData["ErrorMessage"] = "Please upload your room photos before uploading your furniture photos";
                    return RedirectToAction("Index", "RoomPhoto", new { id = id });
                }
                else
                {
                    // get and list all furniture pieces for this room
                    var furnitures = furnitureRepository.FindAllFurnitureForRoom(id).ToList();  // get's all of the furniture for this room
                    TempData["RoomId"] = id;        // set a TempData variable that will be used for future requests.
                    TempData["MenuStep"] = menuStepId;  // set the menu step. This is used to display the "continue to next step button"

                    roomRepository.UpdateRoomDateUpdated(id, DateTime.Now);     //update the room - This return a bool value, but we're not doing anything with this value right now

                    // return the list of all the furniture pieces for this room
                    return View("Index", furnitures);
                }
            }
        }

        /// <summary>
        /// GET: /Furniture/Details/5
        /// Get's the specific details for a piece of furniture and passes the infor to the details view
        /// </summary>
        /// <param name="roomId"></param>
        /// <returns>Details View containing the piece of furniture</returns>
        [Authorize]
        public ActionResult Details(int id)
        {
            Furniture furniture = furnitureRepository.GetFurnitureById(id);        // get the piece of furniture
            if (furniture == null)
            {
                TempData["ErrorMessage"] = "Sorry, this piece of furniture was not found in the database.";
                return View("Error");
            }

            string userName = HttpContext.User.Identity.Name.ToString();        // Get's the current logged on user name
            if(!roomRepository.DoesRoomExist(furniture.RoomId, userName))       // verify the current user is the owner of this piece of furniture
            {
                TempData["ErrorMessage"] = "Sorry, this room was not found in the database.";
                return View("Error");
            }            
            else
            {
                return View("Details", furniture);
            }
        }


        /// <summary>
        /// GET: /Furniture/Create
        /// Renders view to create a new piece of furniture
        /// </summary>
        /// <param name="id">Room Id</param>
        /// <returns>view with piece of furniture</returns>
        [Authorize]
        public ActionResult Create(int id)
        {
            TempData["RoomId"] = id;    // re-set this value so that it keeps persisting to the next view
            Furniture furniture = new Furniture()
            {
                RoomId = id
            };

            return View(furniture);
        }


        //
        // POST: /Furniture/Create

        [HttpPost, Authorize]
        public ActionResult Create(Furniture furniture)
        {
            string userName = HttpContext.User.Identity.Name.ToString();

            // if the modelstate is valid, create a piece of furniture
            if (ModelState.IsValid)
            {
                furniture.Height = Convert.ToDecimal(furniture.Height);
                furniture.Depth = Convert.ToDecimal(furniture.Depth);
                furniture.Height = Convert.ToDecimal(furniture.Height);
                furnitureRepository.Add(furniture);
                furnitureRepository.Save();

                Room room = roomRepository.GetRoomById(furniture.RoomId);     // fetch the current room from the database so that we can update it's last updated time
                room.DateUpdated = DateTime.Now;                        // update the date/time
                if (room.MenuStepId < 6)        // if the step is less than 6, then the room photos have beeb uploaded . If it's greater than 6, the user is just adding more furniture pieces
                {
                    room.MenuStepId = 6;                          // mark the furniture creation step complete
                    room.CurrentStepId = 4;                     // mark the furniture creation step complete
                }
                roomRepository.Save();                          // save the updated room values to the DB

                TempData["SuccessMessage"] = "Piece of furniture created. Please upload photos of your piece of furniture.";
                return RedirectToAction("Upload", "FurniturePhoto", new { id = furniture.FurnitureId });     // the furniture piece has been created.  Send user to this screen to upload their photos.
                //return RedirectToAction("Index", new { id = furniture.RoomId });
            }
            else
            {
                // the model state is not valid, so return an error
                TempData["ErrorMessage"] = "Sorry, an unknown error occured while creating your piece of furniture.";
                return View(furniture);
            }
            
        }



        //
        // GET: /Furniture/Edit/5

        [Authorize]
        public ActionResult Edit(int id)
        {
            Furniture furniture = furnitureRepository.GetFurnitureById(id);
            if (furniture == null)
            {
                TempData["ErrorMessage"] = "Sorry, this piece of furniture was not found in the database.";
                return View("Error");
            }

            string userName = HttpContext.User.Identity.Name.ToString();        // Get's the current logged on user name
            if (!roomRepository.DoesRoomExist(furniture.RoomId, userName))      // verify the current user is the owner of this piece of furniture
            {
                TempData["ErrorMessage"] = "Sorry, this room was not found in the database.";
                return View("Error");
            }
            else
            {
                return View(furniture);
            }
        }


        //
        // POST: /Furniture/Edit/5

        [Authorize]
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            
            Furniture furniture = furnitureRepository.GetFurnitureById(id);
            if (furniture == null)
            {
                TempData["ErrorMessage"] = "Sorry, this piece of furniture was not found in the database.";
                return View("Error");
            }

            string userName = HttpContext.User.Identity.Name.ToString();        // Get's the current logged on user name
            if (!roomRepository.DoesRoomExist(furniture.RoomId, userName))      // verify the current user is the owner of this piece of furniture
            {
                TempData["ErrorMessage"] = "Sorry, this room was not found in the database.";
                return View("Error");
            }

            if (ModelState.IsValid && TryUpdateModel(furniture))
            {
                UpdateModel(furniture);             // update the piece of furniture
                furnitureRepository.Save();
                Room room = roomRepository.GetRoomById(furniture.RoomId);     // fetch the current room from the database so that we can update it's last updated time
                room.DateUpdated = DateTime.Now;                        // update the date/time
                roomRepository.Save();                          // save the updated room values to the DB
                TempData["SuccessMessage"] = "Your piece of furniture has been successfully updated";
                return RedirectToAction("Index", new { id = furniture.RoomId });
            }
            return View(furniture);
        }

        //
        // GET: /Furniture/DisplayToDesigner
        /// <summary>
        /// Displays all of the furniture pieces for a room to the deisnger
        /// </summary>
        /// <param name="id">room id</param>
        /// <returns></returns>
        [Authorize(Roles = "Designers")]
        public ActionResult DisplayToDesigner(int id)
        {
            // get and list all furniture pieces for this room
            IQueryable<Furniture> furnitures = furnitureRepository.FindAllFurnitureForRoom(id);  // get's all of the furniture for this room
            TempData["RoomId"] = id;        // set a TempData variable that will be used for future requests.
            return View(furnitures);
        }


        //
        // GET: /Furniture/Delete/5

        [Authorize]
        public ActionResult Delete(int id)
        {
            Furniture furniture = furnitureRepository.GetFurnitureById(id);
            if (furniture == null)
            {
                TempData["ErrorMessage"] = "Sorry, this piece of furniture was not found in the database.";
                return View("Error");
            }

            string userName = HttpContext.User.Identity.Name.ToString();        // Get's the current logged on user name
            if (!roomRepository.DoesRoomExist(furniture.RoomId, userName))      // verify the current user is the owner of this piece of furniture
            {
                TempData["ErrorMessage"] = "Sorry, this room was not found in the database.";
                return View("Error");
            }
            else
                //TempData["WarningMessage"] = "Are you sure that you want to delete the piece of furniture, titled: '" + furniture.Name + "'";
                return View(furniture);
        }


        // TODO:  add HTTP POST delete function once the furniture images section is done.


        //
        // POST: /Furniture/Delete/5

        [HttpPost, Authorize]
        public ActionResult Delete(int id, string confirmButton)
        {
            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            Furniture furniture = furnitureRepository.GetFurnitureById(id);     // get the current piece of furniture

            if (furniture == null)
            {
                TempData["ErrorMessage"] = "Sorry, this piece of furniture was not found in the database";
                return View("Error");
            }
            else if (!roomRepository.DoesRoomExist(furniture.RoomId, userName))      // verify the current user is the owner of this piece of furniture
            {
                TempData["ErrorMessage"] = "Sorry, this room was not found in the database.";
                return View("Error");
            }
            else
            {
                TempData["RoomId"] = furniture.RoomId;      // store the room id in temporary view data
                int roomId = furniture.RoomId;          // store the room id so that it can be used after the furniture piece is deleted

                // check if an order exists for this room.  If an order exists, we do not want to delete this room since it contains
                // buisness information
                if (orderRepository.OrderExistsForRoom(furniture.RoomId))
                {
                    TempData["ErrorMessage"] = "Sorry, you can not delete a piece of furniture from a room that you have already paid for. A designer is currently working on your room.  If you need to make a change to the design, please contact the designer directly.";
                    return View("Error");
                }
                else
                {
                    // The directory that stores the files exists, so for each of the files, lets see if they exists.  If they exists, lets delete it
                    IQueryable<FurniturePhoto> furniturePhotos = furniturePhotoRepository.FindAllFurniturePhotosForSpecificFurniturePiece(id);  // get list of all of the photos for this furniture piece  

                    // loop through all of this rooms photos and delete them 1 by 1
                    foreach (FurniturePhoto furniturePhoto in furniturePhotos)
                    {
                        // Begin - Delete the original room photo
                        string fullPhotoPath = @Server.MapPath(furniturePhoto.OriginalServerPath);   // get the original file path and map it to this environment
                        if (System.IO.File.Exists(@fullPhotoPath))
                        {
                            try
                            {
                                System.IO.File.Delete(@fullPhotoPath);
                            }
                            catch (System.IO.IOException ex)
                            {
                                // TODO: Don't pass this error back, enable logging instead
                                TempData["ErrorMessage"] = "Unable to delete photo:  Error: " + ex.Message.ToString();
                                return View("Error");
                            }
                        }
                        // End - Delete the original room photo

                        // Begin - Delete the thumbnail room photo
                        string fullThumbnailPhotoPath = @Server.MapPath(furniturePhoto.ThumbnailServerPath);   // get the original file path and map it to this environment
                        if (System.IO.File.Exists(@fullThumbnailPhotoPath))
                        {
                            try
                            {
                                System.IO.File.Delete(@fullThumbnailPhotoPath);
                            }
                            catch (System.IO.IOException ex)
                            {
                                // TODO: Don't pass this error back, enable logging instead
                                TempData["ErrorMessage"] = "Unable to delete photo:  Error: " + ex.Message.ToString();
                                return View("Error");
                            }
                        }
                        // End - Delete the thumbnail room photo

                        // Begin - Delete the web room photo
                        string fullWebPhotoPath = @Server.MapPath(furniturePhoto.WebServerPath);   // get the original file path and map it to this environment
                        if (System.IO.File.Exists(@fullWebPhotoPath))
                        {
                            try
                            {
                                System.IO.File.Delete(@fullWebPhotoPath);
                            }
                            catch (System.IO.IOException)
                            {
                                // TODO: Don't pass this error back, enable logging instead
                                TempData["ErrorMessage"] = "Unable to delete photo:  Error"; // + ex.Message.ToString();
                                return View("Error");
                            }
                        }
                        // End - Delete the web room photo
                    }

                    // delete the room from the database
                    try
                    {
                        furnitureRepository.Delete(furniture);
                        furnitureRepository.Save();
                        Room room = roomRepository.GetRoomById(roomId);     // fetch the current room from the database so that we can update it's last updated time
                        room.DateUpdated = DateTime.Now;                        // update the date/time
                        roomRepository.Save();                          // save the updated room values to the DB
                        TempData["SuccessMessage"] = "Your piece of furniture piece has been deleted.";
                        return RedirectToAction("Index", new { id = roomId });
                    }
                    catch (Exception)
                    {
                        //TODO:  Log error
                        TempData["ErrorMessage"] = "An error occured while attempting to delete your piece of furniture from the database.";
                        return View("Error");
                    }
                }
            }
        }

        //
        // GET:  /Furniture/Complete/5
        // completes the furniture upload step
        // where id is a room id
        [Authorize]
        public ActionResult Complete(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // get the current room and make sure that the user owns the room
            if (room != null)
            {
                if (room.MenuStepId == 5)       // The room photos has been completed
                {
                    room.MenuStepId = 6;            // set the completed step to 6 since the user in invoking this action because they do not want to keep any furniture
                    room.CurrentStepId = 4;         // set the current user step to 4 since the furniture photos section is now complete
                    room.DateUpdated = DateTime.Now;        // update the current date/time
                    roomRepository.Save();          // save these settings to the database
                    TempData["SuccessMessage"] = "The furniture photos upload section is now complete.  Please book your free consultation below.";
                    return RedirectToAction("Index", "Consultation", new { id = id });      // redirect the user to the book free consulation 

                }
                else if (room.MenuStepId < 5)       // the upload room photos step has not been completed yet
                {
                    TempData["ErrorMessage"] = "Please upload your room photos before uploading your furniture photos";
                    return RedirectToAction("Index", "RoomPhoto", new { id = id });
                }
                else if (room.MenuStepId > 5)       // the upload furniture photos step is already complete
                {
                    TempData["ErrorMessage"] = "The upload furniture photos section is already complete";
                    return RedirectToAction("Index", new { id = id });
                }
            }
            TempData["ErrorMessage"] = "Sorry, we were unable to find this room";
            return View("Error");       // if the room was not found, then throw an error.

        }
    }
}
