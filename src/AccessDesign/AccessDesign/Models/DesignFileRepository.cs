﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Mvc;
using System.Runtime.InteropServices;

using System.Data;
using System.Text;
using System.Data.Objects;
using System.Globalization;

namespace AccessDesign.Models
{
    public class DesignFileRepository
    {
        private AccessDesignEntities entities = new AccessDesignEntities();

        /// <summary>
        /// Get all design files for a specific room
        /// </summary>
        /// <param name="id">room id</param>
        /// <returns></returns>
        public IQueryable<DesignFile> FindAllDesignFiles(int id)
        {
            return from designFile in entities.DesignFiles
                   where designFile.RoomId == id
                   orderby designFile.DesignFileTypeId ascending
                   select designFile;
        }


        /// Get all design files with types for a specific room
        /// </summary>
        /// <param name="id">room id</param>
        /// <returns>a list of design files and their design files types</returns>
        public IQueryable<DesignFile> FindAllDesignFilesWithTypes(int id)
        {
            return from designFile in entities.DesignFiles.Include("DesignFileType")
                   where designFile.RoomId == id
                   orderby designFile.DesignFileTypeId ascending
                   select designFile;
        }

        /// <summary>
        /// Return the design file based on the design file id
        /// </summary>
        /// <param name="id">design file id</param>
        /// <returns></returns>
        public DesignFile GetDesignFileById(int id)
        {
            return entities.DesignFiles.FirstOrDefault(d => d.DesignFileId == id);
        }

        /// <summary>
        /// Returns a list of all of the design file types
        /// </summary>
        /// <returns>list of design file types</returns>
        public IQueryable<DesignFileType> GetAllDesignFileTypes()
        {
            return from designFileType in entities.DesignFileTypes
                   orderby designFileType.Name ascending
                   select designFileType;
        }


        /// <summary>
        /// Returns all of the initial design files for a room. This includes:
        ///  - 1	First Initial Inspiration Board
        ///  - 2	Second Initial Inspiration Board
        ///  - 3	Initial Floor plan
        /// </summary>
        /// <param name="id">room id</param>
        /// <returns></returns>
        public IQueryable<DesignFile> GetInitialDesignFiles(int id)
        {
            return from designFile in entities.DesignFiles
                   where designFile.RoomId == id && (designFile.DesignFileTypeId == 1 || designFile.DesignFileTypeId == 2 || designFile.DesignFileTypeId == 3)
                   orderby designFile.DesignFileTypeId ascending
                   select designFile;
        }


        /// <summary>
        /// Checks to see if a designer file type for a room has already been uploaded.  We use this function since the designer can only have 1 file type per room
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="designerFileTypeId"></param>
        /// <returns></returns>
        public bool DoesFileTypeAlreadyExistForRoom(int roomId, int designerFileTypeId)
        {
            // get the number of design files for this room that match the room number and designerFileTypeId
            // if no files exist, then return false.  Otherwise return true
            int count = entities.DesignFiles.Count(d => d.RoomId == roomId && d.DesignFileTypeId == designerFileTypeId);
            if (count == 0)        
                return false;
            else
                return true;

        }


        /// <summary>
        /// Returns a select list containing all of the Design File Types
        /// </summary>
        /// <param name="id">currently selected id</param>
        /// <returns>list of design file types</returns>
        public SelectList GetDesignFileTypeList(int id)
        {
            IEnumerable<DesignFileType> designFileTypes = GetAllDesignFileTypes();                  // connect to the database and get a list of all of the design file tyoes   
            var selectItems = new SelectList(designFileTypes, "DesignFileTypeId", "Name", id);      // generate a SelectList from the database
            return selectItems;
        }

#region Design Create Insert and Delete Statements
        //
        // Insert
        public void Add(DesignFile designFile)
        {
            entities.DesignFiles.AddObject(designFile);
        }

        /// <summary>
        /// Updates the comments field for a single design file
        /// </summary>
        /// <param name="id">designFileId</param>
        /// <param name="comments">the comments that you want to add/update for the single design file</param>
        /// <returns>true if it updated, otherwise false if it failed</returns>
        public bool UpdateDesignFileComments(int id, string comments)
        {
            int count = -1; // initialize the counter
            try
            {
                count = entities.UpdateDesignFileComments(id, comments);   // update a comment for a design file and return the number of rows that have been updated
            }
            catch (Exception)
            {
                // if an exception occures, return false
                return (false);
            }

            // only one row should be updated since the id is unique. If the update is successful, return true, otherwise return false.
            if (count != 1)
                return (false);
            else
                return (true);
        }

        //
        // Delete
        public void Delete(DesignFile designFile)
        {
            entities.DesignFiles.DeleteObject(designFile);  // delete the design file
        }

        //
        // Persistence
        public void Save()
        {
            entities.SaveChanges();
        }

#endregion
    }
}