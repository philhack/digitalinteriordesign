﻿using System;
using System.Web;

namespace AccessDesign.Models
{
    public interface IFileStore
    {
        bool SaveUploadedFile(HttpPostedFileBase fileBase, string path);
    }
}