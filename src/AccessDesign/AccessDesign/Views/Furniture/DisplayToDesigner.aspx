﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<AccessDesign.Models.Furniture>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Furniture Photos
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Furniture Photos</h2>

    <table>
        <tr>
            <th></th>
            <th>
                Name
            </th>
            <th>
                Width (in.)
            </th>
            <th>
                Height (in.)
            </th>
            <th>
                Depth (in.)
            </th>
            <th>
                Comments
            </th>
        </tr>

    <% foreach (var item in Model) { %>
    
        <tr>
            <td>
                <%: Html.ActionLink("View Photos", "DisplayToDesigner", "FurniturePhoto", new { id=item.FurnitureId }, null) %>
            </td>
            <td>
                <%: item.Name %>
            </td>
            <td>
                <%: String.Format("{0:F}", item.Width) %>
            </td>
            <td>
                <%: String.Format("{0:F}", item.Height) %>
            </td>
            <td>
                <%: String.Format("{0:F}", item.Depth) %>
            </td>
            <td>
                <%: item.Comments %>
            </td>
        </tr>
    
    <% } %>

    </table>

    <p>
         <%: Html.ActionLink("Back to Room", "Details", "Search", new { id = TempData["RoomId"] }, null)%>
    </p>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

