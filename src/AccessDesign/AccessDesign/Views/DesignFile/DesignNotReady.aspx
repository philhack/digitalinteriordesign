﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Download Final Documents
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
<h1>Download Final Documents</h1>
    <br />
    <p>The Digital Interior Design team is still working on your design. You will be notified by email once your design is ready and you will be able to view your design on this screen. In the mean time, if you need to get in touch with us, please email <a href="mailto:info@digitalinteriordesign.com">info@digitalinteriordesign.com</a>, or call us at 1.888.736.9660.</p>
    <br /><br />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>
