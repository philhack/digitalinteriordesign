﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<AccessDesign.Models.DesignFile>>" %>
<%@ Import Namespace="AccessDesign.Models" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Initial Design File Feedback
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h2>Initial Design File Feedback</h2>
    <p></p>
     <% if (Model.Count() > 1)
                  { %>
                       <p>
                                Please fill in your comments below with what you like, dislike, or want changed about the initial inspiration boards and initial floor plan. 
                                Please note that once you click the submit button, your comments are final.  The designer will be notified and will beging working on the Sample Board, 
                                3D Rendering, Floor Plan, and Purchase Orders.
                                
                       </p>
                 <% } %>
    <% using (Html.BeginForm()) {%>

         <% foreach (DesignFile designFile in Model)
               { %>

               <% if (designFile.DesignFileTypeId == 1) // render the first initial inspiration board 
                      
                  {%>
                    <h3>Initial Inspiration Board 1</h3>
                    <p>Please fill in your comments below with what you like and dislike about this inspiration board</p>
                    <a href="<%: designFile.RsUrl %>" target="_blank">View Initial Inspiration Board 1</a>
                    <% Html.RenderPartial("InitialDesignFeedbackComments", designFile); %>               
                <% } %>

                <p></p>

                <% if (designFile.DesignFileTypeId == 2) // render the second initial inspiration board 
                      
                  {%>
                    <h3>Initial Inspiration Board 2</h3>
                    <p>Please fill in your comments below with what you like and dislike about this inspiration board</p>
                    <a href="<%: designFile.RsUrl %>" target="_blank">View Initial Inspiration Board 2</a>
                    <% Html.RenderPartial("InitialDesignFeedbackComments", designFile); %>               
                <% } %>

                <p></p>

                <% if (designFile.DesignFileTypeId == 3) // render the initial floor plan
                      
                  {%>
                    <h3>Initial Floor Plan</h3>
                    <a href="<%: designFile.RsUrl %>" target="_blank">View Initial Floor Plan</a>
                    <p>Please fill in your comments below with what you like and dislike about the floor plan below?</p>
                    <% Html.RenderPartial("InitialDesignFeedbackComments", designFile); %>               
                <% } %>


               <% } %>

               <% if (Model.Count() > 2) { %>
                        <p>


                        </p>
                <% } %>


               <% if (Model.Count() > 1)
                  { %>
                  <p>

                            <%: Html.CheckBox("InitialSignOff", false)%> Check this box if you want want to submit your comments to the designer and sign off on the initial inspiration boards and floor plan.  You will not be able to edit the comments after you submit them.
                  </p>
                       <p>
                                <input type="submit" value="Submit Comments" />
                       </p>
                 <% }
                  else
                  { %>
                        <p>
                                Sorry, no initial designs have been created for this room. Our designers are still working on your design.
                        </p>
       <% } %>


    <% } %>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

