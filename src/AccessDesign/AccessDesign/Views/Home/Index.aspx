﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Index.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Online Room Design by Digital Interior Design
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <br />
        <!-- begin coda -->
        <!-- begin coda-slider-wrapper -->
        <div class="coda-slider-wrapper"> 
             <!-- begin coda-slider .preload --><div class="coda-slider" id="myslider">
                        <!-- begin single display panel -->
                                <div class="panel">
                        	        <div class="panel-wrapper">
                              		        <a href="http://www.digitalinteriordesign.com/howitworks"><img alt="Get Started Designing - Create your Ultimate Dream Room" src="/Content/Images/slider-main-01.jpg" width="644" height="298" /></a>
                                        </div> <!-- .panel-wrapper -->

                                </div><!-- end single display panel -->

                                <!-- begin single display panel -->
                                <div class="panel">
                        	        <div class="panel-wrapper">
	                        	        <div id='mediaspace'>Welcome to Digitial Interior Design</div>
                                        <script type='text/javascript'>
                                            var so = new SWFObject('/Scripts/JwPlayer/player.swf', 'mpl', '644', '297', '9', '#000000');
                                            so.addParam('allowfullscreen', 'true');
                                            so.addParam('allowscriptaccess', 'always');
                                            so.addParam('wmode', 'opaque');
                                            so.addVariable('author', 'Digital Interior Design');
                                            so.addVariable('description', 'Tara, one of the co-founders talks about Digital Interior Design.');
                                            so.addVariable('file', 'https://www.youtube.com/watch?v=QH2pw5-Q89w');
                                            so.addVariable('image', '/Content/Images/welcome-to-treoma-digital-video-preview.jpg');
                                            so.addVariable('title', 'What is Digital Interior Design - A word from one of the co-founders');
                                            so.write('mediaspace');
                                        </script>
                                        </div> <!-- .panel-wrapper -->

                                </div><!-- end single display panel -->

                                <!-- begin single display panel -->
                                <div class="panel">
                        	        <div class="panel-wrapper">
									        <a href="http://www.digitalinteriordesign.com/philosophy"><img alt="Eco-conscious Design - why is it important" src="/Content/Images/slider-main-03.jpg" width="644" height="298" /></a>									
                                        </div> <!-- .panel-wrapper -->

                                </div><!-- end single display panel -->

                                <!-- begin single display panel -->
                                <div class="panel">
                        	        <div class="panel-wrapper">
									        <a href="http://www.digitalinteriordesign.com/examples"><img alt="Your Space - Example of the final product" src="/Content/Images/slider-main-04.jpg" width="644" height="298" /></a>
                                        </div> <!-- .panel-wrapper -->

                                </div><!-- end single display panel -->

                

   			        </div><!-- end coda-slider .preload -->
   			        <div class="coda-nav" id="coda-nav-1">
		                <ul class="coda-nav">
		                    <li class="slider-main-item01"><a href="#1">Get Started Designing - Create your Ultimate Dream Room</a></li>
		                    <li class="slider-main-item02"><a href="#2">What is Digital Interior Design - A word from one of the co-founders</a></li>
		                    <li class="slider-main-item03"><a href="#3">Eco-conscious Design - why is it important</a></li>
		                    <li class="slider-main-item04"><a href="#4">Your Space - Example of the final product</a></li></ul>
		             </div>

        </div><!-- end coda-slider-wrapper -->

				        <script type="text/javascript">
				            jQuery(document).ready(function ($) {
				                $().ready(function () {
				                    $('#myslider').codaSlider({
				                        dynamicTabs: false,
				                        dynamicArrows: false,
				                        autoHeightEaseDuration: 300,
				                        autoHeightEaseFunction: "easeInSine",
				                        slideEaseDuration: 300,
				                        slideEaseFunction: "easeInSine",
				                        autoSlide: false,
				                        autoSlideInterval: 7000,
				                        autoSlideStopWhenClicked: true
				                    });
				                });
				            });
                        </script><!-- end coda -->
    <div id="main-quote"></div>
    <div id="main-content-boxes">
        <div id="main-boxL">
            <h2 class="main-box-hd"><%: Html.ActionLink("online design", "HowItWorks", "Documents")%></h2>
            <div class="inner-box">
                <h6 class="main-box-txt">We’ve created an online tool that allows you to work with a professional Interior Designer from the comfort of your home and at an affordable price. It’s easy to use, so let us help you create your dream home.<br /><a href="http://www.digitalinteriordesign.com/howitworks">Read More</a></h6>
            </div>
        </div>
        <div id="main-boxC">
            
            <h2 class="main-box-hd"><%: Html.ActionLink("click to start", "Register", "Account", new { }, new { style = "color: #81b6b3;" })%></h2>
            <div class="inner-box">
            <p><a href="/Account/Register"><img class="main-box-img" title="create-your-space" src="/Content/Images/start-now-bernard-residence.jpg" alt="Create your space" width="298" height="103" /></a></p>
                
            </div>
        </div>
        <div id="main-boxR">
            <h2 class="main-box-hd"><%: Html.ActionLink("how it works", "HowItWorks", "Documents")%></h2>
            <div class="inner-box">
                <h6 class="main-box-txt">DigitalInteriorDesign.com follows a simple process to create a design just for you - through questionnaires, images and an initial consultation. Our design team will create a plan that is easy to follow and implement.<br /><a href="http://www.digitalinteriordesign.com/howitworks">Read More</a></h6>
            </div>
        </div>
    </div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <meta name="description" content="Digital Interior Design online interior design services." />
    <meta name="keywords" content="digital interior design, online interior design, interior design, interior decorating, edesign, e-design" />
    <script type='text/javascript' src='/Scripts/JwPlayer/swfobject.js'></script>
    <script type='text/javascript' src='/Scripts/jquery.coda-slider-2.0.js'></script>
    <link rel="stylesheet" type="text/css" href="/Content/coda-slider-2.0.1.css" />
</asp:Content>