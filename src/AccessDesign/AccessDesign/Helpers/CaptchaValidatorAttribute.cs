﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AccessDesign.Helpers
{
    /*
     * Action Filter to handle the Captcha validation
     * 
     * How to use:
     * 
     *    [CaptchaValidator]  
          [AcceptVerbs( HttpVerbs.Post )]  
          public ActionResult CreateComment( Int32 id, bool captchaValid )  
          {  
             .. Do something here  
           }  
     * 
     */
    public class CaptchaValidatorAttribute : ActionFilterAttribute   
    {  
        private const string CHALLENGE_FIELD_KEY = "recaptcha_challenge_field";  
        private const string RESPONSE_FIELD_KEY = "recaptcha_response_field";
        private static string reCaptchaPrivateKey = System.Configuration.ConfigurationManager.AppSettings["reCapchaPrivateKey"].Trim().ToString();        // get the reCaptcha public key from the web.config 
              
        public override void OnActionExecuting(ActionExecutingContext filterContext)  
        {  
            var captchaChallengeValue = filterContext.HttpContext.Request.Form[CHALLENGE_FIELD_KEY];  
            var captchaResponseValue = filterContext.HttpContext.Request.Form[RESPONSE_FIELD_KEY];  
            var captchaValidtor = new Recaptcha.RecaptchaValidator  
                                        {
                                            PrivateKey = reCaptchaPrivateKey,
                                            RemoteIP = filterContext.HttpContext.Request.UserHostAddress,  
                                            Challenge = captchaChallengeValue,  
                                            Response = captchaResponseValue  
                                        };  
     
            var recaptchaResponse = captchaValidtor.Validate();  
     
            // this will push the result value into a parameter in our Action  
            filterContext.ActionParameters["captchaValid"] = recaptchaResponse.IsValid;  
     
            base.OnActionExecuting(filterContext);  
        }  
   }  
}