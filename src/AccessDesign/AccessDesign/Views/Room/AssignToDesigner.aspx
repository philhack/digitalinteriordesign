﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Room>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	AssignToDesigner
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>AssignToDesigner</h2>

    <% using (Html.BeginForm()) {%>
        
        <fieldset>
            <legend>Fields</legend>
                <%: Html.HiddenFor(model => model.RoomId) %>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.Name)%>
            </div>
            <div class="display-field"><%: Model.Name %></div>           
            
            <div class="editor-label">
                <%: Html.LabelFor(model => model.UserName)%>
            </div>
            <div class="display-field"><%: Model.UserName %></div>

            <div class="editor-label">
                <%: Html.LabelFor(model => model.AssignedToDesigner) %>
            </div>
            <div class="editor-field">
                <%: Html.DropDownListFor(model => model.AssignedToDesigner, ViewData["Designers"] as SelectList)%>
                <%: Html.ValidationMessageFor(model => model.AssignedToDesigner)%>
            </div>            
            <p>
                <input type="submit" value="Save" />
            </p>
        </fieldset>

    <% } %>

    <div>
        <%: Html.ActionLink("Back to List", "Index") %>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

