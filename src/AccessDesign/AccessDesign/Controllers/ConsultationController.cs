﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessDesign.Models;
using System;
using System.Text;
using System.Globalization;

namespace AccessDesign.Controllers
{
    public class ConsultationController : Controller
    {
        RoomRepository roomRepository = new RoomRepository();       // connect to the room repository
        ProfileRepository profileRepository = new ProfileRepository();      // connect to the profile repository
        ConsultationRepository consultationRepository = new ConsultationRepository();
        private static string tollFreePhoneNumber = System.Configuration.ConfigurationManager.AppSettings["tollFreePhoneNumber"];       // get the toll free phone number from the web.config 
        private static string phoneNumber = System.Configuration.ConfigurationManager.AppSettings["phoneNumber"];      // get the phone number from the web.config
        private static string infoEmailAddress = System.Configuration.ConfigurationManager.AppSettings["infoEmailAddress"];     // get the info email address from the web.config
        private static string datePickerFormat = "MM-dd-yyyy";      // the date/time format that is stored by the date/time picker
        private static int measureYourRoomStep = 9;      // the measure your room menu step number
        private static int finalDocumentsStep = 10;         // the final document menu step number
        private static readonly string fromEmail = System.Configuration.ConfigurationManager.AppSettings["fromEmail"];
        private static readonly string fromDisplayName =System.Configuration.ConfigurationManager.AppSettings["fromDisplayName"];
        private static readonly string passwordResetEmailSubject = System.Configuration.ConfigurationManager.AppSettings["passwordResetEmailSubject"];
        private static readonly string sendUserNameEmailSubject = System.Configuration.ConfigurationManager.AppSettings["sendUserNameEmailSubject"];

        //
        // GET: /Consultation/Index/5
        /// <summary>
        /// Checks if the user 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Authorize]
        public ActionResult Index(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // ensure that the currently logged on user owns this room before displaying it              
            if (room == null)
            {
                TempData["ErrorMessage"] = "This room does not exist.  If you feel this is an error, please contact us.";
                return View("Error");
            }
            else
            {
                if (room.MenuStepId < 6)    // Step 5 is the upload furniture photos.  User must complete this step before booking a consultation
                {
                    TempData["ErrorMessage"] = "You must complete the furniture photo step before scheduling a free consultation with the designer.";
                    return RedirectToAction("Index", "Furniture", new { id = id });
                }
                else if (room.MenuStepId == 6)
                {
                    // The user has not booked the free initial consultation yet.  Display the initial consultation booking screen
                    roomRepository.UpdateRoomDateUpdated(id, DateTime.Now);     // update the room - This return a bool value, but we're not doing anything with this value right now
                    TempData["ConsultationType"] = "initial";       // set the type of appointment to initial.  This can be either "initial" or "followUp".  This will be used in the create and edit views.
                    return RedirectToAction("Create", "Consultation", new { id = id });
                }
                else if (room.MenuStepId == 7)
                {
                    // Step 7 is the initial free consultation booking.  If their current step = 7, that means that they 
                    // have booked their consultation, but they have not paid for their room yet, so they might not have had the consultation yet.  
                    // Let's display the details view to them so they can see when their consultation is and allows them to click an edit button to reschedule it.
                    TempData["ConsultationType"] = "initial";       // set the type of appointment.  This can be either "initial" or "followUp".  This will be used in the create and edit views.

                    // TODO: get the appointment details for this appointment.  We need to check if an appointment exists for this user.
                    // TODO: add appointment type column to the database so that we can keep track of the appointment types.
                    // Only 1 type of appointment should exist for each user

                    // TODO: Also display to the user that if they have completed their consultation, they can click here to pay for their room. Then direct them to the payment screen
                    // TempData["ErrorMessage"] = "Please pay for the design of your room by using the form below. If you have any questions, please feel free to contact us at " + tollFreePhoneNumber + " or at " + infoEmailAddress;
                    // return RedirectToAction("Index", "Commerce", new { id = id });
                    return RedirectToAction("Details", "Consultation", new { id = id });
                }
                else if (room.MenuStepId == 8)
                {
                    // Step 8 is the pay for room. Step 9 is the measure room.  The user has not measured their room. User must complete this step before booking a follow up consultation
                    TempData["ErrorMessage"] = "Please measure your room. If you have require any assistance with this, please feel free to contact us at " + tollFreePhoneNumber + " or at " + infoEmailAddress;
                    return RedirectToAction("Index", "RoomMeasurement", new { id = id });
                    //return View("Error");
                }
                else if (room.MenuStepId == measureYourRoomStep)
                {
                    // Step 9 is the measure room. Step 10 is final documents.  Advise the user that the design is still working on their room
                    TempData["ConsultationType"] = "followUp";       // set the type of appointment.  This can be either "initial" or "followUp".  This will be used in the create and edit views.
                    return RedirectToAction("Download", "DesignFile", new { id = id });
                    //return View("Error");
                }
                else if (room.MenuStepId == finalDocumentsStep)
                {
                    //roomRepository.UpdateRoomDateUpdated(id, DateTime.Now);     // update the room - This return a bool value, but we're not doing anything with this value right now
                    // advise the user that the designer is still 
                    // Step 11 is book follow up consultation. Display the appointment details screen so that the user has the option to rescheule their appointment
                    TempData["ConsultationType"] = "followUp";       // set the type of appointment.  This can be either "initial" or "followUp".  This will be used in the create and edit views.
                    return RedirectToAction("Create", "Consultation", new { id = id });
                }
                else if (room.MenuStepId == 11)
                {
                    // Step 11 is book follow up consultation. Display the appointment details screen so that the user has the option to rescheule their appointment
                    TempData["ConsultationType"] = "followUp";       // set the type of appointment.  This can be either "initial" or "followUp".  This will be used in the create and edit views.
                    return RedirectToAction("Details", "Consultation", new { id = id });
                }
                else
                {
                    TempData["ErrorMessage"] = "Sorry, an error occured.  Please contact our support team.";
                    return View("Error");
                }
            }
            
        }

        // GET: /Consultation/Create/5
        [HttpGet, Authorize]
        public ActionResult Create(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            Profile profile = profileRepository.GetProfileByUserName(userName);
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // ensure that the currently logged on user owns this room before displaying it              
            string consultationType = String.Empty;     // the type of consultation
            ViewData["ConsultationTime"] = ConsultationRepository.GetConsultationTimes("9:00 AM");
            ViewData["PreferedContactMethod"] = ConsultationRepository.GetPreferedContactMethods("phone");
            if (room == null)
            {
                TempData["ErrorMessage"] = "This room does not exist.  If you feel this is an error, please contact us.";
                return View("Error");
            }
            else 
            {
                if (room.MenuStepId < 8)
                {
                    consultationType = "initial";
                    TempData["ConsultationType"] = "initial";
                }
                else
                {
                    consultationType = "followUp";
                    TempData["ConsultationType"] = "followUp";
                }

                if (room.MenuStepId == 6 || room.MenuStepId == 10)
                {
                    // Display a view to create a new appontment
                    Consultation consultation = new Consultation()
                    {
                        RoomId = room.RoomId,
                        Type = consultationType,
                        Complete = false,
                        TimeZone = "PST",
                        SkypeId = profile.SkypeId,
                        PhoneNumber = profile.PhoneNumber
                    };

                    return View(consultation);
                }
                else
                {
                    TempData["ErrorMessage"] = "Unable to create a new consultation booking.";
                    return View("Error");
                }
            }
        }

        // POST: /Consultation/Create/
        [HttpPost, Authorize]
        public ActionResult Create(Consultation consultation)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            int roomId = consultation.RoomId;      // get the room id for the current questionnaire that is being submitted
            string friendlyDate = String.Empty;     // stores the date in a friendly format
            Profile profile = profileRepository.GetProfileByUserName(userName);     // get the profile
            ViewData["ConsultationTime"] = ConsultationRepository.GetConsultationTimes(consultation.Time);
            ViewData["PreferedContactMethod"] = ConsultationRepository.GetPreferedContactMethods(consultation.ContactMethod);
            TempData["ConsultationType"] = consultation.Type;

            if (ModelState.IsValid)
            {
                // TODO: Check if there is already a consultation that exists for this room with the same type.  If there is, throw an errors
                Room room = roomRepository.GetRoomById(roomId);     // Get the room by the roomId.
                // convert the date into a friendly format
                try
                {
                    DateTime result;        // temporary hold the date/time conversion result
                    DateTime.TryParseExact(consultation.Date, datePickerFormat, CultureInfo.CurrentCulture, DateTimeStyles.None, out result);   // Convert the date from our date picker to the .net date/time format
                    friendlyDate = String.Format("{0:dddd, MMMM d, yyyy}", result);  // Format the date as "Saturday, June 18, 2011"
                }
                catch (Exception)
                {
                    // TODO: log this.
                    TempData["ErrorMessage"] = "There was a problem displaying";
                    return View("Error");
                }
                if (room.MenuStepId < 7)        // if the room step is less than 7 (Book Consultation), then update it. Otherwise leave it because the user is scheduling a new appointment time.
                {
                    room.MenuStepId = 7;            // check off the initial booking consultation step
                    room.CurrentStepId = 5;
                }
                else if (room.MenuStepId == finalDocumentsStep)     // the final design documents have been provided by the designer and the user has now booked their follow-up consultation
                {
                    room.MenuStepId = 11;               // check off the final consultation booking step
                    room.CurrentStepId = 8;
                }
                room.DateUpdated = DateTime.Now;      // set the date updated so that this can be used on the left side room navigation menu to order the rooms
                if (!consultationRepository.DoesConsultationExist(consultation.RoomId, consultation.Type))
                {
                    if (TryUpdateModel(room))
                    {
                        consultationRepository.Add(consultation);
                        consultationRepository.Save();
                        roomRepository.Save();

                        //// Send email to designers advising them that the customer has booked a consultation.
                        //try
                        //{
                        //    StringBuilder designerEmailBody = new StringBuilder();      // Body of the emails
                        //    StringBuilder customerEmailBody = new StringBuilder();      // Body of the emails
                        //    string designerEmailSubject = String.Empty;                 // subject line of the emails
                        //    string customerEmailSubject = String.Empty;                 // subject line of the emails
                        //    // Create an email message that contains the new password
                        //    if (consultation.Type == "followUp")      // if they are booking a follow-up consultation, say so.  Otherwise, say it's the free booking.
                        //    {
                        //        // follow-up consultation email to the designer
                        //        designerEmailSubject = "FOLLOW-UP Consulation Booking - " + profile.UserName;
                        //        designerEmailBody.Append("A customer has booked a <b>FOLLOW-UP</b> consultation.<br /><br />");

                        //        // follow-up consultation email to the customer
                        //        customerEmailSubject = "Follow-up Consulation Booking";
                        //        customerEmailBody.Append("Dear " + profile.FirstName + ",<br /><br />");
                        //        customerEmailBody.Append("Thank you for booking a follow-up consultation. We look forward to working with you.  Your appoinment is scheduled for: " + "<br /><br />");
                        //    }
                        //    else
                        //    {
                        //        // initial free consultation email to the designer
                        //        designerEmailSubject = "FREE INITIAL Consulation Booking - " + profile.UserName;
                        //        designerEmailBody.Append("A customer has booked a <b>FREE INITIAL</b> consultation.<br /><br />");

                        //        // intital free consultation email to the customer
                        //        customerEmailSubject = "Free Initial Consulation Booking";
                        //        customerEmailBody.Append("Dear " + profile.FirstName + ",<br /><br />");
                        //        customerEmailBody.Append("Thank you for booking a free initial 30 minute consultation. We look forward to working with you.  Your appoinment is scheduled for: " + "<br /><br />");

                        //    }
                        //    // Designer email - customer and appointment details
                        //    designerEmailBody.Append("<b>Customer Details:</b><br/>");
                        //    designerEmailBody.Append("Customer username:    " + profile.UserName + "<br />");
                        //    designerEmailBody.Append("Customer Name:    " + profile.FirstName + " " + profile.LastName + "<br />");
                        //    designerEmailBody.Append("Customer Email:    " + profile.UserName + "<br />");
                        //    designerEmailBody.Append("Customer Phone Number:    " + profile.PhoneNumber + "<br />");
                        //    designerEmailBody.Append("Customer State/Province:    " + profile.State + "<br />");
                        //    designerEmailBody.Append("Customer Country:    " + profile.Country + "<br />");
                        //    designerEmailBody.Append("<b>Appointment Time:</b><br/>");
                        //    designerEmailBody.Append("Date:    " + friendlyDate + "<br />");
                        //    designerEmailBody.Append("Time:    " + consultation.Time + " " + consultation.TimeZone + "<br />");
                        //    designerEmailBody.Append("Room Name:    " + consultation.Room.Name + "<br />");
                        //    designerEmailBody.Append("Contact Method:    " + consultation.ContactMethod + "<br />");
                        //    designerEmailBody.Append("Skype Id:    " + consultation.SkypeId + "<br />");
                        //    designerEmailBody.Append("Phone Number:    " + consultation.PhoneNumber + "<br />");

                        //    // customer email - appointment details
                        //    customerEmailBody.Append("Date:    " + friendlyDate + "<br />");
                        //    customerEmailBody.Append("Time:    " + consultation.Time + " " + consultation.TimeZone + "<br />");
                        //    customerEmailBody.Append("Room Name:    " + consultation.Room.Name + "<br />");
                        //    customerEmailBody.Append("Contact Method:    " + consultation.ContactMethod + "<br />");
                        //    if (consultation.ContactMethod == "skype")       // They wish to be contacted via skype, so display their skype id.
                        //    {
                        //        customerEmailBody.Append("Skype Id:    " + consultation.SkypeId + "<br />");
                        //    }
                        //    customerEmailBody.Append("Phone Number:    " + consultation.PhoneNumber + "<br />");
                        //    customerEmailBody.Append("<br />");
                        //    customerEmailBody.Append("If you have any questions about working with Digital Interior Design or how to use the site, please email us at <a href=\"mailto:info@digitalinteriordesign.com\">info@digitalinteriordesign.com</a> or phone us at 1.888.736.9660." + "<br /><br />");
                        //    customerEmailBody.Append("Thank you," + "<br /><br />");
                        //    customerEmailBody.Append("Digital Interior Design Customer Service" + "<br />");
                        //    customerEmailBody.Append(System.Configuration.ConfigurationManager.AppSettings["tollFreePhoneNumber"] + "<br />");
                        //    customerEmailBody.Append("<a href=\"mailto:info@digitalinteriordesign.com\">info@digitalinteriordesign.com</a>");

                        //    SendEmail emailMessage = new SendEmail();   // Create a new Send Email instance

                        //    // email the designer
                        //    if (!emailMessage.SendSingleEmail(designerEmailBody, fromEmail, fromDisplayName, fromEmail, designerEmailSubject))
                        //    {
                        //        TempData["ErrorMessage"] = "Unable to create a new consultation booking There was a problem with sending an email. Please contact us to schedule your appointment.";
                        //        return View("Error");
                        //    }

                        //    // email the customer
                        //    if (!emailMessage.SendSingleEmail(customerEmailBody, fromEmail, fromDisplayName, profile.UserName, customerEmailSubject))
                        //    {
                        //        TempData["ErrorMessage"] = "Unable to create a new consultation booking There was a problem with sending an email. Please contact us to schedule your appointment.";
                        //        return View("Error");
                        //    }
                        //}
                        //catch (Exception)
                        //{
                        //    //TODO: Perform some logging here.
                        //    TempData["ErrorMessage"] = "Unable to create a new consultation booking There was a problem with sending an email. Please contact us to schedule your appointment.";
                        //    return View("Error");
                        //}


                        TempData["SuccessMessage"] = "Your consultation has been booked.";
                        return RedirectToAction("Details", "Consultation", new { id = roomId });        // redirect the user to the room photo upload section of the website.
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "Sorry, an unknown error occured while booking your consultation.";
                    return View(consultation);
                }
            }
            TempData["ErrorMessage"] = "Please fill in the required fields below.";
            return View(consultation);
        }

        // GET /Consulation/Edit/5
        // ID is the room id
        public ActionResult Edit(int id)
        {
            // Get the consultation based on the room id
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // ensure that the currently logged on user owns this room before displaying it              
            if (room == null)
            {
                TempData["ErrorMessage"] = "This room does not exist.  If you feel this is an error, please contact us.";
                return View("Error");
            }
            else
            {
                string consultationType = String.Empty;     // set the colsultation type to null
                if (room.MenuStepId < measureYourRoomStep)
                {
                    consultationType = "initial";
                    TempData["ConsultationType"] = "initial";
                }
                else
                {
                    consultationType = "followUp";
                    TempData["ConsultationType"] = "followUp";
                }

                Consultation consultation = consultationRepository.GetConsultationByRoomId(id, consultationType);   // get the consultation
                if (consultation == null)
                {
                    TempData["ErrorMessage"] = "Sorry, there is a problem retrieving the consultations for this room.";
                    return View("Error");
                }
                else
                {
                    ViewData["ConsultationTime"] = ConsultationRepository.GetConsultationTimes(consultation.Time);      // load the consultation time
                    ViewData["PreferedContactMethod"] = ConsultationRepository.GetPreferedContactMethods(consultation.ContactMethod);   // load the contact method
                    return View(consultation);      // return these values to the view
                }
            }
        }


        //
        // POST: /Consultation/Edit/5
        // ID is the room id
        [HttpPost, Authorize]
        public ActionResult Edit(int id, FormCollection collection)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            int currentMenuStep = 0;    // holds the current menu step that the user is on
            string consultationType = String.Empty;
            string friendlyDate = String.Empty;     // stores the date in a friendly format
            if (roomRepository.DoesRoomExist(id, userName))     // check if a room exists for this user
            {
                currentMenuStep = roomRepository.GetCurrentMenuStep(id);
                Profile profile = profileRepository.GetProfileByUserName(userName);
                if (currentMenuStep < measureYourRoomStep)
                {
                    consultationType = "initial";
                    TempData["ConsultationType"] = "initial";
                }
                else
                {
                    consultationType = "followUp";
                    TempData["ConsultationType"] = "followUp";
                }
                Consultation consultation = consultationRepository.GetConsultationByRoomId(id, consultationType);   // get the consultation
                if (consultation == null)
                {
                    TempData["ErrorMessage"] = "Sorry, there is a problem retrieving the consultations for this room.";
                    return View("Error");
                }
                else
                {
                    ViewData["ConsultationTime"] = ConsultationRepository.GetConsultationTimes(consultation.Time);      // load the consultation time
                    ViewData["PreferedContactMethod"] = ConsultationRepository.GetPreferedContactMethods(consultation.ContactMethod);   // load the contact method
                    if (ModelState.IsValid)
                    {
                        // convert the date into a friendly format
                        try
                        {
                            DateTime result;        // temporary hold the date/time conversion result
                            DateTime.TryParseExact(consultation.Date, datePickerFormat, CultureInfo.CurrentCulture, DateTimeStyles.None, out result);   // Convert the date from our date picker to the .net date/time format
                            friendlyDate = String.Format("{0:dddd, MMMM d, yyyy}", result);  // Format the date as "Saturday, June 18, 2011"
                        }
                        catch (Exception)
                        {
                            // TODO: log this.
                            TempData["ErrorMessage"] = "There was a problem displaying";
                            return View("Error");
                        }
                        if (TryUpdateModel(consultation))
                        {
                            consultationRepository.Save();

                            // Send email to designers advising them that the customer has booked a consultation.
                            try
                            {
                                StringBuilder designerEmailBody = new StringBuilder();      // Body of the emails
                                StringBuilder customerEmailBody = new StringBuilder();      // Body of the emails
                                string designerEmailSubject = String.Empty;                 // subject line of the emails
                                string customerEmailSubject = String.Empty;                 // subject line of the emails
                                // Create an email message that contains the new password
                                if (consultation.Type == "followUp")      // if they are booking a follow-up consultation, say so.  Otherwise, say it's the free booking.
                                {
                                    // follow-up consultation email to the designer
                                    designerEmailSubject = "FOLLOW-UP Consulation Rescheduled - " + profile.UserName;
                                    designerEmailBody.Append("A customer has booked a <b>FOLLOW-UP</b> consultation.<br /><br />");

                                    // follow-up consultation email to the customer
                                    customerEmailSubject = "Follow-up Consulation Rescheduled";
                                    customerEmailBody.Append("Dear " + profile.FirstName + ",<br /><br />");
                                    customerEmailBody.Append("Thank you for rescheduling your follow-up consultation. We look forward to working with you.  Your appoinment is scheduled for: " + "<br /><br />");
                                }
                                else
                                {
                                    // initial free consultation email to the designer
                                    designerEmailSubject = "FREE INITIAL Consulation Rescheduled - " + profile.UserName;
                                    designerEmailBody.Append("A customer has rescheduled a <b>FREE INITIAL</b> consultation.<br /><br />");

                                    // intital free consultation email to the customer
                                    customerEmailSubject = "Free Initial Consulation Rescheduled";
                                    customerEmailBody.Append("Dear " + profile.FirstName + ",<br /><br />");
                                    customerEmailBody.Append("Thank you for rescheduling your a free initial 30 minute consultation. We look forward to working with you.  Your appoinment is scheduled for: " + "<br /><br />");

                                }
                                // Designer email - customer and appointment details
                                designerEmailBody.Append("<b>Customer Details:</b><br/>");
                                designerEmailBody.Append("Customer username:    " + profile.UserName + "<br />");
                                designerEmailBody.Append("Customer Name:    " + profile.FirstName + " " + profile.LastName + "<br />");
                                designerEmailBody.Append("Customer Email:    " + profile.UserName + "<br />");
                                designerEmailBody.Append("Customer Phone Number:    " + profile.PhoneNumber + "<br />");
                                designerEmailBody.Append("Customer State/Province:    " + profile.State + "<br />");
                                designerEmailBody.Append("Customer Country:    " + profile.Country + "<br />");
                                designerEmailBody.Append("<b>Appointment Time:</b><br/>");
                                designerEmailBody.Append("Date:    " + friendlyDate + "<br />");
                                designerEmailBody.Append("Time:    " + consultation.Time + " " + consultation.TimeZone + "<br />");
                                designerEmailBody.Append("Room Name:    " + consultation.Room.Name + "<br />");
                                designerEmailBody.Append("Contact Method:    " + consultation.ContactMethod + "<br />");
                                designerEmailBody.Append("Skype Id:    " + consultation.SkypeId + "<br />");
                                designerEmailBody.Append("Phone Number:    " + consultation.PhoneNumber + "<br />");

                                // customer email - appointment details
                                customerEmailBody.Append("Date:    " + friendlyDate + "<br />");
                                customerEmailBody.Append("Time:    " + consultation.Time + " " + consultation.TimeZone + "<br />");
                                customerEmailBody.Append("Room Name:    " + consultation.Room.Name + "<br />");
                                customerEmailBody.Append("Contact Method:    " + consultation.ContactMethod + "<br />");
                                if (consultation.ContactMethod == "skype")       // They wish to be contacted via skype, so display their skype id.
                                {
                                    customerEmailBody.Append("Skype Id:    " + consultation.SkypeId + "<br />");
                                }
                                customerEmailBody.Append("Phone Number:    " + consultation.PhoneNumber + "<br />");
                                customerEmailBody.Append("<br />");
                                customerEmailBody.Append("If you have any questions about working with Digital Interior Design or how to use the site, please email us at <a href=\"mailto:info@digitalinteriordesign.com\">info@digitalinteriordesign.com</a> or phone us at 1.888.736.9660." + "<br /><br />");
                                customerEmailBody.Append("Thank you," + "<br /><br />");
                                customerEmailBody.Append("Digital Interior Design Customer Service" + "<br />");
                                customerEmailBody.Append(System.Configuration.ConfigurationManager.AppSettings["tollFreePhoneNumber"] + "<br />");
                                customerEmailBody.Append("<a href=\"mailto:info@digitalinteriordesign.com\">info@digitalinteriordesign.com</a>");

                                SendEmail emailMessage = new SendEmail();   // Create a new Send Email instance

                                // email the designer
                                if (!emailMessage.SendSingleEmail(designerEmailBody, fromEmail, fromDisplayName, fromDisplayName, designerEmailSubject))
                                {
                                    TempData["ErrorMessage"] = "Unable to create a new consultation booking There was a problem with sending an email. Please contact us to schedule your appointment.";
                                    return View("Error");
                                }

                                // email the customer
                                if (!emailMessage.SendSingleEmail(customerEmailBody, fromEmail, fromDisplayName, profile.UserName, customerEmailSubject))
                                {
                                    TempData["ErrorMessage"] = "Unable to create a new consultation booking There was a problem with sending an email. Please contact us to schedule your appointment.";
                                    return View("Error");
                                }
                            }
                            catch (Exception)
                            {
                                //TODO: Perform some logging here.
                                TempData["ErrorMessage"] = "Unable to create a new consultation booking There was a problem with sending an email. Please contact us to schedule your appointment.";
                                return View("Error");
                            }

                            TempData["SuccessMessage"] = "Your consultation has been successfully re-scheduled.";       // set the status message
                            return RedirectToAction("Edit", new { id = id });
                        }
                    }
                    TempData["ErrorMessage"] = "Sorry, there was an error re-scheduled your consultation.";
                    return View(consultation);
                }
                
            }
            else
            {
                TempData["ErrorMessage"] = "Sorry, there is a problem retrieving the consultations for this room.";
                return View("Error");
            }
        }



        // GET: /Consultation/Details/5
        //  ID is the room id
        [HttpGet, Authorize]
        public ActionResult Details(int id)
        {
            // Get the consultation based on the room id
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // ensure that the currently logged on user owns this room before displaying it              
            if (room == null)
            {
                TempData["ErrorMessage"] = "This room does not exist.  If you feel this is an error, please contact us.";
                return View("Error");
            }
            else
            {
                string consultationType = null;
                if (room.MenuStepId < measureYourRoomStep)
                {
                    consultationType = "initial";
                    TempData["ConsultationType"] = "initial";
                }
                else
                {
                    consultationType = "followUp";
                    TempData["ConsultationType"] = "followUp";
                }

                Consultation consultation = consultationRepository.GetConsultationByRoomId(id, consultationType);
                if (consultation == null)
                {
                    TempData["ErrorMessage"] = "Sorry, there is a problem retrieving the consultations for this room.";
                    return View("Error");
                }
                else
                {

                    try
                    {
                        DateTime result;        // temporary hold the date/time conversion result
                        DateTime.TryParseExact(consultation.Date, datePickerFormat, CultureInfo.CurrentCulture, DateTimeStyles.None, out result);   // Convert the date from our date picker to the .net date/time format
                        consultation.Date = String.Format("{0:dddd, MMMM d, yyyy}", result);  // Format the date as "Saturday, June 18, 2011"
                    }
                    catch (Exception)
                    {
                        // TODO: log this.
                        TempData["ErrorMessage"] = "There was a problem displaying";
                        return View("Error");
                    }
                    return View(consultation);
                }
            }
        }

        //
        // GET: /Consultation/ContactMethod/5
        /// <summary>
        /// Generates the items required to display the contact method screen.  This is the screen that allows the user to either select 
        /// "Phone" or "Skype" as their contact method.
        /// </summary>
        /// <param name="id">Room ID</param>
        /// <returns></returns>
        [HttpGet, Authorize]
        public ActionResult ContactMethod(int id)
        {
            // get the room and profile information from the database
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // ensure that the currently logged on user owns this room before displaying it              
            if (room == null)
            {
                TempData["ErrorMessage"] = "This room does not exist.  If you feel this is an error, please contact us.";
                return View("Error");
            }
            else         // the room exists
            {
                if (room.MenuStepId < 6)        // if the user has not completed step 6 (the furniture photo upload), then throw an error. We check this just incase the user got to this action somehow.
                {
                    TempData["ErrorMessage"] = "Please complete the furnitre section before booking your free consultation";
                    return RedirectToAction("Index", "Furniture", new { id = room.RoomId });
                }
                // get the profile information.  If the user already has a room (which we already checked above), then we don't need to check if they have a profile.
                Profile profile = profileRepository.GetProfileByUserName(userName);     // get the profile

                /*if(profile.ContactTypePreference == null)       // if the contact type preference is null, then set the default value to phone
                {
                    profile.ContactTypePreference = "phone";        
                }*/

                // generate a ContactMethod class by combining the existing profile and room information.
                ContactMethod contactMethod = new ContactMethod
                {
                    RoomId = room.RoomId,
                    SkypeId = profile.SkypeId,
                    PhoneNumber = profile.PhoneNumber,
                    ContactTypePreference = profile.ContactTypePreference
                };

                if (room.MenuStepId >= finalDocumentsStep)      // the user needs to schedule a follow-up consultation
                {
                    TempData["FollowUpConsultation"] = true;       // this flag is used in the view to know what type of consulation is being done
                                                                                                  // set this to true since it's a follow-up consultation
                    return View(contactMethod);                              // send this new ContactMethod class back to the view
                }
                else
                {
                    // the user needs to schedule the initial consultation
                    TempData["FollowUpConsultation"] = false;       // this flag is used in the view to know what type of consulation is being done
                    return View(contactMethod);                             // send this new ContactMethod class back to the view
                }

                
            }
        } 

        //
        // POST: /Consultation/ContactMethod/5

        [HttpPost, Authorize]
        public ActionResult ContactMethod(ContactMethod contactMethod)
        {
            if (ModelState.IsValid)
            {
                // get the room and profile information from the database
                string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
                string emailSubject = null;     // this will be used for the email subject when we sent out the email.
                Room room = roomRepository.GetRoomByIdVerifyUserName(contactMethod.RoomId, userName);     // ensure that the currently logged on user owns this room before displaying it              
                if (room == null)
                {
                    TempData["ErrorMessage"] = "This room does not exist.  If you feel this is an error, please contact us.";
                    return View("Error");
                }
                else         // the room exists
                {
                    // get the profile information.  If the user already has a room (which we already checked above), then we don't need to check if they have a profile.
                    Profile profile = profileRepository.GetProfileByUserName(userName);     // get the profile

                    // update the room and profile information based on the values that were passed to the form
                    if (contactMethod.ContactTypePreference == "skype")
                    {
                        profile.SkypeId = contactMethod.SkypeId;                                   // if the user select "skype" then update the skype id
                    }
                    profile.PhoneNumber = contactMethod.PhoneNumber;                // update the phone number no matter what.  This can be used as an alternative method incase skype fails

                    // update the room step and date/time
                    if (room.MenuStepId < 7)        // if the room step is less than 7 (Book Consultation), then update it. Otherwise leave it because the user is scheduling a new appointment time.
                    {
                        room.MenuStepId = 7;            // check off the initial booking consultation step
                        room.CurrentStepId = 5;
                    }
                    else if (room.MenuStepId == finalDocumentsStep)     // the final design documents have been provided by the designer and the user has now booked their follow-up consultation
                    {
                        room.MenuStepId = 11;               // check off the final consultation booking step
                        room.CurrentStepId = 8;
                    }

                    room.DateUpdated = DateTime.Now;

                    profileRepository.Save();       // save this information to the profile database
                    roomRepository.Save();          // save this information to the room database

                    // Send email to designers warning them that the this customer is about to book a consultation.  This allows them to pro-actively follow up with the customer if the customer does not actually book a time.
                    try
                    {
                        // Create an email message that contains the new password
                        StringBuilder emailBody = new StringBuilder();
                        if (room.MenuStepId >= finalDocumentsStep)      // if they are booking a follow-up consultation, say so.  Otherwise, say it's the free booking.
                        {
                            emailSubject = "FOLLOW-UP Consulation Booking - Heads Up";
                            emailBody.Append("A customer has started the process of booking a <b>FOLLOW-UP</b> consultation.  If you do not see another email in a few minutes with them scheduling an appointment, you may want to check the simplify this calendar and then follow up with the customer to ensure they are not experiencing any issues with booking the appoinment.<br /><br />");
                            TempData["FollowUpConsultation"] = true;       // set the follow-up flag to true so that we can display the correct text in the view
                        }
                        else
                        {
                            emailSubject = "FREE INITIAL Consulation Booking - Heads Up";
                            emailBody.Append("A customer has started the process of booking a <b>FREE INITIAL</b> consultation.  If you do not see another email in a few minutes with them scheduling an appointment, you may want to check the simplify this calendar and then follow up with the customer to ensure they are not experiencing any issues with booking the appoinment.<br /><br />");
                            TempData["FollowUpConsultation"] = false;       // set the follow-up flag to true so that we can display the correct text in the view
                        }
                        emailBody.Append("Customer username:    " + profile.UserName + "<br /><br />");
                        emailBody.Append("Customer Name:    " + profile.FirstName + " " + profile.LastName + "<br /><br />");
                        emailBody.Append("Customer Email:    " + profile.UserName + "<br /><br />");
                        emailBody.Append("Customer Phone Number:    " + profile.PhoneNumber + "<br /><br />");
                        emailBody.Append("Customer State/Province:    " + profile.State + "<br /><br />");
                        emailBody.Append("Customer Country:    " + profile.Country + "<br /><br />");

                        SendEmail emailMessage = new SendEmail();   // Create a new Send Email instance
                        emailMessage.SendSingleEmail(emailBody, fromEmail, fromDisplayName, fromEmail, emailSubject);
                    }
                    catch (Exception)
                    {
                        //TODO: Perform some logging here.  We don't want to display an error to the customer, but would like to silently log this.
                    }

                    TempData["SuccessMessage"] = "Your contact preferences have been updated.";
                    return RedirectToAction("BookTime", new { id = contactMethod.RoomId });
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Please ensure that you have entered all of the required information in the fields below.";
                return View(contactMethod);
            }
            
        }

        //
        // GET: /Consultation/BookTime/5

        [HttpGet, Authorize]
        public ActionResult BookTime(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // load the room and ensure that it belongs to the current user
            if (room != null)     // make sure that this room exists
            {
                return View(room);
            }
            else
            {
                TempData["ErrorMessage"] = "Sorry, there was a problem finding this room.  Please contact our support team";
                return View("Error");
            }
        }
    }
}
