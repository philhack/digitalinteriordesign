﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Mvc;

namespace AccessDesign.Models
{
    public class ProfileRepository
    {
        private AccessDesignEntities entities = new AccessDesignEntities();
            

        //
        // Query Methods

        
        public Profile GetMyProfile(string userName)
        {
            // might need to connect to the HTTP Profiles and have this pull up the information based on User.Idenity.Name

            return entities.Profiles.FirstOrDefault(p => p.UserName == userName);
        }

        // returns the profile based on the username that is passed to it
        public Profile GetProfileByUserName(string userName)
        {
            return entities.Profiles.FirstOrDefault(p => p.UserName == userName);
        }

        // Checks if the profile exists, if it does, it returns true, otherwise it returns false
        public bool DoesProfileExist(string userName)
        {
            if(entities.Profiles.Count(p => p.UserName == userName) == 1)   // check if a profile based on the username exists
                return true;    // the profile exists, so return true
            else
                return false;   // the profile does not exist
        }

        /// <summary>
        /// Returns the profile id that's associated with the username that's passed to it
        /// </summary>
        /// <param name="userName">user's username</param>
        /// <returns></returns>
        public int GetProfileIdByUserName(string userName)
        {
            return entities.Profiles.FirstOrDefault(p => p.UserName == userName).ProfileId;
        }

        /// <summary>
        /// Returns a list of all USA States
        /// </summary>
        /// <returns></returns>
        public IQueryable<State> GetAllUSAStates()
        {
            return from state in entities.States
                   where state.Country == "US"
                   orderby state.StateName ascending
                   select state;
        }

        /// <summary>
        /// Returns a list of all of the Canadian Provinces and their associate tax amounts
        /// </summary>
        /// <returns></returns>
        public IQueryable<State> GetAllCanadianProvinces()
        {
            return from state in entities.States
                   where state.Country == "CA"
                   orderby state.StateName ascending
                   select state;
        }

        /// <summary>
        /// Gets the province details for the province code that is passed in
        /// </summary>
        /// <param name="provinceAbbreviation"></param>
        /// <returns>Provice details</returns>
        public State GetProviceDetails(string provinceAbbreviation)
        {
            return entities.States.FirstOrDefault(s => s.StateAbbreviation == provinceAbbreviation);
        }
        

        /// <summary>
        /// Returns a list of usernames based on the firstname/lastname or first and last name that are passed to it
        /// </summary>
        /// <param name="firstName">Users first name</param>
        /// <param name="lastName">Users last name</param>
        /// <returns></returns>
        public IQueryable<string> GetAllUserNamesFromFullNames(string firstName, string lastName)
        {
            if (!(String.IsNullOrEmpty(firstName)) && (String.IsNullOrEmpty(lastName)))                         // search only by firstName
            {
                return from profile in entities.Profiles
                       where profile.FirstName.Contains(firstName)
                       orderby profile.UserName ascending
                       select profile.UserName;
            }
            else if ((String.IsNullOrEmpty(firstName)) && (!String.IsNullOrEmpty(lastName)))                    // search only by lastName
            {
                return from profile in entities.Profiles
                       where profile.LastName.Contains(lastName)
                       orderby profile.UserName ascending
                       select profile.UserName;
            }
            else if ((!String.IsNullOrEmpty(firstName)) && (!String.IsNullOrEmpty(lastName)))                    // search by both first and last name
            {
                return from profile in entities.Profiles
                       where profile.FirstName.Contains(firstName)
                       where profile.LastName.Contains(lastName)
                       orderby profile.UserName ascending
                       select profile.UserName;
            }
            else if ((String.IsNullOrEmpty(firstName)) && (String.IsNullOrEmpty(lastName)))                    // get all of the rooms that are associated with any user
            {
                return from profile in entities.Profiles
                       orderby profile.UserName ascending
                       select profile.UserName;
            }

            return null;    // we have addresses all possible situations in the if then clause above so this should never be called.
        }

        /// <summary>
        /// Returns the full state name
        /// </summary>
        /// <param name="stateAbbreviation">state abbreviation that you want to retrieve the state name for</param>
        /// <returns></returns>
        public string GetFullStateNameFromStateAbbreviation(string stateAbbreviation)
        {
            return entities.States.FirstOrDefault(s => s.StateAbbreviation == stateAbbreviation).StateName;
        }

        // Insert
        public void Add(Profile profile)
        {
            entities.Profiles.AddObject(profile);
        }


        // Delete
        public void Delete(Profile profile)
        {
            entities.Profiles.DeleteObject(profile);
        }

        // Persistence
        public void Save()
        {
            entities.SaveChanges();
        }

        public static SelectList GetCountryList(String country)
        {
            List<SelectListItem> countryList = new List<SelectListItem>() 
            {
                new SelectListItem() { Value = "US", Text = "United States" },
                new SelectListItem() { Value = "CA", Text = "Canada" }
            };
            return new SelectList(countryList, "Value", "Text", country ?? "USA");
        }

        /// <summary>
        /// Returns a list of all of the deisngers
        /// </summary>
        /// <param name="designer">value of</param>
        /// <returns></returns>
        public static SelectList GetDesignerList(String designer)
        {
            
            // TODO:  Connect to the database, get a list of all of the designers by checking all of the user account and if they are in the designers db
            List<SelectListItem> designerList = new List<SelectListItem>() 
            {
                new SelectListItem() { Value = "heather@digitalinteriordesign.com", Text = "Heather" },
                new SelectListItem() { Value = "tara@digitalinteriordesign.com", Text = "Tara" },
                new SelectListItem() { Value = "phil@digitalinteriordesign.com", Text = "Phil" }
            };
            return new SelectList(designerList, "Value", "Text", designer ?? "heather@digitalinteriordesign.com");
        }

        /// <summary>
        /// Generates a list of states, provinces, their abbreviation codes, and their tax percentages
        /// </summary>
        /// <param name="stateAbbreviation">currenly selected state/province</param>
        /// <returns>select list containing the states and provinces</returns>
        public SelectList GetStateList(String stateAbbreviation)
        {
            IEnumerable<State> usaStates = GetAllUSAStates();                           // load all of the USA States
            IEnumerable<State> cadProvinces = GetAllCanadianProvinces();     // load all of the Canadian Provinces

            // create a new select list with some selector text that notifies the user to select a state
            List<SelectListItem> stateList = new List<SelectListItem>() 
            {
                new SelectListItem() { Value = "stateSelector", Text = "---- Select a State ----" }, 
            };

            foreach (State usaState in usaStates)           // loop through all of the states and add them to the list
            {
                stateList.Add(new SelectListItem() { Value = usaState.StateAbbreviation, Text = usaState.StateName });
            }

            // create some selector text that notifies the user to select a province
            stateList.Add(new SelectListItem() { Value = "provinceSelector", Text = "---- Provinces ----" });

            foreach (State cadProvince in cadProvinces)     // loop through all of the provinces and add them to the list
            {
                stateList.Add(new SelectListItem() { Value = cadProvince.StateAbbreviation, Text = cadProvince.StateName });
            }

            return new SelectList(stateList, "Value", "Text", stateAbbreviation ?? "stateSelector");
        }
    }
}