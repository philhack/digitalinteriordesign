﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AccessDesign.Models
{
    public class DiningRoomQuestionnaireRepository
    {
        private AccessDesignEntities entities = new AccessDesignEntities();

        // Return the diningRoom questions based on the diningRoom question id
        public DiningRoomQuestionnaire GetDiningRoomQuestionnaireByRoomId(int id)
        {
            return entities.DiningRoomQuestionnaires.FirstOrDefault(d => d.RoomId == id);
        }

        public DiningRoomQuestionnaire GetDiningRoomQuestionnaireByDiningRoomQuestionnaireId(int id)
        {
            return entities.DiningRoomQuestionnaires.FirstOrDefault(d => d.DiningRoomQuestionnaireId == id);
        }

        // Checks if the questionnaire exists based on room id, if it does, it returns true, otherwise it returns false
        public bool DoesDiningRoomQuestionnaireExist(int id)
        {
            if (entities.DiningRoomQuestionnaires.Count(d => d.RoomId == id) == 1)   // check if the diningRoom questionnaire exists
                return true;    // the questionnaire exists, so return true
            else
                return false;   // the questionnaire does not exist
        }

        /// <summary>
        /// Returns a list of gender types.  This can then be used in a drop down list for the question: How Do you want your room to feel masculine, feminine, or neutral?
        /// </summary>
        /// <param name="genderType">Gender Type</param>
        /// <returns></returns>
        public static SelectList GetGenderTypeList(string genderType)
        {
            List<SelectListItem> numberList = new List<SelectListItem>() 
            {
				new SelectListItem() { Value = "", Text = "  " },
                new SelectListItem() { Value = "Masculine", Text = " Masculine " },
                new SelectListItem() { Value = "Feminine", Text = " Feminine " },
                new SelectListItem() { Value = "Neutral", Text = " Neutral " }
            };
            return new SelectList(numberList, "Value", "Text", genderType ?? "");
        }

        #region Room Create Insert and Delete Statements
        //
        // Insert
        public void Add(DiningRoomQuestionnaire diningRoomQuestionnaire)
        {
            entities.DiningRoomQuestionnaires.AddObject(diningRoomQuestionnaire);
        }


        //
        // Delete
        public void Delete(DiningRoomQuestionnaire diningRoomQuestionnaire)
        {
            entities.DiningRoomQuestionnaires.DeleteObject(diningRoomQuestionnaire);  // delete the diningRoom Questionnaire
        }

        //
        // Persistence
        public void Save()
        {
            entities.SaveChanges();
        }

        #endregion

    }
}