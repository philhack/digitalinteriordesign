﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<AccessDesign.Models.RoomMeasurement>>" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	View/Edit Room Measurements
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>View/Edit Room Measurements</h1>
    <div id="room-photo-nav">
            <a href="/RoomMeasurement/Upload/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-UploadMeasurements.png" alt="" border="0" class="room-photo-nav-image"/></a><a href="/RoomMeasurement/Details/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-ViewEditMeasurementsTreomaOn.png" alt="" border="0" /></a><a href="/RoomMeasurement/Comments/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-AddComments.png" alt="" border="0" class="room-photo-nav-image"/></a>
    </div>   
    <br />
    <p>Click the picture to view a larger version. To a room measurement photo, just click the delete button.</p>
    <br />
    <div id="gallery">
    <% foreach (var item in Model) { %>
            <div class="gallery">
                            <div class="gallery-item"><a href="<%: item.WebUrlPath %>" title="<%: item.MeasurementName %>. " class="lightbox"><img src="<%: item.ThumbnailUrlPath %>" width="100" height="100" alt="<%: item.MeasurementName %>" border="0" /></a></div>
                            <div class="gallery-delete-button"><%: Html.ActionLink("DELETE", "Delete", new { id=item.RoomMeasurementId })%></div>
            </div>
        <% } %>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">

    <script type="text/javascript" src="/Scripts/jquery.lightbox-0.5.js"></script>
    <link rel="stylesheet" type="text/css" href="/Content/Gallery.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/Content/jquery.lightbox-0.5.css" media="screen" />
    
    <script type="text/javascript">
        $(function () {
            $('a.lightbox').lightBox // Select all links with lightbox class
            ({
                imageBlank: '/Content/Images/LightBox/lightbox-blank.gif',
                imageLoading: '/Content/Images/LightBox/lightbox-ico-loading.gif',
                imageBtnClose: '/Content/Images/LightBox/lightbox-btn-close.gif',
                imageBtnPrev: '/Content/Images/LightBox/lightbox-btn-prev.gif',
                imageBtnNext: '/Content/Images/LightBox/lightbox-btn-next.gif'
            });

            $(".room-photo-nav-image")
            .mouseover(function () {
                var src = $(this).attr("src").match(/[^\.]+/) + "TreomaOn.png";
                $(this).attr("src", src);
            })
            .mouseout(function () {
                var src = $(this).attr("src").replace("TreomaOn", "");
                $(this).attr("src", src);
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarContent" runat="server">
</asp:Content>

