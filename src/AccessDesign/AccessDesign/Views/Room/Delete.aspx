﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Room>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Delete Room Confirmation: <%: Model.Name %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Delete Room Confirmation</h1>
    <br /><br />
    <div>
        <p>Please confirm that you want to delete the room titled:
        <b><i> <%: Model.Name %> </i></b>?</p>
        <br />
        <p><b>All of the information contained within this room will be lost.</b> Please <i>only</i> click <i>delete</i> if you are sure that you want to delete this room.</p>
    </div>
    <br /><br />


    <% using (Html.BeginForm()) { %>
    <div id="standard-button">
        <input name="confirmButton" type="submit"  class="button" value="Delete" />
    </div> 
    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SidebarContent" runat="server">
</asp:Content>

