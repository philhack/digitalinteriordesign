﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.GeneralQuestionnaire>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	General Questionnaire Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>General Questionnaire Details</h2>

    <fieldset>

        <div class="display-label"><%: Html.LabelFor(model => model.UserName)%></div>
        <div class="display-field"><%: Model.UserName %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.DesignStyle)%></div>
        <div class="display-field"><%: Model.DesignStyle %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.DesignStyleOther)%></div>
        <div class="display-field"><%: Model.DesignStyleOther %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.ColorsYouLove)%></div>
        <div class="display-field"><%: Model.ColorsYouLove %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.ColorsYouLoveOther)%></div>
        <div class="display-field"><%: Model.ColorsYouLoveOther %></div>

        <div class="display-label"><%: Html.LabelFor(model => model.ColorsYouDislike)%></div>
        <div class="display-field"><%: Model.ColorsYouDislike %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.ColorsYouDislikeOther)%></div>
        <div class="display-field"><%: Model.ColorsYouDislikeOther %></div>

        <div class="display-label"><%: Html.LabelFor(model => model.TypeOfColorsYouLike)%></div>
        <div class="display-field"><%: Model.TypeOfColorsYouLike %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.HaveASpouse)%></div>
        <div class="display-field"><%: Model.HaveASpouse %></div>

        <div class="display-label"><%: Html.LabelFor(model => model.HaveKids)%></div>
        <div class="display-field"><%: Model.HaveKids %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.HaveKidsAgesOfKids)%></div>
        <div class="display-field"><%: Model.HaveKidsAgesOfKids %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.HavePets)%></div>
        <div class="display-field"><%: Model.HavePets %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.HavePetsTypeAndNumber)%></div>
        <div class="display-field"><%: Model.HavePetsTypeAndNumber %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.HobbiesAndInterests)%></div>
        <div class="display-field"><%: Model.HobbiesAndInterests %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.TypesOfPatternsYouLike)%></div>
        <div class="display-field"><%: Model.TypesOfPatternsYouLike %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.TypesOfPatternsYouLikeOther)%></div>
        <div class="display-field"><%: Model.TypesOfPatternsYouLikeOther %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.BeingGreenImportant)%></div>
        <div class="display-field"><%: Model.BeingGreenImportant %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.BeingGreenImportantOther)%></div>
        <div class="display-field"><%: Model.BeingGreenImportantOther %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.RentOrOwnYourHome)%></div>
        <div class="display-field"><%: Model.RentOrOwnYourHome %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.HowLongDoYouPlanToLiveInHome)%></div>
        <div class="display-field"><%: Model.HowLongDoYouPlanToLiveInHome %></div>

        <div class="display-label"><%: Html.LabelFor(model => model.NeedWheelchairAccess)%></div>
        <div class="display-field"><%: Model.NeedWheelchairAccess %></div>
        
    </fieldset>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="/Content/admin.css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

