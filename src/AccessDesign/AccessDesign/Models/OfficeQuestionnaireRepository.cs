﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AccessDesign.Models
{
    public class OfficeQuestionnaireRepository
    {
        private AccessDesignEntities entities = new AccessDesignEntities();

        // Return the office questions based on the office question id
        public OfficeQuestionnaire GetOfficeQuestionnaireByRoomId(int id)
        {
            return entities.OfficeQuestionnaires.FirstOrDefault(b => b.RoomId == id);
        }

        public OfficeQuestionnaire GetOfficeQuestionnaireByOfficeQuestionnaireId(int id)
        {
            return entities.OfficeQuestionnaires.FirstOrDefault(b => b.OfficeQuestionnaireId == id);
        }

        // Checks if the questionnaire exists based on room id, if it does, it returns true, otherwise it returns false
        public bool DoesOfficeQuestionnaireExist(int id)
        {
            if (entities.OfficeQuestionnaires.Count(u => u.RoomId == id) == 1)   // check if the office questionnaire exists
                return true;    // the questionnaire exists, so return true
            else
                return false;   // the questionnaire does not exist
        }


        /// <summary>
        /// Returns a list of numbers from 1 to 20.  This can then be used in a drop down list for the number of people that share the same office
        /// </summary>
        /// <param name="number">Number of people that share the same office</param>
        /// <returns></returns>
        public static SelectList GetNumberList(string number)
        {
            List<SelectListItem> numberList = new List<SelectListItem>() 
            {
                new SelectListItem() { Value = "1", Text = " 1 " },
                new SelectListItem() { Value = "2", Text = " 2 " },
                new SelectListItem() { Value = "3", Text = " 3 " },
                new SelectListItem() { Value = "4", Text = " 4 " },
                new SelectListItem() { Value = "5", Text = " 5 " },
                new SelectListItem() { Value = "6", Text = " 6 " },
                new SelectListItem() { Value = "7", Text = " 7 " },
                new SelectListItem() { Value = "8", Text = " 8 " },
                new SelectListItem() { Value = "9", Text = " 9 " },
                new SelectListItem() { Value = "10", Text = " 10 " },
                new SelectListItem() { Value = "11", Text = " 11 " },
                new SelectListItem() { Value = "12", Text = " 12 " },
                new SelectListItem() { Value = "13", Text = " 13 " },
                new SelectListItem() { Value = "14", Text = " 14 " },
                new SelectListItem() { Value = "15", Text = " 15 " },
                new SelectListItem() { Value = "16", Text = " 16 " },
                new SelectListItem() { Value = "17", Text = " 17 " },
                new SelectListItem() { Value = "18", Text = " 18 " },
                new SelectListItem() { Value = "19", Text = " 19 " },
                new SelectListItem() { Value = "20", Text = " 20 " }
            };
            return new SelectList(numberList, "Value", "Text", number ?? "1");
        }


        /// <summary>
        /// Returns a list of gender types.  This can then be used in a drop down list for the question: How do you want your room to feel?
        /// </summary>
        /// <param name="genderType">Gender Type</param>
        /// <returns></returns>
        public static SelectList GetGenderTypeList(string genderType)
        {
            List<SelectListItem> numberList = new List<SelectListItem>() 
            {
				new SelectListItem() { Value = "", Text = "  " },
                new SelectListItem() { Value = "Masculine", Text = " Masculine " },
                new SelectListItem() { Value = "Feminine", Text = " Feminine " },
                new SelectListItem() { Value = "Neutral", Text = " Neutral " }
            };
            return new SelectList(numberList, "Value", "Text", genderType ?? "");
        }

        #region Room Create Insert and Delete Statements
        //
        // Insert
        public void Add(OfficeQuestionnaire officeQuestionnaire)
        {
            entities.OfficeQuestionnaires.AddObject(officeQuestionnaire);
        }


        //
        // Delete
        public void Delete(OfficeQuestionnaire officeQuestionnaire)
        {
            entities.OfficeQuestionnaires.DeleteObject(officeQuestionnaire);  // delete the office Questionnaire
        }

        //
        // Persistence
        public void Save()
        {
            entities.SaveChanges();
        }

        #endregion

    }
}