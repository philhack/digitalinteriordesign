﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Furniture>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h2>Your Pieces of Furniture</h2>

    <fieldset>
        <legend>Fields</legend>
                
        <div class="display-label">Name</div>
        <div class="display-field"><%: Model.Name %></div>
        <div class="display-field"><%: Model.Comments %></div>
        <div class="display-field">Width: <%: Model.Width %> inches</div>
        <div class="display-field">Depth: <%: Model.Depth %> inches</div>
        <div class="display-field">Height: <%: Model.Height %> inches</div>

        
    </fieldset>
    <p>
        <%: Html.ActionLink("Upload Photos", "Upload","FurniturePhoto", new { id=Model.FurnitureId }, null) %> |
        <%: Html.ActionLink("View Photos", "Details", "FurniturePhoto", new { id = Model.FurnitureId }, null)%> |
        <%: Html.ActionLink("Edit", "Edit", new { id=Model.FurnitureId }) %> |
        <%: Html.ActionLink("Back to List", "Index", new { id=Model.RoomId })%>
    </p>
    <div>
        <%: Html.ActionLink("Back to List", "Index", new { id = ViewData["RoomId"] }, new{})%>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

