﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Room>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Room/Inspiration Photos 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Room/Inspiration Photos </h1>
    <div id="room-photo-nav">
        <a href="/RoomPhoto/Upload/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-UploadRoomPhotos.png" alt="" border="0" class="room-photo-nav-image"/></a><a href="/RoomPhoto/Details/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-ViewEditPhotos.png" alt="" border="0" class="room-photo-nav-image"/></a><a href="/RoomPhoto/Comments/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-AddComments.png" alt="" border="0" class="room-photo-nav-image"/></a>
     </div>   
     <br />
    <p>Now it's time to take some photos of your room. Please use a digital camera to take photos of your room and use this page to upload your photos.  Please use the navigation above to <%: Html.ActionLink("upload", "Upload", "RoomPhoto", new { id = ViewData["RoomId"] }, new { })%> your photos, <%: Html.ActionLink("view/delete", "Details", "RoomPhoto", new { id = ViewData["RoomId"] }, new { })%> your photos, and <%: Html.ActionLink("add comments", "Comments", "RoomPhoto", new { id = ViewData["RoomId"] }, new { })%> about your photos. Once this is complete, please continue to the <%: Html.ActionLink("furniture photos", "Index", "Furniture", new { id = ViewData["RoomId"] }, new { })%> section.</p>
    <br />
    <br />
     
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    $(function () {
        $(".room-photo-nav-image")
        .mouseover(function () {
            var src = $(this).attr("src").match(/[^\.]+/) + "TreomaOn.png";
            $(this).attr("src", src);
        })
        .mouseout(function () {
            var src = $(this).attr("src").replace("TreomaOn", "");
            $(this).attr("src", src);
        });
    });
</script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

