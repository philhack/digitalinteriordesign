﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.DesignFile>" %>
<%@ Import Namespace="AccessDesign.Helpers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Upload a Design File
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Upload a Design File</h1>
<% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm("Create", "DesignFile", FormMethod.Post, new { enctype = "multipart/form-data" }))
       { %>

        
            <%: Html.HiddenFor(model => model.RoomId) %>

            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.DesignFileTypeId) %>
            </div>
            <div class="dropdown">
                <%: Html.DropDownListFor(model => model.DesignFileTypeId, ViewData["DesignFileTypes"] as SelectList, new { @class = "option" })%>
                <%: Html.ValidationMessageFor(model => model.DesignFileTypeId)%>
            </div>

            <br />
            
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.UrlPath) %>
            </div>
            <div class="text-box">
                <%: Html.FileInput(model => model.UrlPath)%>
                <%: Html.ValidationMessageFor(model => model.UrlPath)%>
            </div>

            <br />
            <div id="standard-button">
                    <input type="submit"  class="button" value="Upload" />
            </div> 

    <% } %>

    <br /><br /><br /><br />
    <div>
        <%: Html.ActionLink("Back to Design Files", "Index", "DesignFile", new { id=Model.RoomId }, null)%>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

