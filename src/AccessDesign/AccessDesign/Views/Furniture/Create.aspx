﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Furniture>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Add a Piece of Furniture
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Add a Piece of Furniture</h1>
    <div id="room-photo-nav">
        <a href="/Furniture/Create/<%: Model.RoomId  %>"><img src="/Content/Images/Furniture-Navigation-NewTreomaOn.png" alt="Create a new piece of furniture" border="0" /></a><img src="/Content/Images/app-steps-blank-02.png" alt="Blank Step 2" border="0" /><img src="/Content/Images/app-steps-blank-03.png" alt="Blank Step 3" border="0" />
     </div>
     <br />
     <p>Create a piece of furniture by filling in the fields below.  Once you have created the piece of furniture, you will be prompted to upload photos for it.</p>
     <br /><br />
    <% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm()) {%>
     <div>
            <div class="editor-field">
                <%: Html.HiddenFor(model => model.RoomId) %>
            </div>

            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.Name) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.Name, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.Name) %>
            </div>

            <br />

            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.Width) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.Width, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.Width)%>
            </div>

            <br />

            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.Depth) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.Depth, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.Depth)%>
            </div>

            <br />

            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.Height) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.Height, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.Height)%>
            </div>
            
            <br />

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.Comments) %>
            </div>
            <div class="text-area">
                <%: Html.TextAreaFor(model => model.Comments, new { @class = "txtarea", @rows = 6, @cols = 80 })%>
                <%: Html.ValidationMessageFor(model => model.Comments)%>
            </div>
            
            <br /><br />
            <div id="standard-button">
                    <input type="submit"  class="button" value="Create" />
            </div> 
        </div>       
    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

