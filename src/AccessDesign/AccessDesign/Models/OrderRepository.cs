﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AccessDesign.Models
{
    public class OrderRepository
    {
        private AccessDesignEntities entities = new AccessDesignEntities();

        #region GET Methods

        public bool OrderExistsForRoom(int roomId)
        {
            if(entities.Orders.Count(o => o.RoomId == roomId) >= 1)   // check if at least 1 order for the room exists
                return true;    // an order exists, so return true
            else
                return false;   // an order does not exists, so return false
        }

        /// <summary>
        /// Returns the order associated with a particular room
        /// </summary>
        /// <param name="id">Room ID</param>
        /// <returns>Order for a particular room</returns>
        public Order GetOrderForRoom(int id)
        {
            return entities.Orders.FirstOrDefault(o => o.RoomId == id);
        }

        #endregion

        /// <summary>
        /// Insert an order into the database
        /// </summary>
        /// <param name="order">the order to be inserted</param>
        public void Add(Order order)
        {
            entities.Orders.AddObject(order);
        }

        /// <summary>
        /// Save the order to the database (Persistence)
        /// </summary>
        public void Save()
        {
            entities.SaveChanges();
        }


    }
}