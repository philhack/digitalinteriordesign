﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Mvc;
using System.Runtime.InteropServices;

using System.Data;
using System.Text;
using System.Data.Objects;
using System.Globalization;

namespace AccessDesign.Models
{
    public class FurnitureRepository
    {
        private AccessDesignEntities entities = new AccessDesignEntities();

        /// <summary>
        /// Get all pieces of furniture
        /// </summary>
        /// <returns>List of all of the pieces of furniture in the database that all customers have input</returns>
        public IQueryable<Furniture> FindAllFurniture()
        {
            return entities.Furnitures;
        }

        /// <summary>
        /// Get's all pieces of furniture that belonging to a particular room id
        /// </summary>
        /// <param name="roomId">The room id</param>
        /// <returns>List of all of the furniture pieces for a particular room</returns>
        public IQueryable<Furniture> FindAllFurnitureForRoom(int roomId)
        {
            // TODO:  May want to change this to order by Room Name asc.
            return from furniture in entities.Furnitures
                   where furniture.RoomId == roomId
                   orderby furniture.RoomId
                   select furniture;
        }


        
        /// <summary>
        /// Return a single piece of furniture based on the furniture id
        /// </summary>
        /// <param name="id">Furniture Id</param>
        /// <returns>a single piece of furniture</returns>
        public Furniture GetFurnitureById(int id)
        {
            return entities.Furnitures.FirstOrDefault(f => f.FurnitureId == id);
        }


        /// <summary>
        /// Gets the Room Id based on the furniture Id
        /// </summary>
        /// <param name="id">furniture Id</param>
        /// <returns>room id associated with that piece of furniture</returns>
        public int GetRoomIdFromFurnitureId(int id)
        {
            return entities.Furnitures.FirstOrDefault(f => f.FurnitureId == id).RoomId;
        }

        /// <summary>
        /// Returns the number of furniture photos that are associated with a paricular piece of furniture
        /// </summary>
        /// <param name="id">furniture id</param>
        /// <returns></returns>
        public int GetNumberOfFurniturePhotosForAPieceOfFurniture(int id)
        {
            return entities.FurniturePhotos.Count(f => f.FurnitureId == id);
        }


#region Furniture Create Insert and Delete Statements

        /// <summary>
        /// Insert a new furniture item
        /// </summary>
        /// <param name="furniture">piece of furniture to insert</param>
        public void Add(Furniture furniture)
        {
            entities.Furnitures.AddObject(furniture);
        }


        /// <summary>
        /// Delete a single piece of furniture
        /// </summary>
        /// <param name="furniture">the piece of furniture to be deleted</param>
        public void Delete(Furniture furniture)
        {
            entities.Furnitures.DeleteObject(furniture);  // delete the piece of furniture
        }

        // Persistence
        /// <summary>
        /// Persistence: Save the changes to the DB
        /// </summary>
        public void Save()
        {
            entities.SaveChanges();
        }


#endregion
    }
}