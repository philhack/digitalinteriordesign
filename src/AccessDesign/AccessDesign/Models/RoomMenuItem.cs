﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccessDesign.Models
{
    public class RoomMenuItem
    {
        public int RoomId { get; set; }
        public int MenuStepId { get; set; }
        public string RoomName { get; set; }
    }
}