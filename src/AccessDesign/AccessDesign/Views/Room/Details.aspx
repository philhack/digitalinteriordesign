﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Room>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Your Room's Type
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>

    <h1>Your Room's Type</h1>
    <br />
    <fieldset>
        <legend></legend>
        
        <div class="display-label">Room Name</div>
        <div class="display-field"><%: Model.Name %></div>
        
        <br />

        <div class="display-label">Room Type</div>
        <div class="display-field"><%: Model.RoomTypeName %></div>

        
    </fieldset>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

