﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Upload Room Photos
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <div id="message"></div>
    <h1>Upload Room Photos</h1>
    <div id="room-photo-nav">
        <a href="/RoomPhoto/Upload/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-UploadRoomPhotosTreomaOn.png" alt="" border="0" /></a><a href="/RoomPhoto/Details/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-ViewEditPhotos.png" alt="" border="0" class="room-photo-nav-image"/></a><a href="/RoomPhoto/Comments/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-AddComments.png" alt="" border="0" class="room-photo-nav-image"/></a>
     </div>
    <p>Click the &quot;Browse&quot; button to select the file to upload. You may upload a maximum of 20 photos at a time</p>
    <br />
    <br />
    <h2>STEP 1</h2><p>Click BROWSE to load your photos.  You can select up to 10 photos at a time. You photos will appear in a queue below.</p>
    <br />
        <p><input type="file" id="filename" name="filename" /></p>
    <br /><br />
    <h2>STEP 2</h2><p>Click UPLOAD to upload your photos.  All of the photos that you have selected will be uploaded to your account for our designers to view.</p>
    <br />
        <p><button id="upload-file"></button></p>
    <br />
    <br />
    <h2>Uploaded Room Photos</h2>
    <p>Below is a listing of room photos that you have uploaded.  Click <%: Html.ActionLink("here", "Details", "RoomPhoto", new { id = ViewData["RoomId"] }, new { })%> to view or delete a photo.</p>
    <br />
        <p id="photoList">
        </p>
    

    <script type="text/javascript">
        $(function () {
            // Get a reference to the div for messages.
            var message = $("#message");
            var auth = "<% = Request.Cookies[FormsAuthentication.FormsCookieName]==null ? string.Empty : Request.Cookies[FormsAuthentication.FormsCookieName].Value %>";   

            $(".room-photo-nav-image")
            .mouseover(function () {
                var src = $(this).attr("src").match(/[^\.]+/) + "TreomaOn.png";
                $(this).attr("src", src);
            })
            .mouseout(function () {
                var src = $(this).attr("src").replace("TreomaOn", "");
                $(this).attr("src", src);
            });

            // Initialize the plugin to the "file" input element.
            $("#filename").uploadify({
                'uploader': '<%= Url.Content("~/Scripts/Uploadify/uploadify.swf") %>',
                'script': '<%= Url.Action("Upload", "RoomPhoto", new RouteValueDictionary(new { id = ViewData["RoomId"] })) %>',
                'scriptData': { token: auth },
                'sizeLimit': 10485760,
                'multi': true,
                'cancelImg': '<%= Url.Content("~/Scripts/uploadify/cancel.png") %>',
                'folder': '/UserData',
                'fileDataName': 'fileData',
                'fileDesc': 'Upload an image file',
                'fileExt': '*.jpg;*.jpeg;',
                'wmode': 'transparent',
                'hideButton': true,
                'queueSizeLimit': 20,
                'onError': function (event, queue, fileInfo, error) {
                    // Display the error message in red.
                    message.html("<div class=\"form-unsuccess\"><div class=\"form-unsuccessful\"><img src=\"/Content/Images/non.png\" alt=\"Oops\" /><h6 class=\"form-unsuccessfully-submitted\">" + error.type +  "</h6></div></div>");
                },
                'onComplete': function (event, queueId, fileObj, response, data) {
                    // Display the successful message in green and fade out 
                    // after 3 seconds.
                    message.html("<div class=\"form-success\"><div class=\"form-successful\"><img src=\"/Content/Images/checkmark.png\" alt=\"Yep\" /><h6 class=\"form-successfully-submitted\">" + response + "</h6></div></div>");
                    message.fadeIn().delay(3000).fadeOut();

                    // Reload the list of photos
                    LoadPhotoLinks();
                }
            });

            // The event handler for the Upload button.
            $("#upload-file").click(function () {
                // Refer to the documentation for this method...
                // $("#some-file-input").uploadifyUpload();
                // http://www.uploadify.com/documentation/
                $("#filename").uploadifyUpload();
            });

            // The function to render the list of photos.
            function LoadPhotoLinks() {
                var photoList = $("#photoList");
                var myDate = new Date();
                var timestamp = myDate.getTime();
                $.get("/RoomPhoto/GetPhotoList/<%: ViewData["RoomId"] %>" + '?t=' + timestamp, null, function (data) {
                    photoList.html(data)
                });
            }

            // Now call this function when the page loads.
            LoadPhotoLinks();
        });
    
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SidebarContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeadContent" runat="server">

    <link href="/Content/Uploadify/uploadify.css" rel="stylesheet" type="text/css" />

    <script src="/Scripts/Uploadify/swfobject.js" type="text/javascript"></script>

    <script src="/Scripts/Uploadify/jquery.uploadify.v2.1.0.min.js" type="text/javascript"></script>

</asp:Content>