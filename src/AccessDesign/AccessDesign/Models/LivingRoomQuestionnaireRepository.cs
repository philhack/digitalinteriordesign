﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AccessDesign.Models
{
    public class LivingRoomQuestionnaireRepository
    {
        private AccessDesignEntities entities = new AccessDesignEntities();

        // Return the livingRoom questions based on the livingRoom question id
        public LivingRoomQuestionnaire GetLivingRoomQuestionnaireByRoomId(int id)
        {
            return entities.LivingRoomQuestionnaires.FirstOrDefault(l => l.RoomId == id);
        }

        public LivingRoomQuestionnaire GetLivingRoomQuestionnaireByLivingRoomQuestionnaireId(int id)
        {
            return entities.LivingRoomQuestionnaires.FirstOrDefault(l => l.LivingRoomQuestionnaireId == id);
        }

        // Checks if the questionnaire exists based on room id, if it does, it returns true, otherwise it returns false
        public bool DoesLivingRoomQuestionnaireExist(int id)
        {
            if (entities.LivingRoomQuestionnaires.Count(l => l.RoomId == id) == 1)   // check if the livingRoom questionnaire exists
                return true;    // the questionnaire exists, so return true
            else
                return false;   // the questionnaire does not exist
        }

        /// <summary>
        /// Returns a list of gender types.  This can then be used in a drop down list for the question: How do you want your room to feel?
        /// </summary>
        /// <param name="genderType">Gender Type</param>
        /// <returns></returns>
        public static SelectList GetGenderTypeList(string genderType)
        {
            List<SelectListItem> numberList = new List<SelectListItem>() 
            {
				new SelectListItem() { Value = "", Text = "  " },
                new SelectListItem() { Value = "Masculine", Text = " Masculine " },
                new SelectListItem() { Value = "Feminine", Text = " Feminine " },
                new SelectListItem() { Value = "Neutral", Text = " Neutral " }
            };
            return new SelectList(numberList, "Value", "Text", genderType ?? "");
        }

        #region Room Create Insert and Delete Statements
        //
        // Insert
        public void Add(LivingRoomQuestionnaire livingRoomQuestionnaire)
        {
            entities.LivingRoomQuestionnaires.AddObject(livingRoomQuestionnaire);
        }


        //
        // Delete
        public void Delete(LivingRoomQuestionnaire livingRoomQuestionnaire)
        {
            entities.LivingRoomQuestionnaires.DeleteObject(livingRoomQuestionnaire);  // delete the livingRoom Questionnaire
        }

        //
        // Persistence
        public void Save()
        {
            entities.SaveChanges();
        }

        #endregion

    }
}