﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Room>" %>
<%@ Import Namespace="AccessDesign.Models" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Room and Customer Details</h1>
    <p>
    <%: Html.ActionLink("General Questionnaire", "Details", "GeneralQuestionnaire", new { id = Model.Profile.UserName }, null)%>  | 
    <% if (Model.CurrentStepId > 2)
       { %>
        <%: Html.ActionLink("Room Questionnaire", "Details", "Questionnaire", new { id = Model.RoomId }, null)%>  | 
    <% } %>
    <%: Html.ActionLink("furniture", "DisplayToDesigner", "Furniture", new { id = Model.RoomId }, null)%> (<%: ViewData["NumberOfFurniturePieces"]%>) | 
    <%: Html.ActionLink("photos", "DisplayToDesigner", "RoomPhoto", new { id = Model.RoomId }, null)%> (<%: ViewData["NumberOfRoomPhotos"] %>) | 
    <%: Html.ActionLink("measurements", "DisplayToDesigner", "RoomMeasurement", new { id = Model.RoomId }, null)%> (<%: ViewData["NumberOfMeasurements"]%>) | 
    <% if (Model.AssignedToDesigner == null)
       { %>
            <%: Html.ActionLink("assign to designer", "AssignToDesigner", "Room", new { id = Model.RoomId }, null)%>
    <% }
       else
       { %>
            <%: Html.ActionLink("change designer", "AssignToDesigner", "Room", new { id = Model.RoomId }, null)%>
    <% } %>
    <% if (Model.CurrentDesignStepId > 5)
           { %>
             |  <%: Html.ActionLink("orders", "DisplayRoomOrderForDesigner")%>
        <% } %>
      | <%: Html.ActionLink("design files", "Index", "DesignFile", new { id = Model.RoomId }, null)%> (<%: ViewData["NumberOfDesginFiles"] %>)
    </p>
    <fieldset>
        <legend><h4>Customer's profile information</h4></legend>

        <table>
            <tr>
                <td><div class="display-label">User Name</div></td>
                <td><div class="display-field"><%: Model.Profile.UserName %></div></td>
            </tr>
            <tr>
                <td><div class="display-label">First Name</div></td>
                <td><div class="display-field"><%: Model.Profile.FirstName %></div></td>
            </tr>
            <tr>
                <td><div class="display-label">Last Name</div></td>
                <td><div class="display-field"><%: Model.Profile.LastName %></div></td>
            </tr>
            <tr>
                <td><div class="display-label">Phone Number</div></td>
                <td><div class="display-field"><%: Model.Profile.PhoneNumber %></div></td>
            </tr>
            <tr>
                <td><div class="display-label">Fax Number</div></td>
                <td><div class="display-field"><%: Model.Profile.FaxNumber %></div></td>
            </tr>
            <tr>
                <td><div class="display-label">Skype Id</div></td>
                <td><div class="display-field"><%: Model.Profile.SkypeId %></div></td>
            </tr>
            <tr>
                <td><div class="display-label">Email Address</div></td>
                <td><div class="display-field"><a href="mailto:<%: Model.Profile.UserName %>"><%: Model.Profile.UserName %></a></div></td>
            </tr>
            <tr>
                <td><div class="display-label">Address</div></td>
                <td><div class="display-field"><%: Model.Profile.Address %> <br /> <%: Model.Profile.City %>, <%: Model.Profile.State %> <br /> <%: Model.Profile.Zip %> <br /> <%: Model.Profile.Country %></div></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
           </tr>
        </table>
    </fieldset>
    <br /><br /><br />
    <fieldset>
        <legend><h4>Room Information</h4></legend>
        
        <table>
            <tr>
                <td><div class="display-label">Room Name</div></td>
                <td><div class="display-field"><%: Model.Name %></div></td>
            </tr>
            <tr>
                <td><div class="display-label">Date Updated</div></td>
                <td><div class="display-field"><%: String.Format("{0:g}", Model.DateUpdated) %></div></td>
            </tr>
            <tr>
                <td><div class="display-label">Room Type</div></td>
                <td><div class="display-field"><%: Model.RoomTypeName %></div></td>
            </tr>
            <tr>
                <td><div class="display-label">Room Paid For</div></td>
                <td><div class="display-field">
                <% if (Model.RoomPaidFor == 1)
                   { %>  
                        Yes
                    <% } else { %>
                        No 
                   <% } %>
                </div></td>
            </tr>
            <tr>
                <td><div class="display-label">Room Cost</div></td>
                <td><div class="display-field"><%: String.Format("{0:F}", Model.RoomCost) %></div></td>
            </tr>
             <% if (Model.AssignedToDesigner == null)
           { %>
            <tr>
                <td><div class="display-label">Assigned To Designer</div></td>
                <td><div class="display-field">This room has not been assigned to a designer, click <%: Html.ActionLink("here", "AssignToDesigner", "Room", new { id = Model.RoomId }, null)%> to assign it to a designer.</div></td>
            </tr>
            <% } else { %>
            <tr>
                <td><div class="display-label">Assigned To Designer</div></td>
                <td><div class="display-field"><%: Model.AssignedToDesigner %></div></td>                
            </tr>
            <% } %>
        </table>
    </fieldset>        
            <br /><br />
    
    <fieldset>
    <legend><h4>Customers Overall Steps (Steps on left menu)</h4></legend>
        <% foreach (MenuStep menuStep in ViewData["MenuSteps"] as IEnumerable<MenuStep>) { %>
        <% if (Model.MenuStepId == menuStep.MenuStepId)
           { %>
                <div class="display-currentstep-selected"><%: menuStep.MenuStepId%> - <%: menuStep.Name%></div>
            <% }
           else
           {%>
                <div class="display-currentstep"><%: menuStep.MenuStepId%> - <%: menuStep.Name%></div>
            <% } %>

        <% } %>
    </fieldset>

    <br /><br />

    <fieldset>
        <legend><h4>Designers Current Step</h4></legend>
            <% foreach (CurrentDesignStep designStep in ViewData["DesignerSteps"] as IEnumerable<CurrentDesignStep>)
           { %>
            <% if (Model.CurrentDesignStepId == designStep.CurrentDesignStepId)
               { %>
                    <div class="display-currentstep-selected"><%: designStep.CurrentDesignStepId%> - <%: designStep.Name%></div>
                <% }
               else
               {%>
                    <div class="display-currentstep"><%: designStep.CurrentDesignStepId%> - <%: designStep.Name%></div>
                <% } %>

            <% } %>
    </fieldset>
    <br /><br />
        
    <fieldset>
    <legend><h4>Customers Current Step</h4></legend>
        <% foreach (CurrentStep step in ViewData["CustomerSteps"] as IEnumerable<CurrentStep>) { %>
        <% if (Model.CurrentStepId == step.CurrentStepId)
           { %>
                <div class="display-currentstep-selected"><%: step.CurrentStepId%> - <%: step.Name %></div>
            <% }
           else
           {%>
                <div class="display-currentstep"><%: step.CurrentStepId%> - <%: step.Name %></div>
            <% } %>

        <% } %>
    </fieldset>
    <br /><br />

    <p>

        <%: Html.ActionLink("Back to List", "DisplayAllUsers")%>
    </p>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="/Content/admin.css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

