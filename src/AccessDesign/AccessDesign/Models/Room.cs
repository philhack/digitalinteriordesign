﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AccessDesign.Models
{
    //
    // Rooms
    [MetadataType(typeof(Room_Validation))]
    public partial class Room
    {
    }

    public class Room_Validation
    {
        [Required(ErrorMessage = "Room Name is required")]
        [StringLength(14, ErrorMessage = "Room Name is too long (Maximum 14 characters)")]
        [DisplayName("Room Name")]
        public string Name { get; set; }
        
        [Required(ErrorMessage = "Room Type is required")]
        [DisplayName("Type of Room")]
        public string RoomTypeName { get; set; }

        [StringLength(10000, ErrorMessage = "Your comments are too long (Maximum 10,000 characters)")]
        public string PhotoDetails { get; set; }

    }
}