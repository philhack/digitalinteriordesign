﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccessDesign.Models
{
    public class Invoice
    {
        public int CustomerId { get; set; }
        public int RoomId { get; set; }
        public string OrderNumber { get; set; }
        public string RoomName { get; set; }
        public string RoomDescription { get; set; }
        public decimal RoomCost { get; set; }
        public string TaxType { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal Discount { get; set; }
        public decimal GrandTotal { get; set; }
        public string CurrencyCode { get; set; }
        public string Username { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string Name { get; set; }
    }
}