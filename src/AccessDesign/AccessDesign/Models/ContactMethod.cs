﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Foolproof;

namespace AccessDesign.Models
{
    /// <summary>
    /// This class is used in the consulation controller to allow the user to select their contact method when they are booking the initial consultation.
    /// The items in this class are saved in the profile database.
    /// </summary>
    public class ContactMethod
    {
        /*
        *  Uses:   http://foolproof.codeplex.com/      :  	Beta 0.9.3774       :   Sun May 2 2010
        *  [Is]
        *  [EqualTo]
        *  [NotEqualTo]
        *  [GreaterThan]
        *  [LessThan]
        *  [GreaterThanOrEqualTo]
        *  [LessThanOrEqualTo]
        *  [RequiredIf]
        *  [RequiredIfNot]
        *  [RequiredIfTrue]
        *  [RequiredIfFalse]
        *  [RequiredIfEmpty]
        *  [RequiredIfNotEmpty]
        *  [RequiredIfRegExMatch]
        *  [RequiredIfNotRegExMatch]
        */

        [HiddenInput]
        public int RoomId { get; set; }

        [DisplayName("How do you want to be contacted?")]     // Radio
        [Required(ErrorMessage = "Please select a contact method below.  We can contact you by either 'Phone' or 'Skype'.")]
        [StringLength(5, ErrorMessage = "Please enter a maximum of 5 characters.")]
        public string ContactTypePreference { get; set; }

        [DisplayName("Skype ID")]
        [RequiredIf("ContactTypePreference", "skype", ErrorMessage = "Required")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        public string SkypeId { get; set; }

        [DisplayName("Phone Number")]
        [Required(ErrorMessage = "A phone number is required. This is required even if we are contacting you by Skype.")]
        [RegularExpression(@"(\(\d{3}\)|\b\d{3})(\.| |\-)\d{3}(\.| |\-)\d{4}\b", ErrorMessage = "Please enter a phone number (ie: 555-555-5555)")]    // may need to fine tune this.
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        public string PhoneNumber { get; set; }
    }
}
