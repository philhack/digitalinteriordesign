﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AccessDesign.Models
{
    [MetadataType(typeof(Profile_Validation))]
    public partial class Profile
    {

    }

    public class Profile_Validation
    {

        [DisplayName("First Name: ")]
        [Required(ErrorMessage = "First Name is required")]
        [StringLength(100, ErrorMessage = "User Name is too long (Maximum 100 characters")]
        public string FirstName { get; set; }

        [DisplayName("Last Name: ")]
        [Required(ErrorMessage = "Last Name is required")]
        [StringLength(100, ErrorMessage = "User Name is too long (Maximum 100 characters")]
        public string LastName { get; set; }

        [DisplayName("Phone Number: ")]
        [Required(ErrorMessage = "Phone Number is required")]
        [RegularExpression(@"(\(\d{3}\)|\b\d{3})(\.| |\-)\d{3}(\.| |\-)\d{4}\b", ErrorMessage = "Please enter a phone number (ie: 555-555-5555)")]    // may need to fine tune this.
        [StringLength(20, ErrorMessage = "Phone Number is too long (Maximum 20 characters")]
        public string PhoneNumber { get; set; }

        [DisplayName("Fax Number: ")]
        [RegularExpression(@"(\(\d{3}\)|\b\d{3})(\.| |\-)\d{3}(\.| |\-)\d{4}\b", ErrorMessage = "Please enter a phone number (ie: 555-555-5555)")]    // may need to fine tune this.
        [StringLength(20, ErrorMessage = "Fax Number is too long (Maximum 20 characters")]
        public string FaxNumber { get; set; }

        [DisplayName("Skype ID: ")]
        [StringLength(256, ErrorMessage = "Skype Id is too long (Maximum 256 characters")]
        public string SkypeId { get; set; }

        [DisplayName("Address: ")]
        [Required(ErrorMessage = "Address is required")]
        [StringLength(256, ErrorMessage = "Address is too long (Maximum 256 characters")]
        public string Address { get; set; }

        [DisplayName("City: ")]
        [Required(ErrorMessage = "City is required")]
        [StringLength(50, ErrorMessage = "City is too long (Maximum 50 characters")]
        public string City { get; set; }

        [DisplayName("Zip/Postal Code: ")]
        [Required(ErrorMessage = "Zip/Postal Code is required")]
        [StringLength(7, ErrorMessage = "Zip/Postal Code is too long (Maximum 7 characters")]
        public string Zip { get; set; }

        [DisplayName("State/Province: ")]
        [Required(ErrorMessage = "State/Province is required")]
        [StringLength(2, ErrorMessage = "Select a valid state or province")]
        public string State { get; set; }

        [DisplayName("Country: ")]
        [Required(ErrorMessage = "Country is required")]
        public string Country { get; set; }

    }
}