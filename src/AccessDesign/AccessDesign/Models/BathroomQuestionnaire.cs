﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Foolproof;


namespace AccessDesign.Models
{
    [MetadataType(typeof(BathroomQuestionnaire_Validation))]
    public partial class BathroomQuestionnaire
    {

    }


    public class BathroomQuestionnaire_Validation
    {
        /*
         *  Uses:   http://foolproof.codeplex.com/      :  	Beta 0.9.3774       :   Sun May 2 2010
         *  [Is]
         *  [EqualTo]
         *  [NotEqualTo]
         *  [GreaterThan]
         *  [LessThan]
         *  [GreaterThanOrEqualTo]
         *  [LessThanOrEqualTo]
         *  [RequiredIf]
         *  [RequiredIfNot]
         *  [RequiredIfTrue]
         *  [RequiredIfFalse]
         *  [RequiredIfEmpty]
         *  [RequiredIfNotEmpty]
         *  [RequiredIfRegExMatch]
         *  [RequiredIfNotRegExMatch]
         */

        //[HiddenInput]
        public string RoomId { get; set; }

        [DisplayName("What design style do you want for this room?")]     // Radio
        [Required(ErrorMessage = "Required")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        public string DesignStyleOfRoom  { get; set; }

        [DisplayName("")]   // single line text field
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        [RequiredIf("DesignStyleOfRoom", "Other", ErrorMessage = "Required")]
        public string DesignStyleOfRoomOther  { get; set; }

        [DisplayName("Are there specific colours you would like to use in this room?")]      // Radio
        [Required(ErrorMessage = "Required")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        public string SpecificColorsYouWantToUse  { get; set; }

        [DisplayName("If yes, please enter the specific colors below")]
        [StringLength(2000, ErrorMessage = "Please enter a maximum of 2000 characters.")]
        [RequiredIf("SpecificColorsYouWantToUse", "Yes", ErrorMessage = "Required")]
        public string SpecificColorsYouWantToUseYes  { get; set; }

        [DisplayName("Do you have any furniture or accessories that are staying in the room? If so, please upload your photos when prompted.")]  
        [Required(ErrorMessage = "Required")]
        [StringLength(3, ErrorMessage = "Please enter a maximum of 3 characters.")]
        public string FurnitureAccessoriesStayingInRoom  { get; set; }

        [DisplayName("What is your budget?  This can be either a number, such as $8000, or a range such a $6000 to $8000.")]  
        [Required(ErrorMessage = "Required")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        public string YourBudget  { get; set; }

        [DisplayName("Are there any must have items in the room that you don't already own? (i.e. magnifying mirror, separate shower and bath)")]  
        [Required(ErrorMessage = "Required")]
        [StringLength(3, ErrorMessage = "Please enter a maximum of 3 characters.")]
        public string MustHavesItemsInRoomNotAlreadyOwned  { get; set; }
        
        [DisplayName("If yes, please describe the items below.")]
        [StringLength(2000, ErrorMessage = "Please enter a maximum of 2000 characters.")]
        [RequiredIf("MustHavesItemsInRoomNotAlreadyOwned", "Yes", ErrorMessage = "Required")]
        public string MustHavesItemsInRoomNotAlreadyOwnedYes  { get; set; }

        [DisplayName("Do you enjoy taking baths?")]
        [Required(ErrorMessage = "Required")]
        [StringLength(3, ErrorMessage = "Please enter a maximum of 3 characters.")]
        public string DoYouEnjoyBaths { get; set; }

        [DisplayName("Do you want two sinks?")]
        [Required(ErrorMessage = "Required")]
        [StringLength(3, ErrorMessage = "Please enter a maximum of 3 characters.")]
        public string DoYouWantTwoSinks { get; set; }

        [DisplayName("How many people use this washroom?")]
        [Required(ErrorMessage = "Required")]
        [StringLength(3, ErrorMessage = "Please enter a maximum of 3 characters.")]
        public string HowManyPeopleUseWashroom { get; set; }

        [DisplayName("Do you need to store linens or other items?")]
        [Required(ErrorMessage = "Required")]
        [StringLength(3, ErrorMessage = "Please enter a maximum of 3 characters.")]
        public string DoYouStoreLinensOrOtherItems { get; set; }

        [DisplayName("How do you want your room to feel? (ie: warm, cozy, fresh, inviting, organized, simple, romantic)")]
        [Required(ErrorMessage = "Required")]
        [StringLength(2000, ErrorMessage = "Please enter a maximum of 2000 characters.")]
        public string HowDoYouWantYourRoomToFeel { get; set; }

        [DisplayName("Do you like contrast?")]
        [Required(ErrorMessage = "Required")]
        [StringLength(3, ErrorMessage = "Please enter a maximum of 3 characters.")]
        public string DoYouLikeContrast { get; set; }

        [DisplayName("Do you like textures?")]
        [Required(ErrorMessage = "Required")]
        [StringLength(3, ErrorMessage = "Please enter a maximum of 3 characters.")]
        public string DoYouLikeTextures { get; set; }

        [DisplayName("Do you want your room to feel masculine, feminine, or neutral?")]
        [Required(ErrorMessage = "Required")]
        [StringLength(256, ErrorMessage = "Please enter a maximum of 256 characters.")]
        public string HowDoYouWantYourRoomToFeelGender { get; set; }

        [DisplayName("If you feel like we’ve missed important information, please add anything here that is important for your project.")]
        [StringLength(2000, ErrorMessage = "Please enter a maximum of 2000 characters.")]
        public string AdditionalImportantInformation { get; set; }
    }
}