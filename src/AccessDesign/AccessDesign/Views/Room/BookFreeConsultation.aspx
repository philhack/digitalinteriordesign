﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Book Free Consultation
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h2>Book Free 15 minute Consultation</h2>
    <iframe src ="https://app.simplifyaccounts.com/Booking/index.html?coguid=dd599e30-33bf-4eb1-8945-b08b881cb67d" width="600" height="500"  allowtransparency="true" frameborder="0" scrolling="no">
  <p>Your browser does not support iframes. Please email info@digitalinteriordesign.com to book an appointment.</p>
</iframe>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarContent" runat="server">
</asp:Content>
