﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Consultation>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<%if (Model.Type == "initial") { %>
    <h1>Free 30 Minute Consultation Details</h1>
    <p>Below are the details of free 30 minute consultation. If you would like to change the date or time, please click <%: Html.ActionLink("here", "Edit", "Consultation", new { id = Model.RoomId }, new { })%> to change the appointment.</p>
    <p>If you have already consulted with the designer, then please proceed to the next step: '<%: Html.ActionLink("Pay for your room", "Index", "RoomMeasurement", new { id = Model.RoomId }, new { })%>'.</p>
<% } else { %>
    <h1>Follow-up Consultation Details</h1>
    <p>Below are the details of follow-up consultation. If you would like to change the date or time, please click <%: Html.ActionLink("here", "Edit", "Consultation", new { id = Model.RoomId }, new { })%> to change the appointment.</p>
<% } %>
    <br />

        <div class="text-box-label-same-line">
            <label for="Date">Date:</label>
        </div>
        <div class="one-line-content-same-line">
            <%: Model.Date %>
        </div>

        <div class="text-box-label-same-line">
            <label for="Time">Time:</label>
        </div>
        <div class="one-line-content-same-line">
            <%: Model.Time %>&nbsp;<%: Model.TimeZone %>
        </div>

        <div class="text-box-label-same-line">
            <label for="ContactMethod">You will be contacted by:</label>
        </div>
        <div class="one-line-content-same-line">
            <%: Model.ContactMethod%>
        </div>

        <div class="text-box-label-same-line">
            <label for="PhoneNumber">Phone Number:</label>
        </div>
        <div class="one-line-content-same-line">
            <%: Model.PhoneNumber%>
        </div>
        <div class="text-box-label-same-line">
            <label for="SkypeId">Skype ID:</label>
        </div>
        <div class="one-line-content-same-line">
            <%: Model.SkypeId %>&nbsp;
        </div>
        <br /><br /><br />
    <div id="content-bottom-navigation">
        <%: Html.ActionLink("Change Appointment Date/Time", "Edit", "Consultation", new { id = Model.RoomId }, new { })%>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

