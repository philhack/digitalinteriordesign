﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessDesign.Models;
using System.Web.Hosting;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web.Security;
using com.mosso.cloudfiles;
using com.mosso.cloudfiles.domain;


namespace AccessDesign.Controllers
{
    public class RoomPhotoController : Controller
    {

        RoomRepository roomRepository = new RoomRepository();       // make a connection to the roomRepository model
        RoomPhotoRepository roomPhotoRepository = new RoomPhotoRepository();    // make a connection to the roomPhotoRepository

        private static readonly string UploadPath = System.Configuration.ConfigurationManager.AppSettings["uploadPath"];
        private static readonly string UploadPathForUrl = System.Configuration.ConfigurationManager.AppSettings["uploadPathForUrl"];
        private static readonly string RoomPhotosOriginalDirectory = System.Configuration.ConfigurationManager.AppSettings["roomPhotosOriginalDirectory"];
        private static readonly string RoomPhotosThumbnailDirectory = System.Configuration.ConfigurationManager.AppSettings["roomPhotosThumbnailDirectory"];
        private static readonly string RoomPhotosWebDirectory = System.Configuration.ConfigurationManager.AppSettings["roomPhotosWebDirectory"];
        private static readonly string RoomPhotosThumbnailRSUrl = System.Configuration.ConfigurationManager.AppSettings["roomPhotosThumbnailRSUrl"];
        private static readonly string RoomPhotosWebRSUrl = System.Configuration.ConfigurationManager.AppSettings["roomPhotosWebRSUrl"];
        private static readonly string RoomPhotosOriginalRSUrl = System.Configuration.ConfigurationManager.AppSettings["roomPhotosOriginalRSUrl"];




#region Upload Photo

        //
        // GET: /RoomPhoto/Upload

        /// <summary>
        ///  Loads the main upload photos web page
        /// </summary>
        /// <param name="id">
        /// id is the current room id
        /// </param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Upload(int id)
        {
            ViewData["RoomId"] = id;
            return View();
        }

        //
        // POST: /RoomPhoto/Upload


        /// <summary>
        /// Save the posted file to the Uploads folder and return a message
        /// back to the uploadify jQuery plugin. Uploading the first photo will also mark the upload photo step as complete
        /// </summary>
        /// <param name="id"></param>
        /// Accepts the id of the current room
        /// <param name="fileData"></param>
        /// <returns></returns>
        //[HttpPost, Authorize]
        [HttpPost]
        public ActionResult Upload(string token, int id, HttpPostedFileBase fileData)
        {
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(token);
             if(ticket!=null)
             {
                 var identity = new FormsIdentity(ticket);
                if(identity.IsAuthenticated)
                {
                    string userName = identity.Name.ToString(); // Get's the current logged on user name based on the forms authentication token
                    ViewData["RoomId"] = id;    // required to keep the view data room id
                    if(roomRepository.DoesRoomExist(id, userName))      // check if the room exists for the currenly logged on user and the id that is passed in.  Ya, this causes more DB reads, but we don't want virus's uploaded.
                    {
                        Sanitizer sanitize = new Sanitizer();   // create a new instance of the sanitizer
                        if (!sanitize.IsValidImage(fileData.FileName))   // check if the file name contains a valid file extension.  We are checking this again incase someone tampered with the javascript.
                            return Content("Error uploading file: The file must end in .jpg, .jpeg");

                        string sanitizedFileName = Sanitizer.CreateGuidRoomPhoto(fileData.FileName.ToString(), id);       // generate the file name "RoomId-GUID.ext"
                        string serverPhotoPath = @Server.MapPath(UploadPath + "\\");      // used as a basis for where to save the files to

                        // setup the initial paths and store them in the database
                        // we're not saving this to the database yet until all of the images have been generated and saved
                        // to use these server paths, you must map them.  IE:   @Server.MapPath(@roomPhoto.OriginalServerPath);
                        RoomPhoto roomPhoto = new RoomPhoto
                        {
                            RoomId = id,
                            PhotoName = sanitizedFileName,
                            OriginalServerPath = UploadPath + "\\" + RoomPhotosOriginalDirectory + "\\" + sanitizedFileName,                    // mapped path to the original image file
                            OriginalUrlPath = UploadPathForUrl + RoomPhotosOriginalDirectory + "/" + sanitizedFileName,
                            ThumbnailServerPath = UploadPath + "\\" + RoomPhotosThumbnailDirectory + "\\" + sanitizedFileName,                 // mapped path to the thumbnail directory
                            ThumbnailUrlPath = UploadPathForUrl + RoomPhotosThumbnailDirectory + "/" + sanitizedFileName,
                            WebServerPath = UploadPath + "\\" + RoomPhotosWebDirectory + "\\" + sanitizedFileName,                              // mapped path to the room photos web directory
                            WebUrlPath = UploadPathForUrl + RoomPhotosWebDirectory + "/" + sanitizedFileName,
                            OriginalRSContainer = RoomPhotosOriginalDirectory,
                            OriginalRSUrl = RoomPhotosOriginalRSUrl + sanitizedFileName,
                            ThumbnailRSContainer = RoomPhotosThumbnailDirectory,
                            ThumbnailRSUrl = RoomPhotosThumbnailRSUrl + sanitizedFileName,
                            WebRSContainer = RoomPhotosWebDirectory,
                            WebRSUrl = RoomPhotosWebRSUrl + sanitizedFileName
                        };


                        // Save the file to disk so that we can generate the thumbnail and web images from it
                        try
                        {
                            fileData.SaveAs(@Server.MapPath(roomPhoto.OriginalServerPath));
                        }
                        catch (Exception ex)
                        {
                            return Content("Error uploading file: " + ex.Message);
                        }


                        // if we're here, the we know that the file has been saved to disk, so let's create a thumbnail and web images.
                        try
                        {
                            // Generate a 100px x 100px thumnnail image. Set the focus as the center of the image. Crop the outsides
                            Image targetThumbnailPhoto = Image.FromFile(@Server.MapPath(roomPhoto.OriginalServerPath));
                            Image imgThumbnailPhoto = ImageResize.Crop(targetThumbnailPhoto, 100, 100, ImageResize.AnchorPosition.Center);
                            imgThumbnailPhoto.Save(@Server.MapPath(roomPhoto.ThumbnailServerPath), ImageFormat.Jpeg);
                            targetThumbnailPhoto.Dispose();
                            imgThumbnailPhoto.Dispose();

                            // Generate a 600px image for the web. Do not crop anything. Instead add additional padding.
                            Image targetWebPhoto = Image.FromFile(@Server.MapPath(roomPhoto.OriginalServerPath));
                            Image imgWebPhoto = ImageResize.FixedSize(targetWebPhoto, 600, 600);
                            imgWebPhoto.Save(@Server.MapPath(roomPhoto.WebServerPath), ImageFormat.Jpeg);
                            targetWebPhoto.Dispose();
                            imgWebPhoto.Dispose();

                        }
                        catch (Exception ex)
                        {
                            return Content("Error uploading file: " + ex.Message);
                        }

                        try
                        {
                            Room room = roomRepository.GetRoomById(id);     // fetch the current room from the database so that we can update it's last updated time
                            room.DateUpdated = DateTime.Now;                        // update the date/time
                            if (room.MenuStepId < 5)        // if the step is less than 5, then the questionnaire is complete. If it's greater than 5, the user is adding more photos
                            {
                                room.MenuStepId = 5;                          // mark the upload room photo step complete
                                room.CurrentStepId = 3;                       // mark the upload room photo step complete
                            }
                            roomPhotoRepository.Add(roomPhoto);                     // Add the photo to the DB
                            roomPhotoRepository.Save();
                            roomRepository.Save();      // save the updated room values to the DB
                        }
                        catch (Exception)
                        {
                            return Content("Error adding file to the database");
                        }

                        // making comments to upload files to rackspace clouds
                        /*
                         * This code will save the files to Rack Space Cloud Files. We need to work out the upload progress bar issue before we use this for the customers
                         *
                        UserCredentials userCreds = new UserCredentials(serverSettingsConfig.RackSpaceUserName, serverSettingsConfig.RackSpaceAPIKey);
                        Connection connection = new com.mosso.cloudfiles.Connection(userCreds);
                        connection.PutStorageItem("rpo", @Server.MapPath(roomPhoto.OriginalServerPath));                 // Container name = "rpo"
                        connection.PutStorageItem("rpt", @Server.MapPath(roomPhoto.ThumbnailServerPath));                 // Container name = "rpt"
                        connection.PutStorageItem("rpw", @Server.MapPath(roomPhoto.WebServerPath));                      // Container name = "rpw"
                        //connection.DeleteStorageItem("rpo", "91-4ac38b51-3ceb-462e-b314-3e18e6e9d217.jpg");            // File to delete under the container "rpo"

                        try
                        {
                            // delete the room images that were temporarily stored on the server
                            System.IO.File.Delete(@Server.MapPath(roomPhoto.OriginalServerPath));
                            System.IO.File.Delete(@Server.MapPath(roomPhoto.ThumbnailServerPath));
                            System.IO.File.Delete(@Server.MapPath(roomPhoto.WebServerPath));
                        }
                        catch (Exception)
                        {
                            TempData["ErrorMessage"] = "Sorry, an errror occured while we attempting to upload your photo.";
                            return RedirectToAction("Upload", new { id = roomPhoto.RoomId });
                            // TODO: add logic to log this error message
                        }

                        */

                        // store the file to the file system
                        // generate the thumbnail and web files
                        // upload the files to rackspace
                        // delete the local files off our computer
                        // we need to store the container name's in the web.config file

                        /*
                         * example:
                         *  Container = rpo
                         *  Path = http://c0003356.cdn2.cloudfiles.rackspacecloud.com/
                         *  File Name = 91-4ac38b51-3ceb-462e-b314-3e18e6e9d217.jpg
                         *  To view the file:  http://c0003356.cdn2.cloudfiles.rackspacecloud.com/91-4ac38b51-3ceb-462e-b314-3e18e6e9d217.jpg
                         *  To delete the file:  connection.DeleteStorageItem("container name", "91-4ac38b51-3ceb-462e-b314-3e18e6e9d217.jpg");
                         * 
                         */





                        return Content("File uploaded successfully!");
                     }
                }
             }
             throw new InvalidOperationException("The user is not authenticated.");
        }

        /// <summary>
        /// Return a list of files in the Uploads folder back 
        /// to the $.get jQuery Ajax call.
        /// </summary>
        /// <param name="id">
        /// The id of the current room
        /// </param>
        /// <returns></returns>
        [HttpGet, Authorize]
        public ActionResult GetPhotoList(int id)
        {   
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            if (!roomRepository.DoesRoomExist(id, userName))                // Check that the currently logged on user owns the room
            {
                TempData["ErrorMessage"] = "Sorry, we were unable to find this room.";
                return View("Error");
            }
            else
            {
                IEnumerable<RoomPhoto> roomPhotos = roomPhotoRepository.FindAllRoomPhotosForSpecificRoom(id);   // load all photos for the room
                return View("ListView", roomPhotos);
            }
        }

#endregion

        //
        // GET: /RoomPhoto/
        /// <summary>
        /// Displays the view that allows the user to navigate through the upload photos section.
        ///             1. Upload Photos
        ///             2. View/Edit Photos
        ///             3. Add Comments
        /// </summary>
        /// <param name="id">Room ID</param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Index(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // get the current room
            if (room == null)                // If the room does not exist, return an error
            {
                TempData["ErrorMessage"] = "Sorry, we were unable to find this room.";
                return View("Error");
            }
            else
            {
                if (room.MenuStepId < 4)    // Step 4 is room questionnaire.  User must complete questionnaire before uploading photos
                {
                    TempData["ErrorMessage"] = "Please complete the questionnaire below before uploading your room photos";
                    return RedirectToAction("Index", "Questionnaire", new { id = id });
                }
                else
                {
                    roomRepository.UpdateRoomDateUpdated(id, DateTime.Now);     //u pdate the room - This return a bool value, but we're not doing anything with this value right now
                    ViewData["RoomId"] = id;    // this is required by the room photo navigation
                    return View(room);      // otherwise return the view with the room
                }
            }
            
        }

        //
        // GET: /RoomPhoto/Details/5
        // parameter int id is the room ID
        [Authorize]
        public ActionResult Details(int id)
        {
            ViewData["RoomId"] = id;    // required to keep the view data room id
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            if (!roomRepository.DoesRoomExist(id, userName))                // Check that the currently logged on user owns the room
            {
                TempData["ErrorMessage"] = "Sorry, we were unable to find this room.";
                return View("Error");
            }
            else
            {
                IEnumerable<RoomPhoto> roomPhotos = roomPhotoRepository.FindAllRoomPhotosForSpecificRoom(id);   // load all photos for the room
                return View(roomPhotos);
            }
        }


        //
        // GET: /RoomPhoto/Comments/5
        // Updates the room photo comments section
        [Authorize]
        public ActionResult Comments(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // get the current room
            if (room == null)                // If the room does not exist, return an error
            {
                TempData["ErrorMessage"] = "Sorry, we were unable to find this room.";
                return View("Error");
            }
            else
            {
                ViewData["RoomId"] = id;    // used by the room photo navigation
                return View(room);      // otherwise return the view with the room
            }
        }

        [Authorize, HttpPost]
        public ActionResult Comments(int id, FormCollection collection)
        {
            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);
            ViewData["RoomId"] = id;    // used by the room photo navigation
            room.DateUpdated = DateTime.Now;        // set the date updated to the current time
            if (ModelState.IsValid && TryUpdateModel(room))
            {
                UpdateModel(room);
                roomRepository.Save();
                TempData["SuccessMessage"] = "Your room photo comments have been successfully updated.";
                return RedirectToAction("Comments", new { id = room.RoomId });
            }
            else
            {
                TempData["ErrorMessage"] = "Sorry, there was a problem updating your room photo comments.  Please try again.";
                return View(room);
            }
        }

        //
        // GET: /RoomPhoto/Delete/5
        // where id is the id of the photo to delete
        [Authorize]
        public ActionResult Delete(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name
            RoomPhoto roomPhoto = roomPhotoRepository.GetSingleRoomPhotoByRoomPhotoId(id);     // get the specific photo from the DB that we want to delete.

            ViewData["RoomId"] = roomPhoto.RoomId;         // used in the view to allow the user go back to a previous page
            if (roomRepository.DoesRoomExist(roomPhoto.RoomId, userName))     // make sure the person who wants to delete this actually owns it
            {
                return View(roomPhoto);        // return the furniture photo
            }
            else
            {
                TempData["ErrorMessage"] = "Sorry there, was a problem locating this rooms photo.";     // someone's trying to delete an image that dosn't belong to them.  give them an error
                return View("Error");
            }
        }

        //
        // POST: /RoomPhoto/Delete/5

        [HttpPost, Authorize]
        public ActionResult Delete(int id, string confirmButton)
        {
            RoomPhoto roomPhoto = roomPhotoRepository.GetSingleRoomPhotoByRoomPhotoId(id);     // get the specific photo from the DB.
            string roomId = roomPhoto.RoomId.ToString();  // extract the roomId from the room before it's deleted
            ViewData["RoomId"] = roomId;     // Set the view data since we'll use it in the next request

            string userName = HttpContext.User.Identity.Name.ToString();  // Get's the current logged on user name

            Room room = roomRepository.GetRoomByIdVerifyUserName(roomPhoto.RoomId, userName);       // get the room and verify the currenly logged on user owns it.
            if(room == null)
            {
                TempData["ErrorMessage"] = "Sorry, we were unable to find this room photo.";      // if the room doesn't exist or the currenly logged on user does not own the room, then throw an error
                return View("Error");
            }

            try
            {
                // delete the room images
                System.IO.File.Delete(@Server.MapPath(roomPhoto.OriginalServerPath));
                System.IO.File.Delete(@Server.MapPath(roomPhoto.ThumbnailServerPath));
                System.IO.File.Delete(@Server.MapPath(roomPhoto.WebServerPath));
            }
            catch (Exception)
            {
                TempData["ErrorMessage"] = "Sorry, we're unable to delete this room photo.";
                return RedirectToAction("Details", "RoomPhoto", new { id = roomId });
                // TODO: add logic to log this error message
            }

            try
            {
                // delete the room photo from the database
                roomPhotoRepository.Delete(roomPhoto);
                roomPhotoRepository.Save();
                room.DateUpdated = DateTime.Now;        // update the last modifed date for the room
                roomRepository.Save();
            }
            catch (Exception)
            {
                TempData["ErrorMessage"] = "Sorry, we're unable to delete that photo";
                return RedirectToAction("Details", "RoomPhoto", new { id = roomId });
            }

            TempData["SuccessMessage"] = "Your room photo has been successfully deleted.";
            return RedirectToAction("Details", "RoomPhoto", new {id = roomId });
        }


        //
        // GET: /RoomPhoto/DisplayToDesigner
        /// <summary>
        /// Displays all of the photos for a room to a designer
        /// </summary>
        /// <param name="id">room id</param>
        /// <returns></returns>
        [Authorize(Roles = "Designers")]
        public ActionResult DisplayToDesigner(int id)
        {
            IEnumerable<RoomPhoto> roomPhotos = roomPhotoRepository.FindAllRoomPhotosForSpecificRoom(id);
            ViewData["RoomPhotoComments"] = roomRepository.GetRoomPhotoComments(id);
            TempData["RoomId"] = id;
            return View(roomPhotos);
        }
    }
}
