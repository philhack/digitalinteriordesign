/****** Object:  ForeignKey [FK__aspnet_Me__Appli__21B6055D]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Me__Appli__21B6055D]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
ALTER TABLE [dbo].[aspnet_Membership] DROP CONSTRAINT [FK__aspnet_Me__Appli__21B6055D]
GO
/****** Object:  ForeignKey [FK__aspnet_Me__UserI__22AA2996]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Me__UserI__22AA2996]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
ALTER TABLE [dbo].[aspnet_Membership] DROP CONSTRAINT [FK__aspnet_Me__UserI__22AA2996]
GO
/****** Object:  ForeignKey [FK__aspnet_Pa__Appli__5AEE82B9]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pa__Appli__5AEE82B9]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]'))
ALTER TABLE [dbo].[aspnet_Paths] DROP CONSTRAINT [FK__aspnet_Pa__Appli__5AEE82B9]
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__PathI__628FA481]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__PathI__628FA481]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers]'))
ALTER TABLE [dbo].[aspnet_PersonalizationAllUsers] DROP CONSTRAINT [FK__aspnet_Pe__PathI__628FA481]
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__PathI__68487DD7]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__PathI__68487DD7]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] DROP CONSTRAINT [FK__aspnet_Pe__PathI__68487DD7]
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__UserI__693CA210]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__UserI__693CA210]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] DROP CONSTRAINT [FK__aspnet_Pe__UserI__693CA210]
GO
/****** Object:  ForeignKey [FK__aspnet_Pr__UserI__38996AB5]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pr__UserI__38996AB5]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Profile]'))
ALTER TABLE [dbo].[aspnet_Profile] DROP CONSTRAINT [FK__aspnet_Pr__UserI__38996AB5]
GO
/****** Object:  ForeignKey [FK__aspnet_Ro__Appli__440B1D61]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Ro__Appli__440B1D61]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]'))
ALTER TABLE [dbo].[aspnet_Roles] DROP CONSTRAINT [FK__aspnet_Ro__Appli__440B1D61]
GO
/****** Object:  ForeignKey [FK__aspnet_Us__Appli__0DAF0CB0]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__Appli__0DAF0CB0]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
ALTER TABLE [dbo].[aspnet_Users] DROP CONSTRAINT [FK__aspnet_Us__Appli__0DAF0CB0]
GO
/****** Object:  ForeignKey [FK__aspnet_Us__RoleI__4AB81AF0]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__RoleI__4AB81AF0]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]'))
ALTER TABLE [dbo].[aspnet_UsersInRoles] DROP CONSTRAINT [FK__aspnet_Us__RoleI__4AB81AF0]
GO
/****** Object:  ForeignKey [FK__aspnet_Us__UserI__49C3F6B7]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__UserI__49C3F6B7]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]'))
ALTER TABLE [dbo].[aspnet_UsersInRoles] DROP CONSTRAINT [FK__aspnet_Us__UserI__49C3F6B7]
GO
/****** Object:  ForeignKey [FK_BathroomQuestionnaires_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_BathroomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[BathroomQuestionnaires]'))
ALTER TABLE [dbo].[BathroomQuestionnaires] DROP CONSTRAINT [FK_BathroomQuestionnaires_Rooms]
GO
/****** Object:  ForeignKey [FK_Consultations_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Consultations_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[Consultations]'))
ALTER TABLE [dbo].[Consultations] DROP CONSTRAINT [FK_Consultations_Rooms]
GO
/****** Object:  ForeignKey [FK_CurrentSteps_CurrentSteps]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CurrentSteps_CurrentSteps]') AND parent_object_id = OBJECT_ID(N'[dbo].[CurrentSteps]'))
ALTER TABLE [dbo].[CurrentSteps] DROP CONSTRAINT [FK_CurrentSteps_CurrentSteps]
GO
/****** Object:  ForeignKey [FK_DesignFiles_DesignFileTypes]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DesignFiles_DesignFileTypes]') AND parent_object_id = OBJECT_ID(N'[dbo].[DesignFiles]'))
ALTER TABLE [dbo].[DesignFiles] DROP CONSTRAINT [FK_DesignFiles_DesignFileTypes]
GO
/****** Object:  ForeignKey [FK_DesignFiles_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DesignFiles_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[DesignFiles]'))
ALTER TABLE [dbo].[DesignFiles] DROP CONSTRAINT [FK_DesignFiles_Rooms]
GO
/****** Object:  ForeignKey [FK_DiningRoomQuestionnaires_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DiningRoomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[DiningRoomQuestionnaires]'))
ALTER TABLE [dbo].[DiningRoomQuestionnaires] DROP CONSTRAINT [FK_DiningRoomQuestionnaires_Rooms]
GO
/****** Object:  ForeignKey [FK_EntryQuestionnaires_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EntryQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[EntryQuestionnaires]'))
ALTER TABLE [dbo].[EntryQuestionnaires] DROP CONSTRAINT [FK_EntryQuestionnaires_Rooms]
GO
/****** Object:  ForeignKey [FK_Furniture_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Furniture_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[Furniture]'))
ALTER TABLE [dbo].[Furniture] DROP CONSTRAINT [FK_Furniture_Rooms]
GO
/****** Object:  ForeignKey [FK_FurniturePhotos_Furniture]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FurniturePhotos_Furniture]') AND parent_object_id = OBJECT_ID(N'[dbo].[FurniturePhotos]'))
ALTER TABLE [dbo].[FurniturePhotos] DROP CONSTRAINT [FK_FurniturePhotos_Furniture]
GO
/****** Object:  ForeignKey [FK_GuestRoomQuestionnaires_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GuestRoomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[GuestRoomQuestionnaires]'))
ALTER TABLE [dbo].[GuestRoomQuestionnaires] DROP CONSTRAINT [FK_GuestRoomQuestionnaires_Rooms]
GO
/****** Object:  ForeignKey [FK_KidsRoomQuestionnaires_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KidsRoomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[KidsRoomQuestionnaires]'))
ALTER TABLE [dbo].[KidsRoomQuestionnaires] DROP CONSTRAINT [FK_KidsRoomQuestionnaires_Rooms]
GO
/****** Object:  ForeignKey [FK_KitchenQuestionnaires_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KitchenQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[KitchenQuestionnaires]'))
ALTER TABLE [dbo].[KitchenQuestionnaires] DROP CONSTRAINT [FK_KitchenQuestionnaires_Rooms]
GO
/****** Object:  ForeignKey [FK_LivingRoomQuestionnaires_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LivingRoomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[LivingRoomQuestionnaires]'))
ALTER TABLE [dbo].[LivingRoomQuestionnaires] DROP CONSTRAINT [FK_LivingRoomQuestionnaires_Rooms]
GO
/****** Object:  ForeignKey [FK_MasterBedroomQuestionnaires_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MasterBedroomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[MasterBedroomQuestionnaires]'))
ALTER TABLE [dbo].[MasterBedroomQuestionnaires] DROP CONSTRAINT [FK_MasterBedroomQuestionnaires_Rooms]
GO
/****** Object:  ForeignKey [FK_MediaRoomQuestionnaires_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MediaRoomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[MediaRoomQuestionnaires]'))
ALTER TABLE [dbo].[MediaRoomQuestionnaires] DROP CONSTRAINT [FK_MediaRoomQuestionnaires_Rooms]
GO
/****** Object:  ForeignKey [FK_OfficeQuestionnaires_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OfficeQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[OfficeQuestionnaires]'))
ALTER TABLE [dbo].[OfficeQuestionnaires] DROP CONSTRAINT [FK_OfficeQuestionnaires_Rooms]
GO
/****** Object:  ForeignKey [FK_Orders_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Orders_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[Orders]'))
ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_Orders_Rooms]
GO
/****** Object:  ForeignKey [FK_RoomMeasurements_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RoomMeasurements_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[RoomMeasurements]'))
ALTER TABLE [dbo].[RoomMeasurements] DROP CONSTRAINT [FK_RoomMeasurements_Rooms]
GO
/****** Object:  ForeignKey [FK_RoomPhotos_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RoomPhotos_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[RoomPhotos]'))
ALTER TABLE [dbo].[RoomPhotos] DROP CONSTRAINT [FK_RoomPhotos_Rooms]
GO
/****** Object:  ForeignKey [FK_Rooms_CurrentDesignSteps]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Rooms_CurrentDesignSteps]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
ALTER TABLE [dbo].[Rooms] DROP CONSTRAINT [FK_Rooms_CurrentDesignSteps]
GO
/****** Object:  ForeignKey [FK_Rooms_CurrentSteps]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Rooms_CurrentSteps]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
ALTER TABLE [dbo].[Rooms] DROP CONSTRAINT [FK_Rooms_CurrentSteps]
GO
/****** Object:  ForeignKey [FK_Rooms_Profiles]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Rooms_Profiles]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
ALTER TABLE [dbo].[Rooms] DROP CONSTRAINT [FK_Rooms_Profiles]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_DeleteProfiles]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_DeleteProfiles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Profile_DeleteProfiles]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetProfiles]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetProfiles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Profile_GetProfiles]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetProperties]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetProperties]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Profile_GetProperties]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_SetProperties]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_SetProperties]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Profile_SetProperties]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_GetPageSettings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_SetPageSettings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_GetPageSettings]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_GetPageSettings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationPerUser_GetPageSettings]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_ResetPageSettings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_SetPageSettings]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_SetPageSettings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationPerUser_SetPageSettings]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_DeleteAllState]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_DeleteAllState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAdministration_DeleteAllState]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_FindState]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_FindState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAdministration_FindState]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_GetCountOfState]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_GetCountOfState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAdministration_GetCountOfState]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_ResetSharedState]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_ResetSharedState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetSharedState]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_ResetUserState]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_ResetUserState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetUserState]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_AnyDataInTables]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_AnyDataInTables]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_AnyDataInTables]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_CreateUser]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_CreateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_CreateUser]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_FindUsersByEmail]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_FindUsersByEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_FindUsersByEmail]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_FindUsersByName]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_FindUsersByName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_FindUsersByName]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetAllUsers]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetAllUsers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetAllUsers]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetNumberOfUsersOnline]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetNumberOfUsersOnline]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetNumberOfUsersOnline]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetPassword]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetPassword]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetPasswordWithFormat]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetPasswordWithFormat]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetPasswordWithFormat]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByEmail]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetUserByEmail]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByName]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetUserByName]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByUserId]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByUserId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetUserByUserId]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_ResetPassword]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_ResetPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_ResetPassword]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_SetPassword]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_SetPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_SetPassword]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UnlockUser]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UnlockUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_UnlockUser]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UpdateUser]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UpdateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_UpdateUser]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UpdateUserInfo]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UpdateUserInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_UpdateUserInfo]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_DeleteRole]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_DeleteRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Roles_DeleteRole]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Users_DeleteUser]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users_DeleteUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Users_DeleteUser]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_AddUsersToRoles]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_AddUsersToRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UsersInRoles_AddUsersToRoles]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_FindUsersInRole]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_FindUsersInRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UsersInRoles_FindUsersInRole]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_GetRolesForUser]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_GetRolesForUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UsersInRoles_GetRolesForUser]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_GetUsersInRoles]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_GetUsersInRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UsersInRoles_GetUsersInRoles]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_IsUserInRole]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_IsUserInRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UsersInRoles_IsUserInRole]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_DeleteInactiveProfiles]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_DeleteInactiveProfiles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Profile_DeleteInactiveProfiles]
GO
/****** Object:  Table [dbo].[FurniturePhotos]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FurniturePhotos]') AND type in (N'U'))
DROP TABLE [dbo].[FurniturePhotos]
GO
/****** Object:  StoredProcedure [dbo].[UpdateDesignFileComments]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateDesignFileComments]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateDesignFileComments]
GO
/****** Object:  View [dbo].[vw_aspnet_MembershipUsers]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_MembershipUsers]'))
DROP VIEW [dbo].[vw_aspnet_MembershipUsers]
GO
/****** Object:  View [dbo].[vw_aspnet_Profiles]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Profiles]'))
DROP VIEW [dbo].[vw_aspnet_Profiles]
GO
/****** Object:  View [dbo].[vw_aspnet_UsersInRoles]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_UsersInRoles]'))
DROP VIEW [dbo].[vw_aspnet_UsersInRoles]
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_Shared]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_Shared]'))
DROP VIEW [dbo].[vw_aspnet_WebPartState_Shared]
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_User]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_User]'))
DROP VIEW [dbo].[vw_aspnet_WebPartState_User]
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_Paths]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_Paths]'))
DROP VIEW [dbo].[vw_aspnet_WebPartState_Paths]
GO
/****** Object:  View [dbo].[vw_aspnet_Roles]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Roles]'))
DROP VIEW [dbo].[vw_aspnet_Roles]
GO
/****** Object:  View [dbo].[vw_aspnet_Users]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Users]'))
DROP VIEW [dbo].[vw_aspnet_Users]
GO
/****** Object:  StoredProcedure [dbo].[UpdateRoomDateUpdated]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateRoomDateUpdated]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateRoomDateUpdated]
GO
/****** Object:  Table [dbo].[OfficeQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OfficeQuestionnaires]') AND type in (N'U'))
DROP TABLE [dbo].[OfficeQuestionnaires]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Orders]') AND type in (N'U'))
DROP TABLE [dbo].[Orders]
GO
/****** Object:  Table [dbo].[GuestRoomQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GuestRoomQuestionnaires]') AND type in (N'U'))
DROP TABLE [dbo].[GuestRoomQuestionnaires]
GO
/****** Object:  Table [dbo].[KidsRoomQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KidsRoomQuestionnaires]') AND type in (N'U'))
DROP TABLE [dbo].[KidsRoomQuestionnaires]
GO
/****** Object:  Table [dbo].[KitchenQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KitchenQuestionnaires]') AND type in (N'U'))
DROP TABLE [dbo].[KitchenQuestionnaires]
GO
/****** Object:  Table [dbo].[LivingRoomQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LivingRoomQuestionnaires]') AND type in (N'U'))
DROP TABLE [dbo].[LivingRoomQuestionnaires]
GO
/****** Object:  Table [dbo].[MasterBedroomQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MasterBedroomQuestionnaires]') AND type in (N'U'))
DROP TABLE [dbo].[MasterBedroomQuestionnaires]
GO
/****** Object:  Table [dbo].[MediaRoomQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MediaRoomQuestionnaires]') AND type in (N'U'))
DROP TABLE [dbo].[MediaRoomQuestionnaires]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_CreateRole]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_CreateRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Roles_CreateRole]
GO
/****** Object:  Table [dbo].[BathroomQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BathroomQuestionnaires]') AND type in (N'U'))
DROP TABLE [dbo].[BathroomQuestionnaires]
GO
/****** Object:  Table [dbo].[Consultations]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Consultations]') AND type in (N'U'))
DROP TABLE [dbo].[Consultations]
GO
/****** Object:  Table [dbo].[DesignFiles]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DesignFiles]') AND type in (N'U'))
DROP TABLE [dbo].[DesignFiles]
GO
/****** Object:  Table [dbo].[DiningRoomQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DiningRoomQuestionnaires]') AND type in (N'U'))
DROP TABLE [dbo].[DiningRoomQuestionnaires]
GO
/****** Object:  Table [dbo].[EntryQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EntryQuestionnaires]') AND type in (N'U'))
DROP TABLE [dbo].[EntryQuestionnaires]
GO
/****** Object:  Table [dbo].[Furniture]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Furniture]') AND type in (N'U'))
DROP TABLE [dbo].[Furniture]
GO
/****** Object:  Table [dbo].[RoomMeasurements]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RoomMeasurements]') AND type in (N'U'))
DROP TABLE [dbo].[RoomMeasurements]
GO
/****** Object:  Table [dbo].[RoomPhotos]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RoomPhotos]') AND type in (N'U'))
DROP TABLE [dbo].[RoomPhotos]
GO
/****** Object:  Table [dbo].[aspnet_UsersInRoles]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_UsersInRoles]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_GetAllRoles]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_GetAllRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Roles_GetAllRoles]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_RoleExists]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_RoleExists]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Roles_RoleExists]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Users_CreateUser]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users_CreateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Users_CreateUser]
GO
/****** Object:  Table [dbo].[aspnet_PersonalizationAllUsers]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_PersonalizationAllUsers]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Paths_CreatePath]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Paths_CreatePath]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Paths_CreatePath]
GO
/****** Object:  Table [dbo].[aspnet_Membership]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Membership]
GO
/****** Object:  Table [dbo].[aspnet_Profile]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Profile]
GO
/****** Object:  Table [dbo].[aspnet_PersonalizationPerUser]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_PersonalizationPerUser]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_RegisterSchemaVersion]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_RegisterSchemaVersion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_RegisterSchemaVersion]
GO
/****** Object:  Table [dbo].[aspnet_Roles]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Roles]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Personalization_GetApplicationId]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Personalization_GetApplicationId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Personalization_GetApplicationId]
GO
/****** Object:  Table [dbo].[aspnet_Paths]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Paths]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Applications_CreateApplication]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications_CreateApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Applications_CreateApplication]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_CheckSchemaVersion]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_CheckSchemaVersion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_CheckSchemaVersion]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UnRegisterSchemaVersion]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UnRegisterSchemaVersion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UnRegisterSchemaVersion]
GO
/****** Object:  Table [dbo].[aspnet_Users]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Users]
GO
/****** Object:  Table [dbo].[Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Rooms]') AND type in (N'U'))
DROP TABLE [dbo].[Rooms]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_WebEvent_LogEvent]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_WebEvent_LogEvent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_WebEvent_LogEvent]
GO
/****** Object:  View [dbo].[vw_aspnet_Applications]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Applications]'))
DROP VIEW [dbo].[vw_aspnet_Applications]
GO
/****** Object:  Table [dbo].[GeneralQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GeneralQuestionnaires]') AND type in (N'U'))
DROP TABLE [dbo].[GeneralQuestionnaires]
GO
/****** Object:  Table [dbo].[PortfolioPictures]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PortfolioPictures]') AND type in (N'U'))
DROP TABLE [dbo].[PortfolioPictures]
GO
/****** Object:  Table [dbo].[Profiles]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Profiles]') AND type in (N'U'))
DROP TABLE [dbo].[Profiles]
GO
/****** Object:  Table [dbo].[MenuSteps]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MenuSteps]') AND type in (N'U'))
DROP TABLE [dbo].[MenuSteps]
GO
/****** Object:  StoredProcedure [dbo].[UpdateAnswerName]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateAnswerName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateAnswerName]
GO
/****** Object:  Table [dbo].[CurrentDesignSteps]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CurrentDesignSteps]') AND type in (N'U'))
DROP TABLE [dbo].[CurrentDesignSteps]
GO
/****** Object:  Table [dbo].[CurrentSteps]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CurrentSteps]') AND type in (N'U'))
DROP TABLE [dbo].[CurrentSteps]
GO
/****** Object:  Table [dbo].[DesignFileTypes]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DesignFileTypes]') AND type in (N'U'))
DROP TABLE [dbo].[DesignFileTypes]
GO
/****** Object:  Table [dbo].[RoomType]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RoomType]') AND type in (N'U'))
DROP TABLE [dbo].[RoomType]
GO
/****** Object:  Table [dbo].[States]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[States]') AND type in (N'U'))
DROP TABLE [dbo].[States]
GO
/****** Object:  Table [dbo].[aspnet_SchemaVersions]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_SchemaVersions]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_SchemaVersions]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Setup_RemoveAllRoleMembers]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Setup_RemoveAllRoleMembers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Setup_RemoveAllRoleMembers]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Setup_RestorePermissions]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Setup_RestorePermissions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Setup_RestorePermissions]
GO
/****** Object:  Table [dbo].[aspnet_WebEvent_Events]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_WebEvent_Events]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_WebEvent_Events]
GO
/****** Object:  Table [dbo].[aspnet_Applications]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Applications]
GO
/****** Object:  Default [DF__aspnet_Ap__Appli__08EA5793]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Ap__Appli__08EA5793]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Applications]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Ap__Appli__08EA5793]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Applications] DROP CONSTRAINT [DF__aspnet_Ap__Appli__08EA5793]
END


End
GO
/****** Object:  Default [DF__aspnet_Me__Passw__239E4DCF]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Me__Passw__239E4DCF]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Me__Passw__239E4DCF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Membership] DROP CONSTRAINT [DF__aspnet_Me__Passw__239E4DCF]
END


End
GO
/****** Object:  Default [DF__aspnet_Pa__PathI__5BE2A6F2]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Pa__PathI__5BE2A6F2]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Pa__PathI__5BE2A6F2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Paths] DROP CONSTRAINT [DF__aspnet_Pa__PathI__5BE2A6F2]
END


End
GO
/****** Object:  Default [DF__aspnet_Perso__Id__6754599E]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Perso__Id__6754599E]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Perso__Id__6754599E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] DROP CONSTRAINT [DF__aspnet_Perso__Id__6754599E]
END


End
GO
/****** Object:  Default [DF__aspnet_Ro__RoleI__44FF419A]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Ro__RoleI__44FF419A]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Ro__RoleI__44FF419A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Roles] DROP CONSTRAINT [DF__aspnet_Ro__RoleI__44FF419A]
END


End
GO
/****** Object:  Default [DF__aspnet_Us__UserI__0EA330E9]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__UserI__0EA330E9]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Us__UserI__0EA330E9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] DROP CONSTRAINT [DF__aspnet_Us__UserI__0EA330E9]
END


End
GO
/****** Object:  Default [DF__aspnet_Us__Mobil__0F975522]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__Mobil__0F975522]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Us__Mobil__0F975522]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] DROP CONSTRAINT [DF__aspnet_Us__Mobil__0F975522]
END


End
GO
/****** Object:  Default [DF__aspnet_Us__IsAno__108B795B]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__IsAno__108B795B]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Us__IsAno__108B795B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] DROP CONSTRAINT [DF__aspnet_Us__IsAno__108B795B]
END


End
GO
/****** Object:  Default [DF_Orders_TaxAmount]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Orders_TaxAmount]') AND parent_object_id = OBJECT_ID(N'[dbo].[Orders]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Orders_TaxAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [DF_Orders_TaxAmount]
END


End
GO
/****** Object:  Default [DF_Orders_Total]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Orders_Total]') AND parent_object_id = OBJECT_ID(N'[dbo].[Orders]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Orders_Total]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [DF_Orders_Total]
END


End
GO
/****** Object:  Default [DF_Rooms_CurrentStep]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Rooms_CurrentStep]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Rooms_CurrentStep]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Rooms] DROP CONSTRAINT [DF_Rooms_CurrentStep]
END


End
GO
/****** Object:  Default [DF_Rooms_RoomPaidFor]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Rooms_RoomPaidFor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Rooms_RoomPaidFor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Rooms] DROP CONSTRAINT [DF_Rooms_RoomPaidFor]
END


End
GO
/****** Object:  Default [DF_Rooms_TotalFreeRevisionsUsed]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Rooms_TotalFreeRevisionsUsed]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Rooms_TotalFreeRevisionsUsed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Rooms] DROP CONSTRAINT [DF_Rooms_TotalFreeRevisionsUsed]
END


End
GO
/****** Object:  Default [DF_Rooms_TotalRevisionsPaidFor]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Rooms_TotalRevisionsPaidFor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Rooms_TotalRevisionsPaidFor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Rooms] DROP CONSTRAINT [DF_Rooms_TotalRevisionsPaidFor]
END


End
GO
/****** Object:  Default [DF_Rooms_CurrentDesignStepId]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Rooms_CurrentDesignStepId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Rooms_CurrentDesignStepId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Rooms] DROP CONSTRAINT [DF_Rooms_CurrentDesignStepId]
END


End
GO
/****** Object:  Default [DF_Rooms_RoomTotalCost]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Rooms_RoomTotalCost]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Rooms_RoomTotalCost]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Rooms] DROP CONSTRAINT [DF_Rooms_RoomTotalCost]
END


End
GO
/****** Object:  Default [DF_Rooms_RoomTaxCost]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Rooms_RoomTaxCost]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Rooms_RoomTaxCost]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Rooms] DROP CONSTRAINT [DF_Rooms_RoomTaxCost]
END


End
GO
/****** Object:  Default [DF_Rooms_TaxPercent]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Rooms_TaxPercent]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Rooms_TaxPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Rooms] DROP CONSTRAINT [DF_Rooms_TaxPercent]
END


End
GO
/****** Object:  Default [DF_States_TaxPercent]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_States_TaxPercent]') AND parent_object_id = OBJECT_ID(N'[dbo].[States]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_States_TaxPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[States] DROP CONSTRAINT [DF_States_TaxPercent]
END


End
GO
/****** Object:  Schema [aspnet_Membership_BasicAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Membership_BasicAccess')
DROP SCHEMA [aspnet_Membership_BasicAccess]
GO
/****** Object:  Schema [aspnet_Membership_FullAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Membership_FullAccess')
DROP SCHEMA [aspnet_Membership_FullAccess]
GO
/****** Object:  Schema [aspnet_Membership_ReportingAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Membership_ReportingAccess')
DROP SCHEMA [aspnet_Membership_ReportingAccess]
GO
/****** Object:  Schema [aspnet_Personalization_BasicAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Personalization_BasicAccess')
DROP SCHEMA [aspnet_Personalization_BasicAccess]
GO
/****** Object:  Schema [aspnet_Personalization_FullAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Personalization_FullAccess')
DROP SCHEMA [aspnet_Personalization_FullAccess]
GO
/****** Object:  Schema [aspnet_Personalization_ReportingAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Personalization_ReportingAccess')
DROP SCHEMA [aspnet_Personalization_ReportingAccess]
GO
/****** Object:  Schema [aspnet_Profile_BasicAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Profile_BasicAccess')
DROP SCHEMA [aspnet_Profile_BasicAccess]
GO
/****** Object:  Schema [aspnet_Profile_FullAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Profile_FullAccess')
DROP SCHEMA [aspnet_Profile_FullAccess]
GO
/****** Object:  Schema [aspnet_Profile_ReportingAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Profile_ReportingAccess')
DROP SCHEMA [aspnet_Profile_ReportingAccess]
GO
/****** Object:  Schema [aspnet_Roles_BasicAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Roles_BasicAccess')
DROP SCHEMA [aspnet_Roles_BasicAccess]
GO
/****** Object:  Schema [aspnet_Roles_FullAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Roles_FullAccess')
DROP SCHEMA [aspnet_Roles_FullAccess]
GO
/****** Object:  Schema [aspnet_Roles_ReportingAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Roles_ReportingAccess')
DROP SCHEMA [aspnet_Roles_ReportingAccess]
GO
/****** Object:  Schema [aspnet_WebEvent_FullAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_WebEvent_FullAccess')
DROP SCHEMA [aspnet_WebEvent_FullAccess]
GO
/****** Object:  Role [aspnet_Membership_BasicAccess]    Script Date: 09/25/2011 10:57:53 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Membership_BasicAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_BasicAccess' AND type = 'R')
DROP ROLE [aspnet_Membership_BasicAccess]
GO
/****** Object:  Role [aspnet_Membership_FullAccess]    Script Date: 09/25/2011 10:57:53 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Membership_FullAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_FullAccess' AND type = 'R')
DROP ROLE [aspnet_Membership_FullAccess]
GO
/****** Object:  Role [aspnet_Membership_ReportingAccess]    Script Date: 09/25/2011 10:57:53 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Membership_ReportingAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_ReportingAccess' AND type = 'R')
DROP ROLE [aspnet_Membership_ReportingAccess]
GO
/****** Object:  Role [aspnet_Personalization_BasicAccess]    Script Date: 09/25/2011 10:57:53 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Personalization_BasicAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Personalization_BasicAccess' AND type = 'R')
DROP ROLE [aspnet_Personalization_BasicAccess]
GO
/****** Object:  Role [aspnet_Personalization_FullAccess]    Script Date: 09/25/2011 10:57:53 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Personalization_FullAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Personalization_FullAccess' AND type = 'R')
DROP ROLE [aspnet_Personalization_FullAccess]
GO
/****** Object:  Role [aspnet_Personalization_ReportingAccess]    Script Date: 09/25/2011 10:57:53 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Personalization_ReportingAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Personalization_ReportingAccess' AND type = 'R')
DROP ROLE [aspnet_Personalization_ReportingAccess]
GO
/****** Object:  Role [aspnet_Profile_BasicAccess]    Script Date: 09/25/2011 10:57:53 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Profile_BasicAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Profile_BasicAccess' AND type = 'R')
DROP ROLE [aspnet_Profile_BasicAccess]
GO
/****** Object:  Role [aspnet_Profile_FullAccess]    Script Date: 09/25/2011 10:57:53 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Profile_FullAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Profile_FullAccess' AND type = 'R')
DROP ROLE [aspnet_Profile_FullAccess]
GO
/****** Object:  Role [aspnet_Profile_ReportingAccess]    Script Date: 09/25/2011 10:57:53 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Profile_ReportingAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Profile_ReportingAccess' AND type = 'R')
DROP ROLE [aspnet_Profile_ReportingAccess]
GO
/****** Object:  Role [aspnet_Roles_BasicAccess]    Script Date: 09/25/2011 10:57:53 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Roles_BasicAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Roles_BasicAccess' AND type = 'R')
DROP ROLE [aspnet_Roles_BasicAccess]
GO
/****** Object:  Role [aspnet_Roles_FullAccess]    Script Date: 09/25/2011 10:57:53 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Roles_FullAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Roles_FullAccess' AND type = 'R')
DROP ROLE [aspnet_Roles_FullAccess]
GO
/****** Object:  Role [aspnet_Roles_ReportingAccess]    Script Date: 09/25/2011 10:57:53 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Roles_ReportingAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Roles_ReportingAccess' AND type = 'R')
DROP ROLE [aspnet_Roles_ReportingAccess]
GO
/****** Object:  Role [aspnet_WebEvent_FullAccess]    Script Date: 09/25/2011 10:57:53 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_WebEvent_FullAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_WebEvent_FullAccess' AND type = 'R')
DROP ROLE [aspnet_WebEvent_FullAccess]
GO
/****** Object:  Role [AccessDesignUser]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'AccessDesignUser')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'AccessDesignUser' AND type = 'R')
CREATE ROLE [AccessDesignUser]

END
GO
/****** Object:  Role [aspnet_Membership_BasicAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_BasicAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_BasicAccess' AND type = 'R')
CREATE ROLE [aspnet_Membership_BasicAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Membership_FullAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_FullAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_FullAccess' AND type = 'R')
CREATE ROLE [aspnet_Membership_FullAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Membership_ReportingAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_ReportingAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_ReportingAccess' AND type = 'R')
CREATE ROLE [aspnet_Membership_ReportingAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Personalization_BasicAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Personalization_BasicAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Personalization_BasicAccess' AND type = 'R')
CREATE ROLE [aspnet_Personalization_BasicAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Personalization_FullAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Personalization_FullAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Personalization_FullAccess' AND type = 'R')
CREATE ROLE [aspnet_Personalization_FullAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Personalization_ReportingAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Personalization_ReportingAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Personalization_ReportingAccess' AND type = 'R')
CREATE ROLE [aspnet_Personalization_ReportingAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Profile_BasicAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Profile_BasicAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Profile_BasicAccess' AND type = 'R')
CREATE ROLE [aspnet_Profile_BasicAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Profile_FullAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Profile_FullAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Profile_FullAccess' AND type = 'R')
CREATE ROLE [aspnet_Profile_FullAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Profile_ReportingAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Profile_ReportingAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Profile_ReportingAccess' AND type = 'R')
CREATE ROLE [aspnet_Profile_ReportingAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Roles_BasicAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Roles_BasicAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Roles_BasicAccess' AND type = 'R')
CREATE ROLE [aspnet_Roles_BasicAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Roles_FullAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Roles_FullAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Roles_FullAccess' AND type = 'R')
CREATE ROLE [aspnet_Roles_FullAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Roles_ReportingAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Roles_ReportingAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Roles_ReportingAccess' AND type = 'R')
CREATE ROLE [aspnet_Roles_ReportingAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_WebEvent_FullAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_WebEvent_FullAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_WebEvent_FullAccess' AND type = 'R')
CREATE ROLE [aspnet_WebEvent_FullAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Schema [aspnet_Membership_BasicAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Membership_BasicAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Membership_BasicAccess] AUTHORIZATION [aspnet_Membership_BasicAccess]'
GO
/****** Object:  Schema [aspnet_Membership_FullAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Membership_FullAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Membership_FullAccess] AUTHORIZATION [aspnet_Membership_FullAccess]'
GO
/****** Object:  Schema [aspnet_Membership_ReportingAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Membership_ReportingAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Membership_ReportingAccess] AUTHORIZATION [aspnet_Membership_ReportingAccess]'
GO
/****** Object:  Schema [aspnet_Personalization_BasicAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Personalization_BasicAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Personalization_BasicAccess] AUTHORIZATION [aspnet_Personalization_BasicAccess]'
GO
/****** Object:  Schema [aspnet_Personalization_FullAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Personalization_FullAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Personalization_FullAccess] AUTHORIZATION [aspnet_Personalization_FullAccess]'
GO
/****** Object:  Schema [aspnet_Personalization_ReportingAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Personalization_ReportingAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Personalization_ReportingAccess] AUTHORIZATION [aspnet_Personalization_ReportingAccess]'
GO
/****** Object:  Schema [aspnet_Profile_BasicAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Profile_BasicAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Profile_BasicAccess] AUTHORIZATION [aspnet_Profile_BasicAccess]'
GO
/****** Object:  Schema [aspnet_Profile_FullAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Profile_FullAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Profile_FullAccess] AUTHORIZATION [aspnet_Profile_FullAccess]'
GO
/****** Object:  Schema [aspnet_Profile_ReportingAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Profile_ReportingAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Profile_ReportingAccess] AUTHORIZATION [aspnet_Profile_ReportingAccess]'
GO
/****** Object:  Schema [aspnet_Roles_BasicAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Roles_BasicAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Roles_BasicAccess] AUTHORIZATION [aspnet_Roles_BasicAccess]'
GO
/****** Object:  Schema [aspnet_Roles_FullAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Roles_FullAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Roles_FullAccess] AUTHORIZATION [aspnet_Roles_FullAccess]'
GO
/****** Object:  Schema [aspnet_Roles_ReportingAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Roles_ReportingAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Roles_ReportingAccess] AUTHORIZATION [aspnet_Roles_ReportingAccess]'
GO
/****** Object:  Schema [aspnet_WebEvent_FullAccess]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_WebEvent_FullAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_WebEvent_FullAccess] AUTHORIZATION [aspnet_WebEvent_FullAccess]'
GO
/****** Object:  Table [dbo].[aspnet_Applications]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Applications](
	[ApplicationName] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LoweredApplicationName] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[Description] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK__aspnet_A__C93A4C98014935CB] PRIMARY KEY NONCLUSTERED 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON),
 CONSTRAINT [UQ__aspnet_A__17477DE40425A276] UNIQUE NONCLUSTERED 
(
	[LoweredApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON),
 CONSTRAINT [UQ__aspnet_A__3091033107020F21] UNIQUE NONCLUSTERED 
(
	[ApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications]') AND name = N'aspnet_Applications_Index')
CREATE CLUSTERED INDEX [aspnet_Applications_Index] ON [dbo].[aspnet_Applications] 
(
	[LoweredApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
INSERT [dbo].[aspnet_Applications] ([ApplicationName], [LoweredApplicationName], [ApplicationId], [Description]) VALUES (N'/', N'/', N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', NULL)
/****** Object:  Table [dbo].[aspnet_WebEvent_Events]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_WebEvent_Events]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_WebEvent_Events](
	[EventId] [char](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[EventTimeUtc] [datetime] NOT NULL,
	[EventTime] [datetime] NOT NULL,
	[EventType] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[EventSequence] [decimal](19, 0) NOT NULL,
	[EventOccurrence] [decimal](19, 0) NOT NULL,
	[EventCode] [int] NOT NULL,
	[EventDetailCode] [int] NOT NULL,
	[Message] [nvarchar](1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ApplicationPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ApplicationVirtualPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MachineName] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[RequestUrl] [nvarchar](1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ExceptionType] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Details] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK__aspnet_W__7944C810797309D9] PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Setup_RestorePermissions]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Setup_RestorePermissions]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Setup_RestorePermissions]
    @name   sysname
AS
BEGIN
    DECLARE @object sysname
    DECLARE @protectType char(10)
    DECLARE @action varchar(60)
    DECLARE @grantee sysname
    DECLARE @cmd nvarchar(500)
    DECLARE c1 cursor FORWARD_ONLY FOR
        SELECT Object, ProtectType, [Action], Grantee FROM #aspnet_Permissions where Object = @name

    OPEN c1

    FETCH c1 INTO @object, @protectType, @action, @grantee
    WHILE (@@fetch_status = 0)
    BEGIN
        SET @cmd = @protectType + '' '' + @action + '' on '' + @object + '' TO ['' + @grantee + '']''
        EXEC (@cmd)
        FETCH c1 INTO @object, @protectType, @action, @grantee
    END

    CLOSE c1
    DEALLOCATE c1
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Setup_RemoveAllRoleMembers]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Setup_RemoveAllRoleMembers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Setup_RemoveAllRoleMembers]
    @name   sysname
AS
BEGIN
    CREATE TABLE #aspnet_RoleMembers
    (
        Group_name      sysname,
        Group_id        smallint,
        Users_in_group  sysname,
        User_id         smallint
    )

    INSERT INTO #aspnet_RoleMembers
    EXEC sp_helpuser @name

    DECLARE @user_id smallint
    DECLARE @cmd nvarchar(500)
    DECLARE c1 cursor FORWARD_ONLY FOR
        SELECT User_id FROM #aspnet_RoleMembers

    OPEN c1

    FETCH c1 INTO @user_id
    WHILE (@@fetch_status = 0)
    BEGIN
        SET @cmd = ''EXEC sp_droprolemember '' + '''''''' + @name + '''''', '''''' + USER_NAME(@user_id) + ''''''''
        EXEC (@cmd)
        FETCH c1 INTO @user_id
    END

    CLOSE c1
    DEALLOCATE c1
END' 
END
GO
/****** Object:  Table [dbo].[aspnet_SchemaVersions]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_SchemaVersions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_SchemaVersions](
	[Feature] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CompatibleSchemaVersion] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsCurrentVersion] [bit] NOT NULL,
 CONSTRAINT [PK__aspnet_S__5A1E6BC11367E606] PRIMARY KEY CLUSTERED 
(
	[Feature] ASC,
	[CompatibleSchemaVersion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'common', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'health monitoring', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'membership', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'personalization', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'profile', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'role manager', N'1', 1)
/****** Object:  Table [dbo].[States]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[States]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[States](
	[StateId] [int] IDENTITY(1,1) NOT NULL,
	[StateName] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[StateAbbreviation] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[TaxPercent] [decimal](9, 0) NOT NULL,
	[Country] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[TaxType] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_States] PRIMARY KEY CLUSTERED 
(
	[StateId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'States', N'COLUMN',N'Country'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Which country does this belong do - CA (Canada)   -  US (USA)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'States', @level2type=N'COLUMN',@level2name=N'Country'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'States', N'COLUMN',N'TaxType'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The type of tax that is charged.  For example, GST, HST' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'States', @level2type=N'COLUMN',@level2name=N'TaxType'
GO
SET IDENTITY_INSERT [dbo].[States] ON
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (1, N'Alberta', N'AB', CAST(5 AS Decimal(9, 0)), N'CA', N'GST')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (2, N'British Columbia', N'BC', CAST(12 AS Decimal(9, 0)), N'CA', N'HST')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (3, N'Manitoba', N'MB', CAST(5 AS Decimal(9, 0)), N'CA', N'GST')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (4, N'New Brunswick', N'NB', CAST(13 AS Decimal(9, 0)), N'CA', N'HST')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (5, N'Newfoundland and Labrador', N'NL', CAST(13 AS Decimal(9, 0)), N'CA', N'HST')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (6, N'Northwest Territories', N'NT', CAST(0 AS Decimal(9, 0)), N'CA', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (7, N'Nova Scotia', N'NS', CAST(15 AS Decimal(9, 0)), N'CA', N'HST')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (8, N'Nunavut', N'NU', CAST(0 AS Decimal(9, 0)), N'CA', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (9, N'Ontario', N'ON', CAST(13 AS Decimal(9, 0)), N'CA', N'HST')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (10, N'Prince Edward Island', N'PE', CAST(5 AS Decimal(9, 0)), N'CA', N'GST')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (11, N'Quebec', N'QC', CAST(5 AS Decimal(9, 0)), N'CA', N'GST')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (12, N'Saskatchewan', N'SK', CAST(5 AS Decimal(9, 0)), N'CA', N'GST')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (13, N'Yukon', N'YT', CAST(0 AS Decimal(9, 0)), N'CA', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (14, N'Alabama', N'AL', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (15, N'Alaska', N'AK', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (16, N'Arizona', N'AZ', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (17, N'Arkansas', N'AR', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (18, N'California', N'CA', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (19, N'Colorado', N'CO', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (20, N'Connecticut', N'CT', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (21, N'Delaware', N'DE', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (22, N'District of Columbia', N'DC', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (23, N'Florida', N'FL', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (24, N'Georgia', N'GA', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (25, N'Hawaii', N'HI', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (26, N'Idaho', N'ID', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (27, N'Illinois', N'IL', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (28, N'Indiana', N'IN', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (29, N'Iowa', N'IA', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (30, N'Kansas', N'KS', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (31, N'Kentucky', N'KY', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (32, N'Louisiana', N'LA', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (33, N'Maine', N'ME', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (34, N'Maryland', N'MD', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (35, N'Massachusetts', N'MA', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (36, N'Michigan', N'MI', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (37, N'Minnesota', N'MN', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (38, N'Mississippi', N'MS', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (39, N'Missouri', N'MO', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (40, N'Montana', N'MT', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (41, N'Nebraska', N'NE', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (42, N'Nevada', N'NV', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (43, N'New Hampshire', N'NH', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (44, N'New Jersey', N'NJ', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (45, N'New Mexico', N'NM', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (46, N'New York', N'NY', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (47, N'North Carolina', N'NC', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (48, N'North Dakota', N'ND', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (49, N'Ohio', N'OH', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (50, N'Oklahoma', N'OK', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (51, N'Oregon', N'OR', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (52, N'Pennsylvania', N'PA', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (53, N'Rhode Island', N'RI', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (54, N'South Carolina', N'SC', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (55, N'South Dakota', N'SD', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (56, N'Tennessee', N'TN', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (57, N'Texas', N'TX', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (58, N'Utah', N'UT', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (59, N'Vermont', N'VT', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (60, N'Virginia', N'VA', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (61, N'Washington', N'WA', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (62, N'West Virginia', N'WV', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (63, N'Wisconsin', N'WI', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (64, N'Wyoming', N'WY', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (65, N'American Samoa', N'AS', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (66, N'Guam', N'GU', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (67, N'Northern Mariana Islands', N'MP', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (68, N'Puerto Rico', N'PR', CAST(0 AS Decimal(9, 0)), N'US', N'')
INSERT [dbo].[States] ([StateId], [StateName], [StateAbbreviation], [TaxPercent], [Country], [TaxType]) VALUES (69, N'Virgin Islands', N'VI', CAST(0 AS Decimal(9, 0)), N'US', N'')
SET IDENTITY_INSERT [dbo].[States] OFF
/****** Object:  Table [dbo].[RoomType]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RoomType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RoomType](
	[RoomTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Cost] [money] NOT NULL,
	[SmallImageUrl] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FullImageUrl] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CostForRevision] [money] NOT NULL,
	[Description] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_RoomType] PRIMARY KEY CLUSTERED 
(
	[RoomTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[RoomType] ON
INSERT [dbo].[RoomType] ([RoomTypeId], [Name], [Cost], [SmallImageUrl], [FullImageUrl], [CostForRevision], [Description]) VALUES (4, N'Entryway', 850.0000, NULL, NULL, 150.0000, N'Description of what is included with an Entryway design')
INSERT [dbo].[RoomType] ([RoomTypeId], [Name], [Cost], [SmallImageUrl], [FullImageUrl], [CostForRevision], [Description]) VALUES (5, N'Bathroom', 850.0000, NULL, NULL, 150.0000, N'Description of what is included with a Powder room design')
INSERT [dbo].[RoomType] ([RoomTypeId], [Name], [Cost], [SmallImageUrl], [FullImageUrl], [CostForRevision], [Description]) VALUES (6, N'Home Office', 1100.0000, NULL, NULL, 150.0000, N'Description of what is included with a home office design')
INSERT [dbo].[RoomType] ([RoomTypeId], [Name], [Cost], [SmallImageUrl], [FullImageUrl], [CostForRevision], [Description]) VALUES (7, N'Dining Room', 1350.0000, NULL, NULL, 150.0000, N'Description of what is included with a dinning room design')
INSERT [dbo].[RoomType] ([RoomTypeId], [Name], [Cost], [SmallImageUrl], [FullImageUrl], [CostForRevision], [Description]) VALUES (8, N'Master Bedroom', 1350.0000, NULL, NULL, 150.0000, N'Description of what is included with a master bedroom design')
INSERT [dbo].[RoomType] ([RoomTypeId], [Name], [Cost], [SmallImageUrl], [FullImageUrl], [CostForRevision], [Description]) VALUES (11, N'Living Room', 1750.0000, NULL, NULL, 150.0000, N'Description of what is included with a living room design')
INSERT [dbo].[RoomType] ([RoomTypeId], [Name], [Cost], [SmallImageUrl], [FullImageUrl], [CostForRevision], [Description]) VALUES (15, N'Kids Room', 1150.0000, NULL, NULL, 150.0000, N'Description of what is included with a kids room design')
INSERT [dbo].[RoomType] ([RoomTypeId], [Name], [Cost], [SmallImageUrl], [FullImageUrl], [CostForRevision], [Description]) VALUES (16, N'Guest Room', 1150.0000, NULL, NULL, 150.0000, N'Description of what is included with a guest room design')
INSERT [dbo].[RoomType] ([RoomTypeId], [Name], [Cost], [SmallImageUrl], [FullImageUrl], [CostForRevision], [Description]) VALUES (17, N'Media Room', 1150.0000, NULL, NULL, 150.0000, N'Description of what is included with a media room design')
INSERT [dbo].[RoomType] ([RoomTypeId], [Name], [Cost], [SmallImageUrl], [FullImageUrl], [CostForRevision], [Description]) VALUES (18, N'Kitchen', 1500.0000, NULL, NULL, 150.0000, N'Description of what is included with a kitchen')
SET IDENTITY_INSERT [dbo].[RoomType] OFF
/****** Object:  Table [dbo].[DesignFileTypes]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DesignFileTypes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DesignFileTypes](
	[DesignFileTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_DesignFileTypes] PRIMARY KEY CLUSTERED 
(
	[DesignFileTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[DesignFileTypes] ON
INSERT [dbo].[DesignFileTypes] ([DesignFileTypeId], [Name]) VALUES (1, N'First Initial Inspiration Board')
INSERT [dbo].[DesignFileTypes] ([DesignFileTypeId], [Name]) VALUES (2, N'Second Initial Inspiration Board')
INSERT [dbo].[DesignFileTypes] ([DesignFileTypeId], [Name]) VALUES (3, N'Initial Floor plan')
INSERT [dbo].[DesignFileTypes] ([DesignFileTypeId], [Name]) VALUES (4, N'Sample Board')
INSERT [dbo].[DesignFileTypes] ([DesignFileTypeId], [Name]) VALUES (5, N'3D Rendering')
INSERT [dbo].[DesignFileTypes] ([DesignFileTypeId], [Name]) VALUES (6, N'Floor Plan')
INSERT [dbo].[DesignFileTypes] ([DesignFileTypeId], [Name]) VALUES (7, N'Purchase Orders')
INSERT [dbo].[DesignFileTypes] ([DesignFileTypeId], [Name]) VALUES (8, N'Elevation Drawing')
INSERT [dbo].[DesignFileTypes] ([DesignFileTypeId], [Name]) VALUES (9, N'Revised Floor Plan')
INSERT [dbo].[DesignFileTypes] ([DesignFileTypeId], [Name]) VALUES (10, N'Revised 3D Rendering')
INSERT [dbo].[DesignFileTypes] ([DesignFileTypeId], [Name]) VALUES (11, N'Revised Elevation Drawing')
INSERT [dbo].[DesignFileTypes] ([DesignFileTypeId], [Name]) VALUES (12, N'Revised Purchase Orders')
SET IDENTITY_INSERT [dbo].[DesignFileTypes] OFF
/****** Object:  Table [dbo].[CurrentSteps]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CurrentSteps]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CurrentSteps](
	[CurrentStepId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Number] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IconUrlPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_CurrentSteps] PRIMARY KEY CLUSTERED 
(
	[CurrentStepId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[CurrentSteps] ON
INSERT [dbo].[CurrentSteps] ([CurrentStepId], [Name], [Number], [Description], [IconUrlPath]) VALUES (1, N'Create Your Room', N'Step 1', N'Give your room a name and select a type of room.', NULL)
INSERT [dbo].[CurrentSteps] ([CurrentStepId], [Name], [Number], [Description], [IconUrlPath]) VALUES (2, N'Fill Out a Questionnaire', N'Step 2', N'Fill Out the Questionnaire based on the room type you have selected.', NULL)
INSERT [dbo].[CurrentSteps] ([CurrentStepId], [Name], [Number], [Description], [IconUrlPath]) VALUES (3, N'Upload Room Photos', N'Step 3', N'Upload photos of your room', NULL)
INSERT [dbo].[CurrentSteps] ([CurrentStepId], [Name], [Number], [Description], [IconUrlPath]) VALUES (4, N'Upload Funiture Photos', N'Step 4', N'Create any furniture pieces and upload their photos', NULL)
INSERT [dbo].[CurrentSteps] ([CurrentStepId], [Name], [Number], [Description], [IconUrlPath]) VALUES (5, N'Initial Consultation', N'Step 5', N'Have your free 15 minute consultation with one of our designers.', NULL)
INSERT [dbo].[CurrentSteps] ([CurrentStepId], [Name], [Number], [Description], [IconUrlPath]) VALUES (6, N'Pay for your room''s design', N'Step 6', N'Pay for the design of your room.  Once the room has been paid for, our designer will start work.', NULL)
INSERT [dbo].[CurrentSteps] ([CurrentStepId], [Name], [Number], [Description], [IconUrlPath]) VALUES (7, N'Measure your room', N'Step 7', N'Measure your room, take a few photos of the measurements and upload the photos.', NULL)
INSERT [dbo].[CurrentSteps] ([CurrentStepId], [Name], [Number], [Description], [IconUrlPath]) VALUES (8, N'Follow-up Consultation', N'Step 8', N'Book your follow up consultation. Once you have receive the design, you can talk to the designer about the design and make a revision if needed.', NULL)
SET IDENTITY_INSERT [dbo].[CurrentSteps] OFF
/****** Object:  Table [dbo].[CurrentDesignSteps]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CurrentDesignSteps]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CurrentDesignSteps](
	[CurrentDesignStepId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Number] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IconUrlPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_CurrentDesignSteps] PRIMARY KEY CLUSTERED 
(
	[CurrentDesignStepId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[CurrentDesignSteps] ON
INSERT [dbo].[CurrentDesignSteps] ([CurrentDesignStepId], [Name], [Number], [Description], [IconUrlPath]) VALUES (1, N'Designer Creates Two Inspiration Boards and Initial Floor Plan', N'Step 1', N'Our designer creates and provides you with 2 inspiration boards and the initial floor plan.', NULL)
INSERT [dbo].[CurrentDesignSteps] ([CurrentDesignStepId], [Name], [Number], [Description], [IconUrlPath]) VALUES (2, N'Review Your Inspiration Boards', N'Step 2', N'Provide the designer with your feedback about what you like and dislike about the two inspiration boards.', NULL)
INSERT [dbo].[CurrentDesignSteps] ([CurrentDesignStepId], [Name], [Number], [Description], [IconUrlPath]) VALUES (3, N'Review Your Proposed Floor Plan', N'Step 3', N'Provide the designer with your feedback about what you like and dislike about the proposed floor plan.', NULL)
INSERT [dbo].[CurrentDesignSteps] ([CurrentDesignStepId], [Name], [Number], [Description], [IconUrlPath]) VALUES (4, N'Designer Reviews Your Comments', N'Step 4', N'Our designers reviews your comments uses these comments to create the final floor plan, sample board, 3D rendering, and purchase orders.', NULL)
INSERT [dbo].[CurrentDesignSteps] ([CurrentDesignStepId], [Name], [Number], [Description], [IconUrlPath]) VALUES (5, N'Designer Creates Your Final Floor Plan, Sample Board, 3D Rendering, and Purchase Orders', N'Step 5', N'Our designers will create and provide your with your final floor plan, sample board, 3D rendering, and purchase orders.', NULL)
INSERT [dbo].[CurrentDesignSteps] ([CurrentDesignStepId], [Name], [Number], [Description], [IconUrlPath]) VALUES (6, N'Your Design Kit has been shipped', N'Step 6', N'Our team has shipped your design kit.', NULL)
SET IDENTITY_INSERT [dbo].[CurrentDesignSteps] OFF
/****** Object:  StoredProcedure [dbo].[UpdateAnswerName]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateAnswerName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UpdateAnswerName]
@AnswerId INT,
@AnswerGiven TEXT
AS
BEGIN
UPDATE Answers
SET AnswerGiven = @AnswerGiven
WHERE AnswerId  = @AnswerId;
END

' 
END
GO
/****** Object:  Table [dbo].[MenuSteps]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MenuSteps]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MenuSteps](
	[MenuStepId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_MenuSteps] PRIMARY KEY CLUSTERED 
(
	[MenuStepId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[MenuSteps] ON
INSERT [dbo].[MenuSteps] ([MenuStepId], [Name]) VALUES (1, N'My Profile')
INSERT [dbo].[MenuSteps] ([MenuStepId], [Name]) VALUES (2, N'My Questionnaire')
INSERT [dbo].[MenuSteps] ([MenuStepId], [Name]) VALUES (3, N'Name Room and Select Type')
INSERT [dbo].[MenuSteps] ([MenuStepId], [Name]) VALUES (4, N'Room Questionnaire')
INSERT [dbo].[MenuSteps] ([MenuStepId], [Name]) VALUES (5, N'Furniture Photos/Description')
INSERT [dbo].[MenuSteps] ([MenuStepId], [Name]) VALUES (6, N'Upload Room Photos')
INSERT [dbo].[MenuSteps] ([MenuStepId], [Name]) VALUES (7, N'Book Consultation')
INSERT [dbo].[MenuSteps] ([MenuStepId], [Name]) VALUES (8, N'Pay For Room')
INSERT [dbo].[MenuSteps] ([MenuStepId], [Name]) VALUES (9, N'Measure Room')
INSERT [dbo].[MenuSteps] ([MenuStepId], [Name]) VALUES (10, N'Final Documents')
INSERT [dbo].[MenuSteps] ([MenuStepId], [Name]) VALUES (11, N'Book Follow-up Consultation')
SET IDENTITY_INSERT [dbo].[MenuSteps] OFF
/****** Object:  Table [dbo].[Profiles]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Profiles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Profiles](
	[ProfileId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LastName] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[PhoneNumber] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FaxNumber] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Address] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[City] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[State] [nvarchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Zip] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Country] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DateUpdated] [datetime] NULL,
	[UserName] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SkypeId] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ContactTypePreference] [nvarchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Profiles] PRIMARY KEY CLUSTERED 
(
	[ProfileId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[Profiles] ON
INSERT [dbo].[Profiles] ([ProfileId], [FirstName], [LastName], [PhoneNumber], [FaxNumber], [Address], [City], [State], [Zip], [Country], [DateUpdated], [UserName], [SkypeId], [ContactTypePreference]) VALUES (7, N'Phil', N'Hack', N'604.555.6621', N'604.555.6600', N'1245 Happy St.', N'Vancouver', N'BC', N'V6K3S5', N'Canada', CAST(0x00009DEE00D57E81 AS DateTime), N'phack', N'phacky', NULL)
INSERT [dbo].[Profiles] ([ProfileId], [FirstName], [LastName], [PhoneNumber], [FaxNumber], [Address], [City], [State], [Zip], [Country], [DateUpdated], [UserName], [SkypeId], [ContactTypePreference]) VALUES (8, N'Jim', N'Chong', N'604-554-5511', NULL, N'1415 main st.', N'Vancouver', N'BC', N'V6K3SW', N'Canada', CAST(0x00009E0C008E81BA AS DateTime), N'a3a99226@telus.net', N'jchong', NULL)
INSERT [dbo].[Profiles] ([ProfileId], [FirstName], [LastName], [PhoneNumber], [FaxNumber], [Address], [City], [State], [Zip], [Country], [DateUpdated], [UserName], [SkypeId], [ContactTypePreference]) VALUES (9, N'Phil', N'Hack', N'604-736-9660', N'512-555-5555', N'1234 21st st', N'Vancouver', N'BC', N'V6E2S6', N'US', CAST(0x00009E2201568539 AS DateTime), N'philliphack@gmail.com', N'myskype', NULL)
INSERT [dbo].[Profiles] ([ProfileId], [FirstName], [LastName], [PhoneNumber], [FaxNumber], [Address], [City], [State], [Zip], [Country], [DateUpdated], [UserName], [SkypeId], [ContactTypePreference]) VALUES (10, N'A', N'A', N'604-555-5555', N'604-555-6222', N'124 Happy St.', N'Vancouver', N'BC', N'V6K 3S5', N'Canada', CAST(0x00009E3C00C00342 AS DateTime), N'a@a.com', NULL, NULL)
INSERT [dbo].[Profiles] ([ProfileId], [FirstName], [LastName], [PhoneNumber], [FaxNumber], [Address], [City], [State], [Zip], [Country], [DateUpdated], [UserName], [SkypeId], [ContactTypePreference]) VALUES (11, N'Phil', N'Hack', N'604-555-5544', N'604-555-5555', N'121', N'Vancouver', N'BC', N'V6K3S4', N'Canada', CAST(0x00009E5100F91665 AS DateTime), N'c@c.com', NULL, NULL)
INSERT [dbo].[Profiles] ([ProfileId], [FirstName], [LastName], [PhoneNumber], [FaxNumber], [Address], [City], [State], [Zip], [Country], [DateUpdated], [UserName], [SkypeId], [ContactTypePreference]) VALUES (12, N'D', N'D2', N'604-555-8888', N'604-555-8888', N'1234 st.', N'vancouver', N'BC', N'V6K3S3', N'Canada', CAST(0x00009E550085632E AS DateTime), N'd@d.com', NULL, NULL)
INSERT [dbo].[Profiles] ([ProfileId], [FirstName], [LastName], [PhoneNumber], [FaxNumber], [Address], [City], [State], [Zip], [Country], [DateUpdated], [UserName], [SkypeId], [ContactTypePreference]) VALUES (13, N'E', N'E2', N'604.555.5555', N'604.555.5555', N'11225', N'vancouver', N'bc', N'v6ks3w', N'Canada', CAST(0x00009E550087D020 AS DateTime), N'e@e.com', NULL, NULL)
INSERT [dbo].[Profiles] ([ProfileId], [FirstName], [LastName], [PhoneNumber], [FaxNumber], [Address], [City], [State], [Zip], [Country], [DateUpdated], [UserName], [SkypeId], [ContactTypePreference]) VALUES (14, N'Phil', N'Hack', N'604-555-5555', N'604-555-9999', N'123 happy lane', N'Vancouver', N'BC', N'v6k2s4', N'Canada', CAST(0x00009E5A00A3CB3D AS DateTime), N'f@f.com', NULL, NULL)
INSERT [dbo].[Profiles] ([ProfileId], [FirstName], [LastName], [PhoneNumber], [FaxNumber], [Address], [City], [State], [Zip], [Country], [DateUpdated], [UserName], [SkypeId], [ContactTypePreference]) VALUES (15, N'Test', N'User', N'604-125-5554', N'604-999-9999', N'1 Main St', N'San Jose', N'CA', N'95131', N'US', CAST(0x00009E5A00BC8C48 AS DateTime), N'g@g.com', N'phack', NULL)
INSERT [dbo].[Profiles] ([ProfileId], [FirstName], [LastName], [PhoneNumber], [FaxNumber], [Address], [City], [State], [Zip], [Country], [DateUpdated], [UserName], [SkypeId], [ContactTypePreference]) VALUES (16, N'Test', N'User', N'416-554-5541', NULL, N'1234 Happy Road', N'Toronto', N'ON', N'V6K3S5', N'CA', CAST(0x00009E6000F2DFD0 AS DateTime), N'h@h.com', NULL, NULL)
INSERT [dbo].[Profiles] ([ProfileId], [FirstName], [LastName], [PhoneNumber], [FaxNumber], [Address], [City], [State], [Zip], [Country], [DateUpdated], [UserName], [SkypeId], [ContactTypePreference]) VALUES (17, N'Phil', N'Hack', N'604-555-5551', NULL, N'124', N'delta', N'AR', N'v63ssd', N'US', CAST(0x00009E6000F4BB89 AS DateTime), N'i@i.com', NULL, NULL)
INSERT [dbo].[Profiles] ([ProfileId], [FirstName], [LastName], [PhoneNumber], [FaxNumber], [Address], [City], [State], [Zip], [Country], [DateUpdated], [UserName], [SkypeId], [ContactTypePreference]) VALUES (18, N'Phil', N'Hack', N'604-555-1111', NULL, N'305 - 2015 Traflagar St.', N'Vancouver', N'BC', N'V6K3S5', N'CA', CAST(0x00009E8200FB67A4 AS DateTime), N'o@o.com', NULL, NULL)
INSERT [dbo].[Profiles] ([ProfileId], [FirstName], [LastName], [PhoneNumber], [FaxNumber], [Address], [City], [State], [Zip], [Country], [DateUpdated], [UserName], [SkypeId], [ContactTypePreference]) VALUES (19, N'Test', N'User1', N'604-555-5555', NULL, N'1235 main st.', N'vancouver', N'BC', N'V6K3S5', N'CA', CAST(0x00009E88012274C9 AS DateTime), N'q@q.com', NULL, NULL)
INSERT [dbo].[Profiles] ([ProfileId], [FirstName], [LastName], [PhoneNumber], [FaxNumber], [Address], [City], [State], [Zip], [Country], [DateUpdated], [UserName], [SkypeId], [ContactTypePreference]) VALUES (20, N'Test', N'Test2', N'604-555-5566', NULL, N'102 - 1112 Main st.', N'Vancouver', N'BC', N'V63SV2', N'CA', CAST(0x00009E8900921A7A AS DateTime), N'aa@aa.com', NULL, NULL)
INSERT [dbo].[Profiles] ([ProfileId], [FirstName], [LastName], [PhoneNumber], [FaxNumber], [Address], [City], [State], [Zip], [Country], [DateUpdated], [UserName], [SkypeId], [ContactTypePreference]) VALUES (21, N'Phil', N'Hack', N'604-736-9660', NULL, N'305 - 2015 Trafalgar St.', N'Vancouver', N'BC', N'V6K3S5', N'CA', CAST(0x00009E89009DBB5B AS DateTime), N'phil@treomadesign.com', NULL, NULL)
INSERT [dbo].[Profiles] ([ProfileId], [FirstName], [LastName], [PhoneNumber], [FaxNumber], [Address], [City], [State], [Zip], [Country], [DateUpdated], [UserName], [SkypeId], [ContactTypePreference]) VALUES (22, N'Test', N'Test', N'604-555-5511', NULL, N'211511 w', N'Van', N'BC', N'V6K3S5', N'CA', CAST(0x00009E9701354B2A AS DateTime), N'hotrodford@telus.net', NULL, NULL)
INSERT [dbo].[Profiles] ([ProfileId], [FirstName], [LastName], [PhoneNumber], [FaxNumber], [Address], [City], [State], [Zip], [Country], [DateUpdated], [UserName], [SkypeId], [ContactTypePreference]) VALUES (23, N'Phil', N'Hack', N'604-555-4444', NULL, N'151', N'Vancouver', N'BC', N'V6K2S4', N'CA', CAST(0x00009EC100B709B1 AS DateTime), N'a@philhack.com', NULL, NULL)
INSERT [dbo].[Profiles] ([ProfileId], [FirstName], [LastName], [PhoneNumber], [FaxNumber], [Address], [City], [State], [Zip], [Country], [DateUpdated], [UserName], [SkypeId], [ContactTypePreference]) VALUES (24, N'Phil', N'Hack', N'604-555-7777', NULL, N'1234 test st.', N'Vancouver', N'BC', N'V6K3S5', N'CA', CAST(0x00009EDD0099481F AS DateTime), N'zz@philhack.com', NULL, NULL)
INSERT [dbo].[Profiles] ([ProfileId], [FirstName], [LastName], [PhoneNumber], [FaxNumber], [Address], [City], [State], [Zip], [Country], [DateUpdated], [UserName], [SkypeId], [ContactTypePreference]) VALUES (25, N'Test', N'Test', N'604-555-7777', NULL, N'1234 test st.', N'Vancouver', N'BC', N'V6K3S5', N'CA', CAST(0x00009EEC007CF435 AS DateTime), N'zzz@philhack.com', NULL, NULL)
INSERT [dbo].[Profiles] ([ProfileId], [FirstName], [LastName], [PhoneNumber], [FaxNumber], [Address], [City], [State], [Zip], [Country], [DateUpdated], [UserName], [SkypeId], [ContactTypePreference]) VALUES (26, N'Test33', N'Test33', N'604-555-1212', NULL, N'607 - 29 Smithe Mews', N'Vancouver', N'BC', N'V6K3S5', N'CA', CAST(0x00009F6900B1A405 AS DateTime), N'phil33@philhack.com', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Profiles] OFF
/****** Object:  Table [dbo].[PortfolioPictures]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PortfolioPictures]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PortfolioPictures](
	[PortfolioPicturesId] [int] IDENTITY(1,1) NOT NULL,
	[OriginalServerPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OriginalUrlPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[WebServerPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[WebUrlPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThumbnailServerPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThumbnailUrlPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OriginalRSContainer] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OriginalRSUrl] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[WebRSContainer] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[WebRSUrl] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThumbnailRSContainer] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThumbnailRSUrl] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_PortfolioPictures] PRIMARY KEY CLUSTERED 
(
	[PortfolioPicturesId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[GeneralQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GeneralQuestionnaires]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GeneralQuestionnaires](
	[GeneralQuestionnaireId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DesignStyle] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DesignStyleOther] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ColorsYouLove] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ColorsYouLoveOther] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ColorsYouDislike] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ColorsYouDislikeOther] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TypeOfColorsYouLike] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HaveASpouse] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[HaveKids] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HaveKidsAgesOfKids] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[HavePets] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HavePetsTypeAndNumber] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[HobbiesAndInterests] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[TypesOfPatternsYouLike] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[TypesOfPatternsYouLikeOther] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BeingGreenImportant] [nvarchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[BeingGreenImportantOther] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RentOrOwnYourHome] [nvarchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowLongDoYouPlanToLiveInHome] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[NeedWheelchairAccess] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_GeneralQuestionnaires] PRIMARY KEY CLUSTERED 
(
	[GeneralQuestionnaireId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[GeneralQuestionnaires] ON
INSERT [dbo].[GeneralQuestionnaires] ([GeneralQuestionnaireId], [UserName], [DesignStyle], [DesignStyleOther], [ColorsYouLove], [ColorsYouLoveOther], [ColorsYouDislike], [ColorsYouDislikeOther], [TypeOfColorsYouLike], [HaveASpouse], [HaveKids], [HaveKidsAgesOfKids], [HavePets], [HavePetsTypeAndNumber], [HobbiesAndInterests], [TypesOfPatternsYouLike], [TypesOfPatternsYouLikeOther], [BeingGreenImportant], [BeingGreenImportantOther], [RentOrOwnYourHome], [HowLongDoYouPlanToLiveInHome], [NeedWheelchairAccess]) VALUES (10, N'a@a.com', N'Other', N'jljljljl', N'Red,Purple,Pink,Brown,White,Other', N'combo colors', N'Orange,Red,Pink', NULL, N'Neutral', N'Yes', N'Yes', N'2 x 32', N'Yes', N'ljljljl', N'jljlkj', N'Textural', NULL, N'Other', N'yes it sure is', N'Own', N'ljlj', N'No')
INSERT [dbo].[GeneralQuestionnaires] ([GeneralQuestionnaireId], [UserName], [DesignStyle], [DesignStyleOther], [ColorsYouLove], [ColorsYouLoveOther], [ColorsYouDislike], [ColorsYouDislikeOther], [TypeOfColorsYouLike], [HaveASpouse], [HaveKids], [HaveKidsAgesOfKids], [HavePets], [HavePetsTypeAndNumber], [HobbiesAndInterests], [TypesOfPatternsYouLike], [TypesOfPatternsYouLikeOther], [BeingGreenImportant], [BeingGreenImportantOther], [RentOrOwnYourHome], [HowLongDoYouPlanToLiveInHome], [NeedWheelchairAccess]) VALUES (11, N'c@c.com', N'Eclectic', NULL, N'Pink,Black', NULL, N'Purple,Black', NULL, N'Neutral', N'Yes', N'No', NULL, N'No', NULL, N'dgdgd', N'Contemporary', NULL, N'No', NULL, N'Own', N'dgdgdg', N'No')
INSERT [dbo].[GeneralQuestionnaires] ([GeneralQuestionnaireId], [UserName], [DesignStyle], [DesignStyleOther], [ColorsYouLove], [ColorsYouLoveOther], [ColorsYouDislike], [ColorsYouDislikeOther], [TypeOfColorsYouLike], [HaveASpouse], [HaveKids], [HaveKidsAgesOfKids], [HavePets], [HavePetsTypeAndNumber], [HobbiesAndInterests], [TypesOfPatternsYouLike], [TypesOfPatternsYouLikeOther], [BeingGreenImportant], [BeingGreenImportantOther], [RentOrOwnYourHome], [HowLongDoYouPlanToLiveInHome], [NeedWheelchairAccess]) VALUES (13, N'e@e.com', N'Eclectic', NULL, N'Orange,Pink,White', NULL, N'Red,Pink,Black', NULL, N'Some Color', N'Yes', N'No', NULL, N'No', NULL, N'sdffsdf', N'Geometric', NULL, N'No', NULL, N'Own', N'sfsfsf', N'No')
INSERT [dbo].[GeneralQuestionnaires] ([GeneralQuestionnaireId], [UserName], [DesignStyle], [DesignStyleOther], [ColorsYouLove], [ColorsYouLoveOther], [ColorsYouDislike], [ColorsYouDislikeOther], [TypeOfColorsYouLike], [HaveASpouse], [HaveKids], [HaveKidsAgesOfKids], [HavePets], [HavePetsTypeAndNumber], [HobbiesAndInterests], [TypesOfPatternsYouLike], [TypesOfPatternsYouLikeOther], [BeingGreenImportant], [BeingGreenImportantOther], [RentOrOwnYourHome], [HowLongDoYouPlanToLiveInHome], [NeedWheelchairAccess]) VALUES (14, N'f@f.com', N'Country Cottage', NULL, N'Red,Grey', NULL, N'', NULL, N'Neutral', N'No', N'No', NULL, N'No', NULL, N'SFSFS', N'Botanical', NULL, N'No', NULL, N'Own', N'DSFSFS', N'No')
INSERT [dbo].[GeneralQuestionnaires] ([GeneralQuestionnaireId], [UserName], [DesignStyle], [DesignStyleOther], [ColorsYouLove], [ColorsYouLoveOther], [ColorsYouDislike], [ColorsYouDislikeOther], [TypeOfColorsYouLike], [HaveASpouse], [HaveKids], [HaveKidsAgesOfKids], [HavePets], [HavePetsTypeAndNumber], [HobbiesAndInterests], [TypesOfPatternsYouLike], [TypesOfPatternsYouLikeOther], [BeingGreenImportant], [BeingGreenImportantOther], [RentOrOwnYourHome], [HowLongDoYouPlanToLiveInHome], [NeedWheelchairAccess]) VALUES (15, N'g@g.com', N'Mid-century Modern', NULL, N'Red,Pink', NULL, N'Purple,Brown', NULL, N'Some Color', N'No', N'No', NULL, N'No', NULL, N'fsfsf', N'Floral', NULL, N'No', NULL, N'Own', N'sfsf', N'No')
INSERT [dbo].[GeneralQuestionnaires] ([GeneralQuestionnaireId], [UserName], [DesignStyle], [DesignStyleOther], [ColorsYouLove], [ColorsYouLoveOther], [ColorsYouDislike], [ColorsYouDislikeOther], [TypeOfColorsYouLike], [HaveASpouse], [HaveKids], [HaveKidsAgesOfKids], [HavePets], [HavePetsTypeAndNumber], [HobbiesAndInterests], [TypesOfPatternsYouLike], [TypesOfPatternsYouLikeOther], [BeingGreenImportant], [BeingGreenImportantOther], [RentOrOwnYourHome], [HowLongDoYouPlanToLiveInHome], [NeedWheelchairAccess]) VALUES (16, N'h@h.com', N'Eclectic', NULL, N'Red,Black', NULL, N'Red,Purple', NULL, N'Neutral', N'No', N'No', NULL, N'No', NULL, N'sfsf', N'Textural', NULL, N'No', NULL, N'Rent', N'sffsfs', N'No')
INSERT [dbo].[GeneralQuestionnaires] ([GeneralQuestionnaireId], [UserName], [DesignStyle], [DesignStyleOther], [ColorsYouLove], [ColorsYouLoveOther], [ColorsYouDislike], [ColorsYouDislikeOther], [TypeOfColorsYouLike], [HaveASpouse], [HaveKids], [HaveKidsAgesOfKids], [HavePets], [HavePetsTypeAndNumber], [HobbiesAndInterests], [TypesOfPatternsYouLike], [TypesOfPatternsYouLikeOther], [BeingGreenImportant], [BeingGreenImportantOther], [RentOrOwnYourHome], [HowLongDoYouPlanToLiveInHome], [NeedWheelchairAccess]) VALUES (17, N'i@i.com', N'Country Cottage', NULL, N'Pink,Grey', NULL, N'Purple,Pink,Brown', NULL, N'Neutral', N'No', N'No', NULL, N'No', NULL, N'sfsf', N'Other', N'sfsf', N'No', NULL, N'Own', N'sdfsfs', N'No')
INSERT [dbo].[GeneralQuestionnaires] ([GeneralQuestionnaireId], [UserName], [DesignStyle], [DesignStyleOther], [ColorsYouLove], [ColorsYouLoveOther], [ColorsYouDislike], [ColorsYouDislikeOther], [TypeOfColorsYouLike], [HaveASpouse], [HaveKids], [HaveKidsAgesOfKids], [HavePets], [HavePetsTypeAndNumber], [HobbiesAndInterests], [TypesOfPatternsYouLike], [TypesOfPatternsYouLikeOther], [BeingGreenImportant], [BeingGreenImportantOther], [RentOrOwnYourHome], [HowLongDoYouPlanToLiveInHome], [NeedWheelchairAccess]) VALUES (18, N'q@q.com', N'Eclectic', NULL, N'Pink,Grey', NULL, N'Green,Grey', NULL, N'Neutral', N'Yes', N'No', NULL, N'Yes', N'sfsfs', N'ssfsf', N'None', NULL, N'Other', N'sfsfsfs', N'Own', N'fdsfs', N'No')
INSERT [dbo].[GeneralQuestionnaires] ([GeneralQuestionnaireId], [UserName], [DesignStyle], [DesignStyleOther], [ColorsYouLove], [ColorsYouLoveOther], [ColorsYouDislike], [ColorsYouDislikeOther], [TypeOfColorsYouLike], [HaveASpouse], [HaveKids], [HaveKidsAgesOfKids], [HavePets], [HavePetsTypeAndNumber], [HobbiesAndInterests], [TypesOfPatternsYouLike], [TypesOfPatternsYouLikeOther], [BeingGreenImportant], [BeingGreenImportantOther], [RentOrOwnYourHome], [HowLongDoYouPlanToLiveInHome], [NeedWheelchairAccess]) VALUES (19, N'hotrodford@telus.net', N'Eclectic', NULL, N'Purple,Pink', NULL, N'Purple,Pink', NULL, N'Color', N'No', N'No', NULL, N'No', NULL, N'gdgd', N'Textural', NULL, N'No', NULL, N'Own', N'gdgdgd', N'No')
INSERT [dbo].[GeneralQuestionnaires] ([GeneralQuestionnaireId], [UserName], [DesignStyle], [DesignStyleOther], [ColorsYouLove], [ColorsYouLoveOther], [ColorsYouDislike], [ColorsYouDislikeOther], [TypeOfColorsYouLike], [HaveASpouse], [HaveKids], [HaveKidsAgesOfKids], [HavePets], [HavePetsTypeAndNumber], [HobbiesAndInterests], [TypesOfPatternsYouLike], [TypesOfPatternsYouLikeOther], [BeingGreenImportant], [BeingGreenImportantOther], [RentOrOwnYourHome], [HowLongDoYouPlanToLiveInHome], [NeedWheelchairAccess]) VALUES (20, N'a3a99226@telus.net', N'Eclectic', NULL, N'Pink', NULL, N'Red', NULL, N'Neutral', N'No', N'No', NULL, N'No', NULL, N'gf', N'Textural', NULL, N'No', NULL, N'Own', N'fbfb', N'No')
INSERT [dbo].[GeneralQuestionnaires] ([GeneralQuestionnaireId], [UserName], [DesignStyle], [DesignStyleOther], [ColorsYouLove], [ColorsYouLoveOther], [ColorsYouDislike], [ColorsYouDislikeOther], [TypeOfColorsYouLike], [HaveASpouse], [HaveKids], [HaveKidsAgesOfKids], [HavePets], [HavePetsTypeAndNumber], [HobbiesAndInterests], [TypesOfPatternsYouLike], [TypesOfPatternsYouLikeOther], [BeingGreenImportant], [BeingGreenImportantOther], [RentOrOwnYourHome], [HowLongDoYouPlanToLiveInHome], [NeedWheelchairAccess]) VALUES (21, N'a@philhack.com', N'Mid-century Modern', NULL, N'Pink,Brown', NULL, N'Purple,Grey', NULL, N'Some Color', N'No', N'No', NULL, N'Yes', N'1', N'dgdgdgd', N'Textural,Yellow,Traditional,Botanical,Other', N'my own pattern', N'No', NULL, N'Own', N'ddgd', N'No')
INSERT [dbo].[GeneralQuestionnaires] ([GeneralQuestionnaireId], [UserName], [DesignStyle], [DesignStyleOther], [ColorsYouLove], [ColorsYouLoveOther], [ColorsYouDislike], [ColorsYouDislikeOther], [TypeOfColorsYouLike], [HaveASpouse], [HaveKids], [HaveKidsAgesOfKids], [HavePets], [HavePetsTypeAndNumber], [HobbiesAndInterests], [TypesOfPatternsYouLike], [TypesOfPatternsYouLikeOther], [BeingGreenImportant], [BeingGreenImportantOther], [RentOrOwnYourHome], [HowLongDoYouPlanToLiveInHome], [NeedWheelchairAccess]) VALUES (22, N'zz@philhack.com', N'Eclectic', NULL, N'Yellow', NULL, N'Purple', NULL, N'Neutral', N'No', N'No', NULL, N'No', NULL, N'gdgdg', N'Traditional', NULL, N'No', NULL, N'Own', N'dgdg', N'No')
INSERT [dbo].[GeneralQuestionnaires] ([GeneralQuestionnaireId], [UserName], [DesignStyle], [DesignStyleOther], [ColorsYouLove], [ColorsYouLoveOther], [ColorsYouDislike], [ColorsYouDislikeOther], [TypeOfColorsYouLike], [HaveASpouse], [HaveKids], [HaveKidsAgesOfKids], [HavePets], [HavePetsTypeAndNumber], [HobbiesAndInterests], [TypesOfPatternsYouLike], [TypesOfPatternsYouLikeOther], [BeingGreenImportant], [BeingGreenImportantOther], [RentOrOwnYourHome], [HowLongDoYouPlanToLiveInHome], [NeedWheelchairAccess]) VALUES (23, N'philliphack@gmail.com', N'Country Cottage', NULL, N'', NULL, N'', NULL, N'Neutral', N'No', N'No', N'fsfs', N'No', N'sfsf', N'hello', N'Traditional', NULL, N'No', NULL, N'Own', N'sfsfs', N'No')
SET IDENTITY_INSERT [dbo].[GeneralQuestionnaires] OFF
/****** Object:  View [dbo].[vw_aspnet_Applications]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Applications]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_Applications]
  AS SELECT [dbo].[aspnet_Applications].[ApplicationName], [dbo].[aspnet_Applications].[LoweredApplicationName], [dbo].[aspnet_Applications].[ApplicationId], [dbo].[aspnet_Applications].[Description]
  FROM [dbo].[aspnet_Applications]
  '
GO
/****** Object:  StoredProcedure [dbo].[aspnet_WebEvent_LogEvent]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_WebEvent_LogEvent]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_WebEvent_LogEvent]
        @EventId         char(32),
        @EventTimeUtc    datetime,
        @EventTime       datetime,
        @EventType       nvarchar(256),
        @EventSequence   decimal(19,0),
        @EventOccurrence decimal(19,0),
        @EventCode       int,
        @EventDetailCode int,
        @Message         nvarchar(1024),
        @ApplicationPath nvarchar(256),
        @ApplicationVirtualPath nvarchar(256),
        @MachineName    nvarchar(256),
        @RequestUrl      nvarchar(1024),
        @ExceptionType   nvarchar(256),
        @Details         ntext
AS
BEGIN
    INSERT
        dbo.aspnet_WebEvent_Events
        (
            EventId,
            EventTimeUtc,
            EventTime,
            EventType,
            EventSequence,
            EventOccurrence,
            EventCode,
            EventDetailCode,
            Message,
            ApplicationPath,
            ApplicationVirtualPath,
            MachineName,
            RequestUrl,
            ExceptionType,
            Details
        )
    VALUES
    (
        @EventId,
        @EventTimeUtc,
        @EventTime,
        @EventType,
        @EventSequence,
        @EventOccurrence,
        @EventCode,
        @EventDetailCode,
        @Message,
        @ApplicationPath,
        @ApplicationVirtualPath,
        @MachineName,
        @RequestUrl,
        @ExceptionType,
        @Details
    )
END' 
END
GO
/****** Object:  Table [dbo].[Rooms]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Rooms]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Rooms](
	[RoomId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Dimensions] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Name] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CurrentStepId] [int] NULL,
	[DateUpdated] [datetime] NOT NULL,
	[RoomTypeName] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RoomPaidFor] [int] NOT NULL,
	[RoomCost] [money] NULL,
	[TotalFreeRevisionsUsed] [int] NOT NULL,
	[TotalRevisionsPaidFor] [int] NOT NULL,
	[CostPerRevision] [money] NULL,
	[PhotoDetails] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MeasurementDetails] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AssignedToDesigner] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CurrentDesignStepId] [int] NULL,
	[ProfileId] [int] NOT NULL,
	[MenuStepId] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[RoomTotalCost] [money] NULL,
	[RoomTaxCost] [money] NULL,
	[RoomTaxType] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RoomTaxPercent] [decimal](9, 2) NULL,
 CONSTRAINT [PK_Rooms] PRIMARY KEY CLUSTERED 
(
	[RoomId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[Rooms] ON
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (1, N'a3a99226@telus.net', NULL, N'new roomy', 8, CAST(0x00009E97015AAF2B AS DateTime), N'Entryway', 1, 850.0000, 0, 0, 150.0000, N'These are some details about the photos...', N'These are some details about the measurements', N'phil@treomadesign.com', 6, 8, 11, CAST(0x00009E1F016186BC AS DateTime), 850.0000, 0.0000, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (2, N'a3a99226@telus.net', NULL, N'another room', 1, CAST(0x00009E200124355F AS DateTime), N'Living Room', 0, 1750.0000, 0, 0, 150.0000, NULL, NULL, N'tara@treomadesign.com', NULL, 8, 1, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (5, N'a3a99226@telus.net', NULL, N'My Bathroom', 1, CAST(0x00009E3500EE983A AS DateTime), N'Bathroom', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 8, 1, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (6, N'a3a99226@telus.net', NULL, N'powder bathroom', 1, CAST(0x00009E3501166FD5 AS DateTime), N'Bathroom', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 8, 1, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (7, N'a3a99226@telus.net', NULL, N'test bathroom', 1, CAST(0x00009E3501377A3B AS DateTime), N'Bathroom', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 8, 1, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (8, N'a3a99226@telus.net', NULL, N'3', 1, CAST(0x00009E350138835E AS DateTime), N'Bathroom', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 8, 1, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (9, N'a3a99226@telus.net', NULL, N'b1', 1, CAST(0x00009E3B006DC0D5 AS DateTime), N'Bathroom', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 8, 1, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (10, N'a3a99226@telus.net', NULL, N'b4', 1, CAST(0x00009E3B010D59A5 AS DateTime), N'Bathroom', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 8, 1, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (11, N'a3a99226@telus.net', NULL, N'b5', 1, CAST(0x00009E5A0099EBB9 AS DateTime), N'Bathroom', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 8, 1, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (12, N'a3a99226@telus.net', NULL, N'b6', 1, CAST(0x00009E3C00AA848B AS DateTime), N'Bathroom', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 8, 1, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (13, N'a@a.com', NULL, N'b1', 1, CAST(0x00009E3C00C01F7F AS DateTime), N'Bathroom', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 10, 11, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (15, N'a@a.com', NULL, N'b3', 1, CAST(0x00009E3C01613EB1 AS DateTime), N'Bathroom', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 10, 2, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (16, N'a@a.com', NULL, N'b4', 1, CAST(0x00009E3D0159B32A AS DateTime), N'Bathroom', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 10, 3, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (17, N'a@a.com', NULL, N'ew1', 1, CAST(0x00009E42007389A7 AS DateTime), N'Entryway', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 10, 4, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (18, N'a@a.com', NULL, N'o1', 1, CAST(0x00009E42009E6B90 AS DateTime), N'Home Office', 0, 1150.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 10, 5, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (19, N'a@a.com', NULL, N'd1', 1, CAST(0x00009E43009CC204 AS DateTime), N'Dining Room', 0, 1350.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 10, 6, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (21, N'a@a.com', NULL, N'mb1', 1, CAST(0x00009E4300B091B7 AS DateTime), N'Master Bedroom', 0, 1350.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 10, 7, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (22, N'a@a.com', NULL, N'L1', 1, CAST(0x00009E4300C07B02 AS DateTime), N'Living Room', 0, 1750.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 10, 8, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (23, N'a@a.com', NULL, N'k1', 1, CAST(0x00009E4300D4B985 AS DateTime), N'Kids Room', 0, 1150.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 10, 9, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (24, N'a@a.com', NULL, N'g1', 1, CAST(0x00009E4300FBC312 AS DateTime), N'Guest Room', 0, 1150.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 10, 10, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (25, N'a@a.com', NULL, N'mr1', 1, CAST(0x00009E5A009D0A8F AS DateTime), N'Media Room', 0, 1150.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 10, 3, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (26, N'c@c.com', NULL, N'glam room', 1, CAST(0x00009E5101273FA5 AS DateTime), N'Guest Room', 0, 1150.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 11, 3, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (27, N'c@c.com', NULL, N'main bathroom', 1, CAST(0x00009E5101275A35 AS DateTime), N'Bathroom', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 11, 2, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (28, N'e@e.com', NULL, N'Bathroom1', 3, CAST(0x00009E5A00994C98 AS DateTime), N'Bathroom', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 13, 5, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (29, N'e@e.com', NULL, N'My Dining Room', 2, CAST(0x00009E5901437CF8 AS DateTime), N'Dining Room', 0, 1350.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 13, 4, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (30, N'e@e.com', NULL, N'my entryway', 1, CAST(0x00009E590145AB5F AS DateTime), N'Entryway', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 13, 3, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (31, N'e@e.com', NULL, N'entry 2', 1, CAST(0x00009E590146125E AS DateTime), N'Entryway', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 13, 3, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (32, N'e@e.com', NULL, N'guest 1', 2, CAST(0x00009E590146CEE3 AS DateTime), N'Guest Room', 0, 1150.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 13, 4, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (33, N'e@e.com', NULL, N'office 1', 1, CAST(0x00009E590146E349 AS DateTime), N'Home Office', 0, 1150.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 13, 3, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (34, N'e@e.com', NULL, N'office 2', 1, CAST(0x00009E5901472249 AS DateTime), N'Home Office', 0, 1150.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 13, 3, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (35, N'e@e.com', NULL, N'kid 2', 1, CAST(0x00009E5901489065 AS DateTime), N'Kids Room', 0, 1150.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 13, 3, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (36, N'e@e.com', NULL, N'living 1', 1, CAST(0x00009E590148A3AC AS DateTime), N'Living Room', 0, 1750.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 13, 3, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (37, N'e@e.com', NULL, N'master 1', 1, CAST(0x00009E590148B9B6 AS DateTime), N'Master Bedroom', 0, 1350.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 13, 3, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (38, N'e@e.com', NULL, N'media 1', 1, CAST(0x00009E590148D828 AS DateTime), N'Media Room', 0, 1150.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 13, 3, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (41, N'f@f.com', NULL, N'test ', 3, CAST(0x00009E5A00BBBDAC AS DateTime), N'Living Room', 0, 1750.0000, 0, 0, 150.0000, N'sfsfsf


I love bear..... :)

I love you !!!!

new 111', NULL, NULL, NULL, 14, 5, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (44, N'g@g.com', NULL, N'yoyo', 2, CAST(0x00009E6200FCB3B4 AS DateTime), N'Media Room', 0, 1150.0000, 0, 0, 150.0000, NULL, NULL, NULL, 5, 15, 7, CAST(0x00009E1F016186BC AS DateTime), 1150.0000, 0.0000, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (45, N'g@g.com', NULL, N'new roomy', 6, CAST(0x00009E6200FE946A AS DateTime), N'Media Room', 1, 1150.0000, 0, 0, 150.0000, N'sfsfs', N'these are my details....!', N'phil@treomadesign.com', 6, 15, 9, CAST(0x00009E1F016186BC AS DateTime), 1150.0000, 0.0000, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (46, N'g@g.com', NULL, N'new 2', 1, CAST(0x00009E7B006F9D32 AS DateTime), N'Kids Room', 0, 1150.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 15, 3, CAST(0x00009E1F016186BC AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (47, N'g@g.com', NULL, N'test bathroom', 1, CAST(0x00009E6200FBBCD4 AS DateTime), N'Bathroom', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 15, 3, CAST(0x00009E5F0101AD46 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (48, N'h@h.com', NULL, N'Phils Bathroom', 6, CAST(0x00009E6200D43C44 AS DateTime), N'Bathroom', 1, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, 5, 16, 8, CAST(0x00009E600109B14C AS DateTime), 960.5000, 110.5000, N'HST', CAST(13.00 AS Decimal(9, 2)))
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (63, N'i@i.com', NULL, N'bathroom', 2, CAST(0x00009E8A00C9979C AS DateTime), N'Bathroom', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 17, 4, CAST(0x00009E8A00C7F72A AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (64, N'i@i.com', NULL, N'Dinning Room', 2, CAST(0x00009E8A00C99F24 AS DateTime), N'Dining Room', 0, 1350.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 17, 4, CAST(0x00009E8A00C886A8 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (66, N'i@i.com', NULL, N'entryway', 2, CAST(0x00009E8A00CE9AE4 AS DateTime), N'Entryway', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 17, 4, CAST(0x00009E8A00CE3CF8 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (69, N'i@i.com', NULL, N'guest room', 2, CAST(0x00009E8A00D8F7A2 AS DateTime), N'Guest Room', 0, 1150.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 17, 4, CAST(0x00009E8A00D1B88C AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (70, N'i@i.com', NULL, N'kids room', 2, CAST(0x00009E8A00D95C54 AS DateTime), N'Kids Room', 0, 1150.0000, 0, 0, 150.0000, NULL, NULL, NULL, 3, 17, 5, CAST(0x00009E8A00D90CE5 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (71, N'i@i.com', NULL, N'Living Room', 2, CAST(0x00009E8A00DEB25B AS DateTime), N'Living Room', 0, 1750.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 17, 4, CAST(0x00009E8A00DCBD22 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (72, N'i@i.com', NULL, N'master bedroom', 5, CAST(0x00009E9D0115A29D AS DateTime), N'Master Bedroom', 0, 1350.0000, 0, 0, 150.0000, NULL, NULL, NULL, 3, 17, 7, CAST(0x00009E8A00E2ACF6 AS DateTime), 1350.0000, 0.0000, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (73, N'I@i.com', NULL, N'media room', 8, CAST(0x00009E9D011A0ED6 AS DateTime), N'Media Room', 1, 1150.0000, 0, 0, 150.0000, NULL, NULL, N'phil@treomadesign.com', 5, 17, 11, CAST(0x00009E8A00E53A4B AS DateTime), 1150.0000, 0.0000, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (74, N'i@i.com', NULL, N'home office', 6, CAST(0x00009E8F01750C0E AS DateTime), N'Home Office', 1, 1150.0000, 0, 0, 150.0000, NULL, N'more comments...', N'phil@treomadesign.com', 6, 17, 10, CAST(0x00009E8A00E8CD0C AS DateTime), 1150.0000, 0.0000, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (78, N'hotrodford@telus.net', NULL, N'my 1st room', 1, CAST(0x00009E9701392107 AS DateTime), N'Entryway', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 22, 3, CAST(0x00009E9701391F90 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (79, N'i@i.com', NULL, N'test', 2, CAST(0x00009E9D011ABF68 AS DateTime), N'Dining Room', 0, 1350.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 17, 4, CAST(0x00009E9D011A63E9 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (94, N'a@philhack.com', NULL, N'bed', 2, CAST(0x00009EC700E56473 AS DateTime), N'Master Bedroom', 0, 1350.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 23, 4, CAST(0x00009EC3015C35CA AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (98, N'a@philhack.com', NULL, N'supa kitchie2', 3, CAST(0x00009ED0010720FA AS DateTime), N'Kitchen', 0, 1500.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 23, 5, CAST(0x00009EC700E50D31 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (99, N'a@philhack.com', NULL, N'entry', 1, CAST(0x00009EC700E580CB AS DateTime), N'Entryway', 0, 850.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 23, 3, CAST(0x00009EC700E580BE AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (103, N'philliphack@gmail.com', NULL, N'kitchen', 5, CAST(0x00009F060179D355 AS DateTime), N'Kitchen', 0, 1500.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 9, 7, CAST(0x00009EEB012A0A04 AS DateTime), 1500.0000, 0.0000, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (104, N'philliphack@gmail.com', NULL, N'Me entry', 8, CAST(0x00009F060164D3EC AS DateTime), N'Entryway', 1, 850.0000, 0, 0, 150.0000, NULL, N'I measured it myself.', NULL, 6, 9, 11, CAST(0x00009EEC008DB97F AS DateTime), 850.0000, 0.0000, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (105, N'philliphack@gmail.com', NULL, N'new room', 8, CAST(0x00009F070099B871 AS DateTime), N'Kitchen', 1, 1500.0000, 0, 0, 150.0000, NULL, N'comments about my room measurement....', N'phil@digitalinteriordesign.com', 6, 9, 11, CAST(0x00009F0700913E66 AS DateTime), 1500.0000, 0.0000, NULL, NULL)
INSERT [dbo].[Rooms] ([RoomId], [UserName], [Dimensions], [Name], [CurrentStepId], [DateUpdated], [RoomTypeName], [RoomPaidFor], [RoomCost], [TotalFreeRevisionsUsed], [TotalRevisionsPaidFor], [CostPerRevision], [PhotoDetails], [MeasurementDetails], [AssignedToDesigner], [CurrentDesignStepId], [ProfileId], [MenuStepId], [DateCreated], [RoomTotalCost], [RoomTaxCost], [RoomTaxType], [RoomTaxPercent]) VALUES (106, N'philliphack@gmail.com', NULL, N'test', 5, CAST(0x00009F2A011A24C8 AS DateTime), N'Living Room', 0, 1750.0000, 0, 0, 150.0000, NULL, NULL, NULL, NULL, 9, 7, CAST(0x00009F2A011719E8 AS DateTime), 1750.0000, 0.0000, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Rooms] OFF
/****** Object:  Table [dbo].[aspnet_Users]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Users](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LoweredUserName] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MobileAlias] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsAnonymous] [bit] NOT NULL,
	[LastActivityDate] [datetime] NOT NULL,
 CONSTRAINT [PK__aspnet_U__1788CC4D0BC6C43E] PRIMARY KEY NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users]') AND name = N'aspnet_Users_Index')
CREATE UNIQUE CLUSTERED INDEX [aspnet_Users_Index] ON [dbo].[aspnet_Users] 
(
	[ApplicationId] ASC,
	[LoweredUserName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users]') AND name = N'aspnet_Users_Index2')
CREATE NONCLUSTERED INDEX [aspnet_Users_Index2] ON [dbo].[aspnet_Users] 
(
	[ApplicationId] ASC,
	[LastActivityDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'b1c8f00a-01f4-42f3-b0ef-a1f097b6c635', N'a@a.com', N'a@a.com', NULL, 0, CAST(0x00009E9000556A02 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'1e9e9a92-f839-47d0-b871-897bf77cd20b', N'a@philhack.com', N'a@philhack.com', NULL, 0, CAST(0x00009ED0018B03F3 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'597c6c12-4299-4d8a-8097-ba15f26a1a2c', N'a3a99226@telus.net', N'a3a99226@telus.net', NULL, 0, CAST(0x00009E980052E547 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'1338a83c-7de8-4732-bad0-560cc73ccc38', N'aa@aa.com', N'aa@aa.com', NULL, 0, CAST(0x00009E890124010E AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'90006fa8-6140-46f7-aa44-5ee8b3c21ea3', N'b@b.com', N'b@b.com', NULL, 0, CAST(0x00009E52001F4FC8 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'a2c86efa-717c-490e-8022-610ce83e789d', N'c@c.com', N'c@c.com', NULL, 0, CAST(0x00009E57014A75D7 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'2d0a1462-3ad3-44ac-b8da-c144263c2ff3', N'd@d.com', N'd@d.com', NULL, 0, CAST(0x00009E55010B2226 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'e2aac83e-ec38-40a4-ae4e-5b126ff97041', N'e@e.com', N'e@e.com', NULL, 0, CAST(0x00009E5A011CE2C5 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'c3dab54c-5449-4644-ac7d-9a83436baac6', N'f@f.com', N'f@f.com', NULL, 0, CAST(0x00009E5A0138BC5B AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'3759fae6-a0b9-4baf-bb45-5a4521839df9', N'g@g.com', N'g@g.com', NULL, 0, CAST(0x00009E970184B3C3 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'9b794b62-ea1a-4518-80d1-cbf8fa06e071', N'h@h.com', N'h@h.com', NULL, 0, CAST(0x00009E9800162C1F AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'3c6c3f00-b218-4009-9772-693b624efa6a', N'hotordford@telus.net', N'hotordford@telus.net', NULL, 0, CAST(0x00009E0C00449796 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'ca36439d-21e5-4b4c-b518-ce9e71a844e6', N'hotrodford@telus.net', N'hotrodford@telus.net', NULL, 0, CAST(0x00009E980031592C AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'8518e486-8a91-4562-a496-d7612ae203d6', N'i@i.com', N'i@i.com', NULL, 0, CAST(0x00009EC00182FDF8 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'ac413ef8-305e-44f4-991c-e224b8f5d469', N'j@j.com', N'j@j.com', NULL, 0, CAST(0x00009E6001797D80 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'cb88886d-c851-47fc-8aba-36231ee4bbd4', N'k@k.com', N'k@k.com', NULL, 0, CAST(0x00009E7C004E1803 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'd0aa7517-fa79-4830-af90-949446d95d41', N'l@l.com', N'l@l.com', NULL, 0, CAST(0x00009E82015CE148 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'3d1f1805-10d9-4784-bd8e-41f0f9145075', N'm@m.com', N'm@m.com', NULL, 0, CAST(0x00009E8201666E00 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'22feb4f9-23ba-4c9c-9e3a-95d77d7fa0fb', N'n@n.com', N'n@n.com', NULL, 0, CAST(0x00009E8201668798 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'178586c9-27c1-4d8d-b36d-d8b411a61ffa', N'o@o.com', N'o@o.com', NULL, 0, CAST(0x00009E82017EE9B9 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'4acf3773-16a6-4aaf-9bbc-591b1e3a0aba', N'p@p.com', N'p@p.com', NULL, 0, CAST(0x00009E82016D0550 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'805c664b-c438-4b2f-8bc9-76cb248126f7', N'phack@telus.net', N'phack@telus.net', NULL, 0, CAST(0x00009E0C004020D5 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'cce32c03-4bee-4af4-a9b6-8b3e45400a94', N'phil.hack@halicom.com', N'phil.hack@halicom.com', NULL, 0, CAST(0x00009E980021BBE8 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'aec294c9-a075-4b0d-abb7-cd0372ad8861', N'phil@treomadesign.com', N'phil@treomadesign.com', NULL, 0, CAST(0x00009F07010CE82C AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'4c0247b0-8b3b-4767-8531-06a8e0868a06', N'phil33@philhack.com', N'phil33@philhack.com', NULL, 0, CAST(0x00009F690124D028 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'b968db8c-4041-4260-8801-e721b20d37f0', N'philliphack@gmail.com', N'philliphack@gmail.com', NULL, 0, CAST(0x00009F2A018A3549 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'24512166-8985-487e-9b2f-d513a72684d8', N'q@q.com', N'q@q.com', NULL, 0, CAST(0x00009E8900222473 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'dc1e420b-59dd-408a-9a18-32932d12673f', N'test@philhack.com', N'test@philhack.com', NULL, 0, CAST(0x00009EC8011EC7A0 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'baf6d079-462a-473f-8174-2e1bcf2013a5', N'testuser@test.com', N'testuser@test.com', NULL, 0, CAST(0x00009E0A00D819E0 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'9748f3cf-a3ea-4742-bbce-f82a876d33b7', N'zz@philhack.com', N'zz@philhack.com', NULL, 0, CAST(0x00009EDD0112E6D7 AS DateTime))
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'46ef0661-4958-4019-996a-3cd37d422bd2', N'zzz@philhack.com', N'zzz@philhack.com', NULL, 0, CAST(0x00009EEC00F03048 AS DateTime))
/****** Object:  StoredProcedure [dbo].[aspnet_UnRegisterSchemaVersion]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UnRegisterSchemaVersion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UnRegisterSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128)
AS
BEGIN
    DELETE FROM dbo.aspnet_SchemaVersions
        WHERE   Feature = LOWER(@Feature) AND @CompatibleSchemaVersion = CompatibleSchemaVersion
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_CheckSchemaVersion]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_CheckSchemaVersion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_CheckSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128)
AS
BEGIN
    IF (EXISTS( SELECT  *
                FROM    dbo.aspnet_SchemaVersions
                WHERE   Feature = LOWER( @Feature ) AND
                        CompatibleSchemaVersion = @CompatibleSchemaVersion ))
        RETURN 0

    RETURN 1
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Applications_CreateApplication]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications_CreateApplication]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Applications_CreateApplication]
    @ApplicationName      nvarchar(256),
    @ApplicationId        uniqueidentifier OUTPUT
AS
BEGIN
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName

    IF(@ApplicationId IS NULL)
    BEGIN
        DECLARE @TranStarted   bit
        SET @TranStarted = 0

        IF( @@TRANCOUNT = 0 )
        BEGIN
	        BEGIN TRANSACTION
	        SET @TranStarted = 1
        END
        ELSE
    	    SET @TranStarted = 0

        SELECT  @ApplicationId = ApplicationId
        FROM dbo.aspnet_Applications WITH (UPDLOCK, HOLDLOCK)
        WHERE LOWER(@ApplicationName) = LoweredApplicationName

        IF(@ApplicationId IS NULL)
        BEGIN
            SELECT  @ApplicationId = NEWID()
            INSERT  dbo.aspnet_Applications (ApplicationId, ApplicationName, LoweredApplicationName)
            VALUES  (@ApplicationId, @ApplicationName, LOWER(@ApplicationName))
        END


        IF( @TranStarted = 1 )
        BEGIN
            IF(@@ERROR = 0)
            BEGIN
	        SET @TranStarted = 0
	        COMMIT TRANSACTION
            END
            ELSE
            BEGIN
                SET @TranStarted = 0
                ROLLBACK TRANSACTION
            END
        END
    END
END' 
END
GO
/****** Object:  Table [dbo].[aspnet_Paths]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Paths](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[PathId] [uniqueidentifier] NOT NULL,
	[Path] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LoweredPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK__aspnet_P__CD67DC5859063A47] PRIMARY KEY NONCLUSTERED 
(
	[PathId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]') AND name = N'aspnet_Paths_index')
CREATE UNIQUE CLUSTERED INDEX [aspnet_Paths_index] ON [dbo].[aspnet_Paths] 
(
	[ApplicationId] ASC,
	[LoweredPath] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Personalization_GetApplicationId]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Personalization_GetApplicationId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Personalization_GetApplicationId] (
    @ApplicationName NVARCHAR(256),
    @ApplicationId UNIQUEIDENTIFIER OUT)
AS
BEGIN
    SELECT @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
END' 
END
GO
/****** Object:  Table [dbo].[aspnet_Roles]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Roles](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[RoleName] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LoweredRoleName] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK__aspnet_R__8AFACE1B4222D4EF] PRIMARY KEY NONCLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]') AND name = N'aspnet_Roles_index1')
CREATE UNIQUE CLUSTERED INDEX [aspnet_Roles_index1] ON [dbo].[aspnet_Roles] 
(
	[ApplicationId] ASC,
	[LoweredRoleName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
INSERT [dbo].[aspnet_Roles] ([ApplicationId], [RoleId], [RoleName], [LoweredRoleName], [Description]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'71143e14-6c3b-4bbf-80c3-fc2612c858c0', N'Administrators', N'administrators', NULL)
INSERT [dbo].[aspnet_Roles] ([ApplicationId], [RoleId], [RoleName], [LoweredRoleName], [Description]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'fa0a4d4c-03bf-44c8-bd1d-e51dc1aac71b', N'Designers', N'designers', NULL)
/****** Object:  StoredProcedure [dbo].[aspnet_RegisterSchemaVersion]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_RegisterSchemaVersion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_RegisterSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128),
    @IsCurrentVersion          bit,
    @RemoveIncompatibleSchema  bit
AS
BEGIN
    IF( @RemoveIncompatibleSchema = 1 )
    BEGIN
        DELETE FROM dbo.aspnet_SchemaVersions WHERE Feature = LOWER( @Feature )
    END
    ELSE
    BEGIN
        IF( @IsCurrentVersion = 1 )
        BEGIN
            UPDATE dbo.aspnet_SchemaVersions
            SET IsCurrentVersion = 0
            WHERE Feature = LOWER( @Feature )
        END
    END

    INSERT  dbo.aspnet_SchemaVersions( Feature, CompatibleSchemaVersion, IsCurrentVersion )
    VALUES( LOWER( @Feature ), @CompatibleSchemaVersion, @IsCurrentVersion )
END' 
END
GO
/****** Object:  Table [dbo].[aspnet_PersonalizationPerUser]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_PersonalizationPerUser](
	[Id] [uniqueidentifier] NOT NULL,
	[PathId] [uniqueidentifier] NULL,
	[UserId] [uniqueidentifier] NULL,
	[PageSettings] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK__aspnet_P__3214EC06656C112C] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]') AND name = N'aspnet_PersonalizationPerUser_index1')
CREATE UNIQUE CLUSTERED INDEX [aspnet_PersonalizationPerUser_index1] ON [dbo].[aspnet_PersonalizationPerUser] 
(
	[PathId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]') AND name = N'aspnet_PersonalizationPerUser_ncindex2')
CREATE UNIQUE NONCLUSTERED INDEX [aspnet_PersonalizationPerUser_ncindex2] ON [dbo].[aspnet_PersonalizationPerUser] 
(
	[UserId] ASC,
	[PathId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
/****** Object:  Table [dbo].[aspnet_Profile]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Profile](
	[UserId] [uniqueidentifier] NOT NULL,
	[PropertyNames] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[PropertyValuesString] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[PropertyValuesBinary] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK__aspnet_P__1788CC4C36B12243] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[aspnet_Membership]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Membership](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Password] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[PasswordFormat] [int] NOT NULL,
	[PasswordSalt] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MobilePIN] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Email] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LoweredEmail] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PasswordQuestion] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PasswordAnswer] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsApproved] [bit] NOT NULL,
	[IsLockedOut] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastLoginDate] [datetime] NOT NULL,
	[LastPasswordChangedDate] [datetime] NOT NULL,
	[LastLockoutDate] [datetime] NOT NULL,
	[FailedPasswordAttemptCount] [int] NOT NULL,
	[FailedPasswordAttemptWindowStart] [datetime] NOT NULL,
	[FailedPasswordAnswerAttemptCount] [int] NOT NULL,
	[FailedPasswordAnswerAttemptWindowStart] [datetime] NOT NULL,
	[Comment] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK__aspnet_M__1788CC4D1FCDBCEB] PRIMARY KEY NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]') AND name = N'aspnet_Membership_index')
CREATE CLUSTERED INDEX [aspnet_Membership_index] ON [dbo].[aspnet_Membership] 
(
	[ApplicationId] ASC,
	[LoweredEmail] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'b1c8f00a-01f4-42f3-b0ef-a1f097b6c635', N'Hvj2fwN5OG+xp+pn0tjkaipfQEA=', 1, N'cPwRnuQwULAK3V6ydvyv1Q==', NULL, N'a@a.com', N'a@a.com', NULL, NULL, 1, 0, CAST(0x00009E3C0143A840 AS DateTime), CAST(0x00009E9000556A02 AS DateTime), CAST(0x00009E90005569FF AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 1, CAST(0x00009EC00173DB8D AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'1e9e9a92-f839-47d0-b871-897bf77cd20b', N'1QvBW/JfpJ8hakx9zKDZiim0iWM=', 1, N'NNfq+OO82nmPZG9rqA/Ulw==', NULL, N'a@philhack.com', N'a@philhack.com', NULL, NULL, 1, 0, CAST(0x00009EC1012A2FF0 AS DateTime), CAST(0x00009ED0018B03F3 AS DateTime), CAST(0x00009EC1012A2FF0 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'597c6c12-4299-4d8a-8097-ba15f26a1a2c', N'ufmNdeNweqcYaBfnswBVnMrJZNw=', 1, N'h2n3pj2ixaek/It+QW8G3Q==', NULL, N'a3a99226@telus.net', N'a3a99226@telus.net', NULL, NULL, 1, 0, CAST(0x00009E0C00468CF0 AS DateTime), CAST(0x00009E980052E547 AS DateTime), CAST(0x00009E91000FBB62 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'1338a83c-7de8-4732-bad0-560cc73ccc38', N'kCXSYL4TwP6sFLO9i6Yx3WEkbc4=', 1, N'tV09WkQyJJd+I6ndXZfZaw==', NULL, N'aa@aa.com', N'aa@aa.com', NULL, NULL, 1, 0, CAST(0x00009E890115BAD4 AS DateTime), CAST(0x00009E890124010E AS DateTime), CAST(0x00009E890115BAD4 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'90006fa8-6140-46f7-aa44-5ee8b3c21ea3', N'TXdOC8LWRa7dTew60kgBIBJ+ibk=', 1, N'aS0G2C4hEIAj4hKMz1ubiA==', NULL, N'b@b.com', N'b@b.com', NULL, NULL, 1, 0, CAST(0x00009E50001D310C AS DateTime), CAST(0x00009E52001F4FC8 AS DateTime), CAST(0x00009E50001D310C AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'a2c86efa-717c-490e-8022-610ce83e789d', N'Sa1jN4I6ohRd8UH6FwIspylEEG8=', 1, N'q1XMfnifiPYRmUDAI7y9tA==', NULL, N'c@c.com', N'c@c.com', NULL, NULL, 1, 0, CAST(0x00009E5101751D30 AS DateTime), CAST(0x00009E57014A75D7 AS DateTime), CAST(0x00009E5101751D30 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'2d0a1462-3ad3-44ac-b8da-c144263c2ff3', N'/wNDF5l2ZIIPIPsnLyi1XyTLqoo=', 1, N'lzNHl9+XMXUe20agw9gZcg==', NULL, N'd@d.com', N'd@d.com', NULL, NULL, 1, 0, CAST(0x00009E55010855EC AS DateTime), CAST(0x00009E55010B2226 AS DateTime), CAST(0x00009E55010855EC AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'e2aac83e-ec38-40a4-ae4e-5b126ff97041', N'YrusHCXBx+Ho8ywkhILMKGAFznA=', 1, N'0xuNrjYe7pHFAxHVE0AyRw==', NULL, N'e@e.com', N'e@e.com', NULL, NULL, 1, 0, CAST(0x00009E55010B4D4C AS DateTime), CAST(0x00009E5A011CE2C5 AS DateTime), CAST(0x00009E55010B4D4C AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'c3dab54c-5449-4644-ac7d-9a83436baac6', N'zFN5ANgZw/m7kRccUoVfwLWBjUc=', 1, N'T6XoQbtU4IQTd0Wn4AgSIA==', NULL, N'f@f.com', N'f@f.com', NULL, NULL, 1, 0, CAST(0x00009E5A01215A38 AS DateTime), CAST(0x00009E5A0138BC5B AS DateTime), CAST(0x00009E5A01215A38 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'baf6d079-462a-473f-8174-2e1bcf2013a5', N'iZzFcBQXOqhXaE3tIzWpZ/ekbW8=', 1, N'D0M9vU7h46wm/FXVvMCxvA==', NULL, N'fastaccord@telus.net', N'fastaccord@telus.net', NULL, NULL, 1, 0, CAST(0x00009E0A00D819E0 AS DateTime), CAST(0x00009E0A00D819E0 AS DateTime), CAST(0x00009E0A00D819E0 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'3759fae6-a0b9-4baf-bb45-5a4521839df9', N'i1jaDPxLKCsoKVLGQ0WU83FNzMM=', 1, N'dEl5p1JllOuuuZjpGJh5Hg==', NULL, N'g@g.com', N'g@g.com', NULL, NULL, 1, 1, CAST(0x00009E5A014013D8 AS DateTime), CAST(0x00009E7B016D651D AS DateTime), CAST(0x00009E7B016D26DB AS DateTime), CAST(0x00009E7B0170740B AS DateTime), 5, CAST(0x00009E7B0170740B AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'9b794b62-ea1a-4518-80d1-cbf8fa06e071', N'mjZc7KzW+Vd6kzhp6R1ehCoeCFA=', 1, N'uGzh6XXvL3klr/kMwwF2fg==', NULL, N'h@h.com', N'h@h.com', NULL, NULL, 1, 0, CAST(0x00009E6001768170 AS DateTime), CAST(0x00009E9800162C1F AS DateTime), CAST(0x00009E98001625A0 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 2, CAST(0x00009EC00182EEFA AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'3c6c3f00-b218-4009-9772-693b624efa6a', N'YsRIUHfSV+e0DwtbVhpd85O5egs=', 1, N'ZvdQOP7oeI1Bj+U5uptdvw==', NULL, N'hotordford@telus.net', N'hotordford@telus.net', NULL, NULL, 1, 0, CAST(0x00009E0C0043A850 AS DateTime), CAST(0x00009E0C00449796 AS DateTime), CAST(0x00009E0C0043A850 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'ca36439d-21e5-4b4c-b518-ce9e71a844e6', N'yExcapHyRiHjqitZDrQ3OJdnHfg=', 1, N'c45mQAqs7XXRLoMmxEfUXA==', NULL, N'hotrodford@telus.net', N'hotrodford@telus.net', NULL, NULL, 1, 0, CAST(0x00009E98002D369C AS DateTime), CAST(0x00009E980031592C AS DateTime), CAST(0x00009E98002D369C AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'8518e486-8a91-4562-a496-d7612ae203d6', N'HcJeX/mU0f2GRJqCK8jUqId2OT8=', 1, N'gYAy8GVGe57+IyAAXaCAoQ==', NULL, N'i@i.com', N'i@i.com', NULL, NULL, 1, 0, CAST(0x00009E6001783EC0 AS DateTime), CAST(0x00009EC00182FDF8 AS DateTime), CAST(0x00009E6001783EC0 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'ac413ef8-305e-44f4-991c-e224b8f5d469', N'y7GrPh/MTPmkLH4kfHm2iJ5qSz4=', 1, N'1GqK1jk6AIa0jko19HcAAw==', NULL, N'j@j.com', N'j@j.com', NULL, NULL, 1, 0, CAST(0x00009E6001797D80 AS DateTime), CAST(0x00009E6001797D80 AS DateTime), CAST(0x00009E6001797D80 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'cb88886d-c851-47fc-8aba-36231ee4bbd4', N'VFSzgFWDqfc4B1b+yXfshc2j+iA=', 1, N'W/szdW8yPAU2j7/VIgd1kQ==', NULL, N'k@k.com', N'k@k.com', NULL, NULL, 1, 0, CAST(0x00009E7C004095FC AS DateTime), CAST(0x00009E7C004E1803 AS DateTime), CAST(0x00009E7C004095FC AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'd0aa7517-fa79-4830-af90-949446d95d41', N'Zd51S57Cvd3oeExyXumr9rv3hAQ=', 1, N'x/jGU+e3/lQWRlVlXQW2kQ==', NULL, N'l@l.com', N'l@l.com', NULL, NULL, 1, 0, CAST(0x00009E82015CE148 AS DateTime), CAST(0x00009E82015CE148 AS DateTime), CAST(0x00009E82015CE148 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'3d1f1805-10d9-4784-bd8e-41f0f9145075', N'OPTpGaWNuHtrIyAkbcHqD9rmdoI=', 1, N'L9cTNaZiwNpmBxykv8wFBQ==', NULL, N'm@m.com', N'm@m.com', NULL, NULL, 1, 0, CAST(0x00009E82015D9F20 AS DateTime), CAST(0x00009E8201666E00 AS DateTime), CAST(0x00009E82015D9F20 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'22feb4f9-23ba-4c9c-9e3a-95d77d7fa0fb', N'UpgJupRGxAfSpRds9u2hyBwXJIs=', 1, N'CrrYE5GglbpBj40QbMRgXA==', NULL, N'n@n.com', N'n@n.com', NULL, NULL, 1, 0, CAST(0x00009E8201668798 AS DateTime), CAST(0x00009E8201668798 AS DateTime), CAST(0x00009E8201668798 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'178586c9-27c1-4d8d-b36d-d8b411a61ffa', N'RtTmI3Scddb2lSnhTiqbevon9lA=', 1, N'uFI79QT1h6vTe2chmiEYhA==', NULL, N'o@o.com', N'o@o.com', NULL, NULL, 1, 0, CAST(0x00009E820167AC90 AS DateTime), CAST(0x00009E82017EE9B9 AS DateTime), CAST(0x00009E820167AC90 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'4acf3773-16a6-4aaf-9bbc-591b1e3a0aba', N'vmX/E9R7H0zVwCbenbvJrhlItzQ=', 1, N'/wfgvnxi51G9+Y77YWMnBw==', NULL, N'p@p.com', N'p@p.com', NULL, NULL, 1, 0, CAST(0x00009E82016D0550 AS DateTime), CAST(0x00009E82016D0550 AS DateTime), CAST(0x00009E82016D0550 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'cce32c03-4bee-4af4-a9b6-8b3e45400a94', N'WXJjCDVAX+DF4NNzVXVS4y7K1UQ=', 1, N'm0e+5V7VtZftYrW50t+sRA==', NULL, N'phil.hack@halicom.com', N'phil.hack@halicom.com', NULL, NULL, 1, 0, CAST(0x00009E500012BB50 AS DateTime), CAST(0x00009E980021BBE8 AS DateTime), CAST(0x00009E980021B9FD AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'805c664b-c438-4b2f-8bc9-76cb248126f7', N'mju3nsJt11yrGVu2T9F/3303ONI=', 1, N'5jKWnU4mUGwj+YBr6MB6rQ==', NULL, N'phil@philhack.com', N'phil@philhack.com', NULL, NULL, 1, 0, CAST(0x00009DEE01480188 AS DateTime), CAST(0x00009E0C004020D5 AS DateTime), CAST(0x00009DEE01480188 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'aec294c9-a075-4b0d-abb7-cd0372ad8861', N'ZUmwPWn2rVojeaTT3VH3m/ghYdA=', 1, N'LRYTmdDKJ4XIlaPCFnc9FQ==', NULL, N'phil@treomadesign.com', N'phil@treomadesign.com', NULL, NULL, 1, 0, CAST(0x00009E0D00FFB4F0 AS DateTime), CAST(0x00009F07010CE82C AS DateTime), CAST(0x00009E890121B1D9 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'4c0247b0-8b3b-4767-8531-06a8e0868a06', N'q2c5NqLrK2IzjdoVHQjOPqV+cPE=', 1, N'KkFLvsPF64UgCb/McXgFYA==', NULL, N'phil33@philhack.com', N'phil33@philhack.com', NULL, NULL, 1, 0, CAST(0x00009F690124D028 AS DateTime), CAST(0x00009F690124D028 AS DateTime), CAST(0x00009F690124D028 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'b968db8c-4041-4260-8801-e721b20d37f0', N'aFe5K/HjJ3fkMWC2m3yOcwWmrfY=', 1, N'pzxCq5F2TnzemffLE7U58A==', NULL, N'philliphack@gmail.com', N'philliphack@gmail.com', NULL, NULL, 1, 0, CAST(0x00009E23003DB3B4 AS DateTime), CAST(0x00009F2A018A3549 AS DateTime), CAST(0x00009EFF00FA397E AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'24512166-8985-487e-9b2f-d513a72684d8', N'MVeTQlEO9NVTNxNORAtBhkCA7nQ=', 1, N'B4ZjU0A0DR9BJf7DYfMUiw==', NULL, N'q@q.com', N'q@q.com', NULL, NULL, 1, 0, CAST(0x00009E8900171CCC AS DateTime), CAST(0x00009E8900222473 AS DateTime), CAST(0x00009E8900171CCC AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'dc1e420b-59dd-408a-9a18-32932d12673f', N'm+7Fb452QVvIo9PFEvwAdDrHNRQ=', 1, N'POkTH4mvCVAgYiFdnllsDA==', NULL, N'test@philhack.com', N'test@philhack.com', NULL, NULL, 1, 0, CAST(0x00009EC8011EC7A0 AS DateTime), CAST(0x00009EC8011EC7A0 AS DateTime), CAST(0x00009EC8011EC7A0 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'9748f3cf-a3ea-4742-bbce-f82a876d33b7', N'C00OeZau29O+geKTaKR9i/Ow00M=', 1, N'1CEEw5KwqLQRIxyli2xWpA==', NULL, N'zz@philhack.com', N'zz@philhack.com', NULL, NULL, 1, 0, CAST(0x00009EDD010C7F28 AS DateTime), CAST(0x00009EDD0112E6D7 AS DateTime), CAST(0x00009EDD010C7F28 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'5f69dd40-ae55-4e4d-b1d8-bf4035a8cff2', N'46ef0661-4958-4019-996a-3cd37d422bd2', N'795gDIMCheg96A7BS8BUHJCrCGQ=', 1, N'UYeN1hx/p6vpoXfrVYFs6Q==', NULL, N'zzz@philhack.com', N'zzz@philhack.com', NULL, NULL, 1, 0, CAST(0x00009EEC00F03048 AS DateTime), CAST(0x00009EEC00F03048 AS DateTime), CAST(0x00009EEC00F03048 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
/****** Object:  StoredProcedure [dbo].[aspnet_Paths_CreatePath]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Paths_CreatePath]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Paths_CreatePath]
    @ApplicationId UNIQUEIDENTIFIER,
    @Path           NVARCHAR(256),
    @PathId         UNIQUEIDENTIFIER OUTPUT
AS
BEGIN
    BEGIN TRANSACTION
    IF (NOT EXISTS(SELECT * FROM dbo.aspnet_Paths WHERE LoweredPath = LOWER(@Path) AND ApplicationId = @ApplicationId))
    BEGIN
        INSERT dbo.aspnet_Paths (ApplicationId, Path, LoweredPath) VALUES (@ApplicationId, @Path, LOWER(@Path))
    END
    COMMIT TRANSACTION
    SELECT @PathId = PathId FROM dbo.aspnet_Paths WHERE LOWER(@Path) = LoweredPath AND ApplicationId = @ApplicationId
END' 
END
GO
/****** Object:  Table [dbo].[aspnet_PersonalizationAllUsers]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_PersonalizationAllUsers](
	[PathId] [uniqueidentifier] NOT NULL,
	[PageSettings] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK__aspnet_P__CD67DC5960A75C0F] PRIMARY KEY CLUSTERED 
(
	[PathId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Users_CreateUser]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users_CreateUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Users_CreateUser]
    @ApplicationId    uniqueidentifier,
    @UserName         nvarchar(256),
    @IsUserAnonymous  bit,
    @LastActivityDate DATETIME,
    @UserId           uniqueidentifier OUTPUT
AS
BEGIN
    IF( @UserId IS NULL )
        SELECT @UserId = NEWID()
    ELSE
    BEGIN
        IF( EXISTS( SELECT UserId FROM dbo.aspnet_Users
                    WHERE @UserId = UserId ) )
            RETURN -1
    END

    INSERT dbo.aspnet_Users (ApplicationId, UserId, UserName, LoweredUserName, IsAnonymous, LastActivityDate)
    VALUES (@ApplicationId, @UserId, @UserName, LOWER(@UserName), @IsUserAnonymous, @LastActivityDate)

    RETURN 0
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_RoleExists]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_RoleExists]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Roles_RoleExists]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(0)
    IF (EXISTS (SELECT RoleName FROM dbo.aspnet_Roles WHERE LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId ))
        RETURN(1)
    ELSE
        RETURN(0)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_GetAllRoles]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_GetAllRoles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Roles_GetAllRoles] (
    @ApplicationName           nvarchar(256))
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN
    SELECT RoleName
    FROM   dbo.aspnet_Roles WHERE ApplicationId = @ApplicationId
    ORDER BY RoleName
END' 
END
GO
/****** Object:  Table [dbo].[aspnet_UsersInRoles]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_UsersInRoles](
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK__aspnet_U__AF2760AD47DBAE45] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]') AND name = N'aspnet_UsersInRoles_index')
CREATE NONCLUSTERED INDEX [aspnet_UsersInRoles_index] ON [dbo].[aspnet_UsersInRoles] 
(
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
INSERT [dbo].[aspnet_UsersInRoles] ([UserId], [RoleId]) VALUES (N'aec294c9-a075-4b0d-abb7-cd0372ad8861', N'fa0a4d4c-03bf-44c8-bd1d-e51dc1aac71b')
/****** Object:  Table [dbo].[RoomPhotos]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RoomPhotos]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RoomPhotos](
	[RoomPhotoId] [int] IDENTITY(1,1) NOT NULL,
	[PhotoName] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[RoomId] [int] NOT NULL,
	[OriginalServerPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OriginalUrlPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[WebServerPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[WebUrlPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThumbnailServerPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThumbnailUrlPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OriginalRSContainer] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OriginalRSUrl] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[WebRSContainer] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[WebRSUrl] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThumbnailRSContainer] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThumbnailRSUrl] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_RoomPhotos] PRIMARY KEY CLUSTERED 
(
	[RoomPhotoId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[RoomPhotos] ON
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (6, N'1-0ae31cd4-b14e-493c-bfc9-1dfde573e9eb.jpg', 1, N'~/UserData/\rpo\1-0ae31cd4-b14e-493c-bfc9-1dfde573e9eb.jpg', N'/UserData/rpo/1-0ae31cd4-b14e-493c-bfc9-1dfde573e9eb.jpg', N'~/UserData/\rpw\1-0ae31cd4-b14e-493c-bfc9-1dfde573e9eb.jpg', N'/UserData/rpw/1-0ae31cd4-b14e-493c-bfc9-1dfde573e9eb.jpg', N'~/UserData/\rpt\1-0ae31cd4-b14e-493c-bfc9-1dfde573e9eb.jpg', N'/UserData/rpt/1-0ae31cd4-b14e-493c-bfc9-1dfde573e9eb.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/1-0ae31cd4-b14e-493c-bfc9-1dfde573e9eb.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/1-0ae31cd4-b14e-493c-bfc9-1dfde573e9eb.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/1-0ae31cd4-b14e-493c-bfc9-1dfde573e9eb.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (7, N'1-a54445f4-1445-4b76-bcba-30e919be106d.jpg', 1, N'~/UserData/\rpo\1-a54445f4-1445-4b76-bcba-30e919be106d.jpg', N'/UserData/rpo/1-a54445f4-1445-4b76-bcba-30e919be106d.jpg', N'~/UserData/\rpw\1-a54445f4-1445-4b76-bcba-30e919be106d.jpg', N'/UserData/rpw/1-a54445f4-1445-4b76-bcba-30e919be106d.jpg', N'~/UserData/\rpt\1-a54445f4-1445-4b76-bcba-30e919be106d.jpg', N'/UserData/rpt/1-a54445f4-1445-4b76-bcba-30e919be106d.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/1-a54445f4-1445-4b76-bcba-30e919be106d.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/1-a54445f4-1445-4b76-bcba-30e919be106d.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/1-a54445f4-1445-4b76-bcba-30e919be106d.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (8, N'1-e9c060b3-81ea-4282-84a4-2d46d48f6524.jpg', 1, N'~/UserData/\rpo\1-e9c060b3-81ea-4282-84a4-2d46d48f6524.jpg', N'/UserData/rpo/1-e9c060b3-81ea-4282-84a4-2d46d48f6524.jpg', N'~/UserData/\rpw\1-e9c060b3-81ea-4282-84a4-2d46d48f6524.jpg', N'/UserData/rpw/1-e9c060b3-81ea-4282-84a4-2d46d48f6524.jpg', N'~/UserData/\rpt\1-e9c060b3-81ea-4282-84a4-2d46d48f6524.jpg', N'/UserData/rpt/1-e9c060b3-81ea-4282-84a4-2d46d48f6524.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/1-e9c060b3-81ea-4282-84a4-2d46d48f6524.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/1-e9c060b3-81ea-4282-84a4-2d46d48f6524.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/1-e9c060b3-81ea-4282-84a4-2d46d48f6524.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (9, N'1-f37aac69-ec25-4f4a-8d26-d5cb4a35e3f8.jpg', 1, N'~/UserData/\rpo\1-f37aac69-ec25-4f4a-8d26-d5cb4a35e3f8.jpg', N'/UserData/rpo/1-f37aac69-ec25-4f4a-8d26-d5cb4a35e3f8.jpg', N'~/UserData/\rpw\1-f37aac69-ec25-4f4a-8d26-d5cb4a35e3f8.jpg', N'/UserData/rpw/1-f37aac69-ec25-4f4a-8d26-d5cb4a35e3f8.jpg', N'~/UserData/\rpt\1-f37aac69-ec25-4f4a-8d26-d5cb4a35e3f8.jpg', N'/UserData/rpt/1-f37aac69-ec25-4f4a-8d26-d5cb4a35e3f8.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/1-f37aac69-ec25-4f4a-8d26-d5cb4a35e3f8.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/1-f37aac69-ec25-4f4a-8d26-d5cb4a35e3f8.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/1-f37aac69-ec25-4f4a-8d26-d5cb4a35e3f8.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (10, N'2-ecfa0899-0041-4cbe-8973-f4167c67b694.jpg', 2, N'~/UserData/\rpo\2-ecfa0899-0041-4cbe-8973-f4167c67b694.jpg', N'/UserData/rpo/2-ecfa0899-0041-4cbe-8973-f4167c67b694.jpg', N'~/UserData/\rpw\2-ecfa0899-0041-4cbe-8973-f4167c67b694.jpg', N'/UserData/rpw/2-ecfa0899-0041-4cbe-8973-f4167c67b694.jpg', N'~/UserData/\rpt\2-ecfa0899-0041-4cbe-8973-f4167c67b694.jpg', N'/UserData/rpt/2-ecfa0899-0041-4cbe-8973-f4167c67b694.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/2-ecfa0899-0041-4cbe-8973-f4167c67b694.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/2-ecfa0899-0041-4cbe-8973-f4167c67b694.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/2-ecfa0899-0041-4cbe-8973-f4167c67b694.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (11, N'2-88b9377b-a9f5-4cf6-92d1-8d39bb540b3f.jpg', 2, N'~/UserData/\rpo\2-88b9377b-a9f5-4cf6-92d1-8d39bb540b3f.jpg', N'/UserData/rpo/2-88b9377b-a9f5-4cf6-92d1-8d39bb540b3f.jpg', N'~/UserData/\rpw\2-88b9377b-a9f5-4cf6-92d1-8d39bb540b3f.jpg', N'/UserData/rpw/2-88b9377b-a9f5-4cf6-92d1-8d39bb540b3f.jpg', N'~/UserData/\rpt\2-88b9377b-a9f5-4cf6-92d1-8d39bb540b3f.jpg', N'/UserData/rpt/2-88b9377b-a9f5-4cf6-92d1-8d39bb540b3f.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/2-88b9377b-a9f5-4cf6-92d1-8d39bb540b3f.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/2-88b9377b-a9f5-4cf6-92d1-8d39bb540b3f.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/2-88b9377b-a9f5-4cf6-92d1-8d39bb540b3f.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (23, N'13-f3098982-d7ff-46d3-be87-0dd9999ca6c1.jpg', 13, N'~/UserData/\rpo\13-f3098982-d7ff-46d3-be87-0dd9999ca6c1.jpg', N'/UserData/rpo/13-f3098982-d7ff-46d3-be87-0dd9999ca6c1.jpg', N'~/UserData/\rpw\13-f3098982-d7ff-46d3-be87-0dd9999ca6c1.jpg', N'/UserData/rpw/13-f3098982-d7ff-46d3-be87-0dd9999ca6c1.jpg', N'~/UserData/\rpt\13-f3098982-d7ff-46d3-be87-0dd9999ca6c1.jpg', N'/UserData/rpt/13-f3098982-d7ff-46d3-be87-0dd9999ca6c1.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/13-f3098982-d7ff-46d3-be87-0dd9999ca6c1.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/13-f3098982-d7ff-46d3-be87-0dd9999ca6c1.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/13-f3098982-d7ff-46d3-be87-0dd9999ca6c1.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (24, N'28-63714e4c-ef75-4521-8bfe-b86272acc532.jpg', 28, N'~/UserData/\rpo\28-63714e4c-ef75-4521-8bfe-b86272acc532.jpg', N'/UserData/rpo/28-63714e4c-ef75-4521-8bfe-b86272acc532.jpg', N'~/UserData/\rpw\28-63714e4c-ef75-4521-8bfe-b86272acc532.jpg', N'/UserData/rpw/28-63714e4c-ef75-4521-8bfe-b86272acc532.jpg', N'~/UserData/\rpt\28-63714e4c-ef75-4521-8bfe-b86272acc532.jpg', N'/UserData/rpt/28-63714e4c-ef75-4521-8bfe-b86272acc532.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/28-63714e4c-ef75-4521-8bfe-b86272acc532.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/28-63714e4c-ef75-4521-8bfe-b86272acc532.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/28-63714e4c-ef75-4521-8bfe-b86272acc532.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (25, N'28-11e7562d-e4a9-4d3f-885c-3c5625ded13f.jpg', 28, N'~/UserData/\rpo\28-11e7562d-e4a9-4d3f-885c-3c5625ded13f.jpg', N'/UserData/rpo/28-11e7562d-e4a9-4d3f-885c-3c5625ded13f.jpg', N'~/UserData/\rpw\28-11e7562d-e4a9-4d3f-885c-3c5625ded13f.jpg', N'/UserData/rpw/28-11e7562d-e4a9-4d3f-885c-3c5625ded13f.jpg', N'~/UserData/\rpt\28-11e7562d-e4a9-4d3f-885c-3c5625ded13f.jpg', N'/UserData/rpt/28-11e7562d-e4a9-4d3f-885c-3c5625ded13f.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/28-11e7562d-e4a9-4d3f-885c-3c5625ded13f.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/28-11e7562d-e4a9-4d3f-885c-3c5625ded13f.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/28-11e7562d-e4a9-4d3f-885c-3c5625ded13f.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (26, N'28-d93cbc94-7422-4053-8756-4c904bd73331.jpg', 28, N'~/UserData/\rpo\28-d93cbc94-7422-4053-8756-4c904bd73331.jpg', N'/UserData/rpo/28-d93cbc94-7422-4053-8756-4c904bd73331.jpg', N'~/UserData/\rpw\28-d93cbc94-7422-4053-8756-4c904bd73331.jpg', N'/UserData/rpw/28-d93cbc94-7422-4053-8756-4c904bd73331.jpg', N'~/UserData/\rpt\28-d93cbc94-7422-4053-8756-4c904bd73331.jpg', N'/UserData/rpt/28-d93cbc94-7422-4053-8756-4c904bd73331.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/28-d93cbc94-7422-4053-8756-4c904bd73331.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/28-d93cbc94-7422-4053-8756-4c904bd73331.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/28-d93cbc94-7422-4053-8756-4c904bd73331.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (27, N'28-61c81591-4b12-4a3c-95ed-149b771adf43.jpg', 28, N'~/UserData/\rpo\28-61c81591-4b12-4a3c-95ed-149b771adf43.jpg', N'/UserData/rpo/28-61c81591-4b12-4a3c-95ed-149b771adf43.jpg', N'~/UserData/\rpw\28-61c81591-4b12-4a3c-95ed-149b771adf43.jpg', N'/UserData/rpw/28-61c81591-4b12-4a3c-95ed-149b771adf43.jpg', N'~/UserData/\rpt\28-61c81591-4b12-4a3c-95ed-149b771adf43.jpg', N'/UserData/rpt/28-61c81591-4b12-4a3c-95ed-149b771adf43.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/28-61c81591-4b12-4a3c-95ed-149b771adf43.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/28-61c81591-4b12-4a3c-95ed-149b771adf43.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/28-61c81591-4b12-4a3c-95ed-149b771adf43.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (28, N'28-39356759-ef89-43c9-bf8a-e9ae66d069b9.jpg', 28, N'~/UserData/\rpo\28-39356759-ef89-43c9-bf8a-e9ae66d069b9.jpg', N'/UserData/rpo/28-39356759-ef89-43c9-bf8a-e9ae66d069b9.jpg', N'~/UserData/\rpw\28-39356759-ef89-43c9-bf8a-e9ae66d069b9.jpg', N'/UserData/rpw/28-39356759-ef89-43c9-bf8a-e9ae66d069b9.jpg', N'~/UserData/\rpt\28-39356759-ef89-43c9-bf8a-e9ae66d069b9.jpg', N'/UserData/rpt/28-39356759-ef89-43c9-bf8a-e9ae66d069b9.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/28-39356759-ef89-43c9-bf8a-e9ae66d069b9.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/28-39356759-ef89-43c9-bf8a-e9ae66d069b9.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/28-39356759-ef89-43c9-bf8a-e9ae66d069b9.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (29, N'28-0216d869-846f-4b38-b49b-0a8f02a65a6f.jpg', 28, N'~/UserData/\rpo\28-0216d869-846f-4b38-b49b-0a8f02a65a6f.jpg', N'/UserData/rpo/28-0216d869-846f-4b38-b49b-0a8f02a65a6f.jpg', N'~/UserData/\rpw\28-0216d869-846f-4b38-b49b-0a8f02a65a6f.jpg', N'/UserData/rpw/28-0216d869-846f-4b38-b49b-0a8f02a65a6f.jpg', N'~/UserData/\rpt\28-0216d869-846f-4b38-b49b-0a8f02a65a6f.jpg', N'/UserData/rpt/28-0216d869-846f-4b38-b49b-0a8f02a65a6f.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/28-0216d869-846f-4b38-b49b-0a8f02a65a6f.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/28-0216d869-846f-4b38-b49b-0a8f02a65a6f.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/28-0216d869-846f-4b38-b49b-0a8f02a65a6f.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (30, N'28-d7c92e81-f86a-44b9-8329-5e4131b2ee64.jpg', 28, N'~/UserData/\rpo\28-d7c92e81-f86a-44b9-8329-5e4131b2ee64.jpg', N'/UserData/rpo/28-d7c92e81-f86a-44b9-8329-5e4131b2ee64.jpg', N'~/UserData/\rpw\28-d7c92e81-f86a-44b9-8329-5e4131b2ee64.jpg', N'/UserData/rpw/28-d7c92e81-f86a-44b9-8329-5e4131b2ee64.jpg', N'~/UserData/\rpt\28-d7c92e81-f86a-44b9-8329-5e4131b2ee64.jpg', N'/UserData/rpt/28-d7c92e81-f86a-44b9-8329-5e4131b2ee64.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/28-d7c92e81-f86a-44b9-8329-5e4131b2ee64.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/28-d7c92e81-f86a-44b9-8329-5e4131b2ee64.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/28-d7c92e81-f86a-44b9-8329-5e4131b2ee64.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (31, N'11-ea9ff31c-5a22-4583-b588-e93ac929afb0.jpg', 11, N'~/UserData/\rpo\11-ea9ff31c-5a22-4583-b588-e93ac929afb0.jpg', N'/UserData/rpo/11-ea9ff31c-5a22-4583-b588-e93ac929afb0.jpg', N'~/UserData/\rpw\11-ea9ff31c-5a22-4583-b588-e93ac929afb0.jpg', N'/UserData/rpw/11-ea9ff31c-5a22-4583-b588-e93ac929afb0.jpg', N'~/UserData/\rpt\11-ea9ff31c-5a22-4583-b588-e93ac929afb0.jpg', N'/UserData/rpt/11-ea9ff31c-5a22-4583-b588-e93ac929afb0.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/11-ea9ff31c-5a22-4583-b588-e93ac929afb0.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/11-ea9ff31c-5a22-4583-b588-e93ac929afb0.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/11-ea9ff31c-5a22-4583-b588-e93ac929afb0.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (34, N'41-769fbf61-b3cb-48a1-91d4-fe5220e95c34.jpg', 41, N'~/UserData/\rpo\41-769fbf61-b3cb-48a1-91d4-fe5220e95c34.jpg', N'/UserData/rpo/41-769fbf61-b3cb-48a1-91d4-fe5220e95c34.jpg', N'~/UserData/\rpw\41-769fbf61-b3cb-48a1-91d4-fe5220e95c34.jpg', N'/UserData/rpw/41-769fbf61-b3cb-48a1-91d4-fe5220e95c34.jpg', N'~/UserData/\rpt\41-769fbf61-b3cb-48a1-91d4-fe5220e95c34.jpg', N'/UserData/rpt/41-769fbf61-b3cb-48a1-91d4-fe5220e95c34.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/41-769fbf61-b3cb-48a1-91d4-fe5220e95c34.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/41-769fbf61-b3cb-48a1-91d4-fe5220e95c34.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/41-769fbf61-b3cb-48a1-91d4-fe5220e95c34.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (35, N'41-bf1d5a5c-bc20-443c-860e-f59a1a5a0b2b.jpg', 41, N'~/UserData/\rpo\41-bf1d5a5c-bc20-443c-860e-f59a1a5a0b2b.jpg', N'/UserData/rpo/41-bf1d5a5c-bc20-443c-860e-f59a1a5a0b2b.jpg', N'~/UserData/\rpw\41-bf1d5a5c-bc20-443c-860e-f59a1a5a0b2b.jpg', N'/UserData/rpw/41-bf1d5a5c-bc20-443c-860e-f59a1a5a0b2b.jpg', N'~/UserData/\rpt\41-bf1d5a5c-bc20-443c-860e-f59a1a5a0b2b.jpg', N'/UserData/rpt/41-bf1d5a5c-bc20-443c-860e-f59a1a5a0b2b.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/41-bf1d5a5c-bc20-443c-860e-f59a1a5a0b2b.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/41-bf1d5a5c-bc20-443c-860e-f59a1a5a0b2b.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/41-bf1d5a5c-bc20-443c-860e-f59a1a5a0b2b.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (36, N'41-be73aedd-b1b3-4e6d-abf0-e0b46c27ba51.jpg', 41, N'~/UserData/\rpo\41-be73aedd-b1b3-4e6d-abf0-e0b46c27ba51.jpg', N'/UserData/rpo/41-be73aedd-b1b3-4e6d-abf0-e0b46c27ba51.jpg', N'~/UserData/\rpw\41-be73aedd-b1b3-4e6d-abf0-e0b46c27ba51.jpg', N'/UserData/rpw/41-be73aedd-b1b3-4e6d-abf0-e0b46c27ba51.jpg', N'~/UserData/\rpt\41-be73aedd-b1b3-4e6d-abf0-e0b46c27ba51.jpg', N'/UserData/rpt/41-be73aedd-b1b3-4e6d-abf0-e0b46c27ba51.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/41-be73aedd-b1b3-4e6d-abf0-e0b46c27ba51.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/41-be73aedd-b1b3-4e6d-abf0-e0b46c27ba51.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/41-be73aedd-b1b3-4e6d-abf0-e0b46c27ba51.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (37, N'41-ac04b27a-9491-40c3-b469-7420b4fcd08b.jpg', 41, N'~/UserData/\rpo\41-ac04b27a-9491-40c3-b469-7420b4fcd08b.jpg', N'/UserData/rpo/41-ac04b27a-9491-40c3-b469-7420b4fcd08b.jpg', N'~/UserData/\rpw\41-ac04b27a-9491-40c3-b469-7420b4fcd08b.jpg', N'/UserData/rpw/41-ac04b27a-9491-40c3-b469-7420b4fcd08b.jpg', N'~/UserData/\rpt\41-ac04b27a-9491-40c3-b469-7420b4fcd08b.jpg', N'/UserData/rpt/41-ac04b27a-9491-40c3-b469-7420b4fcd08b.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/41-ac04b27a-9491-40c3-b469-7420b4fcd08b.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/41-ac04b27a-9491-40c3-b469-7420b4fcd08b.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/41-ac04b27a-9491-40c3-b469-7420b4fcd08b.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (38, N'41-c9e662dd-b379-42cc-9a8d-216a176b031d.jpg', 41, N'~/UserData/\rpo\41-c9e662dd-b379-42cc-9a8d-216a176b031d.jpg', N'/UserData/rpo/41-c9e662dd-b379-42cc-9a8d-216a176b031d.jpg', N'~/UserData/\rpw\41-c9e662dd-b379-42cc-9a8d-216a176b031d.jpg', N'/UserData/rpw/41-c9e662dd-b379-42cc-9a8d-216a176b031d.jpg', N'~/UserData/\rpt\41-c9e662dd-b379-42cc-9a8d-216a176b031d.jpg', N'/UserData/rpt/41-c9e662dd-b379-42cc-9a8d-216a176b031d.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/41-c9e662dd-b379-42cc-9a8d-216a176b031d.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/41-c9e662dd-b379-42cc-9a8d-216a176b031d.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/41-c9e662dd-b379-42cc-9a8d-216a176b031d.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (53, N'44-86c46008-3763-4de8-92d3-084baf0c1c50.jpg', 44, N'~/UserData/\rpo\44-86c46008-3763-4de8-92d3-084baf0c1c50.jpg', N'/UserData/rpo/44-86c46008-3763-4de8-92d3-084baf0c1c50.jpg', N'~/UserData/\rpw\44-86c46008-3763-4de8-92d3-084baf0c1c50.jpg', N'/UserData/rpw/44-86c46008-3763-4de8-92d3-084baf0c1c50.jpg', N'~/UserData/\rpt\44-86c46008-3763-4de8-92d3-084baf0c1c50.jpg', N'/UserData/rpt/44-86c46008-3763-4de8-92d3-084baf0c1c50.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/44-86c46008-3763-4de8-92d3-084baf0c1c50.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/44-86c46008-3763-4de8-92d3-084baf0c1c50.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/44-86c46008-3763-4de8-92d3-084baf0c1c50.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (54, N'44-e4a2817c-8bf2-4f3c-85f3-7fbdf9953e23.jpg', 44, N'~/UserData/\rpo\44-e4a2817c-8bf2-4f3c-85f3-7fbdf9953e23.jpg', N'/UserData/rpo/44-e4a2817c-8bf2-4f3c-85f3-7fbdf9953e23.jpg', N'~/UserData/\rpw\44-e4a2817c-8bf2-4f3c-85f3-7fbdf9953e23.jpg', N'/UserData/rpw/44-e4a2817c-8bf2-4f3c-85f3-7fbdf9953e23.jpg', N'~/UserData/\rpt\44-e4a2817c-8bf2-4f3c-85f3-7fbdf9953e23.jpg', N'/UserData/rpt/44-e4a2817c-8bf2-4f3c-85f3-7fbdf9953e23.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/44-e4a2817c-8bf2-4f3c-85f3-7fbdf9953e23.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/44-e4a2817c-8bf2-4f3c-85f3-7fbdf9953e23.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/44-e4a2817c-8bf2-4f3c-85f3-7fbdf9953e23.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (55, N'45-9cd271a7-4d71-4f3a-b9af-ac72dec6a901.jpg', 45, N'~/UserData/\rpo\45-9cd271a7-4d71-4f3a-b9af-ac72dec6a901.jpg', N'/UserData/rpo/45-9cd271a7-4d71-4f3a-b9af-ac72dec6a901.jpg', N'~/UserData/\rpw\45-9cd271a7-4d71-4f3a-b9af-ac72dec6a901.jpg', N'/UserData/rpw/45-9cd271a7-4d71-4f3a-b9af-ac72dec6a901.jpg', N'~/UserData/\rpt\45-9cd271a7-4d71-4f3a-b9af-ac72dec6a901.jpg', N'/UserData/rpt/45-9cd271a7-4d71-4f3a-b9af-ac72dec6a901.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/45-9cd271a7-4d71-4f3a-b9af-ac72dec6a901.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/45-9cd271a7-4d71-4f3a-b9af-ac72dec6a901.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/45-9cd271a7-4d71-4f3a-b9af-ac72dec6a901.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (56, N'45-eaec4057-3e6c-4307-be62-f037596d89b8.jpg', 45, N'~/UserData/\rpo\45-eaec4057-3e6c-4307-be62-f037596d89b8.jpg', N'/UserData/rpo/45-eaec4057-3e6c-4307-be62-f037596d89b8.jpg', N'~/UserData/\rpw\45-eaec4057-3e6c-4307-be62-f037596d89b8.jpg', N'/UserData/rpw/45-eaec4057-3e6c-4307-be62-f037596d89b8.jpg', N'~/UserData/\rpt\45-eaec4057-3e6c-4307-be62-f037596d89b8.jpg', N'/UserData/rpt/45-eaec4057-3e6c-4307-be62-f037596d89b8.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/45-eaec4057-3e6c-4307-be62-f037596d89b8.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/45-eaec4057-3e6c-4307-be62-f037596d89b8.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/45-eaec4057-3e6c-4307-be62-f037596d89b8.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (57, N'48-0b6b5a28-41ff-4408-b79d-53c6dcfac222.jpg', 48, N'~/UserData/\rpo\48-0b6b5a28-41ff-4408-b79d-53c6dcfac222.jpg', N'/UserData/rpo/48-0b6b5a28-41ff-4408-b79d-53c6dcfac222.jpg', N'~/UserData/\rpw\48-0b6b5a28-41ff-4408-b79d-53c6dcfac222.jpg', N'/UserData/rpw/48-0b6b5a28-41ff-4408-b79d-53c6dcfac222.jpg', N'~/UserData/\rpt\48-0b6b5a28-41ff-4408-b79d-53c6dcfac222.jpg', N'/UserData/rpt/48-0b6b5a28-41ff-4408-b79d-53c6dcfac222.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/48-0b6b5a28-41ff-4408-b79d-53c6dcfac222.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/48-0b6b5a28-41ff-4408-b79d-53c6dcfac222.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/48-0b6b5a28-41ff-4408-b79d-53c6dcfac222.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (58, N'70-1367c839-7fa1-4d5c-aa0c-caaa028b34c1.jpg', 70, N'~/UserData/\rpo\70-1367c839-7fa1-4d5c-aa0c-caaa028b34c1.jpg', N'/UserData/rpo/70-1367c839-7fa1-4d5c-aa0c-caaa028b34c1.jpg', N'~/UserData/\rpw\70-1367c839-7fa1-4d5c-aa0c-caaa028b34c1.jpg', N'/UserData/rpw/70-1367c839-7fa1-4d5c-aa0c-caaa028b34c1.jpg', N'~/UserData/\rpt\70-1367c839-7fa1-4d5c-aa0c-caaa028b34c1.jpg', N'/UserData/rpt/70-1367c839-7fa1-4d5c-aa0c-caaa028b34c1.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/70-1367c839-7fa1-4d5c-aa0c-caaa028b34c1.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/70-1367c839-7fa1-4d5c-aa0c-caaa028b34c1.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/70-1367c839-7fa1-4d5c-aa0c-caaa028b34c1.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (61, N'73-e3fea955-2753-4865-9200-c81318cd50e1.jpg', 73, N'~/UserData/\rpo\73-e3fea955-2753-4865-9200-c81318cd50e1.jpg', N'/UserData/rpo/73-e3fea955-2753-4865-9200-c81318cd50e1.jpg', N'~/UserData/\rpw\73-e3fea955-2753-4865-9200-c81318cd50e1.jpg', N'/UserData/rpw/73-e3fea955-2753-4865-9200-c81318cd50e1.jpg', N'~/UserData/\rpt\73-e3fea955-2753-4865-9200-c81318cd50e1.jpg', N'/UserData/rpt/73-e3fea955-2753-4865-9200-c81318cd50e1.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/73-e3fea955-2753-4865-9200-c81318cd50e1.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/73-e3fea955-2753-4865-9200-c81318cd50e1.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/73-e3fea955-2753-4865-9200-c81318cd50e1.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (62, N'73-4bf45826-9706-4fe5-9b66-4fd770e9bf65.jpg', 73, N'~/UserData/\rpo\73-4bf45826-9706-4fe5-9b66-4fd770e9bf65.jpg', N'/UserData/rpo/73-4bf45826-9706-4fe5-9b66-4fd770e9bf65.jpg', N'~/UserData/\rpw\73-4bf45826-9706-4fe5-9b66-4fd770e9bf65.jpg', N'/UserData/rpw/73-4bf45826-9706-4fe5-9b66-4fd770e9bf65.jpg', N'~/UserData/\rpt\73-4bf45826-9706-4fe5-9b66-4fd770e9bf65.jpg', N'/UserData/rpt/73-4bf45826-9706-4fe5-9b66-4fd770e9bf65.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/73-4bf45826-9706-4fe5-9b66-4fd770e9bf65.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/73-4bf45826-9706-4fe5-9b66-4fd770e9bf65.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/73-4bf45826-9706-4fe5-9b66-4fd770e9bf65.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (63, N'73-c6920bf2-f1b9-48ec-a8ed-3c90feb5b4e1.jpg', 73, N'~/UserData/\rpo\73-c6920bf2-f1b9-48ec-a8ed-3c90feb5b4e1.jpg', N'/UserData/rpo/73-c6920bf2-f1b9-48ec-a8ed-3c90feb5b4e1.jpg', N'~/UserData/\rpw\73-c6920bf2-f1b9-48ec-a8ed-3c90feb5b4e1.jpg', N'/UserData/rpw/73-c6920bf2-f1b9-48ec-a8ed-3c90feb5b4e1.jpg', N'~/UserData/\rpt\73-c6920bf2-f1b9-48ec-a8ed-3c90feb5b4e1.jpg', N'/UserData/rpt/73-c6920bf2-f1b9-48ec-a8ed-3c90feb5b4e1.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/73-c6920bf2-f1b9-48ec-a8ed-3c90feb5b4e1.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/73-c6920bf2-f1b9-48ec-a8ed-3c90feb5b4e1.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/73-c6920bf2-f1b9-48ec-a8ed-3c90feb5b4e1.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (64, N'73-03ecc773-09ae-44a0-b5eb-73ff6461db02.jpg', 73, N'~/UserData/\rpo\73-03ecc773-09ae-44a0-b5eb-73ff6461db02.jpg', N'/UserData/rpo/73-03ecc773-09ae-44a0-b5eb-73ff6461db02.jpg', N'~/UserData/\rpw\73-03ecc773-09ae-44a0-b5eb-73ff6461db02.jpg', N'/UserData/rpw/73-03ecc773-09ae-44a0-b5eb-73ff6461db02.jpg', N'~/UserData/\rpt\73-03ecc773-09ae-44a0-b5eb-73ff6461db02.jpg', N'/UserData/rpt/73-03ecc773-09ae-44a0-b5eb-73ff6461db02.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/73-03ecc773-09ae-44a0-b5eb-73ff6461db02.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/73-03ecc773-09ae-44a0-b5eb-73ff6461db02.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/73-03ecc773-09ae-44a0-b5eb-73ff6461db02.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (65, N'73-064a8454-52d0-436b-93fe-4bc3c53d1211.jpg', 73, N'~/UserData/\rpo\73-064a8454-52d0-436b-93fe-4bc3c53d1211.jpg', N'/UserData/rpo/73-064a8454-52d0-436b-93fe-4bc3c53d1211.jpg', N'~/UserData/\rpw\73-064a8454-52d0-436b-93fe-4bc3c53d1211.jpg', N'/UserData/rpw/73-064a8454-52d0-436b-93fe-4bc3c53d1211.jpg', N'~/UserData/\rpt\73-064a8454-52d0-436b-93fe-4bc3c53d1211.jpg', N'/UserData/rpt/73-064a8454-52d0-436b-93fe-4bc3c53d1211.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/73-064a8454-52d0-436b-93fe-4bc3c53d1211.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/73-064a8454-52d0-436b-93fe-4bc3c53d1211.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/73-064a8454-52d0-436b-93fe-4bc3c53d1211.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (66, N'73-50199cae-38b4-4c52-acfd-a504456e5785.jpg', 73, N'~/UserData/\rpo\73-50199cae-38b4-4c52-acfd-a504456e5785.jpg', N'/UserData/rpo/73-50199cae-38b4-4c52-acfd-a504456e5785.jpg', N'~/UserData/\rpw\73-50199cae-38b4-4c52-acfd-a504456e5785.jpg', N'/UserData/rpw/73-50199cae-38b4-4c52-acfd-a504456e5785.jpg', N'~/UserData/\rpt\73-50199cae-38b4-4c52-acfd-a504456e5785.jpg', N'/UserData/rpt/73-50199cae-38b4-4c52-acfd-a504456e5785.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/73-50199cae-38b4-4c52-acfd-a504456e5785.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/73-50199cae-38b4-4c52-acfd-a504456e5785.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/73-50199cae-38b4-4c52-acfd-a504456e5785.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (67, N'73-889dd500-6f65-4b1b-a8be-e683f9c0b55c.jpg', 73, N'~/UserData/\rpo\73-889dd500-6f65-4b1b-a8be-e683f9c0b55c.jpg', N'/UserData/rpo/73-889dd500-6f65-4b1b-a8be-e683f9c0b55c.jpg', N'~/UserData/\rpw\73-889dd500-6f65-4b1b-a8be-e683f9c0b55c.jpg', N'/UserData/rpw/73-889dd500-6f65-4b1b-a8be-e683f9c0b55c.jpg', N'~/UserData/\rpt\73-889dd500-6f65-4b1b-a8be-e683f9c0b55c.jpg', N'/UserData/rpt/73-889dd500-6f65-4b1b-a8be-e683f9c0b55c.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/73-889dd500-6f65-4b1b-a8be-e683f9c0b55c.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/73-889dd500-6f65-4b1b-a8be-e683f9c0b55c.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/73-889dd500-6f65-4b1b-a8be-e683f9c0b55c.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (68, N'73-221b3dbd-96d2-4375-af97-c11146a05158.jpg', 73, N'~/UserData/\rpo\73-221b3dbd-96d2-4375-af97-c11146a05158.jpg', N'/UserData/rpo/73-221b3dbd-96d2-4375-af97-c11146a05158.jpg', N'~/UserData/\rpw\73-221b3dbd-96d2-4375-af97-c11146a05158.jpg', N'/UserData/rpw/73-221b3dbd-96d2-4375-af97-c11146a05158.jpg', N'~/UserData/\rpt\73-221b3dbd-96d2-4375-af97-c11146a05158.jpg', N'/UserData/rpt/73-221b3dbd-96d2-4375-af97-c11146a05158.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/73-221b3dbd-96d2-4375-af97-c11146a05158.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/73-221b3dbd-96d2-4375-af97-c11146a05158.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/73-221b3dbd-96d2-4375-af97-c11146a05158.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (69, N'73-99673493-4a64-4c59-be12-2e3e7bdd02a2.jpg', 73, N'~/UserData/\rpo\73-99673493-4a64-4c59-be12-2e3e7bdd02a2.jpg', N'/UserData/rpo/73-99673493-4a64-4c59-be12-2e3e7bdd02a2.jpg', N'~/UserData/\rpw\73-99673493-4a64-4c59-be12-2e3e7bdd02a2.jpg', N'/UserData/rpw/73-99673493-4a64-4c59-be12-2e3e7bdd02a2.jpg', N'~/UserData/\rpt\73-99673493-4a64-4c59-be12-2e3e7bdd02a2.jpg', N'/UserData/rpt/73-99673493-4a64-4c59-be12-2e3e7bdd02a2.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/73-99673493-4a64-4c59-be12-2e3e7bdd02a2.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/73-99673493-4a64-4c59-be12-2e3e7bdd02a2.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/73-99673493-4a64-4c59-be12-2e3e7bdd02a2.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (70, N'73-1ca92ec1-68bf-42ee-84ac-6b2389e04d59.jpg', 73, N'~/UserData/\rpo\73-1ca92ec1-68bf-42ee-84ac-6b2389e04d59.jpg', N'/UserData/rpo/73-1ca92ec1-68bf-42ee-84ac-6b2389e04d59.jpg', N'~/UserData/\rpw\73-1ca92ec1-68bf-42ee-84ac-6b2389e04d59.jpg', N'/UserData/rpw/73-1ca92ec1-68bf-42ee-84ac-6b2389e04d59.jpg', N'~/UserData/\rpt\73-1ca92ec1-68bf-42ee-84ac-6b2389e04d59.jpg', N'/UserData/rpt/73-1ca92ec1-68bf-42ee-84ac-6b2389e04d59.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/73-1ca92ec1-68bf-42ee-84ac-6b2389e04d59.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/73-1ca92ec1-68bf-42ee-84ac-6b2389e04d59.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/73-1ca92ec1-68bf-42ee-84ac-6b2389e04d59.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (71, N'73-35403864-f6d4-42a1-b569-e3105300a332.jpg', 73, N'~/UserData/\rpo\73-35403864-f6d4-42a1-b569-e3105300a332.jpg', N'/UserData/rpo/73-35403864-f6d4-42a1-b569-e3105300a332.jpg', N'~/UserData/\rpw\73-35403864-f6d4-42a1-b569-e3105300a332.jpg', N'/UserData/rpw/73-35403864-f6d4-42a1-b569-e3105300a332.jpg', N'~/UserData/\rpt\73-35403864-f6d4-42a1-b569-e3105300a332.jpg', N'/UserData/rpt/73-35403864-f6d4-42a1-b569-e3105300a332.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/73-35403864-f6d4-42a1-b569-e3105300a332.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/73-35403864-f6d4-42a1-b569-e3105300a332.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/73-35403864-f6d4-42a1-b569-e3105300a332.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (73, N'74-20f82b63-a7e4-4102-bac6-2e4348bbbbfa.jpg', 74, N'~/UserData/\rpo\74-20f82b63-a7e4-4102-bac6-2e4348bbbbfa.jpg', N'/UserData/rpo/74-20f82b63-a7e4-4102-bac6-2e4348bbbbfa.jpg', N'~/UserData/\rpw\74-20f82b63-a7e4-4102-bac6-2e4348bbbbfa.jpg', N'/UserData/rpw/74-20f82b63-a7e4-4102-bac6-2e4348bbbbfa.jpg', N'~/UserData/\rpt\74-20f82b63-a7e4-4102-bac6-2e4348bbbbfa.jpg', N'/UserData/rpt/74-20f82b63-a7e4-4102-bac6-2e4348bbbbfa.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/74-20f82b63-a7e4-4102-bac6-2e4348bbbbfa.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/74-20f82b63-a7e4-4102-bac6-2e4348bbbbfa.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/74-20f82b63-a7e4-4102-bac6-2e4348bbbbfa.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (74, N'74-48fa2769-f032-4df7-a624-535f943545ec.jpg', 74, N'~/UserData/\rpo\74-48fa2769-f032-4df7-a624-535f943545ec.jpg', N'/UserData/rpo/74-48fa2769-f032-4df7-a624-535f943545ec.jpg', N'~/UserData/\rpw\74-48fa2769-f032-4df7-a624-535f943545ec.jpg', N'/UserData/rpw/74-48fa2769-f032-4df7-a624-535f943545ec.jpg', N'~/UserData/\rpt\74-48fa2769-f032-4df7-a624-535f943545ec.jpg', N'/UserData/rpt/74-48fa2769-f032-4df7-a624-535f943545ec.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/74-48fa2769-f032-4df7-a624-535f943545ec.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/74-48fa2769-f032-4df7-a624-535f943545ec.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/74-48fa2769-f032-4df7-a624-535f943545ec.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (75, N'74-8b8d6ed2-e77c-4bed-abbf-d2b5034da16d.jpg', 74, N'~/UserData/\rpo\74-8b8d6ed2-e77c-4bed-abbf-d2b5034da16d.jpg', N'/UserData/rpo/74-8b8d6ed2-e77c-4bed-abbf-d2b5034da16d.jpg', N'~/UserData/\rpw\74-8b8d6ed2-e77c-4bed-abbf-d2b5034da16d.jpg', N'/UserData/rpw/74-8b8d6ed2-e77c-4bed-abbf-d2b5034da16d.jpg', N'~/UserData/\rpt\74-8b8d6ed2-e77c-4bed-abbf-d2b5034da16d.jpg', N'/UserData/rpt/74-8b8d6ed2-e77c-4bed-abbf-d2b5034da16d.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/74-8b8d6ed2-e77c-4bed-abbf-d2b5034da16d.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/74-8b8d6ed2-e77c-4bed-abbf-d2b5034da16d.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/74-8b8d6ed2-e77c-4bed-abbf-d2b5034da16d.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (76, N'74-112e2a7e-6d3e-448b-b6be-1b71789533a8.jpg', 74, N'~/UserData/\rpo\74-112e2a7e-6d3e-448b-b6be-1b71789533a8.jpg', N'/UserData/rpo/74-112e2a7e-6d3e-448b-b6be-1b71789533a8.jpg', N'~/UserData/\rpw\74-112e2a7e-6d3e-448b-b6be-1b71789533a8.jpg', N'/UserData/rpw/74-112e2a7e-6d3e-448b-b6be-1b71789533a8.jpg', N'~/UserData/\rpt\74-112e2a7e-6d3e-448b-b6be-1b71789533a8.jpg', N'/UserData/rpt/74-112e2a7e-6d3e-448b-b6be-1b71789533a8.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/74-112e2a7e-6d3e-448b-b6be-1b71789533a8.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/74-112e2a7e-6d3e-448b-b6be-1b71789533a8.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/74-112e2a7e-6d3e-448b-b6be-1b71789533a8.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (79, N'72-c7879059-52f1-4bd8-8956-64de81cd6328.jpg', 72, N'~/UserData/\rpo\72-c7879059-52f1-4bd8-8956-64de81cd6328.jpg', N'/UserData/rpo/72-c7879059-52f1-4bd8-8956-64de81cd6328.jpg', N'~/UserData/\rpw\72-c7879059-52f1-4bd8-8956-64de81cd6328.jpg', N'/UserData/rpw/72-c7879059-52f1-4bd8-8956-64de81cd6328.jpg', N'~/UserData/\rpt\72-c7879059-52f1-4bd8-8956-64de81cd6328.jpg', N'/UserData/rpt/72-c7879059-52f1-4bd8-8956-64de81cd6328.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/72-c7879059-52f1-4bd8-8956-64de81cd6328.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/72-c7879059-52f1-4bd8-8956-64de81cd6328.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/72-c7879059-52f1-4bd8-8956-64de81cd6328.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (80, N'72-78348fc2-9abf-4c79-830c-c737b1836737.jpg', 72, N'~/UserData/\rpo\72-78348fc2-9abf-4c79-830c-c737b1836737.jpg', N'/UserData/rpo/72-78348fc2-9abf-4c79-830c-c737b1836737.jpg', N'~/UserData/\rpw\72-78348fc2-9abf-4c79-830c-c737b1836737.jpg', N'/UserData/rpw/72-78348fc2-9abf-4c79-830c-c737b1836737.jpg', N'~/UserData/\rpt\72-78348fc2-9abf-4c79-830c-c737b1836737.jpg', N'/UserData/rpt/72-78348fc2-9abf-4c79-830c-c737b1836737.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/72-78348fc2-9abf-4c79-830c-c737b1836737.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/72-78348fc2-9abf-4c79-830c-c737b1836737.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/72-78348fc2-9abf-4c79-830c-c737b1836737.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (81, N'98-5d726060-a554-47b0-b6f4-2a38808a3415.jpg', 98, N'~/UserData/\rpo\98-5d726060-a554-47b0-b6f4-2a38808a3415.jpg', N'/UserData/rpo/98-5d726060-a554-47b0-b6f4-2a38808a3415.jpg', N'~/UserData/\rpw\98-5d726060-a554-47b0-b6f4-2a38808a3415.jpg', N'/UserData/rpw/98-5d726060-a554-47b0-b6f4-2a38808a3415.jpg', N'~/UserData/\rpt\98-5d726060-a554-47b0-b6f4-2a38808a3415.jpg', N'/UserData/rpt/98-5d726060-a554-47b0-b6f4-2a38808a3415.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/98-5d726060-a554-47b0-b6f4-2a38808a3415.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/98-5d726060-a554-47b0-b6f4-2a38808a3415.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/98-5d726060-a554-47b0-b6f4-2a38808a3415.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (103, N'104-0732b218-ad9c-4ed0-8a22-cb4a0c56ffce.jpg', 104, N'~/UserData/\rpo\104-0732b218-ad9c-4ed0-8a22-cb4a0c56ffce.jpg', N'/UserData/rpo/104-0732b218-ad9c-4ed0-8a22-cb4a0c56ffce.jpg', N'~/UserData/\rpw\104-0732b218-ad9c-4ed0-8a22-cb4a0c56ffce.jpg', N'/UserData/rpw/104-0732b218-ad9c-4ed0-8a22-cb4a0c56ffce.jpg', N'~/UserData/\rpt\104-0732b218-ad9c-4ed0-8a22-cb4a0c56ffce.jpg', N'/UserData/rpt/104-0732b218-ad9c-4ed0-8a22-cb4a0c56ffce.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/104-0732b218-ad9c-4ed0-8a22-cb4a0c56ffce.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/104-0732b218-ad9c-4ed0-8a22-cb4a0c56ffce.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/104-0732b218-ad9c-4ed0-8a22-cb4a0c56ffce.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (104, N'103-7d31e7f6-b990-4828-a231-6ff4febf7c9a.jpg', 103, N'~/UserData/\rpo\103-7d31e7f6-b990-4828-a231-6ff4febf7c9a.jpg', N'/UserData/rpo/103-7d31e7f6-b990-4828-a231-6ff4febf7c9a.jpg', N'~/UserData/\rpw\103-7d31e7f6-b990-4828-a231-6ff4febf7c9a.jpg', N'/UserData/rpw/103-7d31e7f6-b990-4828-a231-6ff4febf7c9a.jpg', N'~/UserData/\rpt\103-7d31e7f6-b990-4828-a231-6ff4febf7c9a.jpg', N'/UserData/rpt/103-7d31e7f6-b990-4828-a231-6ff4febf7c9a.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/103-7d31e7f6-b990-4828-a231-6ff4febf7c9a.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/103-7d31e7f6-b990-4828-a231-6ff4febf7c9a.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/103-7d31e7f6-b990-4828-a231-6ff4febf7c9a.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (105, N'105-db5f12d1-1c30-4e6d-93c5-287f4162b505.jpg', 105, N'~/UserData/\rpo\105-db5f12d1-1c30-4e6d-93c5-287f4162b505.jpg', N'/UserData/rpo/105-db5f12d1-1c30-4e6d-93c5-287f4162b505.jpg', N'~/UserData/\rpw\105-db5f12d1-1c30-4e6d-93c5-287f4162b505.jpg', N'/UserData/rpw/105-db5f12d1-1c30-4e6d-93c5-287f4162b505.jpg', N'~/UserData/\rpt\105-db5f12d1-1c30-4e6d-93c5-287f4162b505.jpg', N'/UserData/rpt/105-db5f12d1-1c30-4e6d-93c5-287f4162b505.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/105-db5f12d1-1c30-4e6d-93c5-287f4162b505.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/105-db5f12d1-1c30-4e6d-93c5-287f4162b505.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/105-db5f12d1-1c30-4e6d-93c5-287f4162b505.jpg')
INSERT [dbo].[RoomPhotos] ([RoomPhotoId], [PhotoName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (106, N'106-c95eba5b-23aa-4733-8a02-0cf1e231e080.jpg', 106, N'~/UserData/\rpo\106-c95eba5b-23aa-4733-8a02-0cf1e231e080.jpg', N'/UserData/rpo/106-c95eba5b-23aa-4733-8a02-0cf1e231e080.jpg', N'~/UserData/\rpw\106-c95eba5b-23aa-4733-8a02-0cf1e231e080.jpg', N'/UserData/rpw/106-c95eba5b-23aa-4733-8a02-0cf1e231e080.jpg', N'~/UserData/\rpt\106-c95eba5b-23aa-4733-8a02-0cf1e231e080.jpg', N'/UserData/rpt/106-c95eba5b-23aa-4733-8a02-0cf1e231e080.jpg', N'rpo', N'http://c0003356.cdn2.cloudfiles.rackspacecloud.com/106-c95eba5b-23aa-4733-8a02-0cf1e231e080.jpg', N'rpw', N'http://c0003358.cdn2.cloudfiles.rackspacecloud.com/106-c95eba5b-23aa-4733-8a02-0cf1e231e080.jpg', N'rpt', N'http://c0003357.cdn2.cloudfiles.rackspacecloud.com/106-c95eba5b-23aa-4733-8a02-0cf1e231e080.jpg')
SET IDENTITY_INSERT [dbo].[RoomPhotos] OFF
/****** Object:  Table [dbo].[RoomMeasurements]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RoomMeasurements]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RoomMeasurements](
	[RoomMeasurementId] [int] IDENTITY(1,1) NOT NULL,
	[MeasurementName] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[RoomId] [int] NOT NULL,
	[OriginalServerPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OriginalUrlPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[WebServerPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[WebUrlPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThumbnailServerPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThumbnailUrlPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OriginalRSContainer] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OriginalRSUrl] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[WebRSContainer] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[WebRSUrl] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThumbnailRSContainer] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThumbnailRSUrl] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_RoomMeasurements] PRIMARY KEY CLUSTERED 
(
	[RoomMeasurementId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[RoomMeasurements] ON
INSERT [dbo].[RoomMeasurements] ([RoomMeasurementId], [MeasurementName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (4, N'1-8b7f0bc3-83bc-4050-83d9-343c8751663b.jpg', 1, N'~/UserData/\rmo\1-8b7f0bc3-83bc-4050-83d9-343c8751663b.jpg', N'/UserData/rmo/1-8b7f0bc3-83bc-4050-83d9-343c8751663b.jpg', N'~/UserData/\rmw\1-8b7f0bc3-83bc-4050-83d9-343c8751663b.jpg', N'/UserData/rmw/1-8b7f0bc3-83bc-4050-83d9-343c8751663b.jpg', N'~/UserData/\rmt\1-8b7f0bc3-83bc-4050-83d9-343c8751663b.jpg', N'/UserData/rmt/1-8b7f0bc3-83bc-4050-83d9-343c8751663b.jpg', N'rmo', N'http://c0003353.cdn2.cloudfiles.rackspacecloud.com/1-8b7f0bc3-83bc-4050-83d9-343c8751663b.jpg', N'rmw', N'http://c0003355.cdn2.cloudfiles.rackspacecloud.com/1-8b7f0bc3-83bc-4050-83d9-343c8751663b.jpg', N'rmt', N'http://c0003354.cdn2.cloudfiles.rackspacecloud.com/1-8b7f0bc3-83bc-4050-83d9-343c8751663b.jpg')
INSERT [dbo].[RoomMeasurements] ([RoomMeasurementId], [MeasurementName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (8, N'45-e58ce51b-6ec0-49af-b3be-2632738a0e58.jpg', 45, N'~/UserData/\rmo\45-e58ce51b-6ec0-49af-b3be-2632738a0e58.jpg', N'/UserData/rmo/45-e58ce51b-6ec0-49af-b3be-2632738a0e58.jpg', N'~/UserData/\rmw\45-e58ce51b-6ec0-49af-b3be-2632738a0e58.jpg', N'/UserData/rmw/45-e58ce51b-6ec0-49af-b3be-2632738a0e58.jpg', N'~/UserData/\rmt\45-e58ce51b-6ec0-49af-b3be-2632738a0e58.jpg', N'/UserData/rmt/45-e58ce51b-6ec0-49af-b3be-2632738a0e58.jpg', N'rmo', N'http://c0003353.cdn2.cloudfiles.rackspacecloud.com/45-e58ce51b-6ec0-49af-b3be-2632738a0e58.jpg', N'rmw', N'http://c0003355.cdn2.cloudfiles.rackspacecloud.com/45-e58ce51b-6ec0-49af-b3be-2632738a0e58.jpg', N'rmt', N'http://c0003354.cdn2.cloudfiles.rackspacecloud.com/45-e58ce51b-6ec0-49af-b3be-2632738a0e58.jpg')
INSERT [dbo].[RoomMeasurements] ([RoomMeasurementId], [MeasurementName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (10, N'1-e4d6b12c-0158-4eee-8753-8c599beb49b0.jpg', 1, N'~/UserData/\rmo\1-e4d6b12c-0158-4eee-8753-8c599beb49b0.jpg', N'/UserData/rmo/1-e4d6b12c-0158-4eee-8753-8c599beb49b0.jpg', N'~/UserData/\rmw\1-e4d6b12c-0158-4eee-8753-8c599beb49b0.jpg', N'/UserData/rmw/1-e4d6b12c-0158-4eee-8753-8c599beb49b0.jpg', N'~/UserData/\rmt\1-e4d6b12c-0158-4eee-8753-8c599beb49b0.jpg', N'/UserData/rmt/1-e4d6b12c-0158-4eee-8753-8c599beb49b0.jpg', N'rmo', N'http://c0003353.cdn2.cloudfiles.rackspacecloud.com/1-e4d6b12c-0158-4eee-8753-8c599beb49b0.jpg', N'rmw', N'http://c0003355.cdn2.cloudfiles.rackspacecloud.com/1-e4d6b12c-0158-4eee-8753-8c599beb49b0.jpg', N'rmt', N'http://c0003354.cdn2.cloudfiles.rackspacecloud.com/1-e4d6b12c-0158-4eee-8753-8c599beb49b0.jpg')
INSERT [dbo].[RoomMeasurements] ([RoomMeasurementId], [MeasurementName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (11, N'74-3a2e47d3-9bd4-4ce6-ad7f-37c3ed5be547.jpg', 74, N'~/UserData/\rmo\74-3a2e47d3-9bd4-4ce6-ad7f-37c3ed5be547.jpg', N'/UserData/rmo/74-3a2e47d3-9bd4-4ce6-ad7f-37c3ed5be547.jpg', N'~/UserData/\rmw\74-3a2e47d3-9bd4-4ce6-ad7f-37c3ed5be547.jpg', N'/UserData/rmw/74-3a2e47d3-9bd4-4ce6-ad7f-37c3ed5be547.jpg', N'~/UserData/\rmt\74-3a2e47d3-9bd4-4ce6-ad7f-37c3ed5be547.jpg', N'/UserData/rmt/74-3a2e47d3-9bd4-4ce6-ad7f-37c3ed5be547.jpg', N'rmo', N'http://c0003353.cdn2.cloudfiles.rackspacecloud.com/74-3a2e47d3-9bd4-4ce6-ad7f-37c3ed5be547.jpg', N'rmw', N'http://c0003355.cdn2.cloudfiles.rackspacecloud.com/74-3a2e47d3-9bd4-4ce6-ad7f-37c3ed5be547.jpg', N'rmt', N'http://c0003354.cdn2.cloudfiles.rackspacecloud.com/74-3a2e47d3-9bd4-4ce6-ad7f-37c3ed5be547.jpg')
INSERT [dbo].[RoomMeasurements] ([RoomMeasurementId], [MeasurementName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (12, N'74-756ee657-2958-4f2d-8e43-697318342794.jpg', 74, N'~/UserData/\rmo\74-756ee657-2958-4f2d-8e43-697318342794.jpg', N'/UserData/rmo/74-756ee657-2958-4f2d-8e43-697318342794.jpg', N'~/UserData/\rmw\74-756ee657-2958-4f2d-8e43-697318342794.jpg', N'/UserData/rmw/74-756ee657-2958-4f2d-8e43-697318342794.jpg', N'~/UserData/\rmt\74-756ee657-2958-4f2d-8e43-697318342794.jpg', N'/UserData/rmt/74-756ee657-2958-4f2d-8e43-697318342794.jpg', N'rmo', N'http://c0003353.cdn2.cloudfiles.rackspacecloud.com/74-756ee657-2958-4f2d-8e43-697318342794.jpg', N'rmw', N'http://c0003355.cdn2.cloudfiles.rackspacecloud.com/74-756ee657-2958-4f2d-8e43-697318342794.jpg', N'rmt', N'http://c0003354.cdn2.cloudfiles.rackspacecloud.com/74-756ee657-2958-4f2d-8e43-697318342794.jpg')
INSERT [dbo].[RoomMeasurements] ([RoomMeasurementId], [MeasurementName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (13, N'74-b5e26b72-2da6-4476-bcb1-a8963929f975.jpg', 74, N'~/UserData/\rmo\74-b5e26b72-2da6-4476-bcb1-a8963929f975.jpg', N'/UserData/rmo/74-b5e26b72-2da6-4476-bcb1-a8963929f975.jpg', N'~/UserData/\rmw\74-b5e26b72-2da6-4476-bcb1-a8963929f975.jpg', N'/UserData/rmw/74-b5e26b72-2da6-4476-bcb1-a8963929f975.jpg', N'~/UserData/\rmt\74-b5e26b72-2da6-4476-bcb1-a8963929f975.jpg', N'/UserData/rmt/74-b5e26b72-2da6-4476-bcb1-a8963929f975.jpg', N'rmo', N'http://c0003353.cdn2.cloudfiles.rackspacecloud.com/74-b5e26b72-2da6-4476-bcb1-a8963929f975.jpg', N'rmw', N'http://c0003355.cdn2.cloudfiles.rackspacecloud.com/74-b5e26b72-2da6-4476-bcb1-a8963929f975.jpg', N'rmt', N'http://c0003354.cdn2.cloudfiles.rackspacecloud.com/74-b5e26b72-2da6-4476-bcb1-a8963929f975.jpg')
INSERT [dbo].[RoomMeasurements] ([RoomMeasurementId], [MeasurementName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (14, N'74-c881677b-7dc8-4bc0-8c7e-6e39614601f9.jpg', 74, N'~/UserData/\rmo\74-c881677b-7dc8-4bc0-8c7e-6e39614601f9.jpg', N'/UserData/rmo/74-c881677b-7dc8-4bc0-8c7e-6e39614601f9.jpg', N'~/UserData/\rmw\74-c881677b-7dc8-4bc0-8c7e-6e39614601f9.jpg', N'/UserData/rmw/74-c881677b-7dc8-4bc0-8c7e-6e39614601f9.jpg', N'~/UserData/\rmt\74-c881677b-7dc8-4bc0-8c7e-6e39614601f9.jpg', N'/UserData/rmt/74-c881677b-7dc8-4bc0-8c7e-6e39614601f9.jpg', N'rmo', N'http://c0003353.cdn2.cloudfiles.rackspacecloud.com/74-c881677b-7dc8-4bc0-8c7e-6e39614601f9.jpg', N'rmw', N'http://c0003355.cdn2.cloudfiles.rackspacecloud.com/74-c881677b-7dc8-4bc0-8c7e-6e39614601f9.jpg', N'rmt', N'http://c0003354.cdn2.cloudfiles.rackspacecloud.com/74-c881677b-7dc8-4bc0-8c7e-6e39614601f9.jpg')
INSERT [dbo].[RoomMeasurements] ([RoomMeasurementId], [MeasurementName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (15, N'74-c6e46435-a035-4a17-b3e8-d25caec49407.jpg', 74, N'~/UserData/\rmo\74-c6e46435-a035-4a17-b3e8-d25caec49407.jpg', N'/UserData/rmo/74-c6e46435-a035-4a17-b3e8-d25caec49407.jpg', N'~/UserData/\rmw\74-c6e46435-a035-4a17-b3e8-d25caec49407.jpg', N'/UserData/rmw/74-c6e46435-a035-4a17-b3e8-d25caec49407.jpg', N'~/UserData/\rmt\74-c6e46435-a035-4a17-b3e8-d25caec49407.jpg', N'/UserData/rmt/74-c6e46435-a035-4a17-b3e8-d25caec49407.jpg', N'rmo', N'http://c0003353.cdn2.cloudfiles.rackspacecloud.com/74-c6e46435-a035-4a17-b3e8-d25caec49407.jpg', N'rmw', N'http://c0003355.cdn2.cloudfiles.rackspacecloud.com/74-c6e46435-a035-4a17-b3e8-d25caec49407.jpg', N'rmt', N'http://c0003354.cdn2.cloudfiles.rackspacecloud.com/74-c6e46435-a035-4a17-b3e8-d25caec49407.jpg')
INSERT [dbo].[RoomMeasurements] ([RoomMeasurementId], [MeasurementName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (16, N'74-a91ed40c-28c7-46c4-b57e-51900cb57117.jpg', 74, N'~/UserData/\rmo\74-a91ed40c-28c7-46c4-b57e-51900cb57117.jpg', N'/UserData/rmo/74-a91ed40c-28c7-46c4-b57e-51900cb57117.jpg', N'~/UserData/\rmw\74-a91ed40c-28c7-46c4-b57e-51900cb57117.jpg', N'/UserData/rmw/74-a91ed40c-28c7-46c4-b57e-51900cb57117.jpg', N'~/UserData/\rmt\74-a91ed40c-28c7-46c4-b57e-51900cb57117.jpg', N'/UserData/rmt/74-a91ed40c-28c7-46c4-b57e-51900cb57117.jpg', N'rmo', N'http://c0003353.cdn2.cloudfiles.rackspacecloud.com/74-a91ed40c-28c7-46c4-b57e-51900cb57117.jpg', N'rmw', N'http://c0003355.cdn2.cloudfiles.rackspacecloud.com/74-a91ed40c-28c7-46c4-b57e-51900cb57117.jpg', N'rmt', N'http://c0003354.cdn2.cloudfiles.rackspacecloud.com/74-a91ed40c-28c7-46c4-b57e-51900cb57117.jpg')
INSERT [dbo].[RoomMeasurements] ([RoomMeasurementId], [MeasurementName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (17, N'74-589bdbd2-548e-4906-b754-85e32c563f3f.jpg', 74, N'~/UserData/\rmo\74-589bdbd2-548e-4906-b754-85e32c563f3f.jpg', N'/UserData/rmo/74-589bdbd2-548e-4906-b754-85e32c563f3f.jpg', N'~/UserData/\rmw\74-589bdbd2-548e-4906-b754-85e32c563f3f.jpg', N'/UserData/rmw/74-589bdbd2-548e-4906-b754-85e32c563f3f.jpg', N'~/UserData/\rmt\74-589bdbd2-548e-4906-b754-85e32c563f3f.jpg', N'/UserData/rmt/74-589bdbd2-548e-4906-b754-85e32c563f3f.jpg', N'rmo', N'http://c0003353.cdn2.cloudfiles.rackspacecloud.com/74-589bdbd2-548e-4906-b754-85e32c563f3f.jpg', N'rmw', N'http://c0003355.cdn2.cloudfiles.rackspacecloud.com/74-589bdbd2-548e-4906-b754-85e32c563f3f.jpg', N'rmt', N'http://c0003354.cdn2.cloudfiles.rackspacecloud.com/74-589bdbd2-548e-4906-b754-85e32c563f3f.jpg')
INSERT [dbo].[RoomMeasurements] ([RoomMeasurementId], [MeasurementName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (18, N'74-c827939c-a4a1-4b3a-a6ac-3bf5b0ba2e02.jpg', 74, N'~/UserData/\rmo\74-c827939c-a4a1-4b3a-a6ac-3bf5b0ba2e02.jpg', N'/UserData/rmo/74-c827939c-a4a1-4b3a-a6ac-3bf5b0ba2e02.jpg', N'~/UserData/\rmw\74-c827939c-a4a1-4b3a-a6ac-3bf5b0ba2e02.jpg', N'/UserData/rmw/74-c827939c-a4a1-4b3a-a6ac-3bf5b0ba2e02.jpg', N'~/UserData/\rmt\74-c827939c-a4a1-4b3a-a6ac-3bf5b0ba2e02.jpg', N'/UserData/rmt/74-c827939c-a4a1-4b3a-a6ac-3bf5b0ba2e02.jpg', N'rmo', N'http://c0003353.cdn2.cloudfiles.rackspacecloud.com/74-c827939c-a4a1-4b3a-a6ac-3bf5b0ba2e02.jpg', N'rmw', N'http://c0003355.cdn2.cloudfiles.rackspacecloud.com/74-c827939c-a4a1-4b3a-a6ac-3bf5b0ba2e02.jpg', N'rmt', N'http://c0003354.cdn2.cloudfiles.rackspacecloud.com/74-c827939c-a4a1-4b3a-a6ac-3bf5b0ba2e02.jpg')
INSERT [dbo].[RoomMeasurements] ([RoomMeasurementId], [MeasurementName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (19, N'74-a7afeda6-eb90-4c5c-a45e-1fc73ddf7a61.jpg', 74, N'~/UserData/\rmo\74-a7afeda6-eb90-4c5c-a45e-1fc73ddf7a61.jpg', N'/UserData/rmo/74-a7afeda6-eb90-4c5c-a45e-1fc73ddf7a61.jpg', N'~/UserData/\rmw\74-a7afeda6-eb90-4c5c-a45e-1fc73ddf7a61.jpg', N'/UserData/rmw/74-a7afeda6-eb90-4c5c-a45e-1fc73ddf7a61.jpg', N'~/UserData/\rmt\74-a7afeda6-eb90-4c5c-a45e-1fc73ddf7a61.jpg', N'/UserData/rmt/74-a7afeda6-eb90-4c5c-a45e-1fc73ddf7a61.jpg', N'rmo', N'http://c0003353.cdn2.cloudfiles.rackspacecloud.com/74-a7afeda6-eb90-4c5c-a45e-1fc73ddf7a61.jpg', N'rmw', N'http://c0003355.cdn2.cloudfiles.rackspacecloud.com/74-a7afeda6-eb90-4c5c-a45e-1fc73ddf7a61.jpg', N'rmt', N'http://c0003354.cdn2.cloudfiles.rackspacecloud.com/74-a7afeda6-eb90-4c5c-a45e-1fc73ddf7a61.jpg')
INSERT [dbo].[RoomMeasurements] ([RoomMeasurementId], [MeasurementName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (20, N'73-b0787b6a-d324-4d9e-9d33-c38273f93161.jpg', 73, N'~/UserData/\rmo\73-b0787b6a-d324-4d9e-9d33-c38273f93161.jpg', N'/UserData/rmo/73-b0787b6a-d324-4d9e-9d33-c38273f93161.jpg', N'~/UserData/\rmw\73-b0787b6a-d324-4d9e-9d33-c38273f93161.jpg', N'/UserData/rmw/73-b0787b6a-d324-4d9e-9d33-c38273f93161.jpg', N'~/UserData/\rmt\73-b0787b6a-d324-4d9e-9d33-c38273f93161.jpg', N'/UserData/rmt/73-b0787b6a-d324-4d9e-9d33-c38273f93161.jpg', N'rmo', N'http://c0003353.cdn2.cloudfiles.rackspacecloud.com/73-b0787b6a-d324-4d9e-9d33-c38273f93161.jpg', N'rmw', N'http://c0003355.cdn2.cloudfiles.rackspacecloud.com/73-b0787b6a-d324-4d9e-9d33-c38273f93161.jpg', N'rmt', N'http://c0003354.cdn2.cloudfiles.rackspacecloud.com/73-b0787b6a-d324-4d9e-9d33-c38273f93161.jpg')
INSERT [dbo].[RoomMeasurements] ([RoomMeasurementId], [MeasurementName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (22, N'104-ea8104d5-0eb3-40f1-aa2e-72244ad86177.jpg', 104, N'~/UserData/\rmo\104-ea8104d5-0eb3-40f1-aa2e-72244ad86177.jpg', N'/UserData/rmo/104-ea8104d5-0eb3-40f1-aa2e-72244ad86177.jpg', N'~/UserData/\rmw\104-ea8104d5-0eb3-40f1-aa2e-72244ad86177.jpg', N'/UserData/rmw/104-ea8104d5-0eb3-40f1-aa2e-72244ad86177.jpg', N'~/UserData/\rmt\104-ea8104d5-0eb3-40f1-aa2e-72244ad86177.jpg', N'/UserData/rmt/104-ea8104d5-0eb3-40f1-aa2e-72244ad86177.jpg', N'rmo', N'http://c0003353.cdn2.cloudfiles.rackspacecloud.com/104-ea8104d5-0eb3-40f1-aa2e-72244ad86177.jpg', N'rmw', N'http://c0003355.cdn2.cloudfiles.rackspacecloud.com/104-ea8104d5-0eb3-40f1-aa2e-72244ad86177.jpg', N'rmt', N'http://c0003354.cdn2.cloudfiles.rackspacecloud.com/104-ea8104d5-0eb3-40f1-aa2e-72244ad86177.jpg')
INSERT [dbo].[RoomMeasurements] ([RoomMeasurementId], [MeasurementName], [RoomId], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (23, N'105-e7685f1a-79d5-42bf-8cce-cb4932583219.jpg', 105, N'~/UserData/\rmo\105-e7685f1a-79d5-42bf-8cce-cb4932583219.jpg', N'/UserData/rmo/105-e7685f1a-79d5-42bf-8cce-cb4932583219.jpg', N'~/UserData/\rmw\105-e7685f1a-79d5-42bf-8cce-cb4932583219.jpg', N'/UserData/rmw/105-e7685f1a-79d5-42bf-8cce-cb4932583219.jpg', N'~/UserData/\rmt\105-e7685f1a-79d5-42bf-8cce-cb4932583219.jpg', N'/UserData/rmt/105-e7685f1a-79d5-42bf-8cce-cb4932583219.jpg', N'rmo', N'http://c0003353.cdn2.cloudfiles.rackspacecloud.com/105-e7685f1a-79d5-42bf-8cce-cb4932583219.jpg', N'rmw', N'http://c0003355.cdn2.cloudfiles.rackspacecloud.com/105-e7685f1a-79d5-42bf-8cce-cb4932583219.jpg', N'rmt', N'http://c0003354.cdn2.cloudfiles.rackspacecloud.com/105-e7685f1a-79d5-42bf-8cce-cb4932583219.jpg')
SET IDENTITY_INSERT [dbo].[RoomMeasurements] OFF
/****** Object:  Table [dbo].[Furniture]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Furniture]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Furniture](
	[FurnitureId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Comments] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RoomId] [int] NOT NULL,
	[Width] [numeric](18, 0) NOT NULL,
	[Height] [numeric](18, 0) NOT NULL,
	[Depth] [numeric](18, 0) NOT NULL,
	[Image] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Furniture] PRIMARY KEY CLUSTERED 
(
	[FurnitureId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Furniture', N'COLUMN',N'Image'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stores the path to a single thumbnail image of the piece of funiture' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Furniture', @level2type=N'COLUMN',@level2name=N'Image'
GO
SET IDENTITY_INSERT [dbo].[Furniture] ON
INSERT [dbo].[Furniture] ([FurnitureId], [Name], [Comments], [RoomId], [Width], [Height], [Depth], [Image]) VALUES (4, N'arm char', N'gdgdgdgd', 1, CAST(11 AS Numeric(18, 0)), CAST(11 AS Numeric(18, 0)), CAST(12 AS Numeric(18, 0)), NULL)
INSERT [dbo].[Furniture] ([FurnitureId], [Name], [Comments], [RoomId], [Width], [Height], [Depth], [Image]) VALUES (20, N'new piece', N'fsfsfs', 44, CAST(10 AS Numeric(18, 0)), CAST(12 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'/UserData/rfpt/44-20-316ef106-39bc-449b-8f6b-ba4767e8861e.jpg')
INSERT [dbo].[Furniture] ([FurnitureId], [Name], [Comments], [RoomId], [Width], [Height], [Depth], [Image]) VALUES (22, N'my chair', N'this is my cool', 74, CAST(25 AS Numeric(18, 0)), CAST(60 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'/UserData/rfpt/74-22-dc139338-6951-4c8a-8298-0f76aa398f99.jpg')
INSERT [dbo].[Furniture] ([FurnitureId], [Name], [Comments], [RoomId], [Width], [Height], [Depth], [Image]) VALUES (23, N'taras chair', N'my chair', 74, CAST(15 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'/UserData/rfpt/74-23-646484fa-4ab0-4432-a66a-c8f8ee20ae2e.jpg')
INSERT [dbo].[Furniture] ([FurnitureId], [Name], [Comments], [RoomId], [Width], [Height], [Depth], [Image]) VALUES (29, N'coolio', N'fsfsfsfs', 74, CAST(15 AS Numeric(18, 0)), CAST(11 AS Numeric(18, 0)), CAST(12 AS Numeric(18, 0)), NULL)
INSERT [dbo].[Furniture] ([FurnitureId], [Name], [Comments], [RoomId], [Width], [Height], [Depth], [Image]) VALUES (31, N'tests', NULL, 74, CAST(0 AS Numeric(18, 0)), CAST(0 AS Numeric(18, 0)), CAST(0 AS Numeric(18, 0)), NULL)
SET IDENTITY_INSERT [dbo].[Furniture] OFF
/****** Object:  Table [dbo].[EntryQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EntryQuestionnaires]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EntryQuestionnaires](
	[EntryQuestionnaireId] [int] IDENTITY(1,1) NOT NULL,
	[RoomId] [int] NOT NULL,
	[DesignStyleOfRoom] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DesignStyleOfRoomOther] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SpecificColorsYouWantToUse] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SpecificColorsYouWantToUseYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FurnitureAccessoriesStayingInRoom] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[YourBudget] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MustHavesItemsInRoomNotAlreadyOwned] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MustHavesItemsInRoomNotAlreadyOwnedYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[HowDoYouUseThisSpace] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYourStoreAnythingElseInRoom] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouWantYourRoomToFeel] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouLikeContrast] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouLikeTextures] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouWantYourRoomToFeelGender] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AdditionalImportantInformation] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_EntryQuestionnaires] PRIMARY KEY CLUSTERED 
(
	[EntryQuestionnaireId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[EntryQuestionnaires] ON
INSERT [dbo].[EntryQuestionnaires] ([EntryQuestionnaireId], [RoomId], [DesignStyleOfRoom], [DesignStyleOfRoomOther], [SpecificColorsYouWantToUse], [SpecificColorsYouWantToUseYes], [FurnitureAccessoriesStayingInRoom], [YourBudget], [MustHavesItemsInRoomNotAlreadyOwned], [MustHavesItemsInRoomNotAlreadyOwnedYes], [HowDoYouUseThisSpace], [DoYourStoreAnythingElseInRoom], [HowDoYouWantYourRoomToFeel], [DoYouLikeContrast], [DoYouLikeTextures], [HowDoYouWantYourRoomToFeelGender], [AdditionalImportantInformation]) VALUES (2, 104, N'Other', N'other styles', N'No', NULL, N'No', N'$500', N'No', NULL, N'sfsf', N'sffs', N'fsfs', N'No', N'No', N'Feminine', NULL)
SET IDENTITY_INSERT [dbo].[EntryQuestionnaires] OFF
/****** Object:  Table [dbo].[DiningRoomQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DiningRoomQuestionnaires]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DiningRoomQuestionnaires](
	[DiningRoomQuestionnaireId] [int] IDENTITY(1,1) NOT NULL,
	[RoomId] [int] NOT NULL,
	[DesignStyleOfRoom] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DesignStyleOfRoomOther] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SpecificColorsYouWantToUse] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SpecificColorsYouWantToUseYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FurnitureAccessoriesStayingInRoom] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[YourBudget] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MustHavesItemsInRoomNotAlreadyOwned] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MustHavesItemsInRoomNotAlreadyOwnedYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StoreAnythingElseInRoom] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouUseThisSpace] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouEntertain] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouEntertainYesHowManyPeople] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[HowDoYouWantYourRoomToFeel] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouLikeContrast] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouLikeTextures] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouWantYourRoomToFeelGender] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AdditionalImportantInformation] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_DiningRoomQuestionnaires] PRIMARY KEY CLUSTERED 
(
	[DiningRoomQuestionnaireId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[DesignFiles]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DesignFiles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DesignFiles](
	[DesignFileId] [int] IDENTITY(1,1) NOT NULL,
	[DesignFileTypeId] [int] NOT NULL,
	[ServerPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[UrlPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[RoomId] [int] NOT NULL,
	[Comments] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RsContainer] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[RsUrl] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FileName] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_DesignFiles] PRIMARY KEY CLUSTERED 
(
	[DesignFileId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[DesignFiles] ON
INSERT [dbo].[DesignFiles] ([DesignFileId], [DesignFileTypeId], [ServerPath], [UrlPath], [RoomId], [Comments], [RsContainer], [RsUrl], [FileName]) VALUES (2, 1, N'~/UserData/\rdf\1-7c3cc87f-c3d5-4813-ad38-59b1ed9f015f.jpg', N'/UserData/rdf/1-7c3cc87f-c3d5-4813-ad38-59b1ed9f015f.jpg', 1, N'new comment  - inspiration board 1


I like it a lot!!!!', N'rdf', N'http://c0004479.cdn2.cloudfiles.rackspacecloud.com/1-7c3cc87f-c3d5-4813-ad38-59b1ed9f015f.jpg', N'1-7c3cc87f-c3d5-4813-ad38-59b1ed9f015f.jpg')
INSERT [dbo].[DesignFiles] ([DesignFileId], [DesignFileTypeId], [ServerPath], [UrlPath], [RoomId], [Comments], [RsContainer], [RsUrl], [FileName]) VALUES (3, 5, N'~/UserData/\rdf\1-b72e91fb-f8c4-4b70-8ab6-30b2becc121b.jpg', N'/UserData/rdf/1-b72e91fb-f8c4-4b70-8ab6-30b2becc121b.jpg', 1, NULL, N'rdf', N'http://c0004479.cdn2.cloudfiles.rackspacecloud.com/1-b72e91fb-f8c4-4b70-8ab6-30b2becc121b.jpg', N'1-b72e91fb-f8c4-4b70-8ab6-30b2becc121b.jpg')
INSERT [dbo].[DesignFiles] ([DesignFileId], [DesignFileTypeId], [ServerPath], [UrlPath], [RoomId], [Comments], [RsContainer], [RsUrl], [FileName]) VALUES (8, 2, N'~/UserData/\rdf\1-eba75399-aff4-4698-8c03-90584f34cb26.jpg', N'/UserData/rdf/1-eba75399-aff4-4698-8c03-90584f34cb26.jpg', 1, N'inpir 2   yaya!!!', N'rdf', N'http://c0004479.cdn2.cloudfiles.rackspacecloud.com/1-eba75399-aff4-4698-8c03-90584f34cb26.jpg', N'1-eba75399-aff4-4698-8c03-90584f34cb26.jpg')
INSERT [dbo].[DesignFiles] ([DesignFileId], [DesignFileTypeId], [ServerPath], [UrlPath], [RoomId], [Comments], [RsContainer], [RsUrl], [FileName]) VALUES (9, 3, N'~/UserData/\rdf\1-2bb1eb7b-180a-4fce-877b-9ef731165bbd.jpg', N'/UserData/rdf/1-2bb1eb7b-180a-4fce-877b-9ef731165bbd.jpg', 1, N'floor




me like!!!!!!', N'rdf', N'http://c0004479.cdn2.cloudfiles.rackspacecloud.com/1-2bb1eb7b-180a-4fce-877b-9ef731165bbd.jpg', N'1-2bb1eb7b-180a-4fce-877b-9ef731165bbd.jpg')
INSERT [dbo].[DesignFiles] ([DesignFileId], [DesignFileTypeId], [ServerPath], [UrlPath], [RoomId], [Comments], [RsContainer], [RsUrl], [FileName]) VALUES (10, 1, N'~/UserData/\rdf\5-bfbf0551-03d9-4f51-a20b-1b6993cc67d5.jpg', N'/UserData/rdf/5-bfbf0551-03d9-4f51-a20b-1b6993cc67d5.jpg', 5, N'I liket his about board number 1', N'rdf', N'http://c0004479.cdn2.cloudfiles.rackspacecloud.com/5-bfbf0551-03d9-4f51-a20b-1b6993cc67d5.jpg', N'5-bfbf0551-03d9-4f51-a20b-1b6993cc67d5.jpg')
INSERT [dbo].[DesignFiles] ([DesignFileId], [DesignFileTypeId], [ServerPath], [UrlPath], [RoomId], [Comments], [RsContainer], [RsUrl], [FileName]) VALUES (11, 2, N'~/UserData/\rdf\5-9ffb23e8-8d8c-4b5b-aefb-3a82372ab7df.jpg', N'/UserData/rdf/5-9ffb23e8-8d8c-4b5b-aefb-3a82372ab7df.jpg', 5, N'I like board # 2', N'rdf', N'http://c0004479.cdn2.cloudfiles.rackspacecloud.com/5-9ffb23e8-8d8c-4b5b-aefb-3a82372ab7df.jpg', N'5-9ffb23e8-8d8c-4b5b-aefb-3a82372ab7df.jpg')
INSERT [dbo].[DesignFiles] ([DesignFileId], [DesignFileTypeId], [ServerPath], [UrlPath], [RoomId], [Comments], [RsContainer], [RsUrl], [FileName]) VALUES (12, 1, N'~/UserData/\rdf\45-0b3e9a31-46a2-421b-856b-9db5745ef8e4.pdf', N'/UserData/rdf/45-0b3e9a31-46a2-421b-856b-9db5745ef8e4.pdf', 45, NULL, N'rdf', N'http://c0004479.cdn2.cloudfiles.rackspacecloud.com/45-0b3e9a31-46a2-421b-856b-9db5745ef8e4.pdf', N'45-0b3e9a31-46a2-421b-856b-9db5745ef8e4.pdf')
INSERT [dbo].[DesignFiles] ([DesignFileId], [DesignFileTypeId], [ServerPath], [UrlPath], [RoomId], [Comments], [RsContainer], [RsUrl], [FileName]) VALUES (13, 2, N'~/UserData/\rdf\45-d5c154b2-c3a4-4069-aff6-dbc542961bc3.pdf', N'/UserData/rdf/45-d5c154b2-c3a4-4069-aff6-dbc542961bc3.pdf', 45, N'fsfsfs', N'rdf', N'http://c0004479.cdn2.cloudfiles.rackspacecloud.com/45-d5c154b2-c3a4-4069-aff6-dbc542961bc3.pdf', N'45-d5c154b2-c3a4-4069-aff6-dbc542961bc3.pdf')
INSERT [dbo].[DesignFiles] ([DesignFileId], [DesignFileTypeId], [ServerPath], [UrlPath], [RoomId], [Comments], [RsContainer], [RsUrl], [FileName]) VALUES (14, 7, N'~/UserData/\rdf\45-11375a8e-95c4-40ea-b238-bb023f1af606.jpg', N'/UserData/rdf/45-11375a8e-95c4-40ea-b238-bb023f1af606.jpg', 45, NULL, N'rdf', N'http://c0004479.cdn2.cloudfiles.rackspacecloud.com/45-11375a8e-95c4-40ea-b238-bb023f1af606.jpg', N'45-11375a8e-95c4-40ea-b238-bb023f1af606.jpg')
INSERT [dbo].[DesignFiles] ([DesignFileId], [DesignFileTypeId], [ServerPath], [UrlPath], [RoomId], [Comments], [RsContainer], [RsUrl], [FileName]) VALUES (15, 3, N'~/UserData/\rdf\45-737edc65-aa42-4e87-859e-9423de64fb51.jpg', N'/UserData/rdf/45-737edc65-aa42-4e87-859e-9423de64fb51.jpg', 45, N'dgdgdgd', N'rdf', N'http://c0004479.cdn2.cloudfiles.rackspacecloud.com/45-737edc65-aa42-4e87-859e-9423de64fb51.jpg', N'45-737edc65-aa42-4e87-859e-9423de64fb51.jpg')
INSERT [dbo].[DesignFiles] ([DesignFileId], [DesignFileTypeId], [ServerPath], [UrlPath], [RoomId], [Comments], [RsContainer], [RsUrl], [FileName]) VALUES (16, 6, N'~/UserData/\rdf\74-030195ed-d34e-439e-ab26-001185c2b4a1.jpg', N'/UserData/rdf/74-030195ed-d34e-439e-ab26-001185c2b4a1.jpg', 74, NULL, N'rdf', N'http://c0004479.cdn2.cloudfiles.rackspacecloud.com/74-030195ed-d34e-439e-ab26-001185c2b4a1.jpg', N'74-030195ed-d34e-439e-ab26-001185c2b4a1.jpg')
INSERT [dbo].[DesignFiles] ([DesignFileId], [DesignFileTypeId], [ServerPath], [UrlPath], [RoomId], [Comments], [RsContainer], [RsUrl], [FileName]) VALUES (17, 7, N'~/UserData/\rdf\74-6cc93026-e2e3-4c0f-99ef-f14f4c4035bc.jpg', N'/UserData/rdf/74-6cc93026-e2e3-4c0f-99ef-f14f4c4035bc.jpg', 74, NULL, N'rdf', N'http://c0004479.cdn2.cloudfiles.rackspacecloud.com/74-6cc93026-e2e3-4c0f-99ef-f14f4c4035bc.jpg', N'74-6cc93026-e2e3-4c0f-99ef-f14f4c4035bc.jpg')
INSERT [dbo].[DesignFiles] ([DesignFileId], [DesignFileTypeId], [ServerPath], [UrlPath], [RoomId], [Comments], [RsContainer], [RsUrl], [FileName]) VALUES (23, 8, N'~/UserData/\rdf\13-24baea65-6674-44bf-8077-85baa1a26b54.jpg', N'/UserData/rdf/13-24baea65-6674-44bf-8077-85baa1a26b54.jpg', 13, NULL, N'rdf', N'http://c0004479.cdn2.cloudfiles.rackspacecloud.com/13-24baea65-6674-44bf-8077-85baa1a26b54.jpg', N'13-24baea65-6674-44bf-8077-85baa1a26b54.jpg')
INSERT [dbo].[DesignFiles] ([DesignFileId], [DesignFileTypeId], [ServerPath], [UrlPath], [RoomId], [Comments], [RsContainer], [RsUrl], [FileName]) VALUES (24, 6, N'~/UserData/\rdf\13-526f1831-57ec-4cd2-b7fe-050c45d68415.pdf', N'/UserData/rdf/13-526f1831-57ec-4cd2-b7fe-050c45d68415.pdf', 13, NULL, N'rdf', N'http://c0004479.cdn2.cloudfiles.rackspacecloud.com/13-526f1831-57ec-4cd2-b7fe-050c45d68415.pdf', N'13-526f1831-57ec-4cd2-b7fe-050c45d68415.pdf')
INSERT [dbo].[DesignFiles] ([DesignFileId], [DesignFileTypeId], [ServerPath], [UrlPath], [RoomId], [Comments], [RsContainer], [RsUrl], [FileName]) VALUES (26, 6, N'~/UserData/\rdf\73-d5117e41-58da-480e-8b3e-da42d3a20ab8.pdf', N'/UserData/rdf/73-d5117e41-58da-480e-8b3e-da42d3a20ab8.pdf', 73, NULL, N'rdf', N'http://c0004479.cdn2.cloudfiles.rackspacecloud.com/73-d5117e41-58da-480e-8b3e-da42d3a20ab8.pdf', N'73-d5117e41-58da-480e-8b3e-da42d3a20ab8.pdf')
INSERT [dbo].[DesignFiles] ([DesignFileId], [DesignFileTypeId], [ServerPath], [UrlPath], [RoomId], [Comments], [RsContainer], [RsUrl], [FileName]) VALUES (27, 5, N'~/UserData/\rdf\104-e34f1546-7ca6-4377-9bf5-9baacce88157.jpg', N'/UserData/rdf/104-e34f1546-7ca6-4377-9bf5-9baacce88157.jpg', 104, NULL, N'rdf', N'http://c0004479.cdn2.cloudfiles.rackspacecloud.com/104-e34f1546-7ca6-4377-9bf5-9baacce88157.jpg', N'104-e34f1546-7ca6-4377-9bf5-9baacce88157.jpg')
INSERT [dbo].[DesignFiles] ([DesignFileId], [DesignFileTypeId], [ServerPath], [UrlPath], [RoomId], [Comments], [RsContainer], [RsUrl], [FileName]) VALUES (28, 5, N'~/UserData/\rdf\105-4de4e5ec-95a5-4bbd-a437-0eb2e1249d1d.jpg', N'/UserData/rdf/105-4de4e5ec-95a5-4bbd-a437-0eb2e1249d1d.jpg', 105, NULL, N'rdf', N'http://c0004479.cdn2.cloudfiles.rackspacecloud.com/105-4de4e5ec-95a5-4bbd-a437-0eb2e1249d1d.jpg', N'105-4de4e5ec-95a5-4bbd-a437-0eb2e1249d1d.jpg')
SET IDENTITY_INSERT [dbo].[DesignFiles] OFF
/****** Object:  Table [dbo].[Consultations]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Consultations]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Consultations](
	[ConsultationId] [int] IDENTITY(1,1) NOT NULL,
	[RoomId] [int] NOT NULL,
	[Date] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Time] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[TimeZone] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ContactMethod] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SkypeId] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PhoneNumber] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Type] [nvarchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Complete] [bit] NOT NULL,
 CONSTRAINT [PK_Consultations] PRIMARY KEY CLUSTERED 
(
	[ConsultationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[Consultations] ON
INSERT [dbo].[Consultations] ([ConsultationId], [RoomId], [Date], [Time], [TimeZone], [ContactMethod], [SkypeId], [PhoneNumber], [Type], [Complete]) VALUES (6, 104, N'06-18-2011', N'11:30 AM', N'PST', N'skype', N'philskype', N'604-736-9661', N'initial', 0)
INSERT [dbo].[Consultations] ([ConsultationId], [RoomId], [Date], [Time], [TimeZone], [ContactMethod], [SkypeId], [PhoneNumber], [Type], [Complete]) VALUES (7, 104, N'06-21-2011', N'1:30 PM', N'PST', N'phone', N'myskype', N'604-736-9660', N'followUp', 0)
INSERT [dbo].[Consultations] ([ConsultationId], [RoomId], [Date], [Time], [TimeZone], [ContactMethod], [SkypeId], [PhoneNumber], [Type], [Complete]) VALUES (10, 103, N'07-21-2011', N'10:00 AM', N'PST', N'phone', N'myskype123', N'604-736-9662', N'initial', 0)
INSERT [dbo].[Consultations] ([ConsultationId], [RoomId], [Date], [Time], [TimeZone], [ContactMethod], [SkypeId], [PhoneNumber], [Type], [Complete]) VALUES (11, 105, N'07-06-2011', N'2:30 PM', N'PST', N'phone', N'myskyper', N'604-736-9660', N'initial', 0)
INSERT [dbo].[Consultations] ([ConsultationId], [RoomId], [Date], [Time], [TimeZone], [ContactMethod], [SkypeId], [PhoneNumber], [Type], [Complete]) VALUES (12, 105, N'06-24-2011', N'10:30 AM', N'PST', N'phone', N'myskype', N'604-736-9660', N'followUp', 0)
INSERT [dbo].[Consultations] ([ConsultationId], [RoomId], [Date], [Time], [TimeZone], [ContactMethod], [SkypeId], [PhoneNumber], [Type], [Complete]) VALUES (13, 106, N'07-26-2011', N'9:00 AM', N'PST', N'phone', N'myskype', N'604-736-9660', N'initial', 0)
SET IDENTITY_INSERT [dbo].[Consultations] OFF
/****** Object:  Table [dbo].[BathroomQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BathroomQuestionnaires]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[BathroomQuestionnaires](
	[BathroomQuestionnaireId] [int] IDENTITY(1,1) NOT NULL,
	[RoomId] [int] NOT NULL,
	[DesignStyleOfRoom] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DesignStyleOfRoomOther] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SpecificColorsYouWantToUse] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SpecificColorsYouWantToUseYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FurnitureAccessoriesStayingInRoom] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[YourBudget] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MustHavesItemsInRoomNotAlreadyOwned] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MustHavesItemsInRoomNotAlreadyOwnedYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DoYouEnjoyBaths] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouWantTwoSinks] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowManyPeopleUseWashroom] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouStoreLinensOrOtherItems] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouWantYourRoomToFeel] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouLikeContrast] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouLikeTextures] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouWantYourRoomToFeelGender] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AdditionalImportantInformation] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_BathroomQuestionnaire] PRIMARY KEY CLUSTERED 
(
	[BathroomQuestionnaireId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_CreateRole]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_CreateRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Roles_CreateRole]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
        SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF (EXISTS(SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId))
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    INSERT INTO dbo.aspnet_Roles
                (ApplicationId, RoleName, LoweredRoleName)
         VALUES (@ApplicationId, @RoleName, LOWER(@RoleName))

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        COMMIT TRANSACTION
    END

    RETURN(0)

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
/****** Object:  Table [dbo].[MediaRoomQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MediaRoomQuestionnaires]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MediaRoomQuestionnaires](
	[MediaRoomQuestionnaireId] [int] IDENTITY(1,1) NOT NULL,
	[RoomId] [int] NOT NULL,
	[DesignStyleOfRoom] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DesignStyleOfRoomOther] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SpecificColorsYouWantToUse] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SpecificColorsYouWantToUseYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FurnitureAccessoriesStayingInRoom] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[YourBudget] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MustHavesItemsInRoomNotAlreadyOwned] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MustHavesItemsInRoomNotAlreadyOwnedYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StoreAnythingElseInRoom] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouWantYourRoomToFeel] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouLikeContrast] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouLikeTextures] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouWantYourRoomToFeelGender] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AdditionalImportantInformation] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_MediaRoomQuestionnaires] PRIMARY KEY CLUSTERED 
(
	[MediaRoomQuestionnaireId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[MasterBedroomQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MasterBedroomQuestionnaires]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MasterBedroomQuestionnaires](
	[MasterBedroomQuestionnaireId] [int] IDENTITY(1,1) NOT NULL,
	[RoomId] [int] NOT NULL,
	[DesignStyleOfRoom] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DesignStyleOfRoomOther] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SpecificColorsYouWantToUse] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SpecificColorsYouWantToUseYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FurnitureAccessoriesStayingInRoom] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[YourBudget] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MustHavesItemsInRoomNotAlreadyOwned] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MustHavesItemsInRoomNotAlreadyOwnedYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BedSize] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ClothingStorage] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OtherStorage] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouWantYourRoomToFeel] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouLikeContrast] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouLikeTextures] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouWantYourRoomToFeelGender] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AdditionalImportantInformation] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SeatingAreaOrAreaForRelaxing] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_MasterBedroomQuestionnaires] PRIMARY KEY CLUSTERED 
(
	[MasterBedroomQuestionnaireId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[MasterBedroomQuestionnaires] ON
INSERT [dbo].[MasterBedroomQuestionnaires] ([MasterBedroomQuestionnaireId], [RoomId], [DesignStyleOfRoom], [DesignStyleOfRoomOther], [SpecificColorsYouWantToUse], [SpecificColorsYouWantToUseYes], [FurnitureAccessoriesStayingInRoom], [YourBudget], [MustHavesItemsInRoomNotAlreadyOwned], [MustHavesItemsInRoomNotAlreadyOwnedYes], [BedSize], [ClothingStorage], [OtherStorage], [HowDoYouWantYourRoomToFeel], [DoYouLikeContrast], [DoYouLikeTextures], [HowDoYouWantYourRoomToFeelGender], [AdditionalImportantInformation], [SeatingAreaOrAreaForRelaxing]) VALUES (1, 94, N'Eclectic', NULL, N'No', N'dgdg', N'No', N'3434', N'No', N'dggd', N'Two Twins', N'No', N'dggdgd', N'dgdgd', N'Yes', N'No', N'Feminine', NULL, N'No')
SET IDENTITY_INSERT [dbo].[MasterBedroomQuestionnaires] OFF
/****** Object:  Table [dbo].[LivingRoomQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LivingRoomQuestionnaires]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LivingRoomQuestionnaires](
	[LivingRoomQuestionnaireId] [int] IDENTITY(1,1) NOT NULL,
	[RoomId] [int] NOT NULL,
	[DesignStyleOfRoom] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DesignStyleOfRoomOther] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SpecificColorsYouWantToUse] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SpecificColorsYouWantToUseYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FurnitureAccessoriesStayingInRoom] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[YourBudget] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MustHavesItemsInRoomNotAlreadyOwned] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MustHavesItemsInRoomNotAlreadyOwnedYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StoreAnythingElseInRoom] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouUseThisSpace] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouEntertain] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouEntertainYesHowManyPeople] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[HowDoYouWantYourRoomToFeel] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouLikeContrast] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouLikeTextures] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouWantYourRoomToFeelGender] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AdditionalImportantInformation] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AnythingSpecificYouWantToDisplay] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_LivingRoomQuestionnaires] PRIMARY KEY CLUSTERED 
(
	[LivingRoomQuestionnaireId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[LivingRoomQuestionnaires] ON
INSERT [dbo].[LivingRoomQuestionnaires] ([LivingRoomQuestionnaireId], [RoomId], [DesignStyleOfRoom], [DesignStyleOfRoomOther], [SpecificColorsYouWantToUse], [SpecificColorsYouWantToUseYes], [FurnitureAccessoriesStayingInRoom], [YourBudget], [MustHavesItemsInRoomNotAlreadyOwned], [MustHavesItemsInRoomNotAlreadyOwnedYes], [StoreAnythingElseInRoom], [HowDoYouUseThisSpace], [DoYouEntertain], [DoYouEntertainYesHowManyPeople], [HowDoYouWantYourRoomToFeel], [DoYouLikeContrast], [DoYouLikeTextures], [HowDoYouWantYourRoomToFeelGender], [AdditionalImportantInformation], [AnythingSpecificYouWantToDisplay]) VALUES (3, 106, N'Eclectic', NULL, N'No', NULL, N'No', N'34234', N'No', NULL, N'sf', N'fe', N'No', N'3', N'sf', N'No', N'No', N'Feminine', N'f', N'f')
SET IDENTITY_INSERT [dbo].[LivingRoomQuestionnaires] OFF
/****** Object:  Table [dbo].[KitchenQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KitchenQuestionnaires]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KitchenQuestionnaires](
	[KitchenQuestionnaireId] [int] IDENTITY(1,1) NOT NULL,
	[RoomId] [int] NOT NULL,
	[DesignStyleOfRoom] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DesignStyleOfRoomOther] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SpecificColorsYouWantToUse] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SpecificColorsYouWantToUseYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[HowDoYouWantRoomToFeel] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FurnitureAccessoriesStayingInRoom] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[YourBudget] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MustHavesItemsInRoomNotAlreadyOwned] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MustHavesItemsInRoomNotAlreadyOwnedYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NumberOfCooks] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouCookAlot] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[TypeOfOven] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OtherActivitiesInKitchen] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OtherActivitiesInKitchenYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MainEatingRoom] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[EntertainFrequently] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouWantYourRoomToFeel] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouLikeContrast] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouLikeTextures] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouWantYourRoomToFeelGender] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AdditionalImportantInformation] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_KitchenQuestionnaires] PRIMARY KEY CLUSTERED 
(
	[KitchenQuestionnaireId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[KitchenQuestionnaires] ON
INSERT [dbo].[KitchenQuestionnaires] ([KitchenQuestionnaireId], [RoomId], [DesignStyleOfRoom], [DesignStyleOfRoomOther], [SpecificColorsYouWantToUse], [SpecificColorsYouWantToUseYes], [HowDoYouWantRoomToFeel], [FurnitureAccessoriesStayingInRoom], [YourBudget], [MustHavesItemsInRoomNotAlreadyOwned], [MustHavesItemsInRoomNotAlreadyOwnedYes], [NumberOfCooks], [DoYouCookAlot], [TypeOfOven], [OtherActivitiesInKitchen], [OtherActivitiesInKitchenYes], [MainEatingRoom], [EntertainFrequently], [HowDoYouWantYourRoomToFeel], [DoYouLikeContrast], [DoYouLikeTextures], [HowDoYouWantYourRoomToFeelGender], [AdditionalImportantInformation]) VALUES (1, 98, N'Eclectic', NULL, N'No', NULL, N'efefef', N'No', N'$2000', N'No', NULL, N'$1551', N'No', N'Electric', N'No', NULL, N'No', N'No', N'fef', N'No', N'No', N'Feminine', NULL)
INSERT [dbo].[KitchenQuestionnaires] ([KitchenQuestionnaireId], [RoomId], [DesignStyleOfRoom], [DesignStyleOfRoomOther], [SpecificColorsYouWantToUse], [SpecificColorsYouWantToUseYes], [HowDoYouWantRoomToFeel], [FurnitureAccessoriesStayingInRoom], [YourBudget], [MustHavesItemsInRoomNotAlreadyOwned], [MustHavesItemsInRoomNotAlreadyOwnedYes], [NumberOfCooks], [DoYouCookAlot], [TypeOfOven], [OtherActivitiesInKitchen], [OtherActivitiesInKitchenYes], [MainEatingRoom], [EntertainFrequently], [HowDoYouWantYourRoomToFeel], [DoYouLikeContrast], [DoYouLikeTextures], [HowDoYouWantYourRoomToFeelGender], [AdditionalImportantInformation]) VALUES (2, 103, N'Mid-century Modern', NULL, N'No', NULL, N'sfsfs', N'No', N'4000', N'No', NULL, N'4', N'No', N'Electric', N'No', NULL, N'No', N'No', N'sffsfs', N'No', N'No', N'Feminine', NULL)
INSERT [dbo].[KitchenQuestionnaires] ([KitchenQuestionnaireId], [RoomId], [DesignStyleOfRoom], [DesignStyleOfRoomOther], [SpecificColorsYouWantToUse], [SpecificColorsYouWantToUseYes], [HowDoYouWantRoomToFeel], [FurnitureAccessoriesStayingInRoom], [YourBudget], [MustHavesItemsInRoomNotAlreadyOwned], [MustHavesItemsInRoomNotAlreadyOwnedYes], [NumberOfCooks], [DoYouCookAlot], [TypeOfOven], [OtherActivitiesInKitchen], [OtherActivitiesInKitchenYes], [MainEatingRoom], [EntertainFrequently], [HowDoYouWantYourRoomToFeel], [DoYouLikeContrast], [DoYouLikeTextures], [HowDoYouWantYourRoomToFeelGender], [AdditionalImportantInformation]) VALUES (3, 105, N'Eclectic', NULL, N'No', NULL, N'clean', N'No', N'4000', N'No', NULL, N'3', N'No', N'Electric', N'No', NULL, N'No', N'No', N'testing', N'No', N'No', N'Feminine', NULL)
SET IDENTITY_INSERT [dbo].[KitchenQuestionnaires] OFF
/****** Object:  Table [dbo].[KidsRoomQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KidsRoomQuestionnaires]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KidsRoomQuestionnaires](
	[KidsRoomQuestionnaireId] [int] IDENTITY(1,1) NOT NULL,
	[RoomId] [int] NOT NULL,
	[DesignStyleOfRoom] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DesignStyleOfRoomOther] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SpecificColorsYouWantToUse] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SpecificColorsYouWantToUseYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FurnitureAccessoriesStayingInRoom] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[YourBudget] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MustHavesItemsInRoomNotAlreadyOwned] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MustHavesItemsInRoomNotAlreadyOwnedYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StoreAnythingElseInRoom] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouUseThisSpace] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MoreThanOneChild] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ChildsAge] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouWantYourRoomToFeel] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouLikeContrast] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouLikeTextures] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouWantYourRoomToFeelGender] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AdditionalImportantInformation] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[GirlsRoomOrBoysRoom] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[GenderSpecificRoom] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThemeChildWants] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThemeChildWantsYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShouldRoomTransitionAsChildAges] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_KidsRoomQuestionnaires] PRIMARY KEY CLUSTERED 
(
	[KidsRoomQuestionnaireId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[GuestRoomQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GuestRoomQuestionnaires]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GuestRoomQuestionnaires](
	[GuestRoomQuestionnaireId] [int] IDENTITY(1,1) NOT NULL,
	[RoomId] [int] NOT NULL,
	[DesignStyleOfRoom] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DesignStyleOfRoomOther] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SpecificColorsYouWantToUse] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SpecificColorsYouWantToUseYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FurnitureAccessoriesStayingInRoom] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[YourBudget] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MustHavesItemsInRoomNotAlreadyOwned] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MustHavesItemsInRoomNotAlreadyOwnedYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StoreAnythingElseInRoom] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouUseThisSpace] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouWantYourRoomToFeel] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouLikeContrast] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouLikeTextures] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouWantYourRoomToFeelGender] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AdditionalImportantInformation] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CateringRoomToSpecificPerson] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CateringRoomToSpecificPersonWho] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CateringRoomToSpecificPersonRequirements] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_GuestRoomQuestionnaires] PRIMARY KEY CLUSTERED 
(
	[GuestRoomQuestionnaireId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Orders]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Orders](
	[OrderId] [int] IDENTITY(1,1) NOT NULL,
	[AddedDate] [datetime] NULL,
	[AddedBy] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Status] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ShippingMethod] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SubTotal] [money] NOT NULL,
	[Shipping] [money] NOT NULL,
	[ShippingFirstName] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ShippingLastName] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ShippingStreet] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ShippingPostalCode] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ShippingCity] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ShippingState] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ShippingCountry] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CustomerEmail] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ShippedDate] [datetime] NULL,
	[TransactionId] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TrackingId] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RoomId] [int] NOT NULL,
	[OrderRoomName] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OrderType] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[TaxType] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TaxAmount] [money] NULL,
	[Total] [money] NOT NULL,
	[Currency] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ReceiptId] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[Orders] ON
INSERT [dbo].[Orders] ([OrderId], [AddedDate], [AddedBy], [Status], [ShippingMethod], [SubTotal], [Shipping], [ShippingFirstName], [ShippingLastName], [ShippingStreet], [ShippingPostalCode], [ShippingCity], [ShippingState], [ShippingCountry], [CustomerEmail], [ShippedDate], [TransactionId], [TrackingId], [RoomId], [OrderRoomName], [OrderType], [TaxType], [TaxAmount], [Total], [Currency], [ReceiptId]) VALUES (15, CAST(0x00009E6200C412DC AS DateTime), N'h@h.com', N'Completed', N'Included', 850.0000, 0.0000, N'Test', N'User', N'1234 Happy Road', N'V6K3S5', N'Toronto', N'Ontario', N'Canada', N'233232@philhack.com', NULL, N'2NW237901M2456110', NULL, 48, N'Professional Interior Design for a bathroom, titled: Phils Bathroom', N'Room', N'HST', 110.5000, 960.5000, N'CAD', N'2293-7705-5108-0351')
INSERT [dbo].[Orders] ([OrderId], [AddedDate], [AddedBy], [Status], [ShippingMethod], [SubTotal], [Shipping], [ShippingFirstName], [ShippingLastName], [ShippingStreet], [ShippingPostalCode], [ShippingCity], [ShippingState], [ShippingCountry], [CustomerEmail], [ShippedDate], [TransactionId], [TrackingId], [RoomId], [OrderRoomName], [OrderType], [TaxType], [TaxAmount], [Total], [Currency], [ReceiptId]) VALUES (17, CAST(0x00009E6200D612EA AS DateTime), N'g@g.com', N'Completed', N'Included', 1150.0000, 0.0000, N'Test', N'User', N'1 Main St', N'95131', N'San Jose', N'CA', N'United States', N'phil_1_1294084155_per@philhack.com', NULL, N'19T226315H276425S', NULL, 45, N'Professional Interior Design for a media room, titled: new roomy', N'Room', NULL, 0.0000, 1150.0000, N'USD', N'0.00')
INSERT [dbo].[Orders] ([OrderId], [AddedDate], [AddedBy], [Status], [ShippingMethod], [SubTotal], [Shipping], [ShippingFirstName], [ShippingLastName], [ShippingStreet], [ShippingPostalCode], [ShippingCity], [ShippingState], [ShippingCountry], [CustomerEmail], [ShippedDate], [TransactionId], [TrackingId], [RoomId], [OrderRoomName], [OrderType], [TaxType], [TaxAmount], [Total], [Currency], [ReceiptId]) VALUES (18, CAST(0x00009E8900F1A2A5 AS DateTime), N'a3a99226@telus.net', N'Completed', N'Included', 850.0000, 0.0000, N'Test', N'User', N'windows-1252', N'windows-1252', N'windows-1252', N'windows-1252', N'windows-1252', N'phil_1_1294084155_per@philhack.com', NULL, N'3J1756113H259605X', NULL, 1, N'Professional Interior Design for a entryway, titled: new roomy', N'Room', NULL, 0.0000, 850.0000, N'USD', N'windows-1252')
INSERT [dbo].[Orders] ([OrderId], [AddedDate], [AddedBy], [Status], [ShippingMethod], [SubTotal], [Shipping], [ShippingFirstName], [ShippingLastName], [ShippingStreet], [ShippingPostalCode], [ShippingCity], [ShippingState], [ShippingCountry], [CustomerEmail], [ShippedDate], [TransactionId], [TrackingId], [RoomId], [OrderRoomName], [OrderType], [TaxType], [TaxAmount], [Total], [Currency], [ReceiptId]) VALUES (19, CAST(0x00009E8F0102E1B1 AS DateTime), N'i@i.com', N'Completed', N'Included', 1150.0000, 0.0000, N'Test', N'User', N'windows-1252', N'windows-1252', N'windows-1252', N'windows-1252', N'windows-1252', N'phil_1284332367_per@philhack.com', NULL, N'6DL17793JR161805A', NULL, 74, N'Professional Interior Design for a home office, titled: home office', N'Room', NULL, 0.0000, 1150.0000, N'USD', N'windows-1252')
INSERT [dbo].[Orders] ([OrderId], [AddedDate], [AddedBy], [Status], [ShippingMethod], [SubTotal], [Shipping], [ShippingFirstName], [ShippingLastName], [ShippingStreet], [ShippingPostalCode], [ShippingCity], [ShippingState], [ShippingCountry], [CustomerEmail], [ShippedDate], [TransactionId], [TrackingId], [RoomId], [OrderRoomName], [OrderType], [TaxType], [TaxAmount], [Total], [Currency], [ReceiptId]) VALUES (20, CAST(0x00009E8F010D8BD1 AS DateTime), N'i@i.com', N'Completed', N'Included', 1150.0000, 0.0000, N'Test', N'User', N'windows-1252', N'windows-1252', N'windows-1252', N'windows-1252', N'windows-1252', N'phil_1284332367_per@philhack.com', NULL, N'9BP70495VD864671X', NULL, 73, N'Professional Interior Design for a media room, titled: media room', N'Room', NULL, 0.0000, 1150.0000, N'USD', N'windows-1252')
INSERT [dbo].[Orders] ([OrderId], [AddedDate], [AddedBy], [Status], [ShippingMethod], [SubTotal], [Shipping], [ShippingFirstName], [ShippingLastName], [ShippingStreet], [ShippingPostalCode], [ShippingCity], [ShippingState], [ShippingCountry], [CustomerEmail], [ShippedDate], [TransactionId], [TrackingId], [RoomId], [OrderRoomName], [OrderType], [TaxType], [TaxAmount], [Total], [Currency], [ReceiptId]) VALUES (21, CAST(0x00009F0601509168 AS DateTime), N'philliphack@gmail.com', N'Completed', N'Included', 850.0000, 0.0000, N'Test', N'User', N'windows-1252', N'windows-1252', N'windows-1252', N'windows-1252', N'windows-1252', N'phil_1284332367_per@philhack.com', NULL, N'6JD29970EU312570F', NULL, 104, N'Professional Interior Design for a entryway, titled: Me entry', N'Room', NULL, 0.0000, 850.0000, N'USD', N'windows-1252')
INSERT [dbo].[Orders] ([OrderId], [AddedDate], [AddedBy], [Status], [ShippingMethod], [SubTotal], [Shipping], [ShippingFirstName], [ShippingLastName], [ShippingStreet], [ShippingPostalCode], [ShippingCity], [ShippingState], [ShippingCountry], [CustomerEmail], [ShippedDate], [TransactionId], [TrackingId], [RoomId], [OrderRoomName], [OrderType], [TaxType], [TaxAmount], [Total], [Currency], [ReceiptId]) VALUES (22, CAST(0x00009F070093CC4F AS DateTime), N'philliphack@gmail.com', N'Completed', N'Included', 1500.0000, 0.0000, N'Test', N'User', N'windows-1252', N'windows-1252', N'windows-1252', N'windows-1252', N'windows-1252', N'phil_1284332367_per@philhack.com', NULL, N'77P117416S093810X', NULL, 105, N'Professional Interior Design for a kitchen, titled: new room', N'Room', NULL, 0.0000, 1500.0000, N'USD', N'windows-1252')
SET IDENTITY_INSERT [dbo].[Orders] OFF
/****** Object:  Table [dbo].[OfficeQuestionnaires]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OfficeQuestionnaires]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OfficeQuestionnaires](
	[OfficeQuestionnaireId] [int] IDENTITY(1,1) NOT NULL,
	[RoomId] [int] NOT NULL,
	[DesignStyleOfRoom] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DesignStyleOfRoomOther] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SpecificColorsYouWantToUse] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SpecificColorsYouWantToUseYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FurnitureAccessoriesStayingInRoom] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[YourBudget] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MustHavesItemsInRoomNotAlreadyOwned] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MustHavesItemsInRoomNotAlreadyOwnedYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[HowDoYouUseThisSpace] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowManyPeopleUseThisSpace] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[StoreAnythingElseInRoom] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HaveFilesThatNeedToBeStored] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HaveFilesThatNeedToBeStoredYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WorkFromHome] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[WorkFromHomeYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BringClientsIntoSpace] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[BringClientsIntoSpaceYes] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LayOutWorkOrKeepInCabinets] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouWantYourRoomToFeel] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouLikeContrast] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DoYouLikeTextures] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HowDoYouWantYourRoomToFeelGender] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AdditionalImportantInformation] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_OfficeQuestionnaires] PRIMARY KEY CLUSTERED 
(
	[OfficeQuestionnaireId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  StoredProcedure [dbo].[UpdateRoomDateUpdated]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateRoomDateUpdated]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UpdateRoomDateUpdated]
@RoomId INT,
@DateUpdated DATETIME
AS
BEGIN
UPDATE Rooms
SET DateUpdated = @DateUpdated
WHERE RoomId  = @RoomId;
END' 
END
GO
/****** Object:  View [dbo].[vw_aspnet_Users]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Users]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_Users]
  AS SELECT [dbo].[aspnet_Users].[ApplicationId], [dbo].[aspnet_Users].[UserId], [dbo].[aspnet_Users].[UserName], [dbo].[aspnet_Users].[LoweredUserName], [dbo].[aspnet_Users].[MobileAlias], [dbo].[aspnet_Users].[IsAnonymous], [dbo].[aspnet_Users].[LastActivityDate]
  FROM [dbo].[aspnet_Users]
  '
GO
/****** Object:  View [dbo].[vw_aspnet_Roles]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Roles]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_Roles]
  AS SELECT [dbo].[aspnet_Roles].[ApplicationId], [dbo].[aspnet_Roles].[RoleId], [dbo].[aspnet_Roles].[RoleName], [dbo].[aspnet_Roles].[LoweredRoleName], [dbo].[aspnet_Roles].[Description]
  FROM [dbo].[aspnet_Roles]
  '
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_Paths]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_Paths]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_WebPartState_Paths]
  AS SELECT [dbo].[aspnet_Paths].[ApplicationId], [dbo].[aspnet_Paths].[PathId], [dbo].[aspnet_Paths].[Path], [dbo].[aspnet_Paths].[LoweredPath]
  FROM [dbo].[aspnet_Paths]
  '
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_User]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_User]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_WebPartState_User]
  AS SELECT [dbo].[aspnet_PersonalizationPerUser].[PathId], [dbo].[aspnet_PersonalizationPerUser].[UserId], [DataSize]=DATALENGTH([dbo].[aspnet_PersonalizationPerUser].[PageSettings]), [dbo].[aspnet_PersonalizationPerUser].[LastUpdatedDate]
  FROM [dbo].[aspnet_PersonalizationPerUser]
  '
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_Shared]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_Shared]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_WebPartState_Shared]
  AS SELECT [dbo].[aspnet_PersonalizationAllUsers].[PathId], [DataSize]=DATALENGTH([dbo].[aspnet_PersonalizationAllUsers].[PageSettings]), [dbo].[aspnet_PersonalizationAllUsers].[LastUpdatedDate]
  FROM [dbo].[aspnet_PersonalizationAllUsers]
  '
GO
/****** Object:  View [dbo].[vw_aspnet_UsersInRoles]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_UsersInRoles]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_UsersInRoles]
  AS SELECT [dbo].[aspnet_UsersInRoles].[UserId], [dbo].[aspnet_UsersInRoles].[RoleId]
  FROM [dbo].[aspnet_UsersInRoles]
  '
GO
/****** Object:  View [dbo].[vw_aspnet_Profiles]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Profiles]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_Profiles]
  AS SELECT [dbo].[aspnet_Profile].[UserId], [dbo].[aspnet_Profile].[LastUpdatedDate],
      [DataSize]=  DATALENGTH([dbo].[aspnet_Profile].[PropertyNames])
                 + DATALENGTH([dbo].[aspnet_Profile].[PropertyValuesString])
                 + DATALENGTH([dbo].[aspnet_Profile].[PropertyValuesBinary])
  FROM [dbo].[aspnet_Profile]
  '
GO
/****** Object:  View [dbo].[vw_aspnet_MembershipUsers]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_MembershipUsers]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_MembershipUsers]
  AS SELECT [dbo].[aspnet_Membership].[UserId],
            [dbo].[aspnet_Membership].[PasswordFormat],
            [dbo].[aspnet_Membership].[MobilePIN],
            [dbo].[aspnet_Membership].[Email],
            [dbo].[aspnet_Membership].[LoweredEmail],
            [dbo].[aspnet_Membership].[PasswordQuestion],
            [dbo].[aspnet_Membership].[PasswordAnswer],
            [dbo].[aspnet_Membership].[IsApproved],
            [dbo].[aspnet_Membership].[IsLockedOut],
            [dbo].[aspnet_Membership].[CreateDate],
            [dbo].[aspnet_Membership].[LastLoginDate],
            [dbo].[aspnet_Membership].[LastPasswordChangedDate],
            [dbo].[aspnet_Membership].[LastLockoutDate],
            [dbo].[aspnet_Membership].[FailedPasswordAttemptCount],
            [dbo].[aspnet_Membership].[FailedPasswordAttemptWindowStart],
            [dbo].[aspnet_Membership].[FailedPasswordAnswerAttemptCount],
            [dbo].[aspnet_Membership].[FailedPasswordAnswerAttemptWindowStart],
            [dbo].[aspnet_Membership].[Comment],
            [dbo].[aspnet_Users].[ApplicationId],
            [dbo].[aspnet_Users].[UserName],
            [dbo].[aspnet_Users].[MobileAlias],
            [dbo].[aspnet_Users].[IsAnonymous],
            [dbo].[aspnet_Users].[LastActivityDate]
  FROM [dbo].[aspnet_Membership] INNER JOIN [dbo].[aspnet_Users]
      ON [dbo].[aspnet_Membership].[UserId] = [dbo].[aspnet_Users].[UserId]
  '
GO
/****** Object:  StoredProcedure [dbo].[UpdateDesignFileComments]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateDesignFileComments]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UpdateDesignFileComments]
@DesignFileId INT,
@Comments TEXT
AS
BEGIN
UPDATE DesignFiles
SET Comments = @Comments
WHERE DesignFileId  = @DesignFileId;
END' 
END
GO
/****** Object:  Table [dbo].[FurniturePhotos]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FurniturePhotos]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FurniturePhotos](
	[FurniturePhotoId] [int] IDENTITY(1,1) NOT NULL,
	[FurnitureId] [int] NOT NULL,
	[PhotoName] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OriginalServerPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OriginalUrlPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[WebServerPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[WebUrlPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThumbnailServerPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThumbnailUrlPath] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OriginalRSContainer] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OriginalRSUrl] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[WebRSContainer] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[WebRSUrl] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThumbnailRSContainer] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThumbnailRSUrl] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_FurniturePhotos] PRIMARY KEY CLUSTERED 
(
	[FurniturePhotoId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[FurniturePhotos] ON
INSERT [dbo].[FurniturePhotos] ([FurniturePhotoId], [FurnitureId], [PhotoName], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (2, 4, N'1-4-4aa2fa4c-1fec-4e82-a756-cf56babfce1c.jpg', N'~/UserData/\rfpo\1-4-4aa2fa4c-1fec-4e82-a756-cf56babfce1c.jpg', N'/UserData/rfpo/1-4-4aa2fa4c-1fec-4e82-a756-cf56babfce1c.jpg', N'~/UserData/\rfpw\1-4-4aa2fa4c-1fec-4e82-a756-cf56babfce1c.jpg', N'/UserData/rfpw/1-4-4aa2fa4c-1fec-4e82-a756-cf56babfce1c.jpg', N'~/UserData/\rfpt\1-4-4aa2fa4c-1fec-4e82-a756-cf56babfce1c.jpg', N'/UserData/rfpt/1-4-4aa2fa4c-1fec-4e82-a756-cf56babfce1c.jpg', N'rfpo', N'http://c0003350.cdn2.cloudfiles.rackspacecloud.com/1-4-4aa2fa4c-1fec-4e82-a756-cf56babfce1c.jpg', N'rfpw', N'http://c0003352.cdn2.cloudfiles.rackspacecloud.com/1-4-4aa2fa4c-1fec-4e82-a756-cf56babfce1c.jpg', N'rfpt', N'http://c0003351.cdn2.cloudfiles.rackspacecloud.com/1-4-4aa2fa4c-1fec-4e82-a756-cf56babfce1c.jpg')
INSERT [dbo].[FurniturePhotos] ([FurniturePhotoId], [FurnitureId], [PhotoName], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (3, 4, N'1-4-265ea58d-a3ec-4426-ad7a-fc007e0f97f3.jpg', N'~/UserData/\rfpo\1-4-265ea58d-a3ec-4426-ad7a-fc007e0f97f3.jpg', N'/UserData/rfpo/1-4-265ea58d-a3ec-4426-ad7a-fc007e0f97f3.jpg', N'~/UserData/\rfpw\1-4-265ea58d-a3ec-4426-ad7a-fc007e0f97f3.jpg', N'/UserData/rfpw/1-4-265ea58d-a3ec-4426-ad7a-fc007e0f97f3.jpg', N'~/UserData/\rfpt\1-4-265ea58d-a3ec-4426-ad7a-fc007e0f97f3.jpg', N'/UserData/rfpt/1-4-265ea58d-a3ec-4426-ad7a-fc007e0f97f3.jpg', N'rfpo', N'http://c0003350.cdn2.cloudfiles.rackspacecloud.com/1-4-265ea58d-a3ec-4426-ad7a-fc007e0f97f3.jpg', N'rfpw', N'http://c0003352.cdn2.cloudfiles.rackspacecloud.com/1-4-265ea58d-a3ec-4426-ad7a-fc007e0f97f3.jpg', N'rfpt', N'http://c0003351.cdn2.cloudfiles.rackspacecloud.com/1-4-265ea58d-a3ec-4426-ad7a-fc007e0f97f3.jpg')
INSERT [dbo].[FurniturePhotos] ([FurniturePhotoId], [FurnitureId], [PhotoName], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (4, 4, N'1-4-d04f8c31-4fbc-43fc-a06e-fd045912dca4.jpg', N'~/UserData/\rfpo\1-4-d04f8c31-4fbc-43fc-a06e-fd045912dca4.jpg', N'/UserData/rfpo/1-4-d04f8c31-4fbc-43fc-a06e-fd045912dca4.jpg', N'~/UserData/\rfpw\1-4-d04f8c31-4fbc-43fc-a06e-fd045912dca4.jpg', N'/UserData/rfpw/1-4-d04f8c31-4fbc-43fc-a06e-fd045912dca4.jpg', N'~/UserData/\rfpt\1-4-d04f8c31-4fbc-43fc-a06e-fd045912dca4.jpg', N'/UserData/rfpt/1-4-d04f8c31-4fbc-43fc-a06e-fd045912dca4.jpg', N'rfpo', N'http://c0003350.cdn2.cloudfiles.rackspacecloud.com/1-4-d04f8c31-4fbc-43fc-a06e-fd045912dca4.jpg', N'rfpw', N'http://c0003352.cdn2.cloudfiles.rackspacecloud.com/1-4-d04f8c31-4fbc-43fc-a06e-fd045912dca4.jpg', N'rfpt', N'http://c0003351.cdn2.cloudfiles.rackspacecloud.com/1-4-d04f8c31-4fbc-43fc-a06e-fd045912dca4.jpg')
INSERT [dbo].[FurniturePhotos] ([FurniturePhotoId], [FurnitureId], [PhotoName], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (30, 20, N'44-20-316ef106-39bc-449b-8f6b-ba4767e8861e.jpg', N'~/UserData/\rfpo\44-20-316ef106-39bc-449b-8f6b-ba4767e8861e.jpg', N'/UserData/rfpo/44-20-316ef106-39bc-449b-8f6b-ba4767e8861e.jpg', N'~/UserData/\rfpw\44-20-316ef106-39bc-449b-8f6b-ba4767e8861e.jpg', N'/UserData/rfpw/44-20-316ef106-39bc-449b-8f6b-ba4767e8861e.jpg', N'~/UserData/\rfpt\44-20-316ef106-39bc-449b-8f6b-ba4767e8861e.jpg', N'/UserData/rfpt/44-20-316ef106-39bc-449b-8f6b-ba4767e8861e.jpg', N'rfpo', N'http://c0003350.cdn2.cloudfiles.rackspacecloud.com/44-20-316ef106-39bc-449b-8f6b-ba4767e8861e.jpg', N'rfpw', N'http://c0003352.cdn2.cloudfiles.rackspacecloud.com/44-20-316ef106-39bc-449b-8f6b-ba4767e8861e.jpg', N'rfpt', N'http://c0003351.cdn2.cloudfiles.rackspacecloud.com/44-20-316ef106-39bc-449b-8f6b-ba4767e8861e.jpg')
INSERT [dbo].[FurniturePhotos] ([FurniturePhotoId], [FurnitureId], [PhotoName], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (39, 22, N'74-22-1d368f27-fcfd-41c0-bd03-137ab674668a.jpg', N'~/UserData/\rfpo\74-22-1d368f27-fcfd-41c0-bd03-137ab674668a.jpg', N'/UserData/rfpo/74-22-1d368f27-fcfd-41c0-bd03-137ab674668a.jpg', N'~/UserData/\rfpw\74-22-1d368f27-fcfd-41c0-bd03-137ab674668a.jpg', N'/UserData/rfpw/74-22-1d368f27-fcfd-41c0-bd03-137ab674668a.jpg', N'~/UserData/\rfpt\74-22-1d368f27-fcfd-41c0-bd03-137ab674668a.jpg', N'/UserData/rfpt/74-22-1d368f27-fcfd-41c0-bd03-137ab674668a.jpg', N'rfpo', N'http://c0003350.cdn2.cloudfiles.rackspacecloud.com/74-22-1d368f27-fcfd-41c0-bd03-137ab674668a.jpg', N'rfpw', N'http://c0003352.cdn2.cloudfiles.rackspacecloud.com/74-22-1d368f27-fcfd-41c0-bd03-137ab674668a.jpg', N'rfpt', N'http://c0003351.cdn2.cloudfiles.rackspacecloud.com/74-22-1d368f27-fcfd-41c0-bd03-137ab674668a.jpg')
INSERT [dbo].[FurniturePhotos] ([FurniturePhotoId], [FurnitureId], [PhotoName], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (40, 22, N'74-22-dc139338-6951-4c8a-8298-0f76aa398f99.jpg', N'~/UserData/\rfpo\74-22-dc139338-6951-4c8a-8298-0f76aa398f99.jpg', N'/UserData/rfpo/74-22-dc139338-6951-4c8a-8298-0f76aa398f99.jpg', N'~/UserData/\rfpw\74-22-dc139338-6951-4c8a-8298-0f76aa398f99.jpg', N'/UserData/rfpw/74-22-dc139338-6951-4c8a-8298-0f76aa398f99.jpg', N'~/UserData/\rfpt\74-22-dc139338-6951-4c8a-8298-0f76aa398f99.jpg', N'/UserData/rfpt/74-22-dc139338-6951-4c8a-8298-0f76aa398f99.jpg', N'rfpo', N'http://c0003350.cdn2.cloudfiles.rackspacecloud.com/74-22-dc139338-6951-4c8a-8298-0f76aa398f99.jpg', N'rfpw', N'http://c0003352.cdn2.cloudfiles.rackspacecloud.com/74-22-dc139338-6951-4c8a-8298-0f76aa398f99.jpg', N'rfpt', N'http://c0003351.cdn2.cloudfiles.rackspacecloud.com/74-22-dc139338-6951-4c8a-8298-0f76aa398f99.jpg')
INSERT [dbo].[FurniturePhotos] ([FurniturePhotoId], [FurnitureId], [PhotoName], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (41, 23, N'74-23-10f96a21-2c07-4a71-82a4-2ee34a5dfb5d.jpg', N'~/UserData/\rfpo\74-23-10f96a21-2c07-4a71-82a4-2ee34a5dfb5d.jpg', N'/UserData/rfpo/74-23-10f96a21-2c07-4a71-82a4-2ee34a5dfb5d.jpg', N'~/UserData/\rfpw\74-23-10f96a21-2c07-4a71-82a4-2ee34a5dfb5d.jpg', N'/UserData/rfpw/74-23-10f96a21-2c07-4a71-82a4-2ee34a5dfb5d.jpg', N'~/UserData/\rfpt\74-23-10f96a21-2c07-4a71-82a4-2ee34a5dfb5d.jpg', N'/UserData/rfpt/74-23-10f96a21-2c07-4a71-82a4-2ee34a5dfb5d.jpg', N'rfpo', N'http://c0003350.cdn2.cloudfiles.rackspacecloud.com/74-23-10f96a21-2c07-4a71-82a4-2ee34a5dfb5d.jpg', N'rfpw', N'http://c0003352.cdn2.cloudfiles.rackspacecloud.com/74-23-10f96a21-2c07-4a71-82a4-2ee34a5dfb5d.jpg', N'rfpt', N'http://c0003351.cdn2.cloudfiles.rackspacecloud.com/74-23-10f96a21-2c07-4a71-82a4-2ee34a5dfb5d.jpg')
INSERT [dbo].[FurniturePhotos] ([FurniturePhotoId], [FurnitureId], [PhotoName], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (42, 23, N'74-23-53b3a1f3-ee68-4ab7-b62e-064dbec4c2c1.jpg', N'~/UserData/\rfpo\74-23-53b3a1f3-ee68-4ab7-b62e-064dbec4c2c1.jpg', N'/UserData/rfpo/74-23-53b3a1f3-ee68-4ab7-b62e-064dbec4c2c1.jpg', N'~/UserData/\rfpw\74-23-53b3a1f3-ee68-4ab7-b62e-064dbec4c2c1.jpg', N'/UserData/rfpw/74-23-53b3a1f3-ee68-4ab7-b62e-064dbec4c2c1.jpg', N'~/UserData/\rfpt\74-23-53b3a1f3-ee68-4ab7-b62e-064dbec4c2c1.jpg', N'/UserData/rfpt/74-23-53b3a1f3-ee68-4ab7-b62e-064dbec4c2c1.jpg', N'rfpo', N'http://c0003350.cdn2.cloudfiles.rackspacecloud.com/74-23-53b3a1f3-ee68-4ab7-b62e-064dbec4c2c1.jpg', N'rfpw', N'http://c0003352.cdn2.cloudfiles.rackspacecloud.com/74-23-53b3a1f3-ee68-4ab7-b62e-064dbec4c2c1.jpg', N'rfpt', N'http://c0003351.cdn2.cloudfiles.rackspacecloud.com/74-23-53b3a1f3-ee68-4ab7-b62e-064dbec4c2c1.jpg')
INSERT [dbo].[FurniturePhotos] ([FurniturePhotoId], [FurnitureId], [PhotoName], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (45, 23, N'74-23-f2543ceb-d0e9-4730-a019-c3d3ab1d2a80.jpg', N'~/UserData/\rfpo\74-23-f2543ceb-d0e9-4730-a019-c3d3ab1d2a80.jpg', N'/UserData/rfpo/74-23-f2543ceb-d0e9-4730-a019-c3d3ab1d2a80.jpg', N'~/UserData/\rfpw\74-23-f2543ceb-d0e9-4730-a019-c3d3ab1d2a80.jpg', N'/UserData/rfpw/74-23-f2543ceb-d0e9-4730-a019-c3d3ab1d2a80.jpg', N'~/UserData/\rfpt\74-23-f2543ceb-d0e9-4730-a019-c3d3ab1d2a80.jpg', N'/UserData/rfpt/74-23-f2543ceb-d0e9-4730-a019-c3d3ab1d2a80.jpg', N'rfpo', N'http://c0003350.cdn2.cloudfiles.rackspacecloud.com/74-23-f2543ceb-d0e9-4730-a019-c3d3ab1d2a80.jpg', N'rfpw', N'http://c0003352.cdn2.cloudfiles.rackspacecloud.com/74-23-f2543ceb-d0e9-4730-a019-c3d3ab1d2a80.jpg', N'rfpt', N'http://c0003351.cdn2.cloudfiles.rackspacecloud.com/74-23-f2543ceb-d0e9-4730-a019-c3d3ab1d2a80.jpg')
INSERT [dbo].[FurniturePhotos] ([FurniturePhotoId], [FurnitureId], [PhotoName], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (46, 23, N'74-23-f9ea9de7-d024-4bbe-bd3e-516eaa338d3b.jpg', N'~/UserData/\rfpo\74-23-f9ea9de7-d024-4bbe-bd3e-516eaa338d3b.jpg', N'/UserData/rfpo/74-23-f9ea9de7-d024-4bbe-bd3e-516eaa338d3b.jpg', N'~/UserData/\rfpw\74-23-f9ea9de7-d024-4bbe-bd3e-516eaa338d3b.jpg', N'/UserData/rfpw/74-23-f9ea9de7-d024-4bbe-bd3e-516eaa338d3b.jpg', N'~/UserData/\rfpt\74-23-f9ea9de7-d024-4bbe-bd3e-516eaa338d3b.jpg', N'/UserData/rfpt/74-23-f9ea9de7-d024-4bbe-bd3e-516eaa338d3b.jpg', N'rfpo', N'http://c0003350.cdn2.cloudfiles.rackspacecloud.com/74-23-f9ea9de7-d024-4bbe-bd3e-516eaa338d3b.jpg', N'rfpw', N'http://c0003352.cdn2.cloudfiles.rackspacecloud.com/74-23-f9ea9de7-d024-4bbe-bd3e-516eaa338d3b.jpg', N'rfpt', N'http://c0003351.cdn2.cloudfiles.rackspacecloud.com/74-23-f9ea9de7-d024-4bbe-bd3e-516eaa338d3b.jpg')
INSERT [dbo].[FurniturePhotos] ([FurniturePhotoId], [FurnitureId], [PhotoName], [OriginalServerPath], [OriginalUrlPath], [WebServerPath], [WebUrlPath], [ThumbnailServerPath], [ThumbnailUrlPath], [OriginalRSContainer], [OriginalRSUrl], [WebRSContainer], [WebRSUrl], [ThumbnailRSContainer], [ThumbnailRSUrl]) VALUES (47, 23, N'74-23-646484fa-4ab0-4432-a66a-c8f8ee20ae2e.jpg', N'~/UserData/\rfpo\74-23-646484fa-4ab0-4432-a66a-c8f8ee20ae2e.jpg', N'/UserData/rfpo/74-23-646484fa-4ab0-4432-a66a-c8f8ee20ae2e.jpg', N'~/UserData/\rfpw\74-23-646484fa-4ab0-4432-a66a-c8f8ee20ae2e.jpg', N'/UserData/rfpw/74-23-646484fa-4ab0-4432-a66a-c8f8ee20ae2e.jpg', N'~/UserData/\rfpt\74-23-646484fa-4ab0-4432-a66a-c8f8ee20ae2e.jpg', N'/UserData/rfpt/74-23-646484fa-4ab0-4432-a66a-c8f8ee20ae2e.jpg', N'rfpo', N'http://c0003350.cdn2.cloudfiles.rackspacecloud.com/74-23-646484fa-4ab0-4432-a66a-c8f8ee20ae2e.jpg', N'rfpw', N'http://c0003352.cdn2.cloudfiles.rackspacecloud.com/74-23-646484fa-4ab0-4432-a66a-c8f8ee20ae2e.jpg', N'rfpt', N'http://c0003351.cdn2.cloudfiles.rackspacecloud.com/74-23-646484fa-4ab0-4432-a66a-c8f8ee20ae2e.jpg')
SET IDENTITY_INSERT [dbo].[FurniturePhotos] OFF
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_DeleteInactiveProfiles]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_DeleteInactiveProfiles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Profile_DeleteInactiveProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @InactiveSinceDate      datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
    BEGIN
        SELECT  0
        RETURN
    END

    DELETE
    FROM    dbo.aspnet_Profile
    WHERE   UserId IN
            (   SELECT  UserId
                FROM    dbo.aspnet_Users u
                WHERE   ApplicationId = @ApplicationId
                        AND (LastActivityDate <= @InactiveSinceDate)
                        AND (
                                (@ProfileAuthOptions = 2)
                             OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                             OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
                            )
            )

    SELECT  @@ROWCOUNT
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]
	@ApplicationName  nvarchar(256),
	@UserNames		  nvarchar(4000),
	@RoleNames		  nvarchar(4000)
AS
BEGIN
	DECLARE @AppId uniqueidentifier
	SELECT  @AppId = NULL
	SELECT  @AppId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
	IF (@AppId IS NULL)
		RETURN(2)


	DECLARE @TranStarted   bit
	SET @TranStarted = 0

	IF( @@TRANCOUNT = 0 )
	BEGIN
		BEGIN TRANSACTION
		SET @TranStarted = 1
	END

	DECLARE @tbNames  table(Name nvarchar(256) NOT NULL PRIMARY KEY)
	DECLARE @tbRoles  table(RoleId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @tbUsers  table(UserId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @Num	  int
	DECLARE @Pos	  int
	DECLARE @NextPos  int
	DECLARE @Name	  nvarchar(256)
	DECLARE @CountAll int
	DECLARE @CountU	  int
	DECLARE @CountR	  int


	SET @Num = 0
	SET @Pos = 1
	WHILE(@Pos <= LEN(@RoleNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N'','', @RoleNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@RoleNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@RoleNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbRoles
	  SELECT RoleId
	  FROM   dbo.aspnet_Roles ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredRoleName AND ar.ApplicationId = @AppId
	SELECT @CountR = @@ROWCOUNT

	IF (@CountR <> @Num)
	BEGIN
		SELECT TOP 1 N'''', Name
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT ar.LoweredRoleName FROM dbo.aspnet_Roles ar,  @tbRoles r WHERE r.RoleId = ar.RoleId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(2)
	END


	DELETE FROM @tbNames WHERE 1=1
	SET @Num = 0
	SET @Pos = 1


	WHILE(@Pos <= LEN(@UserNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N'','', @UserNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@UserNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@UserNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbUsers
	  SELECT UserId
	  FROM   dbo.aspnet_Users ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredUserName AND ar.ApplicationId = @AppId

	SELECT @CountU = @@ROWCOUNT
	IF (@CountU <> @Num)
	BEGIN
		SELECT TOP 1 Name, N''''
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT au.LoweredUserName FROM dbo.aspnet_Users au,  @tbUsers u WHERE u.UserId = au.UserId)

		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(1)
	END

	SELECT  @CountAll = COUNT(*)
	FROM	dbo.aspnet_UsersInRoles ur, @tbUsers u, @tbRoles r
	WHERE   ur.UserId = u.UserId AND ur.RoleId = r.RoleId

	IF (@CountAll <> @CountU * @CountR)
	BEGIN
		SELECT TOP 1 UserName, RoleName
		FROM		 @tbUsers tu, @tbRoles tr, dbo.aspnet_Users u, dbo.aspnet_Roles r
		WHERE		 u.UserId = tu.UserId AND r.RoleId = tr.RoleId AND
					 tu.UserId NOT IN (SELECT ur.UserId FROM dbo.aspnet_UsersInRoles ur WHERE ur.RoleId = tr.RoleId) AND
					 tr.RoleId NOT IN (SELECT ur.RoleId FROM dbo.aspnet_UsersInRoles ur WHERE ur.UserId = tu.UserId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(3)
	END

	DELETE FROM dbo.aspnet_UsersInRoles
	WHERE UserId IN (SELECT UserId FROM @tbUsers)
	  AND RoleId IN (SELECT RoleId FROM @tbRoles)
	IF( @TranStarted = 1 )
		COMMIT TRANSACTION
	RETURN(0)
END
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_IsUserInRole]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_IsUserInRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_IsUserInRole]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(2)
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    DECLARE @RoleId uniqueidentifier
    SELECT  @RoleId = NULL

    SELECT  @UserId = UserId
    FROM    dbo.aspnet_Users
    WHERE   LoweredUserName = LOWER(@UserName) AND ApplicationId = @ApplicationId

    IF (@UserId IS NULL)
        RETURN(2)

    SELECT  @RoleId = RoleId
    FROM    dbo.aspnet_Roles
    WHERE   LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId

    IF (@RoleId IS NULL)
        RETURN(3)

    IF (EXISTS( SELECT * FROM dbo.aspnet_UsersInRoles WHERE  UserId = @UserId AND RoleId = @RoleId))
        RETURN(1)
    ELSE
        RETURN(0)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_GetUsersInRoles]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_GetUsersInRoles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_GetUsersInRoles]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
     DECLARE @RoleId uniqueidentifier
     SELECT  @RoleId = NULL

     SELECT  @RoleId = RoleId
     FROM    dbo.aspnet_Roles
     WHERE   LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId

     IF (@RoleId IS NULL)
         RETURN(1)

    SELECT u.UserName
    FROM   dbo.aspnet_Users u, dbo.aspnet_UsersInRoles ur
    WHERE  u.UserId = ur.UserId AND @RoleId = ur.RoleId AND u.ApplicationId = @ApplicationId
    ORDER BY u.UserName
    RETURN(0)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_GetRolesForUser]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_GetRolesForUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_GetRolesForUser]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL

    SELECT  @UserId = UserId
    FROM    dbo.aspnet_Users
    WHERE   LoweredUserName = LOWER(@UserName) AND ApplicationId = @ApplicationId

    IF (@UserId IS NULL)
        RETURN(1)

    SELECT r.RoleName
    FROM   dbo.aspnet_Roles r, dbo.aspnet_UsersInRoles ur
    WHERE  r.RoleId = ur.RoleId AND r.ApplicationId = @ApplicationId AND ur.UserId = @UserId
    ORDER BY r.RoleName
    RETURN (0)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_FindUsersInRole]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_FindUsersInRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_FindUsersInRole]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256),
    @UserNameToMatch  nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
     DECLARE @RoleId uniqueidentifier
     SELECT  @RoleId = NULL

     SELECT  @RoleId = RoleId
     FROM    dbo.aspnet_Roles
     WHERE   LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId

     IF (@RoleId IS NULL)
         RETURN(1)

    SELECT u.UserName
    FROM   dbo.aspnet_Users u, dbo.aspnet_UsersInRoles ur
    WHERE  u.UserId = ur.UserId AND @RoleId = ur.RoleId AND u.ApplicationId = @ApplicationId AND LoweredUserName LIKE LOWER(@UserNameToMatch)
    ORDER BY u.UserName
    RETURN(0)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_AddUsersToRoles]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_AddUsersToRoles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_AddUsersToRoles]
	@ApplicationName  nvarchar(256),
	@UserNames		  nvarchar(4000),
	@RoleNames		  nvarchar(4000),
	@CurrentTimeUtc   datetime
AS
BEGIN
	DECLARE @AppId uniqueidentifier
	SELECT  @AppId = NULL
	SELECT  @AppId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
	IF (@AppId IS NULL)
		RETURN(2)
	DECLARE @TranStarted   bit
	SET @TranStarted = 0

	IF( @@TRANCOUNT = 0 )
	BEGIN
		BEGIN TRANSACTION
		SET @TranStarted = 1
	END

	DECLARE @tbNames	table(Name nvarchar(256) NOT NULL PRIMARY KEY)
	DECLARE @tbRoles	table(RoleId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @tbUsers	table(UserId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @Num		int
	DECLARE @Pos		int
	DECLARE @NextPos	int
	DECLARE @Name		nvarchar(256)

	SET @Num = 0
	SET @Pos = 1
	WHILE(@Pos <= LEN(@RoleNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N'','', @RoleNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@RoleNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@RoleNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbRoles
	  SELECT RoleId
	  FROM   dbo.aspnet_Roles ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredRoleName AND ar.ApplicationId = @AppId

	IF (@@ROWCOUNT <> @Num)
	BEGIN
		SELECT TOP 1 Name
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT ar.LoweredRoleName FROM dbo.aspnet_Roles ar,  @tbRoles r WHERE r.RoleId = ar.RoleId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(2)
	END

	DELETE FROM @tbNames WHERE 1=1
	SET @Num = 0
	SET @Pos = 1

	WHILE(@Pos <= LEN(@UserNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N'','', @UserNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@UserNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@UserNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbUsers
	  SELECT UserId
	  FROM   dbo.aspnet_Users ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredUserName AND ar.ApplicationId = @AppId

	IF (@@ROWCOUNT <> @Num)
	BEGIN
		DELETE FROM @tbNames
		WHERE LOWER(Name) IN (SELECT LoweredUserName FROM dbo.aspnet_Users au,  @tbUsers u WHERE au.UserId = u.UserId)

		INSERT dbo.aspnet_Users (ApplicationId, UserId, UserName, LoweredUserName, IsAnonymous, LastActivityDate)
		  SELECT @AppId, NEWID(), Name, LOWER(Name), 0, @CurrentTimeUtc
		  FROM   @tbNames

		INSERT INTO @tbUsers
		  SELECT  UserId
		  FROM	dbo.aspnet_Users au, @tbNames t
		  WHERE   LOWER(t.Name) = au.LoweredUserName AND au.ApplicationId = @AppId
	END

	IF (EXISTS (SELECT * FROM dbo.aspnet_UsersInRoles ur, @tbUsers tu, @tbRoles tr WHERE tu.UserId = ur.UserId AND tr.RoleId = ur.RoleId))
	BEGIN
		SELECT TOP 1 UserName, RoleName
		FROM		 dbo.aspnet_UsersInRoles ur, @tbUsers tu, @tbRoles tr, aspnet_Users u, aspnet_Roles r
		WHERE		u.UserId = tu.UserId AND r.RoleId = tr.RoleId AND tu.UserId = ur.UserId AND tr.RoleId = ur.RoleId

		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(3)
	END

	INSERT INTO dbo.aspnet_UsersInRoles (UserId, RoleId)
	SELECT UserId, RoleId
	FROM @tbUsers, @tbRoles

	IF( @TranStarted = 1 )
		COMMIT TRANSACTION
	RETURN(0)
END                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Users_DeleteUser]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users_DeleteUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Users_DeleteUser]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @TablesToDeleteFrom int,
    @NumTablesDeletedFrom int OUTPUT
AS
BEGIN
    DECLARE @UserId               uniqueidentifier
    SELECT  @UserId               = NULL
    SELECT  @NumTablesDeletedFrom = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
	SET @TranStarted = 0

    DECLARE @ErrorCode   int
    DECLARE @RowCount    int

    SET @ErrorCode = 0
    SET @RowCount  = 0

    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a
    WHERE   u.LoweredUserName       = LOWER(@UserName)
        AND u.ApplicationId         = a.ApplicationId
        AND LOWER(@ApplicationName) = a.LoweredApplicationName

    IF (@UserId IS NULL)
    BEGIN
        GOTO Cleanup
    END

    -- Delete from Membership table if (@TablesToDeleteFrom & 1) is set
    IF ((@TablesToDeleteFrom & 1) <> 0 AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_MembershipUsers'') AND (type = ''V''))))
    BEGIN
        DELETE FROM dbo.aspnet_Membership WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
               @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_UsersInRoles table if (@TablesToDeleteFrom & 2) is set
    IF ((@TablesToDeleteFrom & 2) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_UsersInRoles'') AND (type = ''V''))) )
    BEGIN
        DELETE FROM dbo.aspnet_UsersInRoles WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_Profile table if (@TablesToDeleteFrom & 4) is set
    IF ((@TablesToDeleteFrom & 4) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_Profiles'') AND (type = ''V''))) )
    BEGIN
        DELETE FROM dbo.aspnet_Profile WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_PersonalizationPerUser table if (@TablesToDeleteFrom & 8) is set
    IF ((@TablesToDeleteFrom & 8) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_WebPartState_User'') AND (type = ''V''))) )
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationPerUser WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_Users table if (@TablesToDeleteFrom & 1,2,4 & 8) are all set
    IF ((@TablesToDeleteFrom & 1) <> 0 AND
        (@TablesToDeleteFrom & 2) <> 0 AND
        (@TablesToDeleteFrom & 4) <> 0 AND
        (@TablesToDeleteFrom & 8) <> 0 AND
        (EXISTS (SELECT UserId FROM dbo.aspnet_Users WHERE @UserId = UserId)))
    BEGIN
        DELETE FROM dbo.aspnet_Users WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    IF( @TranStarted = 1 )
    BEGIN
	    SET @TranStarted = 0
	    COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:
    SET @NumTablesDeletedFrom = 0

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
	    ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_DeleteRole]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_DeleteRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Roles_DeleteRole]
    @ApplicationName            nvarchar(256),
    @RoleName                   nvarchar(256),
    @DeleteOnlyIfRoleIsEmpty    bit
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
        SET @TranStarted = 0

    DECLARE @RoleId   uniqueidentifier
    SELECT  @RoleId = NULL
    SELECT  @RoleId = RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId

    IF (@RoleId IS NULL)
    BEGIN
        SELECT @ErrorCode = 1
        GOTO Cleanup
    END
    IF (@DeleteOnlyIfRoleIsEmpty <> 0)
    BEGIN
        IF (EXISTS (SELECT RoleId FROM dbo.aspnet_UsersInRoles  WHERE @RoleId = RoleId))
        BEGIN
            SELECT @ErrorCode = 2
            GOTO Cleanup
        END
    END


    DELETE FROM dbo.aspnet_UsersInRoles  WHERE @RoleId = RoleId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    DELETE FROM dbo.aspnet_Roles WHERE @RoleId = RoleId  AND ApplicationId = @ApplicationId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        COMMIT TRANSACTION
    END

    RETURN(0)

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UpdateUserInfo]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UpdateUserInfo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_UpdateUserInfo]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @IsPasswordCorrect              bit,
    @UpdateLastLoginActivityDate    bit,
    @MaxInvalidPasswordAttempts     int,
    @PasswordAttemptWindow          int,
    @CurrentTimeUtc                 datetime,
    @LastLoginDate                  datetime,
    @LastActivityDate               datetime
AS
BEGIN
    DECLARE @UserId                                 uniqueidentifier
    DECLARE @IsApproved                             bit
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId,
            @IsApproved = m.IsApproved,
            @IsLockedOut = m.IsLockedOut,
            @LastLockoutDate = m.LastLockoutDate,
            @FailedPasswordAttemptCount = m.FailedPasswordAttemptCount,
            @FailedPasswordAttemptWindowStart = m.FailedPasswordAttemptWindowStart,
            @FailedPasswordAnswerAttemptCount = m.FailedPasswordAnswerAttemptCount,
            @FailedPasswordAnswerAttemptWindowStart = m.FailedPasswordAnswerAttemptWindowStart
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m WITH ( UPDLOCK )
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF ( @@rowcount = 0 )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    IF( @IsLockedOut = 1 )
    BEGIN
        GOTO Cleanup
    END

    IF( @IsPasswordCorrect = 0 )
    BEGIN
        IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAttemptWindowStart ) )
        BEGIN
            SET @FailedPasswordAttemptWindowStart = @CurrentTimeUtc
            SET @FailedPasswordAttemptCount = 1
        END
        ELSE
        BEGIN
            SET @FailedPasswordAttemptWindowStart = @CurrentTimeUtc
            SET @FailedPasswordAttemptCount = @FailedPasswordAttemptCount + 1
        END

        BEGIN
            IF( @FailedPasswordAttemptCount >= @MaxInvalidPasswordAttempts )
            BEGIN
                SET @IsLockedOut = 1
                SET @LastLockoutDate = @CurrentTimeUtc
            END
        END
    END
    ELSE
    BEGIN
        IF( @FailedPasswordAttemptCount > 0 OR @FailedPasswordAnswerAttemptCount > 0 )
        BEGIN
            SET @FailedPasswordAttemptCount = 0
            SET @FailedPasswordAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )
            SET @FailedPasswordAnswerAttemptCount = 0
            SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )
            SET @LastLockoutDate = CONVERT( datetime, ''17540101'', 112 )
        END
    END

    IF( @UpdateLastLoginActivityDate = 1 )
    BEGIN
        UPDATE  dbo.aspnet_Users
        SET     LastActivityDate = @LastActivityDate
        WHERE   @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END

        UPDATE  dbo.aspnet_Membership
        SET     LastLoginDate = @LastLoginDate
        WHERE   UserId = @UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END


    UPDATE dbo.aspnet_Membership
    SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
        FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
        FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
        FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
        FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
    WHERE @UserId = UserId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UpdateUser]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UpdateUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_UpdateUser]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @Email                nvarchar(256),
    @Comment              ntext,
    @IsApproved           bit,
    @LastLoginDate        datetime,
    @LastActivityDate     datetime,
    @UniqueEmail          int,
    @CurrentTimeUtc       datetime
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId, @ApplicationId = a.ApplicationId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF (@UserId IS NULL)
        RETURN(1)

    IF (@UniqueEmail = 1)
    BEGIN
        IF (EXISTS (SELECT *
                    FROM  dbo.aspnet_Membership WITH (UPDLOCK, HOLDLOCK)
                    WHERE ApplicationId = @ApplicationId  AND @UserId <> UserId AND LoweredEmail = LOWER(@Email)))
        BEGIN
            RETURN(7)
        END
    END

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
	SET @TranStarted = 0

    UPDATE dbo.aspnet_Users WITH (ROWLOCK)
    SET
         LastActivityDate = @LastActivityDate
    WHERE
       @UserId = UserId

    IF( @@ERROR <> 0 )
        GOTO Cleanup

    UPDATE dbo.aspnet_Membership WITH (ROWLOCK)
    SET
         Email            = @Email,
         LoweredEmail     = LOWER(@Email),
         Comment          = @Comment,
         IsApproved       = @IsApproved,
         LastLoginDate    = @LastLoginDate
    WHERE
       @UserId = UserId

    IF( @@ERROR <> 0 )
        GOTO Cleanup

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN -1
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UnlockUser]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UnlockUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_UnlockUser]
    @ApplicationName                         nvarchar(256),
    @UserName                                nvarchar(256)
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF ( @UserId IS NULL )
        RETURN 1

    UPDATE dbo.aspnet_Membership
    SET IsLockedOut = 0,
        FailedPasswordAttemptCount = 0,
        FailedPasswordAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 ),
        FailedPasswordAnswerAttemptCount = 0,
        FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 ),
        LastLockoutDate = CONVERT( datetime, ''17540101'', 112 )
    WHERE @UserId = UserId

    RETURN 0
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_SetPassword]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_SetPassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_SetPassword]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @NewPassword      nvarchar(128),
    @PasswordSalt     nvarchar(128),
    @CurrentTimeUtc   datetime,
    @PasswordFormat   int = 0
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF (@UserId IS NULL)
        RETURN(1)

    UPDATE dbo.aspnet_Membership
    SET Password = @NewPassword, PasswordFormat = @PasswordFormat, PasswordSalt = @PasswordSalt,
        LastPasswordChangedDate = @CurrentTimeUtc
    WHERE @UserId = UserId
    RETURN(0)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_ResetPassword]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_ResetPassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_ResetPassword]
    @ApplicationName             nvarchar(256),
    @UserName                    nvarchar(256),
    @NewPassword                 nvarchar(128),
    @MaxInvalidPasswordAttempts  int,
    @PasswordAttemptWindow       int,
    @PasswordSalt                nvarchar(128),
    @CurrentTimeUtc              datetime,
    @PasswordFormat              int = 0,
    @PasswordAnswer              nvarchar(128) = NULL
AS
BEGIN
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @UserId                                 uniqueidentifier
    SET     @UserId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF ( @UserId IS NULL )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    SELECT @IsLockedOut = IsLockedOut,
           @LastLockoutDate = LastLockoutDate,
           @FailedPasswordAttemptCount = FailedPasswordAttemptCount,
           @FailedPasswordAttemptWindowStart = FailedPasswordAttemptWindowStart,
           @FailedPasswordAnswerAttemptCount = FailedPasswordAnswerAttemptCount,
           @FailedPasswordAnswerAttemptWindowStart = FailedPasswordAnswerAttemptWindowStart
    FROM dbo.aspnet_Membership WITH ( UPDLOCK )
    WHERE @UserId = UserId

    IF( @IsLockedOut = 1 )
    BEGIN
        SET @ErrorCode = 99
        GOTO Cleanup
    END

    UPDATE dbo.aspnet_Membership
    SET    Password = @NewPassword,
           LastPasswordChangedDate = @CurrentTimeUtc,
           PasswordFormat = @PasswordFormat,
           PasswordSalt = @PasswordSalt
    WHERE  @UserId = UserId AND
           ( ( @PasswordAnswer IS NULL ) OR ( LOWER( PasswordAnswer ) = LOWER( @PasswordAnswer ) ) )

    IF ( @@ROWCOUNT = 0 )
        BEGIN
            IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAnswerAttemptWindowStart ) )
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = 1
            END
            ELSE
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount + 1
            END

            BEGIN
                IF( @FailedPasswordAnswerAttemptCount >= @MaxInvalidPasswordAttempts )
                BEGIN
                    SET @IsLockedOut = 1
                    SET @LastLockoutDate = @CurrentTimeUtc
                END
            END

            SET @ErrorCode = 3
        END
    ELSE
        BEGIN
            IF( @FailedPasswordAnswerAttemptCount > 0 )
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = 0
                SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )
            END
        END

    IF( NOT ( @PasswordAnswer IS NULL ) )
    BEGIN
        UPDATE dbo.aspnet_Membership
        SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
            FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
            FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
            FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
            FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
        WHERE @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByUserId]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByUserId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByUserId]
    @UserId               uniqueidentifier,
    @CurrentTimeUtc       datetime,
    @UpdateLastActivity   bit = 0
AS
BEGIN
    IF ( @UpdateLastActivity = 1 )
    BEGIN
        UPDATE   dbo.aspnet_Users
        SET      LastActivityDate = @CurrentTimeUtc
        FROM     dbo.aspnet_Users
        WHERE    @UserId = UserId

        IF ( @@ROWCOUNT = 0 ) -- User ID not found
            RETURN -1
    END

    SELECT  m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate, m.LastLoginDate, u.LastActivityDate,
            m.LastPasswordChangedDate, u.UserName, m.IsLockedOut,
            m.LastLockoutDate
    FROM    dbo.aspnet_Users u, dbo.aspnet_Membership m
    WHERE   @UserId = u.UserId AND u.UserId = m.UserId

    IF ( @@ROWCOUNT = 0 ) -- User ID not found
       RETURN -1

    RETURN 0
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByName]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByName]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @CurrentTimeUtc       datetime,
    @UpdateLastActivity   bit = 0
AS
BEGIN
    DECLARE @UserId uniqueidentifier

    IF (@UpdateLastActivity = 1)
    BEGIN
        -- select user ID from aspnet_users table
        SELECT TOP 1 @UserId = u.UserId
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE    LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                LOWER(@UserName) = u.LoweredUserName AND u.UserId = m.UserId

        IF (@@ROWCOUNT = 0) -- Username not found
            RETURN -1

        UPDATE   dbo.aspnet_Users
        SET      LastActivityDate = @CurrentTimeUtc
        WHERE    @UserId = UserId

        SELECT m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
                m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate,
                u.UserId, m.IsLockedOut, m.LastLockoutDate
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE  @UserId = u.UserId AND u.UserId = m.UserId 
    END
    ELSE
    BEGIN
        SELECT TOP 1 m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
                m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate,
                u.UserId, m.IsLockedOut,m.LastLockoutDate
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE    LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                LOWER(@UserName) = u.LoweredUserName AND u.UserId = m.UserId

        IF (@@ROWCOUNT = 0) -- Username not found
            RETURN -1
    END

    RETURN 0
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByEmail]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByEmail]
    @ApplicationName  nvarchar(256),
    @Email            nvarchar(256)
AS
BEGIN
    IF( @Email IS NULL )
        SELECT  u.UserName
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                u.UserId = m.UserId AND
                m.LoweredEmail IS NULL
    ELSE
        SELECT  u.UserName
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                u.UserId = m.UserId AND
                LOWER(@Email) = m.LoweredEmail

    IF (@@rowcount = 0)
        RETURN(1)
    RETURN(0)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetPasswordWithFormat]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetPasswordWithFormat]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetPasswordWithFormat]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @UpdateLastLoginActivityDate    bit,
    @CurrentTimeUtc                 datetime
AS
BEGIN
    DECLARE @IsLockedOut                        bit
    DECLARE @UserId                             uniqueidentifier
    DECLARE @Password                           nvarchar(128)
    DECLARE @PasswordSalt                       nvarchar(128)
    DECLARE @PasswordFormat                     int
    DECLARE @FailedPasswordAttemptCount         int
    DECLARE @FailedPasswordAnswerAttemptCount   int
    DECLARE @IsApproved                         bit
    DECLARE @LastActivityDate                   datetime
    DECLARE @LastLoginDate                      datetime

    SELECT  @UserId          = NULL

    SELECT  @UserId = u.UserId, @IsLockedOut = m.IsLockedOut, @Password=Password, @PasswordFormat=PasswordFormat,
            @PasswordSalt=PasswordSalt, @FailedPasswordAttemptCount=FailedPasswordAttemptCount,
		    @FailedPasswordAnswerAttemptCount=FailedPasswordAnswerAttemptCount, @IsApproved=IsApproved,
            @LastActivityDate = LastActivityDate, @LastLoginDate = LastLoginDate
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF (@UserId IS NULL)
        RETURN 1

    IF (@IsLockedOut = 1)
        RETURN 99

    SELECT   @Password, @PasswordFormat, @PasswordSalt, @FailedPasswordAttemptCount,
             @FailedPasswordAnswerAttemptCount, @IsApproved, @LastLoginDate, @LastActivityDate

    IF (@UpdateLastLoginActivityDate = 1 AND @IsApproved = 1)
    BEGIN
        UPDATE  dbo.aspnet_Membership
        SET     LastLoginDate = @CurrentTimeUtc
        WHERE   UserId = @UserId

        UPDATE  dbo.aspnet_Users
        SET     LastActivityDate = @CurrentTimeUtc
        WHERE   @UserId = UserId
    END


    RETURN 0
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetPassword]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetPassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetPassword]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @MaxInvalidPasswordAttempts     int,
    @PasswordAttemptWindow          int,
    @CurrentTimeUtc                 datetime,
    @PasswordAnswer                 nvarchar(128) = NULL
AS
BEGIN
    DECLARE @UserId                                 uniqueidentifier
    DECLARE @PasswordFormat                         int
    DECLARE @Password                               nvarchar(128)
    DECLARE @passAns                                nvarchar(128)
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId,
            @Password = m.Password,
            @passAns = m.PasswordAnswer,
            @PasswordFormat = m.PasswordFormat,
            @IsLockedOut = m.IsLockedOut,
            @LastLockoutDate = m.LastLockoutDate,
            @FailedPasswordAttemptCount = m.FailedPasswordAttemptCount,
            @FailedPasswordAttemptWindowStart = m.FailedPasswordAttemptWindowStart,
            @FailedPasswordAnswerAttemptCount = m.FailedPasswordAnswerAttemptCount,
            @FailedPasswordAnswerAttemptWindowStart = m.FailedPasswordAnswerAttemptWindowStart
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m WITH ( UPDLOCK )
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF ( @@rowcount = 0 )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    IF( @IsLockedOut = 1 )
    BEGIN
        SET @ErrorCode = 99
        GOTO Cleanup
    END

    IF ( NOT( @PasswordAnswer IS NULL ) )
    BEGIN
        IF( ( @passAns IS NULL ) OR ( LOWER( @passAns ) <> LOWER( @PasswordAnswer ) ) )
        BEGIN
            IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAnswerAttemptWindowStart ) )
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = 1
            END
            ELSE
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount + 1
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
            END

            BEGIN
                IF( @FailedPasswordAnswerAttemptCount >= @MaxInvalidPasswordAttempts )
                BEGIN
                    SET @IsLockedOut = 1
                    SET @LastLockoutDate = @CurrentTimeUtc
                END
            END

            SET @ErrorCode = 3
        END
        ELSE
        BEGIN
            IF( @FailedPasswordAnswerAttemptCount > 0 )
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = 0
                SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )
            END
        END

        UPDATE dbo.aspnet_Membership
        SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
            FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
            FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
            FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
            FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
        WHERE @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    IF( @ErrorCode = 0 )
        SELECT @Password, @PasswordFormat

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetNumberOfUsersOnline]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetNumberOfUsersOnline]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetNumberOfUsersOnline]
    @ApplicationName            nvarchar(256),
    @MinutesSinceLastInActive   int,
    @CurrentTimeUtc             datetime
AS
BEGIN
    DECLARE @DateActive datetime
    SELECT  @DateActive = DATEADD(minute,  -(@MinutesSinceLastInActive), @CurrentTimeUtc)

    DECLARE @NumOnline int
    SELECT  @NumOnline = COUNT(*)
    FROM    dbo.aspnet_Users u(NOLOCK),
            dbo.aspnet_Applications a(NOLOCK),
            dbo.aspnet_Membership m(NOLOCK)
    WHERE   u.ApplicationId = a.ApplicationId                  AND
            LastActivityDate > @DateActive                     AND
            a.LoweredApplicationName = LOWER(@ApplicationName) AND
            u.UserId = m.UserId
    RETURN(@NumOnline)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetAllUsers]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetAllUsers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetAllUsers]
    @ApplicationName       nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0


    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
    SELECT u.UserId
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u
    WHERE  u.ApplicationId = @ApplicationId AND u.UserId = m.UserId
    ORDER BY u.UserName

    SELECT @TotalRecords = @@ROWCOUNT

    SELECT u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY u.UserName
    RETURN @TotalRecords
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_FindUsersByName]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_FindUsersByName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_FindUsersByName]
    @ApplicationName       nvarchar(256),
    @UserNameToMatch       nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
        SELECT u.UserId
        FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND u.LoweredUserName LIKE LOWER(@UserNameToMatch)
        ORDER BY u.UserName


    SELECT  u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY u.UserName

    SELECT  @TotalRecords = COUNT(*)
    FROM    #PageIndexForUsers
    RETURN @TotalRecords
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_FindUsersByEmail]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_FindUsersByEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_FindUsersByEmail]
    @ApplicationName       nvarchar(256),
    @EmailToMatch          nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    IF( @EmailToMatch IS NULL )
        INSERT INTO #PageIndexForUsers (UserId)
            SELECT u.UserId
            FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
            WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND m.Email IS NULL
            ORDER BY m.LoweredEmail
    ELSE
        INSERT INTO #PageIndexForUsers (UserId)
            SELECT u.UserId
            FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
            WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND m.LoweredEmail LIKE LOWER(@EmailToMatch)
            ORDER BY m.LoweredEmail

    SELECT  u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY m.LoweredEmail

    SELECT  @TotalRecords = COUNT(*)
    FROM    #PageIndexForUsers
    RETURN @TotalRecords
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_CreateUser]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_CreateUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_CreateUser]
    @ApplicationName                        nvarchar(256),
    @UserName                               nvarchar(256),
    @Password                               nvarchar(128),
    @PasswordSalt                           nvarchar(128),
    @Email                                  nvarchar(256),
    @PasswordQuestion                       nvarchar(256),
    @PasswordAnswer                         nvarchar(128),
    @IsApproved                             bit,
    @CurrentTimeUtc                         datetime,
    @CreateDate                             datetime = NULL,
    @UniqueEmail                            int      = 0,
    @PasswordFormat                         int      = 0,
    @UserId                                 uniqueidentifier OUTPUT
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @NewUserId uniqueidentifier
    SELECT @NewUserId = NULL

    DECLARE @IsLockedOut bit
    SET @IsLockedOut = 0

    DECLARE @LastLockoutDate  datetime
    SET @LastLockoutDate = CONVERT( datetime, ''17540101'', 112 )

    DECLARE @FailedPasswordAttemptCount int
    SET @FailedPasswordAttemptCount = 0

    DECLARE @FailedPasswordAttemptWindowStart  datetime
    SET @FailedPasswordAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )

    DECLARE @FailedPasswordAnswerAttemptCount int
    SET @FailedPasswordAnswerAttemptCount = 0

    DECLARE @FailedPasswordAnswerAttemptWindowStart  datetime
    SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )

    DECLARE @NewUserCreated bit
    DECLARE @ReturnValue   int
    SET @ReturnValue = 0

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    SET @CreateDate = @CurrentTimeUtc

    SELECT  @NewUserId = UserId FROM dbo.aspnet_Users WHERE LOWER(@UserName) = LoweredUserName AND @ApplicationId = ApplicationId
    IF ( @NewUserId IS NULL )
    BEGIN
        SET @NewUserId = @UserId
        EXEC @ReturnValue = dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, 0, @CreateDate, @NewUserId OUTPUT
        SET @NewUserCreated = 1
    END
    ELSE
    BEGIN
        SET @NewUserCreated = 0
        IF( @NewUserId <> @UserId AND @UserId IS NOT NULL )
        BEGIN
            SET @ErrorCode = 6
            GOTO Cleanup
        END
    END

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @ReturnValue = -1 )
    BEGIN
        SET @ErrorCode = 10
        GOTO Cleanup
    END

    IF ( EXISTS ( SELECT UserId
                  FROM   dbo.aspnet_Membership
                  WHERE  @NewUserId = UserId ) )
    BEGIN
        SET @ErrorCode = 6
        GOTO Cleanup
    END

    SET @UserId = @NewUserId

    IF (@UniqueEmail = 1)
    BEGIN
        IF (EXISTS (SELECT *
                    FROM  dbo.aspnet_Membership m WITH ( UPDLOCK, HOLDLOCK )
                    WHERE ApplicationId = @ApplicationId AND LoweredEmail = LOWER(@Email)))
        BEGIN
            SET @ErrorCode = 7
            GOTO Cleanup
        END
    END

    IF (@NewUserCreated = 0)
    BEGIN
        UPDATE dbo.aspnet_Users
        SET    LastActivityDate = @CreateDate
        WHERE  @UserId = UserId
        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    INSERT INTO dbo.aspnet_Membership
                ( ApplicationId,
                  UserId,
                  Password,
                  PasswordSalt,
                  Email,
                  LoweredEmail,
                  PasswordQuestion,
                  PasswordAnswer,
                  PasswordFormat,
                  IsApproved,
                  IsLockedOut,
                  CreateDate,
                  LastLoginDate,
                  LastPasswordChangedDate,
                  LastLockoutDate,
                  FailedPasswordAttemptCount,
                  FailedPasswordAttemptWindowStart,
                  FailedPasswordAnswerAttemptCount,
                  FailedPasswordAnswerAttemptWindowStart )
         VALUES ( @ApplicationId,
                  @UserId,
                  @Password,
                  @PasswordSalt,
                  @Email,
                  LOWER(@Email),
                  @PasswordQuestion,
                  @PasswordAnswer,
                  @PasswordFormat,
                  @IsApproved,
                  @IsLockedOut,
                  @CreateDate,
                  @CreateDate,
                  @CreateDate,
                  @LastLockoutDate,
                  @FailedPasswordAttemptCount,
                  @FailedPasswordAttemptWindowStart,
                  @FailedPasswordAnswerAttemptCount,
                  @FailedPasswordAnswerAttemptWindowStart )

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
	    SET @TranStarted = 0
	    COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]
    @ApplicationName       nvarchar(256),
    @UserName              nvarchar(256),
    @NewPasswordQuestion   nvarchar(256),
    @NewPasswordAnswer     nvarchar(128)
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Membership m, dbo.aspnet_Users u, dbo.aspnet_Applications a
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId
    IF (@UserId IS NULL)
    BEGIN
        RETURN(1)
    END

    UPDATE dbo.aspnet_Membership
    SET    PasswordQuestion = @NewPasswordQuestion, PasswordAnswer = @NewPasswordAnswer
    WHERE  UserId=@UserId
    RETURN(0)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_AnyDataInTables]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_AnyDataInTables]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_AnyDataInTables]
    @TablesToCheck int
AS
BEGIN
    -- Check Membership table if (@TablesToCheck & 1) is set
    IF ((@TablesToCheck & 1) <> 0 AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_MembershipUsers'') AND (type = ''V''))))
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Membership))
        BEGIN
            SELECT N''aspnet_Membership''
            RETURN
        END
    END

    -- Check aspnet_Roles table if (@TablesToCheck & 2) is set
    IF ((@TablesToCheck & 2) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_Roles'') AND (type = ''V''))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 RoleId FROM dbo.aspnet_Roles))
        BEGIN
            SELECT N''aspnet_Roles''
            RETURN
        END
    END

    -- Check aspnet_Profile table if (@TablesToCheck & 4) is set
    IF ((@TablesToCheck & 4) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_Profiles'') AND (type = ''V''))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Profile))
        BEGIN
            SELECT N''aspnet_Profile''
            RETURN
        END
    END

    -- Check aspnet_PersonalizationPerUser table if (@TablesToCheck & 8) is set
    IF ((@TablesToCheck & 8) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_WebPartState_User'') AND (type = ''V''))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_PersonalizationPerUser))
        BEGIN
            SELECT N''aspnet_PersonalizationPerUser''
            RETURN
        END
    END

    -- Check aspnet_PersonalizationPerUser table if (@TablesToCheck & 16) is set
    IF ((@TablesToCheck & 16) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''aspnet_WebEvent_LogEvent'') AND (type = ''P''))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 * FROM dbo.aspnet_WebEvent_Events))
        BEGIN
            SELECT N''aspnet_WebEvent_Events''
            RETURN
        END
    END

    -- Check aspnet_Users table if (@TablesToCheck & 1,2,4 & 8) are all set
    IF ((@TablesToCheck & 1) <> 0 AND
        (@TablesToCheck & 2) <> 0 AND
        (@TablesToCheck & 4) <> 0 AND
        (@TablesToCheck & 8) <> 0 AND
        (@TablesToCheck & 32) <> 0 AND
        (@TablesToCheck & 128) <> 0 AND
        (@TablesToCheck & 256) <> 0 AND
        (@TablesToCheck & 512) <> 0 AND
        (@TablesToCheck & 1024) <> 0)
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Users))
        BEGIN
            SELECT N''aspnet_Users''
            RETURN
        END
        IF (EXISTS(SELECT TOP 1 ApplicationId FROM dbo.aspnet_Applications))
        BEGIN
            SELECT N''aspnet_Applications''
            RETURN
        END
    END
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_ResetUserState]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_ResetUserState]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetUserState] (
    @Count                  int                 OUT,
    @ApplicationName        NVARCHAR(256),
    @InactiveSinceDate      DATETIME            = NULL,
    @UserName               NVARCHAR(256)       = NULL,
    @Path                   NVARCHAR(256)       = NULL)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationPerUser
        WHERE Id IN (SELECT PerUser.Id
                     FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
                     WHERE Paths.ApplicationId = @ApplicationId
                           AND PerUser.UserId = Users.UserId
                           AND PerUser.PathId = Paths.PathId
                           AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
                           AND (@UserName IS NULL OR Users.LoweredUserName = LOWER(@UserName))
                           AND (@Path IS NULL OR Paths.LoweredPath = LOWER(@Path)))

        SELECT @Count = @@ROWCOUNT
    END
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_ResetSharedState]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_ResetSharedState]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetSharedState] (
    @Count int OUT,
    @ApplicationName NVARCHAR(256),
    @Path NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationAllUsers
        WHERE PathId IN
            (SELECT AllUsers.PathId
             FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
             WHERE Paths.ApplicationId = @ApplicationId
                   AND AllUsers.PathId = Paths.PathId
                   AND Paths.LoweredPath = LOWER(@Path))

        SELECT @Count = @@ROWCOUNT
    END
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_GetCountOfState]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_GetCountOfState]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_GetCountOfState] (
    @Count int OUT,
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @Path NVARCHAR(256) = NULL,
    @UserName NVARCHAR(256) = NULL,
    @InactiveSinceDate DATETIME = NULL)
AS
BEGIN

    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
        IF (@AllUsersScope = 1)
            SELECT @Count = COUNT(*)
            FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
            WHERE Paths.ApplicationId = @ApplicationId
                  AND AllUsers.PathId = Paths.PathId
                  AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
        ELSE
            SELECT @Count = COUNT(*)
            FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
            WHERE Paths.ApplicationId = @ApplicationId
                  AND PerUser.UserId = Users.UserId
                  AND PerUser.PathId = Paths.PathId
                  AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
                  AND (@UserName IS NULL OR Users.LoweredUserName LIKE LOWER(@UserName))
                  AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_FindState]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_FindState]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_FindState] (
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @PageIndex              INT,
    @PageSize               INT,
    @Path NVARCHAR(256) = NULL,
    @UserName NVARCHAR(256) = NULL,
    @InactiveSinceDate DATETIME = NULL)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        RETURN

    -- Set the page bounds
    DECLARE @PageLowerBound INT
    DECLARE @PageUpperBound INT
    DECLARE @TotalRecords   INT
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table to store the selected results
    CREATE TABLE #PageIndex (
        IndexId int IDENTITY (0, 1) NOT NULL,
        ItemId UNIQUEIDENTIFIER
    )

    IF (@AllUsersScope = 1)
    BEGIN
        -- Insert into our temp table
        INSERT INTO #PageIndex (ItemId)
        SELECT Paths.PathId
        FROM dbo.aspnet_Paths Paths,
             ((SELECT Paths.PathId
               FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
               WHERE Paths.ApplicationId = @ApplicationId
                      AND AllUsers.PathId = Paths.PathId
                      AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              ) AS SharedDataPerPath
              FULL OUTER JOIN
              (SELECT DISTINCT Paths.PathId
               FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Paths Paths
               WHERE Paths.ApplicationId = @ApplicationId
                      AND PerUser.PathId = Paths.PathId
                      AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              ) AS UserDataPerPath
              ON SharedDataPerPath.PathId = UserDataPerPath.PathId
             )
        WHERE Paths.PathId = SharedDataPerPath.PathId OR Paths.PathId = UserDataPerPath.PathId
        ORDER BY Paths.Path ASC

        SELECT @TotalRecords = @@ROWCOUNT

        SELECT Paths.Path,
               SharedDataPerPath.LastUpdatedDate,
               SharedDataPerPath.SharedDataLength,
               UserDataPerPath.UserDataLength,
               UserDataPerPath.UserCount
        FROM dbo.aspnet_Paths Paths,
             ((SELECT PageIndex.ItemId AS PathId,
                      AllUsers.LastUpdatedDate AS LastUpdatedDate,
                      DATALENGTH(AllUsers.PageSettings) AS SharedDataLength
               FROM dbo.aspnet_PersonalizationAllUsers AllUsers, #PageIndex PageIndex
               WHERE AllUsers.PathId = PageIndex.ItemId
                     AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
              ) AS SharedDataPerPath
              FULL OUTER JOIN
              (SELECT PageIndex.ItemId AS PathId,
                      SUM(DATALENGTH(PerUser.PageSettings)) AS UserDataLength,
                      COUNT(*) AS UserCount
               FROM aspnet_PersonalizationPerUser PerUser, #PageIndex PageIndex
               WHERE PerUser.PathId = PageIndex.ItemId
                     AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
               GROUP BY PageIndex.ItemId
              ) AS UserDataPerPath
              ON SharedDataPerPath.PathId = UserDataPerPath.PathId
             )
        WHERE Paths.PathId = SharedDataPerPath.PathId OR Paths.PathId = UserDataPerPath.PathId
        ORDER BY Paths.Path ASC
    END
    ELSE
    BEGIN
        -- Insert into our temp table
        INSERT INTO #PageIndex (ItemId)
        SELECT PerUser.Id
        FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
        WHERE Paths.ApplicationId = @ApplicationId
              AND PerUser.UserId = Users.UserId
              AND PerUser.PathId = Paths.PathId
              AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              AND (@UserName IS NULL OR Users.LoweredUserName LIKE LOWER(@UserName))
              AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
        ORDER BY Paths.Path ASC, Users.UserName ASC

        SELECT @TotalRecords = @@ROWCOUNT

        SELECT Paths.Path, PerUser.LastUpdatedDate, DATALENGTH(PerUser.PageSettings), Users.UserName, Users.LastActivityDate
        FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths, #PageIndex PageIndex
        WHERE PerUser.Id = PageIndex.ItemId
              AND PerUser.UserId = Users.UserId
              AND PerUser.PathId = Paths.PathId
              AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
        ORDER BY Paths.Path ASC, Users.UserName ASC
    END

    RETURN @TotalRecords
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_DeleteAllState]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_DeleteAllState]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_DeleteAllState] (
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @Count int OUT)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        IF (@AllUsersScope = 1)
            DELETE FROM aspnet_PersonalizationAllUsers
            WHERE PathId IN
               (SELECT Paths.PathId
                FROM dbo.aspnet_Paths Paths
                WHERE Paths.ApplicationId = @ApplicationId)
        ELSE
            DELETE FROM aspnet_PersonalizationPerUser
            WHERE PathId IN
               (SELECT Paths.PathId
                FROM dbo.aspnet_Paths Paths
                WHERE Paths.ApplicationId = @ApplicationId)

        SELECT @Count = @@ROWCOUNT
    END
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_SetPageSettings]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_SetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_SetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @PageSettings     IMAGE,
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Paths_CreatePath @ApplicationId, @Path, @PathId OUTPUT
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, 0, @CurrentTimeUtc, @UserId OUTPUT
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    IF (EXISTS(SELECT PathId FROM dbo.aspnet_PersonalizationPerUser WHERE UserId = @UserId AND PathId = @PathId))
        UPDATE dbo.aspnet_PersonalizationPerUser SET PageSettings = @PageSettings, LastUpdatedDate = @CurrentTimeUtc WHERE UserId = @UserId AND PathId = @PathId
    ELSE
        INSERT INTO dbo.aspnet_PersonalizationPerUser(UserId, PathId, PageSettings, LastUpdatedDate) VALUES (@UserId, @PathId, @PageSettings, @CurrentTimeUtc)
    RETURN 0
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_ResetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        RETURN
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    DELETE FROM dbo.aspnet_PersonalizationPerUser WHERE PathId = @PathId AND UserId = @UserId
    RETURN 0
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_GetPageSettings]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_GetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_GetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        RETURN
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    SELECT p.PageSettings FROM dbo.aspnet_PersonalizationPerUser p WHERE p.PathId = @PathId AND p.UserId = @UserId
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_SetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path             NVARCHAR(256),
    @PageSettings     IMAGE,
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Paths_CreatePath @ApplicationId, @Path, @PathId OUTPUT
    END

    IF (EXISTS(SELECT PathId FROM dbo.aspnet_PersonalizationAllUsers WHERE PathId = @PathId))
        UPDATE dbo.aspnet_PersonalizationAllUsers SET PageSettings = @PageSettings, LastUpdatedDate = @CurrentTimeUtc WHERE PathId = @PathId
    ELSE
        INSERT INTO dbo.aspnet_PersonalizationAllUsers(PathId, PageSettings, LastUpdatedDate) VALUES (@PathId, @PageSettings, @CurrentTimeUtc)
    RETURN 0
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path              NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    DELETE FROM dbo.aspnet_PersonalizationAllUsers WHERE PathId = @PathId
    RETURN 0
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_GetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path              NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT p.PageSettings FROM dbo.aspnet_PersonalizationAllUsers p WHERE p.PathId = @PathId
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_SetProperties]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_SetProperties]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Profile_SetProperties]
    @ApplicationName        nvarchar(256),
    @PropertyNames          ntext,
    @PropertyValuesString   ntext,
    @PropertyValuesBinary   image,
    @UserName               nvarchar(256),
    @IsUserAnonymous        bit,
    @CurrentTimeUtc         datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
       BEGIN TRANSACTION
       SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    DECLARE @UserId uniqueidentifier
    DECLARE @LastActivityDate datetime
    SELECT  @UserId = NULL
    SELECT  @LastActivityDate = @CurrentTimeUtc

    SELECT @UserId = UserId
    FROM   dbo.aspnet_Users
    WHERE  ApplicationId = @ApplicationId AND LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
        EXEC dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, @IsUserAnonymous, @LastActivityDate, @UserId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    UPDATE dbo.aspnet_Users
    SET    LastActivityDate=@CurrentTimeUtc
    WHERE  UserId = @UserId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF (EXISTS( SELECT *
               FROM   dbo.aspnet_Profile
               WHERE  UserId = @UserId))
        UPDATE dbo.aspnet_Profile
        SET    PropertyNames=@PropertyNames, PropertyValuesString = @PropertyValuesString,
               PropertyValuesBinary = @PropertyValuesBinary, LastUpdatedDate=@CurrentTimeUtc
        WHERE  UserId = @UserId
    ELSE
        INSERT INTO dbo.aspnet_Profile(UserId, PropertyNames, PropertyValuesString, PropertyValuesBinary, LastUpdatedDate)
             VALUES (@UserId, @PropertyNames, @PropertyValuesString, @PropertyValuesBinary, @CurrentTimeUtc)

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
    	SET @TranStarted = 0
    	COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetProperties]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetProperties]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Profile_GetProperties]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @CurrentTimeUtc       datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN

    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL

    SELECT @UserId = UserId
    FROM   dbo.aspnet_Users
    WHERE  ApplicationId = @ApplicationId AND LoweredUserName = LOWER(@UserName)

    IF (@UserId IS NULL)
        RETURN
    SELECT TOP 1 PropertyNames, PropertyValuesString, PropertyValuesBinary
    FROM         dbo.aspnet_Profile
    WHERE        UserId = @UserId

    IF (@@ROWCOUNT > 0)
    BEGIN
        UPDATE dbo.aspnet_Users
        SET    LastActivityDate=@CurrentTimeUtc
        WHERE  UserId = @UserId
    END
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetProfiles]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetProfiles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Profile_GetProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @PageIndex              int,
    @PageSize               int,
    @UserNameToMatch        nvarchar(256) = NULL,
    @InactiveSinceDate      datetime      = NULL
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
        SELECT  u.UserId
        FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p
        WHERE   ApplicationId = @ApplicationId
            AND u.UserId = p.UserId
            AND (@InactiveSinceDate IS NULL OR LastActivityDate <= @InactiveSinceDate)
            AND (     (@ProfileAuthOptions = 2)
                   OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                   OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
                 )
            AND (@UserNameToMatch IS NULL OR LoweredUserName LIKE LOWER(@UserNameToMatch))
        ORDER BY UserName

    SELECT  u.UserName, u.IsAnonymous, u.LastActivityDate, p.LastUpdatedDate,
            DATALENGTH(p.PropertyNames) + DATALENGTH(p.PropertyValuesString) + DATALENGTH(p.PropertyValuesBinary)
    FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p, #PageIndexForUsers i
    WHERE   u.UserId = p.UserId AND p.UserId = i.UserId AND i.IndexId >= @PageLowerBound AND i.IndexId <= @PageUpperBound

    SELECT COUNT(*)
    FROM   #PageIndexForUsers

    DROP TABLE #PageIndexForUsers
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @InactiveSinceDate      datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
    BEGIN
        SELECT 0
        RETURN
    END

    SELECT  COUNT(*)
    FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p
    WHERE   ApplicationId = @ApplicationId
        AND u.UserId = p.UserId
        AND (LastActivityDate <= @InactiveSinceDate)
        AND (
                (@ProfileAuthOptions = 2)
                OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
            )
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_DeleteProfiles]    Script Date: 09/25/2011 10:57:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_DeleteProfiles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Profile_DeleteProfiles]
    @ApplicationName        nvarchar(256),
    @UserNames              nvarchar(4000)
AS
BEGIN
    DECLARE @UserName     nvarchar(256)
    DECLARE @CurrentPos   int
    DECLARE @NextPos      int
    DECLARE @NumDeleted   int
    DECLARE @DeletedUser  int
    DECLARE @TranStarted  bit
    DECLARE @ErrorCode    int

    SET @ErrorCode = 0
    SET @CurrentPos = 1
    SET @NumDeleted = 0
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    WHILE (@CurrentPos <= LEN(@UserNames))
    BEGIN
        SELECT @NextPos = CHARINDEX(N'','', @UserNames,  @CurrentPos)
        IF (@NextPos = 0 OR @NextPos IS NULL)
            SELECT @NextPos = LEN(@UserNames) + 1

        SELECT @UserName = SUBSTRING(@UserNames, @CurrentPos, @NextPos - @CurrentPos)
        SELECT @CurrentPos = @NextPos+1

        IF (LEN(@UserName) > 0)
        BEGIN
            SELECT @DeletedUser = 0
            EXEC dbo.aspnet_Users_DeleteUser @ApplicationName, @UserName, 4, @DeletedUser OUTPUT
            IF( @@ERROR <> 0 )
            BEGIN
                SET @ErrorCode = -1
                GOTO Cleanup
            END
            IF (@DeletedUser <> 0)
                SELECT @NumDeleted = @NumDeleted + 1
        END
    END
    SELECT @NumDeleted
    IF (@TranStarted = 1)
    BEGIN
    	SET @TranStarted = 0
    	COMMIT TRANSACTION
    END
    SET @TranStarted = 0

    RETURN 0

Cleanup:
    IF (@TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END
    RETURN @ErrorCode
END' 
END
GO
/****** Object:  Default [DF__aspnet_Ap__Appli__08EA5793]    Script Date: 09/25/2011 10:57:53 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Ap__Appli__08EA5793]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Applications]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Ap__Appli__08EA5793]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Applications] ADD  CONSTRAINT [DF__aspnet_Ap__Appli__08EA5793]  DEFAULT (newid()) FOR [ApplicationId]
END


End
GO
/****** Object:  Default [DF__aspnet_Me__Passw__239E4DCF]    Script Date: 09/25/2011 10:57:53 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Me__Passw__239E4DCF]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Me__Passw__239E4DCF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Membership] ADD  CONSTRAINT [DF__aspnet_Me__Passw__239E4DCF]  DEFAULT ((0)) FOR [PasswordFormat]
END


End
GO
/****** Object:  Default [DF__aspnet_Pa__PathI__5BE2A6F2]    Script Date: 09/25/2011 10:57:53 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Pa__PathI__5BE2A6F2]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Pa__PathI__5BE2A6F2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Paths] ADD  CONSTRAINT [DF__aspnet_Pa__PathI__5BE2A6F2]  DEFAULT (newid()) FOR [PathId]
END


End
GO
/****** Object:  Default [DF__aspnet_Perso__Id__6754599E]    Script Date: 09/25/2011 10:57:53 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Perso__Id__6754599E]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Perso__Id__6754599E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] ADD  CONSTRAINT [DF__aspnet_Perso__Id__6754599E]  DEFAULT (newid()) FOR [Id]
END


End
GO
/****** Object:  Default [DF__aspnet_Ro__RoleI__44FF419A]    Script Date: 09/25/2011 10:57:53 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Ro__RoleI__44FF419A]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Ro__RoleI__44FF419A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Roles] ADD  CONSTRAINT [DF__aspnet_Ro__RoleI__44FF419A]  DEFAULT (newid()) FOR [RoleId]
END


End
GO
/****** Object:  Default [DF__aspnet_Us__UserI__0EA330E9]    Script Date: 09/25/2011 10:57:53 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__UserI__0EA330E9]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Us__UserI__0EA330E9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] ADD  CONSTRAINT [DF__aspnet_Us__UserI__0EA330E9]  DEFAULT (newid()) FOR [UserId]
END


End
GO
/****** Object:  Default [DF__aspnet_Us__Mobil__0F975522]    Script Date: 09/25/2011 10:57:53 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__Mobil__0F975522]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Us__Mobil__0F975522]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] ADD  CONSTRAINT [DF__aspnet_Us__Mobil__0F975522]  DEFAULT (NULL) FOR [MobileAlias]
END


End
GO
/****** Object:  Default [DF__aspnet_Us__IsAno__108B795B]    Script Date: 09/25/2011 10:57:53 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__IsAno__108B795B]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Us__IsAno__108B795B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] ADD  CONSTRAINT [DF__aspnet_Us__IsAno__108B795B]  DEFAULT ((0)) FOR [IsAnonymous]
END


End
GO
/****** Object:  Default [DF_Orders_TaxAmount]    Script Date: 09/25/2011 10:57:53 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Orders_TaxAmount]') AND parent_object_id = OBJECT_ID(N'[dbo].[Orders]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Orders_TaxAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Orders] ADD  CONSTRAINT [DF_Orders_TaxAmount]  DEFAULT ((0.0)) FOR [TaxAmount]
END


End
GO
/****** Object:  Default [DF_Orders_Total]    Script Date: 09/25/2011 10:57:53 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Orders_Total]') AND parent_object_id = OBJECT_ID(N'[dbo].[Orders]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Orders_Total]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Orders] ADD  CONSTRAINT [DF_Orders_Total]  DEFAULT ((0.0)) FOR [Total]
END


End
GO
/****** Object:  Default [DF_Rooms_CurrentStep]    Script Date: 09/25/2011 10:57:53 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Rooms_CurrentStep]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Rooms_CurrentStep]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF_Rooms_CurrentStep]  DEFAULT ((1)) FOR [CurrentStepId]
END


End
GO
/****** Object:  Default [DF_Rooms_RoomPaidFor]    Script Date: 09/25/2011 10:57:53 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Rooms_RoomPaidFor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Rooms_RoomPaidFor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF_Rooms_RoomPaidFor]  DEFAULT ((0)) FOR [RoomPaidFor]
END


End
GO
/****** Object:  Default [DF_Rooms_TotalFreeRevisionsUsed]    Script Date: 09/25/2011 10:57:53 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Rooms_TotalFreeRevisionsUsed]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Rooms_TotalFreeRevisionsUsed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF_Rooms_TotalFreeRevisionsUsed]  DEFAULT ((0)) FOR [TotalFreeRevisionsUsed]
END


End
GO
/****** Object:  Default [DF_Rooms_TotalRevisionsPaidFor]    Script Date: 09/25/2011 10:57:53 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Rooms_TotalRevisionsPaidFor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Rooms_TotalRevisionsPaidFor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF_Rooms_TotalRevisionsPaidFor]  DEFAULT ((0)) FOR [TotalRevisionsPaidFor]
END


End
GO
/****** Object:  Default [DF_Rooms_CurrentDesignStepId]    Script Date: 09/25/2011 10:57:53 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Rooms_CurrentDesignStepId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Rooms_CurrentDesignStepId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF_Rooms_CurrentDesignStepId]  DEFAULT ((0)) FOR [CurrentDesignStepId]
END


End
GO
/****** Object:  Default [DF_Rooms_RoomTotalCost]    Script Date: 09/25/2011 10:57:53 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Rooms_RoomTotalCost]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Rooms_RoomTotalCost]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF_Rooms_RoomTotalCost]  DEFAULT ((0.0)) FOR [RoomTotalCost]
END


End
GO
/****** Object:  Default [DF_Rooms_RoomTaxCost]    Script Date: 09/25/2011 10:57:53 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Rooms_RoomTaxCost]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Rooms_RoomTaxCost]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF_Rooms_RoomTaxCost]  DEFAULT ((0.0)) FOR [RoomTaxCost]
END


End
GO
/****** Object:  Default [DF_Rooms_TaxPercent]    Script Date: 09/25/2011 10:57:53 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Rooms_TaxPercent]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Rooms_TaxPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Rooms] ADD  CONSTRAINT [DF_Rooms_TaxPercent]  DEFAULT ((0.0)) FOR [RoomTaxPercent]
END


End
GO
/****** Object:  Default [DF_States_TaxPercent]    Script Date: 09/25/2011 10:57:53 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_States_TaxPercent]') AND parent_object_id = OBJECT_ID(N'[dbo].[States]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_States_TaxPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[States] ADD  CONSTRAINT [DF_States_TaxPercent]  DEFAULT ((0)) FOR [TaxPercent]
END


End
GO
/****** Object:  ForeignKey [FK__aspnet_Me__Appli__21B6055D]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Me__Appli__21B6055D]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
ALTER TABLE [dbo].[aspnet_Membership]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Me__Appli__21B6055D] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Me__Appli__21B6055D]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
ALTER TABLE [dbo].[aspnet_Membership] CHECK CONSTRAINT [FK__aspnet_Me__Appli__21B6055D]
GO
/****** Object:  ForeignKey [FK__aspnet_Me__UserI__22AA2996]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Me__UserI__22AA2996]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
ALTER TABLE [dbo].[aspnet_Membership]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Me__UserI__22AA2996] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Me__UserI__22AA2996]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
ALTER TABLE [dbo].[aspnet_Membership] CHECK CONSTRAINT [FK__aspnet_Me__UserI__22AA2996]
GO
/****** Object:  ForeignKey [FK__aspnet_Pa__Appli__5AEE82B9]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pa__Appli__5AEE82B9]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]'))
ALTER TABLE [dbo].[aspnet_Paths]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Pa__Appli__5AEE82B9] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pa__Appli__5AEE82B9]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]'))
ALTER TABLE [dbo].[aspnet_Paths] CHECK CONSTRAINT [FK__aspnet_Pa__Appli__5AEE82B9]
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__PathI__628FA481]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__PathI__628FA481]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers]'))
ALTER TABLE [dbo].[aspnet_PersonalizationAllUsers]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Pe__PathI__628FA481] FOREIGN KEY([PathId])
REFERENCES [dbo].[aspnet_Paths] ([PathId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__PathI__628FA481]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers]'))
ALTER TABLE [dbo].[aspnet_PersonalizationAllUsers] CHECK CONSTRAINT [FK__aspnet_Pe__PathI__628FA481]
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__PathI__68487DD7]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__PathI__68487DD7]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Pe__PathI__68487DD7] FOREIGN KEY([PathId])
REFERENCES [dbo].[aspnet_Paths] ([PathId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__PathI__68487DD7]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] CHECK CONSTRAINT [FK__aspnet_Pe__PathI__68487DD7]
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__UserI__693CA210]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__UserI__693CA210]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Pe__UserI__693CA210] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__UserI__693CA210]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] CHECK CONSTRAINT [FK__aspnet_Pe__UserI__693CA210]
GO
/****** Object:  ForeignKey [FK__aspnet_Pr__UserI__38996AB5]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pr__UserI__38996AB5]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Profile]'))
ALTER TABLE [dbo].[aspnet_Profile]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Pr__UserI__38996AB5] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pr__UserI__38996AB5]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Profile]'))
ALTER TABLE [dbo].[aspnet_Profile] CHECK CONSTRAINT [FK__aspnet_Pr__UserI__38996AB5]
GO
/****** Object:  ForeignKey [FK__aspnet_Ro__Appli__440B1D61]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Ro__Appli__440B1D61]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]'))
ALTER TABLE [dbo].[aspnet_Roles]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Ro__Appli__440B1D61] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Ro__Appli__440B1D61]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]'))
ALTER TABLE [dbo].[aspnet_Roles] CHECK CONSTRAINT [FK__aspnet_Ro__Appli__440B1D61]
GO
/****** Object:  ForeignKey [FK__aspnet_Us__Appli__0DAF0CB0]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__Appli__0DAF0CB0]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
ALTER TABLE [dbo].[aspnet_Users]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Us__Appli__0DAF0CB0] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__Appli__0DAF0CB0]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
ALTER TABLE [dbo].[aspnet_Users] CHECK CONSTRAINT [FK__aspnet_Us__Appli__0DAF0CB0]
GO
/****** Object:  ForeignKey [FK__aspnet_Us__RoleI__4AB81AF0]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__RoleI__4AB81AF0]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]'))
ALTER TABLE [dbo].[aspnet_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Us__RoleI__4AB81AF0] FOREIGN KEY([RoleId])
REFERENCES [dbo].[aspnet_Roles] ([RoleId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__RoleI__4AB81AF0]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]'))
ALTER TABLE [dbo].[aspnet_UsersInRoles] CHECK CONSTRAINT [FK__aspnet_Us__RoleI__4AB81AF0]
GO
/****** Object:  ForeignKey [FK__aspnet_Us__UserI__49C3F6B7]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__UserI__49C3F6B7]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]'))
ALTER TABLE [dbo].[aspnet_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Us__UserI__49C3F6B7] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__UserI__49C3F6B7]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]'))
ALTER TABLE [dbo].[aspnet_UsersInRoles] CHECK CONSTRAINT [FK__aspnet_Us__UserI__49C3F6B7]
GO
/****** Object:  ForeignKey [FK_BathroomQuestionnaires_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_BathroomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[BathroomQuestionnaires]'))
ALTER TABLE [dbo].[BathroomQuestionnaires]  WITH CHECK ADD  CONSTRAINT [FK_BathroomQuestionnaires_Rooms] FOREIGN KEY([RoomId])
REFERENCES [dbo].[Rooms] ([RoomId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_BathroomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[BathroomQuestionnaires]'))
ALTER TABLE [dbo].[BathroomQuestionnaires] CHECK CONSTRAINT [FK_BathroomQuestionnaires_Rooms]
GO
/****** Object:  ForeignKey [FK_Consultations_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Consultations_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[Consultations]'))
ALTER TABLE [dbo].[Consultations]  WITH CHECK ADD  CONSTRAINT [FK_Consultations_Rooms] FOREIGN KEY([RoomId])
REFERENCES [dbo].[Rooms] ([RoomId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Consultations_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[Consultations]'))
ALTER TABLE [dbo].[Consultations] CHECK CONSTRAINT [FK_Consultations_Rooms]
GO
/****** Object:  ForeignKey [FK_CurrentSteps_CurrentSteps]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CurrentSteps_CurrentSteps]') AND parent_object_id = OBJECT_ID(N'[dbo].[CurrentSteps]'))
ALTER TABLE [dbo].[CurrentSteps]  WITH CHECK ADD  CONSTRAINT [FK_CurrentSteps_CurrentSteps] FOREIGN KEY([CurrentStepId])
REFERENCES [dbo].[CurrentSteps] ([CurrentStepId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CurrentSteps_CurrentSteps]') AND parent_object_id = OBJECT_ID(N'[dbo].[CurrentSteps]'))
ALTER TABLE [dbo].[CurrentSteps] CHECK CONSTRAINT [FK_CurrentSteps_CurrentSteps]
GO
/****** Object:  ForeignKey [FK_DesignFiles_DesignFileTypes]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DesignFiles_DesignFileTypes]') AND parent_object_id = OBJECT_ID(N'[dbo].[DesignFiles]'))
ALTER TABLE [dbo].[DesignFiles]  WITH CHECK ADD  CONSTRAINT [FK_DesignFiles_DesignFileTypes] FOREIGN KEY([DesignFileTypeId])
REFERENCES [dbo].[DesignFileTypes] ([DesignFileTypeId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DesignFiles_DesignFileTypes]') AND parent_object_id = OBJECT_ID(N'[dbo].[DesignFiles]'))
ALTER TABLE [dbo].[DesignFiles] CHECK CONSTRAINT [FK_DesignFiles_DesignFileTypes]
GO
/****** Object:  ForeignKey [FK_DesignFiles_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DesignFiles_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[DesignFiles]'))
ALTER TABLE [dbo].[DesignFiles]  WITH CHECK ADD  CONSTRAINT [FK_DesignFiles_Rooms] FOREIGN KEY([RoomId])
REFERENCES [dbo].[Rooms] ([RoomId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DesignFiles_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[DesignFiles]'))
ALTER TABLE [dbo].[DesignFiles] CHECK CONSTRAINT [FK_DesignFiles_Rooms]
GO
/****** Object:  ForeignKey [FK_DiningRoomQuestionnaires_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DiningRoomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[DiningRoomQuestionnaires]'))
ALTER TABLE [dbo].[DiningRoomQuestionnaires]  WITH CHECK ADD  CONSTRAINT [FK_DiningRoomQuestionnaires_Rooms] FOREIGN KEY([RoomId])
REFERENCES [dbo].[Rooms] ([RoomId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DiningRoomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[DiningRoomQuestionnaires]'))
ALTER TABLE [dbo].[DiningRoomQuestionnaires] CHECK CONSTRAINT [FK_DiningRoomQuestionnaires_Rooms]
GO
/****** Object:  ForeignKey [FK_EntryQuestionnaires_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EntryQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[EntryQuestionnaires]'))
ALTER TABLE [dbo].[EntryQuestionnaires]  WITH CHECK ADD  CONSTRAINT [FK_EntryQuestionnaires_Rooms] FOREIGN KEY([RoomId])
REFERENCES [dbo].[Rooms] ([RoomId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EntryQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[EntryQuestionnaires]'))
ALTER TABLE [dbo].[EntryQuestionnaires] CHECK CONSTRAINT [FK_EntryQuestionnaires_Rooms]
GO
/****** Object:  ForeignKey [FK_Furniture_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Furniture_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[Furniture]'))
ALTER TABLE [dbo].[Furniture]  WITH CHECK ADD  CONSTRAINT [FK_Furniture_Rooms] FOREIGN KEY([RoomId])
REFERENCES [dbo].[Rooms] ([RoomId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Furniture_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[Furniture]'))
ALTER TABLE [dbo].[Furniture] CHECK CONSTRAINT [FK_Furniture_Rooms]
GO
/****** Object:  ForeignKey [FK_FurniturePhotos_Furniture]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FurniturePhotos_Furniture]') AND parent_object_id = OBJECT_ID(N'[dbo].[FurniturePhotos]'))
ALTER TABLE [dbo].[FurniturePhotos]  WITH CHECK ADD  CONSTRAINT [FK_FurniturePhotos_Furniture] FOREIGN KEY([FurnitureId])
REFERENCES [dbo].[Furniture] ([FurnitureId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FurniturePhotos_Furniture]') AND parent_object_id = OBJECT_ID(N'[dbo].[FurniturePhotos]'))
ALTER TABLE [dbo].[FurniturePhotos] CHECK CONSTRAINT [FK_FurniturePhotos_Furniture]
GO
/****** Object:  ForeignKey [FK_GuestRoomQuestionnaires_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GuestRoomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[GuestRoomQuestionnaires]'))
ALTER TABLE [dbo].[GuestRoomQuestionnaires]  WITH CHECK ADD  CONSTRAINT [FK_GuestRoomQuestionnaires_Rooms] FOREIGN KEY([RoomId])
REFERENCES [dbo].[Rooms] ([RoomId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GuestRoomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[GuestRoomQuestionnaires]'))
ALTER TABLE [dbo].[GuestRoomQuestionnaires] CHECK CONSTRAINT [FK_GuestRoomQuestionnaires_Rooms]
GO
/****** Object:  ForeignKey [FK_KidsRoomQuestionnaires_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KidsRoomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[KidsRoomQuestionnaires]'))
ALTER TABLE [dbo].[KidsRoomQuestionnaires]  WITH CHECK ADD  CONSTRAINT [FK_KidsRoomQuestionnaires_Rooms] FOREIGN KEY([RoomId])
REFERENCES [dbo].[Rooms] ([RoomId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KidsRoomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[KidsRoomQuestionnaires]'))
ALTER TABLE [dbo].[KidsRoomQuestionnaires] CHECK CONSTRAINT [FK_KidsRoomQuestionnaires_Rooms]
GO
/****** Object:  ForeignKey [FK_KitchenQuestionnaires_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KitchenQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[KitchenQuestionnaires]'))
ALTER TABLE [dbo].[KitchenQuestionnaires]  WITH CHECK ADD  CONSTRAINT [FK_KitchenQuestionnaires_Rooms] FOREIGN KEY([RoomId])
REFERENCES [dbo].[Rooms] ([RoomId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KitchenQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[KitchenQuestionnaires]'))
ALTER TABLE [dbo].[KitchenQuestionnaires] CHECK CONSTRAINT [FK_KitchenQuestionnaires_Rooms]
GO
/****** Object:  ForeignKey [FK_LivingRoomQuestionnaires_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LivingRoomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[LivingRoomQuestionnaires]'))
ALTER TABLE [dbo].[LivingRoomQuestionnaires]  WITH CHECK ADD  CONSTRAINT [FK_LivingRoomQuestionnaires_Rooms] FOREIGN KEY([RoomId])
REFERENCES [dbo].[Rooms] ([RoomId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LivingRoomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[LivingRoomQuestionnaires]'))
ALTER TABLE [dbo].[LivingRoomQuestionnaires] CHECK CONSTRAINT [FK_LivingRoomQuestionnaires_Rooms]
GO
/****** Object:  ForeignKey [FK_MasterBedroomQuestionnaires_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MasterBedroomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[MasterBedroomQuestionnaires]'))
ALTER TABLE [dbo].[MasterBedroomQuestionnaires]  WITH CHECK ADD  CONSTRAINT [FK_MasterBedroomQuestionnaires_Rooms] FOREIGN KEY([RoomId])
REFERENCES [dbo].[Rooms] ([RoomId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MasterBedroomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[MasterBedroomQuestionnaires]'))
ALTER TABLE [dbo].[MasterBedroomQuestionnaires] CHECK CONSTRAINT [FK_MasterBedroomQuestionnaires_Rooms]
GO
/****** Object:  ForeignKey [FK_MediaRoomQuestionnaires_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MediaRoomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[MediaRoomQuestionnaires]'))
ALTER TABLE [dbo].[MediaRoomQuestionnaires]  WITH CHECK ADD  CONSTRAINT [FK_MediaRoomQuestionnaires_Rooms] FOREIGN KEY([RoomId])
REFERENCES [dbo].[Rooms] ([RoomId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MediaRoomQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[MediaRoomQuestionnaires]'))
ALTER TABLE [dbo].[MediaRoomQuestionnaires] CHECK CONSTRAINT [FK_MediaRoomQuestionnaires_Rooms]
GO
/****** Object:  ForeignKey [FK_OfficeQuestionnaires_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OfficeQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[OfficeQuestionnaires]'))
ALTER TABLE [dbo].[OfficeQuestionnaires]  WITH CHECK ADD  CONSTRAINT [FK_OfficeQuestionnaires_Rooms] FOREIGN KEY([RoomId])
REFERENCES [dbo].[Rooms] ([RoomId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OfficeQuestionnaires_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[OfficeQuestionnaires]'))
ALTER TABLE [dbo].[OfficeQuestionnaires] CHECK CONSTRAINT [FK_OfficeQuestionnaires_Rooms]
GO
/****** Object:  ForeignKey [FK_Orders_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Orders_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[Orders]'))
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Rooms] FOREIGN KEY([RoomId])
REFERENCES [dbo].[Rooms] ([RoomId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Orders_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[Orders]'))
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Rooms]
GO
/****** Object:  ForeignKey [FK_RoomMeasurements_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RoomMeasurements_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[RoomMeasurements]'))
ALTER TABLE [dbo].[RoomMeasurements]  WITH CHECK ADD  CONSTRAINT [FK_RoomMeasurements_Rooms] FOREIGN KEY([RoomId])
REFERENCES [dbo].[Rooms] ([RoomId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RoomMeasurements_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[RoomMeasurements]'))
ALTER TABLE [dbo].[RoomMeasurements] CHECK CONSTRAINT [FK_RoomMeasurements_Rooms]
GO
/****** Object:  ForeignKey [FK_RoomPhotos_Rooms]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RoomPhotos_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[RoomPhotos]'))
ALTER TABLE [dbo].[RoomPhotos]  WITH CHECK ADD  CONSTRAINT [FK_RoomPhotos_Rooms] FOREIGN KEY([RoomId])
REFERENCES [dbo].[Rooms] ([RoomId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RoomPhotos_Rooms]') AND parent_object_id = OBJECT_ID(N'[dbo].[RoomPhotos]'))
ALTER TABLE [dbo].[RoomPhotos] CHECK CONSTRAINT [FK_RoomPhotos_Rooms]
GO
/****** Object:  ForeignKey [FK_Rooms_CurrentDesignSteps]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Rooms_CurrentDesignSteps]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
ALTER TABLE [dbo].[Rooms]  WITH CHECK ADD  CONSTRAINT [FK_Rooms_CurrentDesignSteps] FOREIGN KEY([CurrentDesignStepId])
REFERENCES [dbo].[CurrentDesignSteps] ([CurrentDesignStepId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Rooms_CurrentDesignSteps]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
ALTER TABLE [dbo].[Rooms] CHECK CONSTRAINT [FK_Rooms_CurrentDesignSteps]
GO
/****** Object:  ForeignKey [FK_Rooms_CurrentSteps]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Rooms_CurrentSteps]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
ALTER TABLE [dbo].[Rooms]  WITH CHECK ADD  CONSTRAINT [FK_Rooms_CurrentSteps] FOREIGN KEY([CurrentStepId])
REFERENCES [dbo].[CurrentSteps] ([CurrentStepId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Rooms_CurrentSteps]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
ALTER TABLE [dbo].[Rooms] CHECK CONSTRAINT [FK_Rooms_CurrentSteps]
GO
/****** Object:  ForeignKey [FK_Rooms_Profiles]    Script Date: 09/25/2011 10:57:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Rooms_Profiles]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
ALTER TABLE [dbo].[Rooms]  WITH CHECK ADD  CONSTRAINT [FK_Rooms_Profiles] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[Profiles] ([ProfileId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Rooms_Profiles]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rooms]'))
ALTER TABLE [dbo].[Rooms] CHECK CONSTRAINT [FK_Rooms_Profiles]
GO
GRANT EXECUTE ON [dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Personalization_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Profile_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Profile_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Roles_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_CheckSchemaVersion] TO [aspnet_WebEvent_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_CreateUser] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_FindUsersByEmail] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_FindUsersByName] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetAllUsers] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetNumberOfUsersOnline] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetNumberOfUsersOnline] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetPassword] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetPasswordWithFormat] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetUserByEmail] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetUserByEmail] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetUserByName] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetUserByName] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetUserByUserId] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetUserByUserId] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_ResetPassword] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_SetPassword] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_UnlockUser] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_UpdateUser] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_UpdateUserInfo] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Paths_CreatePath] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Personalization_GetApplicationId] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationAdministration_DeleteAllState] TO [aspnet_Personalization_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationAdministration_FindState] TO [aspnet_Personalization_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationAdministration_GetCountOfState] TO [aspnet_Personalization_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationAdministration_ResetSharedState] TO [aspnet_Personalization_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationAdministration_ResetUserState] TO [aspnet_Personalization_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationPerUser_GetPageSettings] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationPerUser_SetPageSettings] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Profile_DeleteInactiveProfiles] TO [aspnet_Profile_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Profile_DeleteProfiles] TO [aspnet_Profile_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Profile_GetNumberOfInactiveProfiles] TO [aspnet_Profile_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Profile_GetProfiles] TO [aspnet_Profile_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Profile_GetProperties] TO [aspnet_Profile_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Profile_SetProperties] TO [aspnet_Profile_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Personalization_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Profile_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Profile_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Roles_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_WebEvent_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Roles_CreateRole] TO [aspnet_Roles_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Roles_DeleteRole] TO [aspnet_Roles_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Roles_GetAllRoles] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Roles_RoleExists] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Personalization_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Profile_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Profile_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Roles_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_WebEvent_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Users_DeleteUser] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UsersInRoles_AddUsersToRoles] TO [aspnet_Roles_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UsersInRoles_FindUsersInRole] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UsersInRoles_GetRolesForUser] TO [aspnet_Roles_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UsersInRoles_GetRolesForUser] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UsersInRoles_GetUsersInRoles] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UsersInRoles_IsUserInRole] TO [aspnet_Roles_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UsersInRoles_IsUserInRole] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles] TO [aspnet_Roles_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_WebEvent_LogEvent] TO [aspnet_WebEvent_FullAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Applications] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Applications] TO [aspnet_Personalization_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Applications] TO [aspnet_Profile_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Applications] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_MembershipUsers] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Profiles] TO [aspnet_Profile_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Roles] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Users] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Users] TO [aspnet_Personalization_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Users] TO [aspnet_Profile_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Users] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_UsersInRoles] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_WebPartState_Paths] TO [aspnet_Personalization_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_WebPartState_Shared] TO [aspnet_Personalization_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_WebPartState_User] TO [aspnet_Personalization_ReportingAccess] AS [dbo]
GO
