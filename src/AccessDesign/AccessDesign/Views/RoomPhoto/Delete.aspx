﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.RoomPhoto>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Delete Room Photo
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Delete Confirmation</h1>
    <div id="room-photo-nav">
        <a href="/RoomPhoto/Upload/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-UploadRoomPhotos.png" alt="" border="0" class="room-photo-nav-image" /></a><a href="/RoomPhoto/Details/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-ViewEditPhotosTreomaOn.png" alt="" border="0"/></a><a href="/RoomPhoto/Comments/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-AddComments.png" alt="" border="0" class="room-photo-nav-image"/></a>
     </div>
     <br />
    
        <p>Are you sure that you want to delete this photo? To permanently remove this photo, click the Delete button below.</p>
        <br />
        <div class="delete-photo">
            <img src="<%: Model.ThumbnailUrlPath %>" alt="<%: Model.PhotoName %>" />
        </div>
        <br />
        <% using (Html.BeginForm()) { %>
            <div id="standard-button">
                <input name="confirmButton" type="submit"  class="button" value="Delete" />
            </div> 
    <% } %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".room-photo-nav-image")
            .mouseover(function () {
                var src = $(this).attr("src").match(/[^\.]+/) + "TreomaOn.png";
                $(this).attr("src", src);
            })
            .mouseout(function () {
                var src = $(this).attr("src").replace("TreomaOn", "");
                $(this).attr("src", src);
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarContent" runat="server">
</asp:Content>

