﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<AccessDesign.Models.RoomPhoto>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Room Photos
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h2>Room Photos</h2>

<% foreach (var item in Model) { %>
    
<span class="gallery">
                <a href="<%: item.WebUrlPath %>" title="<%: item.PhotoName %>. " class="lightbox"><img src="<%: item.ThumbnailUrlPath %>" width="100" height="100" alt="<%: item.PhotoName %>" border="0" /></a>
                <a href="<%: item.OriginalUrlPath %>" target="_blank">view full sized version of image</a></span><br />
    <% } %>

    <br /><br /><br />
    <h2>Room Photo Comments</h2>
    <p><%: ViewData["RoomPhotoComments"]%></p>

    <p>
    <%: Html.ActionLink("Back to Room", "Details", "Search", new { id = TempData["RoomId"] }, null)%>
    </p>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="/Scripts/jquery.lightbox-0.5.js"></script>
    <link rel="stylesheet" type="text/css" href="/Content/Gallery.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/Content/jquery.lightbox-0.5.css" media="screen" />
    
    <script type="text/javascript">
        $(function () {
            $('a.lightbox').lightBox // Select all links with lightbox class
            ({
                imageBlank: '/Content/Images/LightBox/lightbox-blank.gif',
                imageLoading: '/Content/Images/LightBox/lightbox-ico-loading.gif',
                imageBtnClose: '/Content/Images/LightBox/lightbox-btn-close.gif',
                imageBtnPrev: '/Content/Images/LightBox/lightbox-btn-prev.gif',
                imageBtnNext: '/Content/Images/LightBox/lightbox-btn-next.gif'
            });

        });
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

