﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.GuestRoomQuestionnaire>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Details</h2>

    <fieldset>        
        <div class="display-label">Room Id</div>
        <div class="display-field"><%: Model.RoomId %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.DesignStyleOfRoom)%></div>
        <div class="display-field"><%: Model.DesignStyleOfRoom %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.DesignStyleOfRoomOther)%></div>
        <div class="display-field"><%: Model.DesignStyleOfRoomOther %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.SpecificColorsYouWantToUse)%></div>
        <div class="display-field"><%: Model.SpecificColorsYouWantToUse %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.SpecificColorsYouWantToUseYes)%></div>
        <div class="display-field"><%: Model.SpecificColorsYouWantToUseYes %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.FurnitureAccessoriesStayingInRoom)%></div>
        <div class="display-field"><%: Model.FurnitureAccessoriesStayingInRoom %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.YourBudget)%></div>
        <div class="display-field"><%: Model.YourBudget %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.MustHavesItemsInRoomNotAlreadyOwned)%></div>
        <div class="display-field"><%: Model.MustHavesItemsInRoomNotAlreadyOwned %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.MustHavesItemsInRoomNotAlreadyOwnedYes)%></div>
        <div class="display-field"><%: Model.MustHavesItemsInRoomNotAlreadyOwnedYes %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.StoreAnythingElseInRoom)%></div>
        <div class="display-field"><%: Model.StoreAnythingElseInRoom %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.HowDoYouUseThisSpace)%></div>
        <div class="display-field"><%: Model.HowDoYouUseThisSpace %></div>
        
        <div class="display-label"><%: Html.LabelFor(model => model.HowDoYouWantYourRoomToFeel)%></div>
        <div class="display-field"><%: Model.HowDoYouWantYourRoomToFeel%></div>

        <div class="display-label"><%: Html.LabelFor(model => model.DoYouLikeContrast)%></div>
        <div class="display-field"><%: Model.DoYouLikeContrast%></div>

        <div class="display-label"><%: Html.LabelFor(model => model.DoYouLikeTextures)%></div>
        <div class="display-field"><%: Model.DoYouLikeTextures%></div>

        <div class="display-label"><%: Html.LabelFor(model => model.HowDoYouWantYourRoomToFeelGender)%></div>
        <div class="display-field"><%: Model.HowDoYouWantYourRoomToFeelGender%></div>

        <div class="display-label"><%: Html.LabelFor(model => model.AdditionalImportantInformation)%></div>
        <div class="display-field"><%: Model.AdditionalImportantInformation%></div>

        <div class="display-label"><%: Html.LabelFor(model => model.CateringRoomToSpecificPerson)%></div>
        <div class="display-field"><%: Model.CateringRoomToSpecificPerson%></div>

        <div class="display-label"><%: Html.LabelFor(model => model.CateringRoomToSpecificPersonWho)%></div>
        <div class="display-field"><%: Model.CateringRoomToSpecificPersonWho%></div>

        <div class="display-label"><%: Html.LabelFor(model => model.CateringRoomToSpecificPersonRequirements)%></div>
        <div class="display-field"><%: Model.CateringRoomToSpecificPersonRequirements%></div>
    </fieldset>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="/Content/admin.css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

