﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<AccessDesign.Models.Furniture>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Your Pieces of Furniture
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Your Pieces of Furniture</h1>
    <div id="room-photo-nav">
        <a href="/Furniture/Create/<%: TempData["RoomId"] %>"><img src="/Content/Images/Furniture-Navigation-New.png" alt="Create new piece of furniture" border="0" class="room-photo-nav-image" /></a><img src="/Content/Images/app-steps-blank-02.png" alt="Blank Step 2" border="0" /><img src="/Content/Images/app-steps-blank-03.png" alt="Blank Step 3" border="0" />
     </div>
     <div class="room-photo-gallery">
        <p>If you have any pieces of furniture that you wish to keep, please measure their height, width, and depth in inches, and take photos of the piece of furniture. Once you have done this, click on <%: Html.ActionLink("new piece of furniture", "Create", new { id = TempData["RoomId"] })%> to add the piece of furniture to your room.  You can add as many pieces of furniture as you want.</p>

         <% if (Convert.ToInt32(TempData["MenuStep"]) < 6)
       { %>
        <br />
        <p>If you don't have an furniture pieces that you wish to keep, simply click <%: Html.ActionLink("here to bypass this step and proceed with booking your free consultation", "Complete", new { id = TempData["RoomId"] })%>.</p>
    <% } %>
    <br />        
     </div>
    <div id="gallery">
        <% foreach (var item in Model) { %>

                <% if (item.Image == null)
                   {  %>
                       <div class="furniture-index-image-list">
                            <div class="furniture-index-image-list-item"><a href="/FurniturePhoto/Details/<%: item.FurnitureId %>"><img src="/Content/Images/furniture-photo-not-available.png" width="100" height="100" alt="Click here to add an image" border="0" /></a></div>
                            <div class="furniture-index-image-list-button"><%: Html.ActionLink(item.Name, "Details", "FurniturePhoto", new { id = item.FurnitureId }, new { })%></div>
                        </div>

                   <% } else { %>
                        <div class="furniture-index-image-list">
                            <div class="furniture-index-image-list-item"><a href="/FurniturePhoto/Details/<%: item.FurnitureId %>"><img src="<%: item.Image  %>" width="100" height="100" alt="<%: item.Name %>" border="0" /></a></div>
                            <div class="furniture-index-image-list-button"><%: Html.ActionLink(item.Name, "Details", "FurniturePhoto", new { id = item.FurnitureId }, new { })%></div>
                        </div>
                <% } %>

        <% } %>
    </div>

    <p>
        
    </p>
   

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    $(function () {
        $(".room-photo-nav-image")
            .mouseover(function () {
                var src = $(this).attr("src").match(/[^\.]+/) + "TreomaOn.png";
                $(this).attr("src", src);
            })
            .mouseout(function () {
                var src = $(this).attr("src").replace("TreomaOn", "");
                $(this).attr("src", src);
            });

    });
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

