﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<link href="/Content/Images/favicon.ico" rel="shortcut icon" type="image/x-icon" /> 
    <link rel="stylesheet" type="text/css" href="/Content/style.css" />
<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/Content/iestyle.css" />
<![endif]-->
    <script src="/Scripts/MicrosoftAjax.js" type="text/javascript"></script>
    <script src="/Scripts/MicrosoftMvcAjax.js" type="text/javascript"></script>
    <script src="/Scripts/MicrosoftMvcValidation.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            // Accordion
            $("#navlist").accordion({
                header: ".expand-link",
                icons: { 'header': '.expand-link', 'headerSelected': 'expand-link active' }
            });
        });
		</script>