﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.RegisterModel>" %>


<asp:Content ID="registerTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Create an account - Digital Interior Design
</asp:Content>

<asp:Content ID="registerContent" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Create a New Account</h1>
    <p>
        Use the form below to create a new account. 
    </p>
    <p>
        Passwords are required to be a minimum of <%: ViewData["PasswordLength"] %> characters in length.
    </p>
    <br />
    <%: Html.ValidationSummary() %>
    <br />
    <% Html.EnableClientValidation(); %>

    <% using (Html.BeginForm()) { %>
        
        <div>                
                <div class="text-box-label-same-line">
                    <%: Html.LabelFor(m => m.Email) %>
                </div>
                <div class="text-box">
                    <%: Html.TextBoxFor(m => m.Email, new { @class = "txtbox", @size = 25 })%>
                    <%: Html.ValidationMessageFor(m => m.Email) %>
                </div>
                <br />
                <div class="text-box-label-same-line">
                    <%: Html.LabelFor(m => m.Password) %>
                </div>
                <div class="text-box">
                    <%: Html.PasswordFor(m => m.Password, new { @class = "txtbox", @size = 25 })%>
                    <%: Html.ValidationMessageFor(m => m.Password) %>
                </div>
                <br />
                <div class="text-box-label-same-line">
                    <%: Html.LabelFor(m => m.ConfirmPassword) %>
                </div>
                <div class="text-box">
                    <%: Html.PasswordFor(m => m.ConfirmPassword, new { @class = "txtbox", @size = 25 })%>
                    <%: Html.ValidationMessageFor(m => m.ConfirmPassword) %>
                </div>
                <br />
                <div class="check-box-label-register">
                    <label>By checking this box, I understand and agree with the <a href="http://www.digitalinteriordesign.com/terms-of-use" target="_blank">terms of use</a> and the <a href="http://www.digitalinteriordesign.com/privacy-policy" target="_blank">privacy policy</a>.</label>
                    <%: Html.CheckBoxFor(m => m.AgreeToTermsOfUse, new {@checked="yes"}) %><br />
                    <%: Html.ValidationMessageFor(m => m.AgreeToTermsOfUse) %>
                </div>
                <div>
                    
                </div>
                <br /><br /><br /><br /><br /><br />
                <div id="standard-button">
                    <input type="submit" class="button" value="Register" />
                 </div>
        </div>
    <% } %>
</asp:Content>

<asp:Content ID="logOnSideBar" ContentPlaceHolderID="SideBarContent" runat="server">
    <ul>
        <li><%: Html.ActionLink("CREATE ACCOUNT", "Register", "Account", new { @class = "selected" })%></li>
        <li><%: Html.ActionLink("LOGIN", "LogOn", "Account")%></li>
    </ul>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .validation-summary-errors ul {display:inline; font-weight:normal}    
    </style>
    <meta name="description" content="Create an account with Digital Interior Design." />
    <meta name="keywords" content="digital interior design, online interior design, digital interior design create account, interior design, interior decorating, edesign, e-design" />
</asp:Content>