﻿<%@ Import Namespace="System.Web.Mvc" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.ResetPasswordModel>" %>
<%@ Import Namespace="AccessDesign.Helpers" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	ResetPassword
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Reset Password</h1>

    <p>To reset your password, please: enter your email address, fill out the reCatcha box below, and click submit.  A new password will then be automatically emailed to you.</p>

    <% Html.EnableClientValidation(); %>
    
    <br />
    <% using (Html.BeginForm()) {%>
            <%: Html.ValidationSummary(true) %>          
            <br />
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.Email) %>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.Email, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.Email) %>
            </div>
            <br />
            <div class="text-box-label-above">
                <label>Type the words you see in the image below.</label>
            </div>
            <br />
            <div class="display-field">
           <%= Html.GenerateCaptcha() %>
            </div>
            <br /><br />
            <div id="standard-button">
                    <input type="submit"  class="button" value="Submit" />
            </div> 

    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .validation-summary-errors ul {display:inline; font-weight:normal}    
    </style>
</asp:Content>

<asp:Content ID="logOnSideBar" ContentPlaceHolderID="SideBarContent" runat="server">
    <ul>
        <li><a href="#">HOW IT WORKS</a></li>
        <li><%: Html.ActionLink("CREATE ACCOUNT", "Register", "Account")%></li>
        <li><%: Html.ActionLink("LOGIN", "LogOn", "Account")%></li>
        <li><%: Html.ActionLink("RESET PASSWORD", "ResetPassword", "Account", new { @class = "selected" })%></li>
    </ul>
</asp:Content>