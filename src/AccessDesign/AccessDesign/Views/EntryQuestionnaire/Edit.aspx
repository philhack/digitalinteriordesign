﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.EntryQuestionnaire>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Update your Entry Way Questionnaire
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Update Entry Way Questionnaire</h1>
    <br />
    <div id="app-steps-generic">
             <% if (Convert.ToInt32(ViewData["CurrentMenuStepId"]) == 4) { %>
	            <a href="/Questionnaire/Index/<%: Model.RoomId %>"><img src="/Content/Images/app-steps-questionnaire-1-blue-checked.png" alt="" border="0" /></a><a href="/RoomPhoto/Index/<%: Model.RoomId %>"><img src="/Content/Images/app-steps-questionnaire-2-blue-unchecked.png" alt="" border="0"/></a><a href="/Furniture/Index/<%: Model.RoomId %>"><img src="/Content/Images/app-steps-questionnaire-3-grey.png" alt="" border="0"/></a>
                <h6 class="step-description">STEP 1/3 COMPLETE. PLEASE COMPLETE STEP 2 - ROOM PHOTOS</h6>
            <% }
                else if (Convert.ToInt32(ViewData["CurrentMenuStepId"]) == 5)
                { %>
	            <a href="/Questionnaire/Index/<%: Model.RoomId %>"><img src="/Content/Images/app-steps-questionnaire-1-blue-checked.png" alt="" border="0" /></a><a href="/RoomPhoto/Index/<%: Model.RoomId %>"><img src="/Content/Images/app-steps-questionnaire-2-blue-checked.png" alt="" border="0"/></a><a href="/Furniture/Index/<%: Model.RoomId %>"><img src="/Content/Images/app-steps-questionnaire-3-grey.png" alt="" border="0"/></a>
                <h6 class="step-description">STEP 2/3 COMPLETE. PLEASE COMPLETE STEP 3 - FURNITURE PHOTOS</h6>
            <% }
                else if (Convert.ToInt32(ViewData["CurrentMenuStepId"]) >= 6)
                { %>
	            <a href="/Questionnaire/Index/<%: Model.RoomId %>"><img src="/Content/Images/app-steps-questionnaire-1-blue-checked.png" alt="" border="0" /></a><a href="/RoomPhoto/Index/<%: Model.RoomId %>"><img src="/Content/Images/app-steps-questionnaire-2-blue-checked.png" alt="" border="0"/></a><a href="/Furniture/Index/<%: Model.RoomId %>"><img src="/Content/Images/app-steps-questionnaire-3-blue-checked.png" alt="" border="0"/></a>
                <h6 class="step-description">STEP 3/3 COMPLETE.</h6>
            <% } %>
    </div>
    <br /><br />
    <p>Please update your entry way questionnaire below.  This questionnaire is specific to the entry way that you want designed.  Once you have updated the fields in the questionnaire, click "Save".</p>
    <br />
    <p>You can view and update this questionnaire by clicking <i>"Room Questionnaire"</i> on the left side menu.</p>
    <br />
    <div class="text-box"><span class="required-field">*</span> Required fields.</div>
    <br /><br />

    <% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm()) {%>

        <% Html.RenderPartial("EditFields", Model); %>
        <%: Html.ValidationSummary(false, "*Please fill in all the required questions above and then click save.")%>
        <br />
        <div id="standard-button">
                    <input type="submit"  class="button" value="Save" />
        </div> 

    <% } %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">

        <script src="/Scripts/MvcFoolproofValidation.js" type="text/javascript"></script>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

