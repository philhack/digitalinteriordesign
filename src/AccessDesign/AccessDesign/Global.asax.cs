﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace AccessDesign
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // route bathroom questionnaire index
            routes.MapRoute(
                "BathroomQuestionnaireIndex", // Route name
                "BathroomQuestionnaire/{id}", // URL with parameters
                new { controller = "BathroomQuestionnaire", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            // route delete room photo
            routes.MapRoute(
                "DeleteRoomPhoto", // Route name
                "RoomPhoto/Delete/{roomId}/{photoId}", // URL with parameters
                new { controller = "RoomPhoto", action = "Delete", roomId = UrlParameter.Optional, photoId = UrlParameter.Optional } // Parameter defaults
            );

            // route entry way questionnaire index
            routes.MapRoute(
                "DiningRoomQuestionnaireIndex", // Route name
                "DiningRoomQuestionnaire/{id}", // URL with parameters
                new { controller = "DiningRoomQuestionnaire", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            // route entry way questionnaire index
            routes.MapRoute(
                "EntryQuestionnaireIndex", // Route name
                "EntryQuestionnaire/{id}", // URL with parameters
                new { controller = "EntryQuestionnaire", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            // route guest room questionnaire index
            routes.MapRoute(
                "GuestRoomQuestionnaireIndex", // Route name
                "GuestRoomQuestionnaire/{id}", // URL with parameters
                new { controller = "GuestRoomQuestionnaire", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            // route kids room questionnaire index
            routes.MapRoute(
                "KidsRoomQuestionnaireIndex", // Route name
                "KidsRoomQuestionnaire/{id}", // URL with parameters
                new { controller = "KidsRoomQuestionnaire", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            // route kitchen questionnaire index
            routes.MapRoute(
                "KitchenuestionnaireIndex", // Route name
                "KitchenQuestionnaire/{id}", // URL with parameters
                new { controller = "KitchenQuestionnaire", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            // route entry way questionnaire index
            routes.MapRoute(
                "LivingRoomQuestionnaireIndex", // Route name
                "LivingRoomQuestionnaire/{id}", // URL with parameters
                new { controller = "LivingRoomQuestionnaire", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            // route office questionnaire index
            routes.MapRoute(
                "OfficeQuestionnaireIndex", // Route name
                "OfficeQuestionnaire/{id}", // URL with parameters
                new { controller = "OfficeQuestionnaire", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            // route master bedroom questionnaire index
            routes.MapRoute(
                "MasterBedroomQuestionnaireIndex", // Route name
                "MasterBedroomQuestionnaire/{id}", // URL with parameters
                new { controller = "MasterBedroomQuestionnaire", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            // route media room questionnaire index
            routes.MapRoute(
                "MediaRoomQuestionnaireIndex", // Route name
                "MediaRoomQuestionnaire/{id}", // URL with parameters
                new { controller = "MediaRoomQuestionnaire", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "Catch All", 
                "{*path}", 
                new { controller = "Home", action = "ErrorPage" }
            );


        }

        protected void Application_Start()
        {
            System.Security.Cryptography.RSACryptoServiceProvider.UseMachineKeyStore = true;
            System.Security.Cryptography.DSACryptoServiceProvider.UseMachineKeyStore = true;

            AreaRegistration.RegisterAllAreas();
            RegisterRoutes(RouteTable.Routes);
        }
    }
}