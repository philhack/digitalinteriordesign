﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessDesign.Models;

namespace AccessDesign.Controllers
{
    public class GuestRoomQuestionnaireController : Controller
    {
        RoomRepository roomRepository = new RoomRepository();
        GuestRoomQuestionnaireRepository guestRoomQuestionnaireRepository = new GuestRoomQuestionnaireRepository();

        //
        // GET: /GuestRoomQuestionnaire/id
        /// <summary>
        /// Returns a view containing the GuestRoomQuestionnaire that's associated with the current room id. If a questionnaire exists, then it allows the user to edit it.
        /// If a questionnaire does not exist, then it redirects the user to create a new questionnaire.  If the room doesn't belong to the currently logged on user, it
        /// will throw an error.
        /// </summary>
        /// <param name="id">RoomId</param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Index(int id)
        {

            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // ensure that the currently logged on user owns this room before displaying it              
            if (room == null)
            {
                TempData["ErrorMessage"] = "This room does not exist.  If you feel this is an error, please contact us.";
                return View("Error");
            }
            else if (room.RoomTypeName.Equals("Guest Room"))      // if the room type is a guestRoom
            {
                if (guestRoomQuestionnaireRepository.DoesGuestRoomQuestionnaireExist(room.RoomId))
                {
                    return RedirectToAction("Edit", "GuestRoomQuestionnaire", new { id = room.RoomId });              // if a guestRoom questionnaire already exists for this room, then redirect them to the edit page
                }
                else
                {
                    return RedirectToAction("Create", "GuestRoomQuestionnaire", new { id = room.RoomId });         // otherwise check the type of room and if's a "guestRoom" type, then generate a new questionnaire
                }
            }
            else
            {
                TempData["ErrorMessage"] = "The room you have requested is not a guest room";
                return View("Error");
            }
        }

        //
        // GET: /GuestRoomQuestionnaire/Details/5
        /// <summary>
        /// Display the details of the questionnaire to the designer
        /// </summary>
        /// <param name="id">room id</param>
        /// <returns></returns>
        [Authorize(Roles = "Designers")]
        public ActionResult Details(int id)
        {
            Room room = roomRepository.GetRoomById(id);     // get the current room so that we can verify the room type
            if (room == null)
            {
                TempData["ErrorMessage"] = "This room does not exist. ";
                return View("Error");
            }
            else if (room.RoomTypeName.Equals("Guest Room"))      // if the room type is a guestRoom
            {
                GuestRoomQuestionnaire guestRoomQuestionnaire = guestRoomQuestionnaireRepository.GetGuestRoomQuestionnaireByRoomId(room.RoomId);
                if (guestRoomQuestionnaire == null)
                {
                    // the questionnaire does not exist.  Throw an error
                    TempData["ErrorMessage"] = "A guest room questionnaire does not exist for this room. ";
                    return View("Error");
                }
                else
                {
                    return View(guestRoomQuestionnaire);
                }
            }
            else
            {
                TempData["ErrorMessage"] = "This room does not exist or it is not a guest room. ";
                return View("Error");
            }
        }

        //
        // GET: /GuestRoomQuestionnaire/Create/5
        /// <summary>
        /// Client creates a guestRoom questionnaire
        /// </summary>
        /// <param name="id">room id</param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Create(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // ensure that the currently logged on user owns this room before displaying it              
            if (room == null)
            {
                TempData["ErrorMessage"] = "This room does not exist.  If you feel this is an error, please contact us.";
                return View("Error");
            }
            else if (room.RoomTypeName.Equals("Guest Room"))      // if the room type is a guestRoom
            {
                if (guestRoomQuestionnaireRepository.DoesGuestRoomQuestionnaireExist(room.RoomId))        // if a guestRoom questionnaire already exists, throw an error message.
                {
                    TempData["ErrorMessage"] = "Sorry, a questionnaire already exists for this room";
                    return View("Error");
                }
                else
                {
                    // otherwise check the type of room and if's a "guestRoom" type, then generate a new questionnaire
                    GuestRoomQuestionnaire guestRoomQuestionnaire = new GuestRoomQuestionnaire()
                    {
                        RoomId = room.RoomId
                    };

                    ViewData["GenderTypeList"] = BathroomQuestionnaireRepository.GetGenderTypeList("");		// generate the drop down list for the gender types

                    return View(guestRoomQuestionnaire);
                }
            }
            else
            {
                TempData["ErrorMessage"] = "The room you have requested is not a guest room";
                return View("Error");
            }
        }

        //
        // POST: /GuestRoomQuestionnaire/Create

        [HttpPost, Authorize]
        public ActionResult Create(GuestRoomQuestionnaire guestRoomQuestionnaire)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            //  check to ensure the currenly logged on user owns this room.  we're verifying this since the form could be tampered with then posted back
            // if the modelstate is valid, create a guestRoom questionnaire

            int roomId = guestRoomQuestionnaire.RoomId;      // get the room id for the current questionnaire that is being submitted

            if (ModelState.IsValid)
            {
                if (roomRepository.DoesRoomExist(roomId, userName))
                {
                    Room room = roomRepository.GetRoomById(roomId);     // Get the room by the roomId.
                    room.MenuStepId = 4;        // set the menu step number id to 4 since the room specific questionnaire is now completed
                    room.CurrentStepId = 2;     // set the user's current step id to 2 since the room specific questionnaire is now completed
                    room.DateUpdated = DateTime.Now;      // set the date updated so that this can be used on the left side room navigation menu to order the rooms
                    if (TryUpdateModel(room))
                    {
                        guestRoomQuestionnaireRepository.Add(guestRoomQuestionnaire);        // add the new questionnaire
                        guestRoomQuestionnaireRepository.Save();                                                // save the new questionnaire to the db
                        roomRepository.Save();      // save the updated room values to the DB
                        TempData["SuccessMessage"] = "Your questionnaire is complete. Please proceed with uploading photos of your room.";
                        return RedirectToAction("Upload", "RoomPhoto", new { id = roomId });        // redirect the user to the room photo upload section of the website.
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "Sorry, an unknown error occured while creating your room's questionnaire.";
                    return View(guestRoomQuestionnaire);
                }
            }
            TempData["ErrorMessage"] = "Please fill in the required fields below.";
            return View(guestRoomQuestionnaire);
        }

        //
        // GET: /GuestRoomQuestionnaire/Edit/5
        /// <summary>
        ///  Client edit's their guestRoom questionnaire
        /// </summary>
        /// <param name="id">room id</param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // ensure that the currently logged on user owns this room before displaying it              
            if (room == null)
            {
                TempData["ErrorMessage"] = "This room does not exist.  If you feel this is an error, please contact us.";
                return View("Error");
            }
            else if (room.RoomTypeName.Equals("Guest Room"))      // if the room type is a guestRoom
            {
                // if a guestRoom questionnaire already exists for this room, then allow the user to edit the questionnaire
                if (guestRoomQuestionnaireRepository.DoesGuestRoomQuestionnaireExist(room.RoomId))
                {
                    // the id has passed through all of the sanity checks.  It's legit.  Allow the user to edit the questionnaire
                    GuestRoomQuestionnaire guestRoomQuestionnaire = guestRoomQuestionnaireRepository.GetGuestRoomQuestionnaireByRoomId(room.RoomId);    // get the guestRoom questionnaire from the DB
                    ViewData["GenderTypeList"] = GuestRoomQuestionnaireRepository.GetGenderTypeList(guestRoomQuestionnaire.HowDoYouWantYourRoomToFeelGender);		// // generate the drop down list for the gender types
                    ViewData["CurrentMenuStepId"] = room.MenuStepId;       // used for the navigation inside the view;
                    return View(guestRoomQuestionnaire);
                }
                else
                {
                    TempData["ErrorMessage"] = "Sorry, a questionnaire does not exist for this room";
                    return View("Error");
                }
            }
            else
            {
                TempData["ErrorMessage"] = "The room you have requested is not a guest room";
                return View("Error");
            }
        }

        //
        // POST: /GuestRoomQuestionnaire/Edit/5
        [HttpPost, Authorize]
        public ActionResult Edit(int id, FormCollection collection)
        //public ActionResult Edit(GuestRoomQuestionnaire guestRoomQuestionnaire)
        {
            GuestRoomQuestionnaire guestRoomQuestionnaire = guestRoomQuestionnaireRepository.GetGuestRoomQuestionnaireByRoomId(id);
            if (ModelState.IsValid)
            {
                if (TryUpdateModel(guestRoomQuestionnaire))
                {
                    guestRoomQuestionnaireRepository.Save();
                    TempData["SuccessMessage"] = "Your questionnaire has been successfully updated.";       // set the status message
                    return RedirectToAction("Edit", new { id = guestRoomQuestionnaire.RoomId });
                }
            }
            TempData["ErrorMessage"] = "Sorry, there was an error updating your questionnaire.";
            return View(guestRoomQuestionnaire);
        }
    }
}