﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<MvcContrib.Pagination.IPagination<AccessDesign.Models.Room>>" %>
<%@ Import Namespace="MvcContrib.UI.Grid" %>
<%@ Import Namespace="MvcContrib.UI.Pager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Display All Users With Room
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Display All Users With Rooms</h1>
    <p>Click on a room to view the details and work on the design for that room.</p>
    <br />
    <div id="display-all-users-admin-table">
    <%= Html.Grid(Model)
            .Sort(ViewData["sort"] as GridSortOptions)
            .Columns(column => {
            column.For(x => Html.ActionLink(x.UserName, "Details", new { id = x.RoomId })).Encode(false).Named("UserName");
            column.For(x => x.Profile.FirstName).Named("First Name").Sortable(false);
            column.For(x => x.Profile.LastName).Named("Last Name").Sortable(false);
            column.For(x => x.Name).Named("Room Name");
            column.For(x => x.AssignedToDesigner).Named("Designer");
            column.For(x => x.MenuStepId).Named("Step").Format("{0:d}");
     	}) %>

	
	<%= Html.Pager(Model) %>
    <br />
    <br />
    </div>
    <p>Build = v<%: ViewData["BuildVersion"] %></p>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="/Content/admin.css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

