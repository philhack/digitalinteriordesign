﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Password Successfully Reset
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Password Successfully Reset</h1>
    <p>Your password has been successfully reset. The new password has been emailed to you.</p>
</asp:Content>

<asp:Content ID="logOnSideBar" ContentPlaceHolderID="SideBarContent" runat="server">
    <ul>
        <li><%: Html.ActionLink("CREATE ACCOUNT", "Register", "Account")%></li>
        <li><%: Html.ActionLink("LOGIN", "LogOn", "Account")%></li>
        <li><%: Html.ActionLink("RESET PASSWORD", "ResetPassword", "Account", new { @class = "selected" })%></li>
    </ul>
</asp:Content>