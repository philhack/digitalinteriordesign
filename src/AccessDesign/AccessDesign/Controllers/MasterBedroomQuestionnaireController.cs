﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessDesign.Models;

namespace AccessDesign.Controllers
{
    public class MasterBedroomQuestionnaireController : Controller
    {
        RoomRepository roomRepository = new RoomRepository();
        MasterBedroomQuestionnaireRepository masterBedroomQuestionnaireRepository = new MasterBedroomQuestionnaireRepository();

        //
        // GET: /MasterBedroomQuestionnaire/id
        /// <summary>
        /// Returns a view containing the MasterBedroomQuestionnaire that's associated with the current room id. If a questionnaire exists, then it allows the user to edit it.
        /// If a questionnaire does not exist, then it redirects the user to create a new questionnaire.  If the room doesn't belong to the currently logged on user, it
        /// will throw an error.
        /// </summary>
        /// <param name="id">RoomId</param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Index(int id)
        {

            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // ensure that the currently logged on user owns this room before displaying it              
            if (room == null)
            {
                TempData["ErrorMessage"] = "This room does not exist.  If you feel this is an error, please contact us.";
                return View("Error");
            }
            else if (room.RoomTypeName.Equals("Master Bedroom"))      // if the room type is a masterBedroom
            {
                if (masterBedroomQuestionnaireRepository.DoesMasterBedroomQuestionnaireExist(room.RoomId))
                {
                    return RedirectToAction("Edit", "MasterBedroomQuestionnaire", new { id = room.RoomId });              // if a masterBedroom questionnaire already exists for this room, then redirect them to the edit page
                }
                else
                {
                    return RedirectToAction("Create", "MasterBedroomQuestionnaire", new { id = room.RoomId });         // otherwise check the type of room and if's a "masterBedroom" type, then generate a new questionnaire
                }
            }
            else
            {
                TempData["ErrorMessage"] = "The room you have requested is not a master bedroom";
                return View("Error");
            }
        }

        //
        // GET: /MasterBedroomQuestionnaire/Details/5
        /// <summary>
        /// Display the details of the questionnaire to the designer
        /// </summary>
        /// <param name="id">room id</param>
        /// <returns></returns>
        [Authorize(Roles = "Designers")]
        public ActionResult Details(int id)
        {
            Room room = roomRepository.GetRoomById(id);     // get the current room so that we can verify the room type
            if (room == null)
            {
                TempData["ErrorMessage"] = "This room does not exist. ";
                return View("Error");
            }
            else if (room.RoomTypeName.Equals("Master Bedroom"))      // if the room type is a masterBedroom
            {
                MasterBedroomQuestionnaire masterBedroomQuestionnaire = masterBedroomQuestionnaireRepository.GetMasterBedroomQuestionnaireByRoomId(room.RoomId);
                if (masterBedroomQuestionnaire == null)
                {
                    // the questionnaire does not exist.  Throw an error
                    TempData["ErrorMessage"] = "A master bedroom questionnaire does not exist for this room. ";
                    return View("Error");
                }
                else
                {
                    return View(masterBedroomQuestionnaire);
                }
            }
            else
            {
                TempData["ErrorMessage"] = "This room does not exist or it is not a master bedroom. ";
                return View("Error");
            }
        }

        //
        // GET: /MasterBedroomQuestionnaire/Create/5
        /// <summary>
        /// Client creates a masterBedroom questionnaire
        /// </summary>
        /// <param name="id">room id</param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Create(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // ensure that the currently logged on user owns this room before displaying it              
            if (room == null)
            {
                TempData["ErrorMessage"] = "This room does not exist.  If you feel this is an error, please contact us.";
                return View("Error");
            }
            else if (room.RoomTypeName.Equals("Master Bedroom"))      // if the room type is a masterBedroom
            {
                if (masterBedroomQuestionnaireRepository.DoesMasterBedroomQuestionnaireExist(room.RoomId))        // if a masterBedroom questionnaire already exists, throw an error message.
                {
                    TempData["ErrorMessage"] = "Sorry, a questionnaire already exists for this room";
                    return View("Error");
                }
                else
                {
                    // otherwise check the type of room and if's a "masterBedroom" type, then generate a new questionnaire
                    MasterBedroomQuestionnaire masterBedroomQuestionnaire = new MasterBedroomQuestionnaire()
                    {
                        RoomId = room.RoomId
                    };

                    // generate the drop down list for the number of people that share a washroom
                    ViewData["BedList"] = MasterBedroomQuestionnaireRepository.GetBedList("");
                    ViewData["GenderTypeList"] = MasterBedroomQuestionnaireRepository.GetGenderTypeList("");		// generate the drop down list for the gender types

                    return View(masterBedroomQuestionnaire);
                }
            }
            else
            {
                TempData["ErrorMessage"] = "The room you have requested is not a MasterBedroom";
                return View("Error");
            }
        }

        //
        // POST: /MasterBedroomQuestionnaire/Create

        [HttpPost, Authorize]
        public ActionResult Create(MasterBedroomQuestionnaire masterBedroomQuestionnaire)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            //  check to ensure the currenly logged on user owns this room.  we're verifying this since the form could be tampered with then posted back
            // if the modelstate is valid, create a masterBedroom questionnaire

            int roomId = masterBedroomQuestionnaire.RoomId;      // get the room id for the current questionnaire that is being submitted

            ViewData["BedList"] = MasterBedroomQuestionnaireRepository.GetBedList(masterBedroomQuestionnaire.BedSize);
            if (ModelState.IsValid)
            {
                if (roomRepository.DoesRoomExist(roomId, userName))
                {
                    Room room = roomRepository.GetRoomById(roomId);     // Get the room by the roomId.
                    room.MenuStepId = 4;        // set the menu step number id to 4 since the room specific questionnaire is now completed
                    room.CurrentStepId = 2;     // set the user's current step id to 2 since the room specific questionnaire is now completed
                    room.DateUpdated = DateTime.Now;      // set the date updated so that this can be used on the left side room navigation menu to order the rooms
                    if (TryUpdateModel(room))
                    {
                        masterBedroomQuestionnaireRepository.Add(masterBedroomQuestionnaire);        // add the new questionnaire
                        masterBedroomQuestionnaireRepository.Save();                                                // save the new questionnaire to the db
                        roomRepository.Save();      // save the updated room values to the DB
                        TempData["SuccessMessage"] = "Your questionnaire is complete. Please proceed with uploading photos of your room.";
                        return RedirectToAction("Upload", "RoomPhoto", new { id = roomId });        // redirect the user to the room photo upload section of the website.
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "Sorry, an unknown error occured while creating your room's questionnaire.";
                    return View(masterBedroomQuestionnaire);
                }
            }
            TempData["ErrorMessage"] = "Please fill in the required fields below.";
            return View(masterBedroomQuestionnaire);
        }

        //
        // GET: /MasterBedroomQuestionnaire/Edit/5
        /// <summary>
        ///  Client edit's their masterBedroom questionnaire
        /// </summary>
        /// <param name="id">room id</param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            string userName = HttpContext.User.Identity.Name.ToString();    // Get's the current logged on user name
            Room room = roomRepository.GetRoomByIdVerifyUserName(id, userName);     // ensure that the currently logged on user owns this room before displaying it              
            if (room == null)
            {
                TempData["ErrorMessage"] = "This room does not exist.  If you feel this is an error, please contact us.";
                return View("Error");
            }
            else if (room.RoomTypeName.Equals("Master Bedroom"))      // if the room type is a masterBedroom
            {
                // if a masterBedroom questionnaire already exists for this room, then allow the user to edit the questionnaire
                if (masterBedroomQuestionnaireRepository.DoesMasterBedroomQuestionnaireExist(room.RoomId))
                {
                    // the id has passed through all of the sanity checks.  It's legit.  Allow the user to edit the questionnaire
                    MasterBedroomQuestionnaire masterBedroomQuestionnaire = masterBedroomQuestionnaireRepository.GetMasterBedroomQuestionnaireByRoomId(room.RoomId);    // get the masterBedroom questionnaire from the DB
                    ViewData["BedList"] = MasterBedroomQuestionnaireRepository.GetBedList(masterBedroomQuestionnaire.BedSize);
                    ViewData["CurrentMenuStepId"] = room.MenuStepId;       // used for the navigation inside the view;
                    ViewData["GenderTypeList"] = MasterBedroomQuestionnaireRepository.GetGenderTypeList(masterBedroomQuestionnaire.HowDoYouWantYourRoomToFeelGender);		// // generate the drop down list for the gender types
                    return View(masterBedroomQuestionnaire);
                }
                else
                {
                    TempData["ErrorMessage"] = "Sorry, a questionnaire does not exist for this room";
                    return View("Error");
                }
            }
            else
            {
                TempData["ErrorMessage"] = "The room you have requested is not a MasterBedroom";
                return View("Error");
            }
        }

        //
        // POST: /MasterBedroomQuestionnaire/Edit/5
        [HttpPost, Authorize]
        public ActionResult Edit(int id, FormCollection collection)
        //public ActionResult Edit(MasterBedroomQuestionnaire masterBedroomQuestionnaire)
        {
            MasterBedroomQuestionnaire masterBedroomQuestionnaire = masterBedroomQuestionnaireRepository.GetMasterBedroomQuestionnaireByRoomId(id);
            ViewData["BedList"] = MasterBedroomQuestionnaireRepository.GetBedList(masterBedroomQuestionnaire.BedSize);

            if (ModelState.IsValid)
            {
                if (TryUpdateModel(masterBedroomQuestionnaire))
                {
                    masterBedroomQuestionnaireRepository.Save();
                    TempData["SuccessMessage"] = "Your questionnaire has been successfully updated.";       // set the status message
                    return RedirectToAction("Edit", new { id = masterBedroomQuestionnaire.RoomId });
                }
            }
            TempData["ErrorMessage"] = "Sorry, there was an error updating your questionnaire.";
            return View(masterBedroomQuestionnaire);
        }
    }
}