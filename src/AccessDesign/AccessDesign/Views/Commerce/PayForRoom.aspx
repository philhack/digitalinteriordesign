﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Room>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Pay For Your Room Design
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Pay For Your Room Design</h1>
    <p>To pay for your room, please click on the 'Pay For Room' button below.  This will transfer you to PayPal, a secure 3rd party credit card transaction company.
    You can enter your credit card information and then click on 'Return to Digital Interior Design's' store to be transfered back to our application. 
    <br /><br />
    <b><%: ViewData["Currency"] %></b>
    </p>
    <br />
    <img src="/Content/Images/combo-of-designs-banner.jpg" alt="Design banner" />
    
    <br />
    <br />

    <div class="room-photo-gallery">
        <div class="payForRoom-left-label-same-line"><label>Your Room Name:</label></div><div class="payForRoom-right-label-same-line"><%: Model.Name %></div>
        <div class="payForRoom-left-label-same-line"><label>Cost for interior design of a <%: Model.RoomTypeName.ToLower() %>:</label></div><div class="payForRoom-right-label-same-line">&#36;<%: String.Format("{0:F}", Model.RoomCost) %></div>
        <%if ( Convert.ToBoolean(ViewData["ShowTax"]))
          { %>
                <div class="payForRoom-left-label-same-line"><label><%: ViewData["TaxType"]%>  (<%: (ViewData["TaxRate"])%>%)</label></div><div class="payForRoom-right-label-same-line">&#36;<%: String.Format("{0:F}", Model.RoomTaxCost)%></div>
        <%} %>

        <div class="payForRoom-left-label-same-line"><label>Total Cost:</label></div><div class="payForRoom-right-label-same-line">&#36;<%: String.Format("{0:F}", Model.RoomTotalCost) %></div>
        <br />
        
                <% using (Html.BeginForm( "CompleteOrder", "Commerce", new { id = Model.RoomId } )) {%>
                        <div id="standard-button">
                            <button type="submit" class="button">Pay For Room</button>
                        </div>
                <% } %>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarContent" runat="server">
</asp:Content>

    