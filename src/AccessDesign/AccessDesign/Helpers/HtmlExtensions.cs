﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.Routing;
using Recaptcha;



namespace AccessDesign.Helpers
{
    public static class HtmlExtensions
    {
        private static string reCaptchaPublicKey = System.Configuration.ConfigurationManager.AppSettings["reCapchaPublicKey"].Trim().ToString();        // get the reCaptcha public key from the web.config 
        private static string reCaptchaPrivateKey = System.Configuration.ConfigurationManager.AppSettings["reCapchaPrivateKey"].Trim().ToString();      // get the reCaptcha private key from the web.config
        public static string GenerateCaptcha(this HtmlHelper helper)
        {
            var captchaControl = new Recaptcha.RecaptchaControl
            {
                ID = "recaptcha",
                Theme = "clean",
                PublicKey = reCaptchaPublicKey,
                PrivateKey = reCaptchaPrivateKey
            };

            var htmlWriter = new HtmlTextWriter(new StringWriter());

            captchaControl.RenderControl(htmlWriter);

            return htmlWriter.InnerWriter.ToString();
        }
    }


    public static class HtmlHelperExtension
    {
        public static MvcHtmlString FileInput<TModel, TValue>(this HtmlHelper<TModel> html,
                                                       Expression<Func<TModel, TValue>> expression)
        {
            String htmlFieldName = ExpressionHelper.GetExpressionText(expression);
            return FileInput(html, htmlFieldName);
        }

        /// <summary>
        /// Returns a file input element by using the specified HTML helper and the name of the form field.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="name">The name of the form field and the <see cref="member">System.Web.Mvc.ViewDataDictionary</see> key that is used to look up the validation errors.</param>
        /// <returns>An input element that has its type attribute set to "file".</returns>
        public static MvcHtmlString FileInput(this HtmlHelper htmlHelper, string name)
        {
            return htmlHelper.FileInput(name, (object)null);
        }

        /// <summary>
        /// Returns a file input element by using the specified HTML helper, the name of the form field, and the HTML attributes.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="name">The name of the form field and the <see cref="member">System.Web.Mvc.ViewDataDictionary</see> key that is used to look up the validation errors.</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes for the element. The attributes are retrieved through reflection by examining the properties of the object. The object is typically created by using object initializer syntax.</param>
        /// <returns>An input element that has its type attribute set to "file".</returns>
        public static MvcHtmlString FileInput(this HtmlHelper htmlHelper, string name, object htmlAttributes)
        {
            return htmlHelper.FileInput(name, new RouteValueDictionary(htmlAttributes));
        }

        /// <summary>
        /// Returns a file input element by using the specified HTML helper, the name of the form field, and the HTML attributes.
        /// </summary>
        /// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
        /// <param name="name">The name of the form field and the <see cref="member">System.Web.Mvc.ViewDataDictionary</see> key that is used to look up the validation errors.</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes for the element. The attributes are retrieved through reflection by examining the properties of the object. The object is typically created by using object initializer syntax.</param>
        /// <returns>An input element that has its type attribute set to "file".</returns>
        public static MvcHtmlString FileInput(this HtmlHelper htmlHelper, string name,
                                       IDictionary<String, Object> htmlAttributes)
        {
            var tagBuilder = new TagBuilder("input");
            tagBuilder.MergeAttributes(htmlAttributes);
            tagBuilder.MergeAttribute("type", "file", true);
            tagBuilder.MergeAttribute("name", name, true);
            tagBuilder.GenerateId(name);

            ModelState modelState;
            if (htmlHelper.ViewData.ModelState.TryGetValue(name, out modelState))
                if (modelState.Errors.Count > 0)
                    tagBuilder.AddCssClass(HtmlHelper.ValidationInputCssClassName);
            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.SelfClosing));
        }
    }
}