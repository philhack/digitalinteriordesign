﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Room>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Add Comments About Your Room
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <% Html.EnableClientValidation(); %>
    <h1>Add Comments About Your Room</h1>
    <div id="room-photo-nav">
        <a href="/RoomPhoto/Upload/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-UploadRoomPhotos.png" alt="" border="0"  class="room-photo-nav-image" /></a><a href="/RoomPhoto/Details/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-ViewEditPhotos.png" alt="" border="0" class="room-photo-nav-image"/></a><a href="/RoomPhoto/Comments/<%: ViewData["RoomId"] %>"><img src="/Content/Images/Room-Photo-Nav-AddCommentsTreomaOn.png" alt="" border="0"/></a>
     </div>
     <br />
    <p>Please add comments about your room and inspiration photos using the field below. Once you are finished, click the submit button.</p>
    <br />
    <% using (Html.BeginForm()) {%>
            <%: Html.HiddenFor(model => model.RoomId) %>

            <div class="text-area">
                <%: Html.TextAreaFor(model => model.PhotoDetails, new { @class = "txtarea", cols = 96, rows = 12 })%>
                <%: Html.ValidationMessageFor(model => model.PhotoDetails) %>
            </div>
            <br />
            <div id="standard-button">
                    <input type="submit"  class="button" value="Submit" />
            </div> 

    <% } %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".room-photo-nav-image")
            .mouseover(function () {
                var src = $(this).attr("src").match(/[^\.]+/) + "TreomaOn.png";
                $(this).attr("src", src);
            })
            .mouseout(function () {
                var src = $(this).attr("src").replace("TreomaOn", "");
                $(this).attr("src", src);
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

