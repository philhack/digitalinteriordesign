﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.Profile>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edit Profile: <%: Model.UserName %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>
    <h1>Update Your Profile</h1>
    <p>Update your profile by filling in the form below.</p>
    <div class="text-box"><span class="required-field">*</span> Required fields.</div>
        <br /><br />
         <div class=display-field"><%: Html.ActionLink("Change Password", "ChangePassword", "Account") %> | <%: Html.ActionLink("Rename / Delete a Room", "Index", "Room") %></div>
         <br /><br />
        
        <% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm()) {%>

    <div>
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.FirstName) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.FirstName, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.FirstName) %>
            </div>
            <br />
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.LastName) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.LastName, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.LastName) %>
            </div>
            <br />
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.PhoneNumber) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.PhoneNumber, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.PhoneNumber) %>
            </div>
            <br />
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.FaxNumber) %>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.FaxNumber, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.FaxNumber) %>
            </div>
            <br />
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.SkypeId) %>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.SkypeId, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.SkypeId)%>
            </div>
            <br />
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.Address) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.Address, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.Address) %>
            </div>
            <br />
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.City) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.City, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.City) %>
            </div>
            <br />
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.State) %><span class="required-field">*</span>
            </div>
            <div class="dropdown">
                <%: Html.DropDownListFor(model => model.State, ViewData["States"] as SelectList, new { @class = "option" })%>
                <%: Html.ValidationMessageFor(model => model.State) %>
            </div>
            <br />
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.Zip) %><span class="required-field">*</span>
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.Zip, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.Zip) %>
            </div>
            <br />
            <div class="text-box-label-same-line">
                <%: Html.LabelFor(model => model.Country) %><span class="required-field">*</span>
            </div>
            <div class="dropdown">
                <%: Html.DropDownListFor(model => model.Country, ViewData["Countries"] as SelectList, new { @class="option" })%>
                <%: Html.ValidationMessageFor(model => model.Country) %>
            </div>
            <br /><br /><br />
            <div id="standard-button">
                    <input type="submit"  class="button" value="Save" />
            </div> 
    </div>
    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SidebarContent" runat="server">
</asp:Content>

