﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%
        var list = this.ViewData["selectList"] as SelectList;
%>

<div>
        <% foreach (var item in list) {
                var radioId = ViewData.TemplateInfo.GetFullHtmlFieldId(item.Value);
                var checkedAttr = item.Selected ? "checked=\"checked\"" : string.Empty;
        %>
                <input type="radio" id="<%: radioId %>" name="<%: ViewData.TemplateInfo.HtmlFieldPrefix %>" value="<%: item.Value %>" <%: checkedAttr %>/>
                <label for="<%: radioId %>"><%: item.Text %></label>
        <% } %>
</div>

