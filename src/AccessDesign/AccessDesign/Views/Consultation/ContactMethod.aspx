﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AccessDesign.Models.ContactMethod>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
     <% if (Convert.ToBoolean(TempData["FollowUpConsultation"]) == true)
        { %>
        Book Follow-up Consultation | Step 1 of 2 - How do you want to be contacted?
    <% } else  { %>
	    Book Free 30 Minute Consultation | Step 1 of 2 - How do you want to be contacted?
    <% } %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% Html.RenderPartial("~/Views/Shared/Message.ascx"); %>

<% if (Convert.ToBoolean(TempData["FollowUpConsultation"]) == true)
   { %>
        <h1>Book Follow-up Consultation</h1>
        <p>Once you have received your design files, you can schedule a 30 minute follow-up appointment with the designer. This will allow you to review the design with them and they will perform a revision if necessary</p>
        <br />
        <p>If you need to book or reschedule your appointment, please select a contact method below and click continue. You will then be prompted to select a date and time for your appointment.</p>
        <br />
    <% } else  { %>
	    <h1>Book Free 30 Minute Consultation</h1>
        <p>If you have already booked your appoinment and talked to the designer, please proceed to the next step: '<%: Html.ActionLink("Pay For Room", "Index", "Commerce", new { id = Model.RoomId }, new { })%>'.</p>
        <br />
        <p>If you need to book or reschedule your appointment, please select a contact method below and click continue. You will then be prompted to select a date and time for your appointment.</p>
        <br />
    <% } %>

    
    <% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm()) {%>

             <%: Html.HiddenFor(model => model.RoomId) %>

             <h3>Step 1 of 2: <%: Html.LabelFor(model => model.ContactTypePreference) %></h3>
             <br />
            <div class="editor-label">
                <%: Html.ValidationMessageFor(model => model.ContactTypePreference) %>
            </div>
            <div class="text-box-label-same-line">
                <%: Html.RadioButtonFor(model => model.ContactTypePreference, "phone")%><b>Phone</b> - 
                <%: Html.LabelFor(model => model.PhoneNumber) %> : 
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.PhoneNumber, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.PhoneNumber)%><br />
            </div>
                <br />
            <div class="text-box-label-same-line">
                <%: Html.RadioButtonFor(model => model.ContactTypePreference, "skype")%><b>Skype</b> - 
                <%: Html.LabelFor(model => model.SkypeId) %> : 
            </div>
            <div class="text-box">
                <%: Html.TextBoxFor(model => model.SkypeId, new { @class = "txtbox", @size = 25 })%>
                <%: Html.ValidationMessageFor(model => model.SkypeId) %><br />
            </div>
            <br /><br />    
            <div id="standard-button">
                <input type="submit" value="Continue" class="button" />
            </div>

    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">

    <script src="/Scripts/MvcFoolproofValidation.js" type="text/javascript"></script>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SideBarContent" runat="server">
</asp:Content>

