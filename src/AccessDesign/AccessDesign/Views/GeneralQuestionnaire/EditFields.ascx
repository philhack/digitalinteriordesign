﻿<%@  Language="C#"  Inherits="System.Web.Mvc.ViewUserControl<AccessDesign.Models.GeneralQuestionnaire>" %>
<%@ Import Namespace="AccessDesign.Helpers" %>

<div>
            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.DesignStyle) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.ValidationMessageFor(model => model.DesignStyle)%>
                <%: Html.RadioButtonFor(model => model.DesignStyle, "Modern")%>Modern <br />
                <%: Html.RadioButtonFor(model => model.DesignStyle, "Traditional")%>Traditional <br />
                <%: Html.RadioButtonFor(model => model.DesignStyle, "Country Cottage")%>Country Cottage <br />
                <%: Html.RadioButtonFor(model => model.DesignStyle, "Eclectic")%>Eclectic <br />
                <%: Html.RadioButtonFor(model => model.DesignStyle, "Mid-century Modern")%>Mid-century Modern <br />
                <%: Html.RadioButtonFor(model => model.DesignStyle, "Glam")%>Glam <br />
                <%: Html.RadioButtonFor(model => model.DesignStyle, "Shabby Chic")%>Shabby Chic <br />
                <%: Html.RadioButtonFor(model => model.DesignStyle, "Other")%>Other - Please specifiy:  <%: Html.TextBoxFor(model => model.DesignStyleOther, new { @class = "txtbox", @size = 50 })%>
                <%: Html.ValidationMessageFor(model => model.DesignStyleOther)%>
            </div>
            
            <br />
                        
            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.ColorsYouLove) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
             <%: Html.CheckBoxList("ColorsYouLoveList", ViewData["ColorsLoveKeyValuePairItems"] as IEnumerable<KeyValuePair<string, string>>, ViewData["ColorsLoveSelectedValues"] as IEnumerable<string>)%>
   
                Other - Please specifiy:  <%: Html.TextBoxFor(model => model.ColorsYouLoveOther, new { @class = "txtbox", @size = 50 })%>

            </div>
            
            <br />
            
            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.ColorsYouDislike)%><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
             <%: Html.CheckBoxList("ColorsYouDislikeList", ViewData["ColorsDislikeKeyValuePairItems"] as IEnumerable<KeyValuePair<string, string>>, ViewData["ColorsDislikeSelectedValues"] as IEnumerable<string>)%>
                Other - Please specifiy:  <%: Html.TextBoxFor(model => model.ColorsYouDislikeOther, new { @class = "txtbox", @size = 50 })%>
            </div>

            <br />
            
            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.TypeOfColorsYouLike) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.TypeOfColorsYouLike, "Color")%>Colour <br />
                <%: Html.RadioButtonFor(model => model.TypeOfColorsYouLike, "Neutral")%>Neutral <br />
                <%: Html.RadioButtonFor(model => model.TypeOfColorsYouLike, "Some Color")%>Some Color <br />
                <%: Html.ValidationMessageFor(model => model.TypeOfColorsYouLike) %>
            </div>

            <br />
                        
           <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.HaveASpouse) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.HaveASpouse, "Yes")%>Yes <br />
                <%: Html.RadioButtonFor(model => model.HaveASpouse, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.HaveASpouse) %>
            </div>
            
            <br />

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.HaveKids) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.HaveKids, "Yes")%>Yes <br />
                <%: Html.RadioButtonFor(model => model.HaveKids, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.HaveKids) %>
            </div>

            <br />
            
            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.HaveKidsAgesOfKids) %>
            </div>
            <div class="text-area">
                <%: Html.TextAreaFor(model => model.HaveKidsAgesOfKids, new { @class = "txtarea", @rows = 6, @cols = 80 })%>
                <%: Html.ValidationMessageFor(model => model.HaveKidsAgesOfKids) %>
            </div>
            
            <br />

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.HavePets) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.HavePets, "Yes")%>Yes <br />
                <%: Html.RadioButtonFor(model => model.HavePets, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.HavePets) %>
            </div>

            <br />
            
            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.HavePetsTypeAndNumber) %>
            </div>
            <div class="text-area">
                <%: Html.TextAreaFor(model => model.HavePetsTypeAndNumber, new { @class = "txtarea", @rows = 6, @cols = 80 })%>
                <%: Html.ValidationMessageFor(model => model.HavePetsTypeAndNumber) %>
            </div>
            
            <br />

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.HobbiesAndInterests) %><span class="required-field">*</span>
            </div>
            <div class="text-area">
                <%: Html.TextAreaFor(model => model.HobbiesAndInterests, new { @class = "txtarea", @rows = 6, @cols = 80 })%>
                <%: Html.ValidationMessageFor(model => model.HobbiesAndInterests) %>
            </div>

            <br />
            ------------------
            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.TypesOfPatternsYouLike) %><span class="required-field">*</span>
            </div>

            <div class="checkbox-questionnaire">
             <%: Html.CheckBoxList("PatternTypesList", ViewData["PatternTypesKeyValuePairItems"] as IEnumerable<KeyValuePair<string, string>>, ViewData["PatternTypesSelectedValues"] as IEnumerable<string>)%>
   
                Other - Please specifiy:  <%: Html.TextBoxFor(model => model.TypesOfPatternsYouLikeOther, new { @class = "txtbox", @size = 50 })%>

            </div>
           
            <br />

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.BeingGreenImportant) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.ValidationMessageFor(model => model.BeingGreenImportant)%>
                <%: Html.RadioButtonFor(model => model.BeingGreenImportant, "Yes")%>Yes <br />
                <%: Html.RadioButtonFor(model => model.BeingGreenImportant, "No")%>No <br />
                <%: Html.RadioButtonFor(model => model.BeingGreenImportant, "Other")%>Other - Please specifiy:  <%: Html.TextBoxFor(model => model.BeingGreenImportantOther, new { @class = "txtbox", @size = 50 })%>
                <%: Html.ValidationMessageFor(model => model.BeingGreenImportantOther)%>
            </div>

            <br />
            
            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.RentOrOwnYourHome) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.RentOrOwnYourHome, "Rent")%>Rent <br />
                <%: Html.RadioButtonFor(model => model.RentOrOwnYourHome, "Own")%>Own <br />
                <%: Html.ValidationMessageFor(model => model.RentOrOwnYourHome) %>
            </div>

            <br />
            
            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.HowLongDoYouPlanToLiveInHome) %><span class="required-field">*</span>
            </div>
            <div class="text-area">
                <%: Html.TextAreaFor(model => model.HowLongDoYouPlanToLiveInHome, new { @class = "txtarea", @rows = 6, @cols = 80 })%>
                <%: Html.ValidationMessageFor(model => model.HowLongDoYouPlanToLiveInHome) %>
            </div>
                        
            <br />

            <div class="text-box-label-above">
                <%: Html.LabelFor(model => model.NeedWheelchairAccess) %><span class="required-field">*</span>
            </div>
            <div class="checkbox-questionnaire">
                <%: Html.RadioButtonFor(model => model.NeedWheelchairAccess, "Yes")%>Yes <br />
                <%: Html.RadioButtonFor(model => model.NeedWheelchairAccess, "No")%>No <br />
                <%: Html.ValidationMessageFor(model => model.NeedWheelchairAccess) %>
            </div>
</div>
 
 